﻿using UnityEngine;
using System.Collections;

public class WindowSourceGuiThumbnail : MonoBehaviour {

	public PopMovieParams	Params;
	PopMovie		Movie;
	Texture			ThumbnailTexture;
	float			MovieTime = 0;
	int				MetaRevision = -1;		//	detect when meta changes

	public void Init(string Filename,Texture _ThumbnailTexture)
	{
		ThumbnailTexture = _ThumbnailTexture;

		var ThumbnailImage = GetComponent<UnityEngine.UI.RawImage>();
		if (ThumbnailImage != null)
		{ 
			ThumbnailImage.texture = ThumbnailTexture;
		}
		
		var Label = GetComponent<UnityEngine.UI.Text>();
		if (Label != null)
		{ 
			Label.text = Filename;
		}

		try
		{
			Movie = new PopMovie(Filename, Params);
		}
		catch (System.Exception e)
		{
			Debug.LogWarning("Failed to create popmovie window capture: "  + Filename ); 
		}

	}

	public void Update()
	{
		MovieTime += Time.deltaTime;
		if (Movie != null && ThumbnailTexture != null)
		{
			Movie.SetTime( MovieTime );
			Movie.UpdateTexture( ThumbnailTexture );
		}

		//	update aspect ratio
		if (Movie != null && MetaRevision != Movie.GetMetaRevision())
		{
			MetaRevision = Movie.GetMetaRevision();
			var Meta = Movie.GetMeta();
			if ( Meta != null )
			{
				float Ratio = Mathf.Max( 1, Meta.Stream0_Width ) / Mathf.Max( 1, Meta.Stream0_Height );
				Debug.Log("Updating aspect ratio to "  + Ratio );
				var RatioFitter = GetComponent<UnityEngine.UI.AspectRatioFitter>();
				if ( RatioFitter != null )
					RatioFitter.aspectRatio = Ratio;
			}
		}
	}


}
