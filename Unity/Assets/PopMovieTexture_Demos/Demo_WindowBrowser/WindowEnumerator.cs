﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class WindowEnum 
{
	public RenderTexture			Texture; 
	public string					Source;
	public GameObject				ThumbnailGuiObject;

	public WindowSourceGuiThumbnail	GetThumbnailGui() 
	{
		return ThumbnailGuiObject ? ThumbnailGuiObject.GetComponentInChildren<WindowSourceGuiThumbnail>() : null;	
	}
};



public class WindowEnumerator : MonoBehaviour {

	public int						ThumbnailWidth = 512;
	public int						ThumbnailHeight = 256;
	public GameObject				TemplateThumbnail;
	public int						MaxWindows = 3;
	private uint					EnumIndex = 0;
	public List<WindowEnum>			Windows;
	public string					WindowPrefix = "window:";
	public string					WindowFilter;
	public Material					InitThumbnailShader;
	static Texture2D				ClearTextureCache;

	void ClearTexture(RenderTexture Tex)
	{
		if ( InitThumbnailShader != null )
		{
			Graphics.Blit( Tex, InitThumbnailShader );
			return;	
		}

		if (ClearTextureCache == null)
		{ 
			ClearTextureCache = new Texture2D(1,1);
			ClearTextureCache.SetPixel( 0, 0, new Color(1,0,0,1) );
			ClearTextureCache.Apply();
		}

		Graphics.Blit( ClearTextureCache, Tex );
	}

	void FindNewSource()
	{ 
		string SourceName = PopMovie.EnumSource(EnumIndex);

		if ( SourceName == null )
			return;

		if (SourceName.StartsWith(WindowPrefix))
		{ 
			bool Filtered = (WindowFilter == null || WindowFilter.Length == 0) ? true : SourceName.Contains(WindowFilter);
			if ( Filtered )
			{
				 AddNewSource(SourceName);
			}
		}

		EnumIndex++;
	}

	void AddNewSource(string SourceName)
	{ 
		Debug.Log("New window " + SourceName );

		if ( TemplateThumbnail == null )
			return;

		if ( Windows != null && Windows.Count >= MaxWindows )
			return;

		WindowEnum Enum = new WindowEnum();
		Enum.Source = SourceName;
		Enum.Texture = new RenderTexture(ThumbnailWidth,ThumbnailHeight,0);
		ClearTexture( Enum.Texture );
		Enum.ThumbnailGuiObject = Instantiate( TemplateThumbnail );
		Enum.ThumbnailGuiObject.gameObject.SetActive( true );
		Enum.ThumbnailGuiObject.transform.SetParent( TemplateThumbnail.transform.parent );
		Enum.GetThumbnailGui().Init( Enum.Source, Enum.Texture );
		if ( Windows == null )
			Windows = new List<WindowEnum>();
		Windows.Add( Enum );
	}

	void Start()
	{ 
		PopMovie.PopMovie_EnumSourcesClear ();
	}

	void Update () {
		FindNewSource();
	}
}
