﻿Shader "PopMovie/PerformanceGraph"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		BackgroundColour("BackgroundColour", COLOR ) = ( 0.16,0.59,1.0,1 )
		CurrentTimeColour("TimeColour", COLOR ) = (1,1,1,1)
		CurrentTimeWidthMs("CurrentTimeWidthMs", Range(1,40) ) = 14
		BarAheadColour("BarAheadColour", COLOR ) = (0.8,1.0,0.39,1)
		BarBehindColour("BarBehindColour", COLOR ) = (1.0,0.22,0.36,1)

		LocaliseGraph("LocaliseGraph", Range(0,1) ) = 1
		TimePaddingLeftMs("TimePaddingLeftMs", Range(0,1000) ) = 30
		TimePaddingRightMs("TimePaddingRightMs", Range(0,1000) ) = 30

		ProfilerHeight("ProfilerHeight", Range(0,1) ) = 0.5
		UpdateTextureGoodColour("UpdateTextureGoodColour", COLOR ) = (0.41,0.98,0.54,1)
		UpdateTextureMediumColour("UpdateTextureMediumColour", COLOR ) = (0.99,0.77,0.42,1)
		UpdateTextureBadColour("UpdateTextureBadColour", COLOR ) = (0.99,0.22,0.36,1)
		UpdateTextureLockColour("UpdateTextureLockColour", COLOR ) = (1,1,1,1)
		UpdateTextureBlitColour("UpdateTextureBlitColour", COLOR ) = (1,0,1,1)
			
		UpdateTextureMsGood("UpdateTextureMsGood", Range(0,33) ) = 3
		UpdateTextureMsMedium("UpdateTextureMsMedium", Range(0,33) ) = 14
		UpdateTextureMsBad("UpdateTextureMsBad", Range(0,33) ) = 33
	}

	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			int		CurrentTime;
			int		MovieDuration;
			int		ExtractedTime;
			int		DecodedSubmittedTime;
			int		DecodedTime;
			int		BufferedTime;
			int		CopiedTime;

			float4	BackgroundColour;
			float4	CurrentTimeColour;
			float4	BarAheadColour;
			float4	BarBehindColour;

			//	padding for visibility
			//	ideally, we'd pad to min N pixels, but we don't know the target size
			uint	CurrentTimeWidthMs;
			int 	LocaliseGraph;
			int 	TimePaddingLeftMs;
			int		TimePaddingRightMs;



			//	profiler
			#define HISTORY_MAX	300
			int		HistorySize;
			float4	UpdateTextureDurations[HISTORY_MAX];	//	x=total, y=lock, z=blit
			float	ProfilerHeight;
			float4	UpdateTextureGoodColour;
			float4	UpdateTextureMediumColour;
			float4	UpdateTextureBadColour;
			float4	UpdateTextureLockColour;
			float4	UpdateTextureBlitColour;
			int		UpdateTextureMsGood;
			int		UpdateTextureMsMedium;
			int		UpdateTextureMsBad;

			sampler2D _MainTex;
			float4 _MainTex_ST;


			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 BackgroundUv : TEXCOORD1;
				int2 TimeMinMax : TEXCOORD2;
				float4 vertex : SV_POSITION;
			};


			int2 GetTimeMinMax()
			{
				int Min = CurrentTime;
				Min = min( Min, ExtractedTime );
				Min = min( Min, DecodedSubmittedTime );
				Min = min( Min, DecodedTime );
				Min = min( Min, BufferedTime );
				Min = min( Min, BufferedTime );
				Min = min( Min, CopiedTime );

				int Max = CurrentTime;
				Max = max( Max, ExtractedTime );
				Max = max( Max, DecodedSubmittedTime );
				Max = max( Max, DecodedTime );
				Max = max( Max, BufferedTime );
				Max = max( Max, BufferedTime );
				Max = max( Max, CopiedTime );

				if ( !LocaliseGraph )
				{
					Min = 0;
					Max = max( Max, MovieDuration );
				}

				return int2( Min, Max );
			}


			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
		
				//	allow uv transform so the user can draw this graph over the top of something else in a corner
				o.BackgroundUv = v.uv;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
			#if !UNITY_UV_STARTS_AT_TOP
				o.uv.y = 1 - o.uv.y;
			#endif
				o.TimeMinMax = GetTimeMinMax();
				return o;
			}


			int GetDataTime(int DataRow)
			{
				int Datas[6] = 
				{
					ExtractedTime,
					DecodedSubmittedTime,
					DecodedTime,
					BufferedTime,
					BufferedTime,
					CopiedTime
				};

				return Datas[DataRow]; 
			}


			float4 GetGraphColour(float2 uv,int2 TimeMinMax)
			{
				float RowCount = 5;
				float Rowf = uv.y * RowCount; 
				int DataRow = (int)Rowf;

				//	some padding between bars
				float Border = 0.05f;
				float BorderTop = Border * ( 1 + (DataRow == 0) );
				float BorderBottom = Border * ( 1 + (DataRow == RowCount-1) );
				if ( frac(Rowf) < BorderTop || frac(Rowf) > (1-BorderBottom) )
				{
					return float4(0,0,0,0);
				}

				int MinTime = TimeMinMax.x - TimePaddingLeftMs;
				int MaxTime = TimeMinMax.y + TimePaddingRightMs;
				MinTime = max( MinTime, 0 );
				if ( MovieDuration > 0 )
					MaxTime = min( MaxTime, MovieDuration );

				int Time = lerp( MinTime, MaxTime, uv.x );
				int CurrentTimeMin = CurrentTime - (CurrentTimeWidthMs/2);
				int CurrentTimeMax = CurrentTime + (CurrentTimeWidthMs/2);
				if ( Time >= CurrentTimeMin && Time <= CurrentTimeMax )
				{
					return CurrentTimeColour;
				}

				//	time in the row we're looking at
				int DataTime = GetDataTime(DataRow);
				if ( Time <= DataTime )
				{
					return DataTime < CurrentTime ? BarBehindColour : BarAheadColour;
				}

				return float4(0,0,0,0);
			}


			float Clamp01(float Value)
			{
				return clamp( Value, 0, 1 );
			}

			float Range(float Min,float Max,float Value)
			{
				return (Value-Min) / (Max-Min);
			}

			float Range01(float Min,float Max,float Value)
			{
				return Clamp01( Range( Min, Max, Value ) );
			}
			
			float4 GetProfilerColour(int DisplayMs,int3 DataMs)
			{
				//	colour for lock
				int LockMs = DataMs.y;
				int BlitMs = DataMs.z;
				int TotalMs = DataMs.x;

				if ( DisplayMs <= LockMs )
					return UpdateTextureLockColour;
				if ( DisplayMs <= LockMs+BlitMs )
					return UpdateTextureBlitColour;

				float Time;
				if ( TotalMs <= UpdateTextureMsGood )
					return UpdateTextureGoodColour;
				if ( TotalMs <= UpdateTextureMsMedium )
					return UpdateTextureMediumColour;
				return UpdateTextureBadColour;
			}

			float4 GetProfilerColour(float2 uv)
			{
				int HistoryIndex = uv.x * HISTORY_MAX;
				int3 HistoryValue = UpdateTextureDurations[HistoryIndex].xyz;

				//	uv height always > 0 so a value of 0 doesn't register
				int UvHeight = (uv.y * UpdateTextureMsBad) + 1;
				
				if ( HistoryValue.x < UvHeight )
				{
					//	draw threshold lines in background
					if ( UvHeight == UpdateTextureMsBad )
						return UpdateTextureBadColour / 2;
					if ( UvHeight == UpdateTextureMsMedium )
						return UpdateTextureMediumColour / 2;
					if ( UvHeight == UpdateTextureMsGood )
						return UpdateTextureGoodColour / 2;
					return float4(0,0,0,0);
				}
				return GetProfilerColour( UvHeight, HistoryValue );
			}

			fixed4 frag (v2f i) : SV_Target
			{
				float4 Background = tex2D( _MainTex, i.BackgroundUv );

				if ( i.uv.x < 0 || i.uv.x > 1 || i.uv.y < 0 || i.uv.y > 1 )
				{
					return Background;
				}

				float4 GraphColour = float4( 0,0,0,0 );

				if ( i.uv.y <= ProfilerHeight )
				{
					GraphColour = GetProfilerColour( float2( i.uv.x, Range( 0, ProfilerHeight, i.uv.y ) ) );
				}
				else
				{
					GraphColour = GetGraphColour( float2( i.uv.x, Range( ProfilerHeight, 1, i.uv.y ) ), i.TimeMinMax );
				}

				if ( GraphColour.a > 0 )
				{
					return GraphColour;
				}

				float a = BackgroundColour.a;
				Background.xyz = ((1-a)*Background) + ((a)*BackgroundColour);
				return Background;
			}
			ENDCG
		}
	}
}
