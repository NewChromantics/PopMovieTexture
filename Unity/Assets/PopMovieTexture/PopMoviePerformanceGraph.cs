﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopMoviePerformanceGraph : MonoBehaviour {

	[Header("Optionally blit to a texture for graph output, otherwise just use the assigned material whereever you like")]
	public RenderTexture	PerformanceTexture;

	[Header("Optional. Provide a material with customised settings")]
	public Material			PerformanceShaderMaterial;
	public Shader			PerformanceShader;

	public PopMovieSimple	Movie;
	[Range(0,10)]
	public int				StreamIndex;

	const int				HISTORY_MAX = 300;
	List<Vector4>			TextureUpdateDurationHistory = new List<Vector4>();


	Material				material
	{
		get
		{
			if ( PerformanceShaderMaterial == null )
			{
				PerformanceShaderMaterial = new Material( PerformanceShader );
			}
			return PerformanceShaderMaterial;
		}
	}

	PopMovie				movie 
	{
		get 
		{
			if (Movie == null) {
				Movie = GetComponent<PopMovieSimple> ();
			}
			if (Movie == null) {
				GameObject.FindObjectOfType<PopMovieSimple> ();
			}
			return Movie ? Movie.Movie : null;
		}
	}


	void UpdateMaterial(PopMoviePerformance.StreamPerformance Stats,uint MovieDuration)
	{
		TextureUpdateDurationHistory.Add ( new Vector4( Stats.UpdateTextureTotalDuration, Stats.UpdateTextureLockDuration, Stats.UpdateTextureBlitDuration, 0 ));
		while (TextureUpdateDurationHistory.Count > HISTORY_MAX)
			TextureUpdateDurationHistory.RemoveAt (0);
		
		var Mat = material;

		Mat.SetInt ("CurrentTime", (int)Stats.Time);
		Mat.SetInt ("MovieDuration", (int)MovieDuration );

		Mat.SetFloat("ExtractedTime", Stats.LastExtractedTime );
		Mat.SetFloat("DecodedSubmittedTime", Stats.LastDecodeSubmittedTime );
		Mat.SetFloat("DecodedTime", Stats.LastDecodedTime );
		Mat.SetFloat("BufferedTime", Stats.LastBufferedTime );
		Mat.SetFloat("CopiedTime", Stats.LastCopiedTime );

		Mat.SetInt ("HistorySize", TextureUpdateDurationHistory.Count );
		//	max out this array to get around unity's material array initalise-size-is-final-size problem
		var TextureUpdateHistoryArray = new Vector4[HISTORY_MAX];
		for (int i = 0;	i < TextureUpdateDurationHistory.Count;	i++)
		{
			TextureUpdateHistoryArray [i] = TextureUpdateDurationHistory [i];
		}
		Mat.SetVectorArray ("UpdateTextureDurations", TextureUpdateHistoryArray);
	}

	void Update () 
	{
		//	grab stats
		var Movie = movie;

		PopMoviePerformance.StreamPerformance Stats = null;
		try
		{
			var AllStats = Movie.GetPerformanceStats ();
			Stats = AllStats.Streams [StreamIndex];
		}
		catch ( System.Exception e)
		{
			//	invalid index, no movie etc
			Debug.LogException (e);
			Stats = new PopMoviePerformance.StreamPerformance ();
		}

		UpdateMaterial (Stats, 0);// Movie.GetDurationMs() );


		if (PerformanceTexture != null) {
			Graphics.Blit (null, PerformanceTexture, material);
		}
	}
}
