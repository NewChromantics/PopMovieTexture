#pragma once

#include <SoyDebug.h>
#include "PopMovieDecoder.h"
#include <SoyUnity.h>
#include <SoyExportManager.h>


class TVideoMeta;
class TGraphRendererBase;
class TAudioRendererBase;
class TGraphBar;

namespace Opengl
{
	class TBlitter;
	class TTexture;
}
namespace Directx
{
	class TBlitter;
	class TTexture;
}
namespace Directx9
{
	class TBlitter;
	class TTexture;
}
namespace Metal
{
	class TBlitter;
	class TTexture;
}
namespace Gnm
{
	class TTexture;
	class TBlitter;
}


//	way too many problems with using a struct, reverting to bit flags. (which may also have problems with endianness, container size etc)
//		android ONLY struct is offset
//		IL2CPP bools are 32 bit and NOT garunteed to have bit 1 set if true
//		long is 32 bit on some platforms and 64 on others
namespace TPluginParams
{
	//	copy & paste this to c#
	enum PopMovieFlags
	{
		None						= 0,
		SkipPushFrames				= 1<<0,
		SkipPopFrames				= 1<<1,
		AllowGpuColourConversion	= 1<<2,
		AllowCpuColourConversion	= 1<<3,
		AllowSlowCopy				= 1<<4,
		AllowFastCopy				= 1<<5,
		PixelClientStorage			= 1<<6,
		DebugFrameSkipping			= 1<<7,
		PeekBeforeDefferedCopy		= 1<<8,
		VerboseDebug				= 1<<9,
		Win7Emulation				= 1<<10,
		DebugNoNewPixelBuffer		= 1<<11,
		DebugRenderThreadCallback	= 1<<12,
		ResetInternalTimestamp		= 1<<13,
		DebugBlit					= 1<<14,
		ApplyVideoTransform			= 1<<15,
		GenerateMipMaps				= 1<<16,
		SplitAudioChannelsIntoStreams	= 1<<17,
		PopNearestFrame				= 1<<18,
		DebugTimers					= 1<<19,
		StretchImage				= 1<<20,
		DecoderUseHardwareBuffer	= 1<<21,
		BlitFatalErrors				= 1<<22,
		ForceNonPlanarOutput		= 1<<23,
		BlitMergeYuv				= 1<<24,
		EnableAudioStreams			= 1<<25,
		CopyFrameAtUpdateTime		= 1<<26,
		OnlyExtractKeyframes		= 1<<27,
		ApplyHeightPadding			= 1<<28,
		WindowIncludeBorders		= 1<<29,
		//	gr: cannot use bit 31 on OSX in mono
	};

	enum PopMovieFlags2
	{
		//None							= 0,
		EnableVideoStreams				= 1 << 0,
		EnableDepthStreams				= 1 << 1,
		EnableSkeletonStreams			= 1 << 2,
		SplitVideoPlanesIntoStreams		= 1 << 3,
		EnableDecoderThreading			= 1 << 4,
		UseLiveTimestampsForStreaming	= 1 << 5,
		CopyBuffersInExtraction			= 1 << 6,
		ExtractorPreDecodeSkip			= 1 << 7,
		EnableAlpha						= 1 << 8,
		ApplyWidthPadding				= 1 << 9,
		ClearBeforeBlit					= 1 << 10,
		ForceYuvRangeFull				= 1 << 11,
		ForceYuvRangeNtsc				= 1 << 12,
		ForceYuvRangeSmptec				= 1 << 13,
		AndroidSingleBufferMode			= 1 << 14,
		DrawTimestamp					= 1 << 15,
		Unused16						= 1 << 16,
		Unused17						= 1 << 17,
		Unused18						= 1 << 18,
		Unused19						= 1 << 19,
		Unused20						= 1 << 20,
		Unused21						= 1 << 21,
		Unused22						= 1 << 22,
		Unused23 						= 1 << 23,
		Unused24 						= 1 << 24,
		Unused25 						= 1 << 25,
		Unused26 						= 1 << 26,
		Unused27 						= 1 << 27,
		Unused28 						= 1 << 28,
		Unused29 						= 1 << 29,
	};
}



class TVideoDecoderParams;
class TVideoDecoder;
class TSourceManager;

namespace PopMovie
{
	class TInstance;
	class TInstanceRef;
	class TPerformanceStats;
	class TPopMovieDecoderFactory;
	class TOnFrameCopiedCallback;
	
	typedef void(*TOnFrameCopiedCallbackFunc)(Unity::uint);


	struct PluginParams;
	
	class TAudioAutoRingBuffer;
	
	std::shared_ptr<TInstance>	Alloc(const TVideoDecoderParams& Params);
	std::shared_ptr<TInstance>	GetInstance(TInstanceRef Instance);
	bool						Free(TInstanceRef Instance);
	
	TExportManager<std::string,char>&		GetExportStringManager();

	
	TSourceManager&					GetSourceManager();
	std::shared_ptr<TSourceManager>	GetSourceManagerPtr();
	void							EnumSources(std::function<void(const std::string&,const std::string&)> AppendFilename,const ArrayBridge<std::string>&& Directories,std::function<bool()> Block,std::function<void()> Throttle);
};







class PopMovie::TInstanceRef
{
public:
	TInstanceRef() :
	mRef		( 0 )
	{
	}
	explicit TInstanceRef(Unity::uint Ref) :
	mRef		( Ref )
	{
	}
	
	Unity::uint	GetInt64() const	{	return mRef;	}
	bool			IsValid() const		{	return mRef != TInstanceRef().mRef;	}
	
	bool	operator<(const TInstanceRef& That) const	{	return mRef < That.mRef;	}
	bool	operator==(const TInstanceRef& That) const	{	return mRef == That.mRef;	}
	bool	operator!=(const TInstanceRef& That) const	{	return mRef != That.mRef;	}
	
public:
	Unity::uint	mRef;
};


//	gr: put this in TVideoMeta?
class PopMovie::TPerformanceStats
{
public:
	TPerformanceStats() :
		mFramesDecoded	( 0 ),
		mFramesPopSkipped	( 0 ),
		mFramesPushSkipped	( 0 ),
		mFramesPushFailed	( 0 )
	{
	}
	
	void		GetMeta(const std::string& Prefix,TJsonWriter& Json);
	
	static void	InitialiseBuffer(ArrayBridge<Unity::uint>&& Data);
	void		PushToBuffer(ArrayBridge<Unity::uint>& Data,size_t StreamIndex,SoyTime StreamTime);
	
public:
	//	timecodes, not actual times - maybe gather those for speed testing
	SoyTime		mLastExtractedTime;			//	basically last known timecode
	SoyTime		mLastDecodeSubmittedTime;
	SoyTime		mLastDecodedTime;			//	last decoded, even if discarded
	SoyTime		mLastBufferedTime;			//	oldest time in the buffer
	SoyTime		mLastCopiedTime;			//	last one copied
	
	SoyTime		mLastUpdateTextureTotalDuration;
	SoyTime		mLastUpdateTextureLockDuration;	//	time spent in OS/pixelbuffer locks, grabbing etc
	SoyTime		mLastUpdateTextureBlitDuration;	//	time spent doing a blit
	
	size_t		mFramesDecoded;
	size_t		mFramesPopSkipped;
	size_t		mFramesPushSkipped;
	size_t		mFramesPushFailed;
};


//	wrapper for meta & func of c# callback
class PopMovie::TOnFrameCopiedCallback
{
public:
	TOnFrameCopiedCallback() :
		mCallback		( nullptr ),
		mStreamIndex	( 0 )
	{
	}
	TOnFrameCopiedCallback(TOnFrameCopiedCallbackFunc Callback,size_t StreamIndex) :
		mCallback		( Callback ),
		mStreamIndex	( StreamIndex )
	{
	}

	void		Call(SoyTime FrameMs);
	
public:
	TOnFrameCopiedCallbackFunc	mCallback;
	size_t						mStreamIndex;
};



class PopMovie::TInstance
{
public:
	TInstance()=delete;
	TInstance(const TInstance& Copy)=delete;
public:
	explicit TInstance(const TInstanceRef& Ref,TVideoDecoderParams Params);
	~TInstance();
	
	TInstanceRef	GetRef() const		{	return mRef;	}
	
	bool			UpdateTexture(Opengl::TTexture& Texture,size_t StreamIndex,SoyTime CopyTimestamp);	//	return if texture changed. throw on error
	bool			UpdateTexture(Directx::TTexture& Texture,size_t StreamIndex,SoyTime CopyTimestamp);	//	return if texture changed. throw on error
	bool			UpdateTexture(Directx9::TTexture& Texture,size_t StreamIndex,SoyTime CopyTimestamp);	//	return if texture changed. throw on error
	bool			UpdateTexture(Gnm::TTexture& Texture,size_t StreamIndex,SoyTime CopyTimestamp);	//	return if texture changed. throw on error
	bool			UpdateTexture(Metal::TTexture& Texture,size_t StreamIndex,SoyTime CopyTimestamp);	//	return if texture changed. throw on error
	bool			UpdateTexture(const std::function<void(SoyPixelsImpl&,SoyTime)>& FrameCallback,size_t StreamIndex);
	void			UpdatePerformanceGraphTexture(Opengl::TTexture& Texture,size_t StreamIndex,bool LocalisedGraph);
	void			UpdatePerformanceGraphTexture(Directx::TTexture& Texture,size_t StreamIndex,bool LocalisedGraph);
	void			UpdatePerformanceGraphTexture(Directx9::TTexture& Texture,size_t StreamIndex,bool LocalisedGraph);
	void			UpdatePerformanceGraphTexture(Gnm::TTexture& Texture,size_t StreamIndex,bool LocalisedGraph);
	void			UpdatePerformanceGraphTexture(Metal::TTexture& Texture,size_t StreamIndex,bool LocalisedGraph);
	const TPerformanceStats&	GetVideoPerformanceStats(size_t VideoStreamIndex);
	void			UpdatePerformanceData(ArrayBridge<Unity::uint>&& Data);

	void			UpdateMemoryGraphTexture(Opengl::TTexture& Texture);
	void			UpdateMemoryGraphTexture(Directx::TTexture& Texture);
	void			UpdateMemoryGraphTexture(Directx9::TTexture& Texture);
	void			UpdateMemoryGraphTexture(Gnm::TTexture& Texture);
	void			UpdateMemoryGraphTexture(Metal::TTexture& Texture);

	void			UpdateAudioTexture(Opengl::TTexture& Texture,size_t StreamIndex);
	void			UpdateAudioTexture(Directx::TTexture& Texture,size_t StreamIndex);
	void			UpdateAudioTexture(Directx9::TTexture& Texture,size_t StreamIndex);
	void			UpdateAudioTexture(Gnm::TTexture& Texture,size_t StreamIndex);
	void			UpdateAudioTexture(Metal::TTexture& Texture,size_t StreamIndex);

	bool			HasNewPixelBuffer(size_t VideoStreamIndex);
	SoyMediaFormat::Type	GetStreamFormat(size_t StreamIndex);	//	returns invalid if non-existant stream
	size_t			ResolveAudioStreamIndex(size_t AudioStreamIndex);	//	throw if no such Nth audio stream index
	size_t			ResolveVideoStreamIndex(size_t VideoStreamIndex);	//	throw if no such Nth video stream index
	size_t			ResolveTextStreamIndex(size_t TextStreamIndex);	//	throw if no such Nth text stream index

	void			PopAudioBuffer(size_t AudioStreamIndex,TAudioBufferBlock& OutputBlock);
	
	void			PushStreamBuffer(ArrayBridge<uint8_t>&& Buffer,size_t StreamIndex);

	bool			PopText(std::stringstream& Output,size_t TextStreamIndex,bool SkipOldText);
	void			GetText(std::stringstream& Output,const SoyTime& Timestamp,size_t TextStreamIndex);
	
	void			Shutdown();
	void			OnDeviceShutdown();
	
	void			SetTimestamp(SoyTime Timestamp);	//	seperate func so later we can use it for seeking or pre-emptive frame discard
	TVideoMeta		GetMeta();
	void			GetMeta(TJsonWriter& Json);
	size_t			GetMetaRevision();
	bool			GetFatalError(std::string& Error);


	void			RegisterOnTextureCopied(size_t VideoStreamIndex,TOnFrameCopiedCallbackFunc Callback);
	void			UnregisterOnTextureCopied(size_t VideoStreamIndex,TOnFrameCopiedCallbackFunc Callback);
	
private:
	void			BindStatsCallbacks(const TVideoDecoderParams& Params,size_t StreamIndex);
	void			OnFrameCopied(SoyTime Time,size_t StreamIndex);
	void			GetPerformanceGraphData(ArrayBridge<TGraphBar>&& Bars,Soy::TRgb& BackgroundColour,float& PlayerTimeProgress,size_t StreamIndex,bool LocalisedGraph);
	void			GetMemoryGraphData(ArrayBridge<TGraphBar>&& Bars,Soy::TRgb& BackgroundColour,float& PlayerTimeProgress);
	
	std::shared_ptr<TPixelBuffer>	PopPixelBuffer(TVideoDecoder& Decoder,SoyTime& Timestamp,size_t StreamIndex);

	Soy::TScopeTimer	GetUpdateTextureTotalTimer(size_t StreamIndex);
	Soy::TScopeTimer	GetUpdateTextureLockTimer(size_t StreamIndex);
	Soy::TScopeTimer	GetUpdateTextureBlitTimer(size_t StreamIndex);

	
public:
	std::mutex							mOnFrameCopiedCallbacksLock;
	Array<TOnFrameCopiedCallback>		mOnFrameCopiedCallbacks;

	SoyEvent<std::pair<SoyTime,size_t>>	mOnFrameCopied;
	
	std::map<size_t,TPerformanceStats>	mPerformanceStats;
	SoyTime								mTimestamp;		//	current player time
	std::shared_ptr<TVideoDecoder>		mDecoder;
	std::map<size_t,std::shared_ptr<PopWorker::TJob>>	mDefferedCopyJobs;	//	job per stream index
	std::shared_ptr<Opengl::TBlitter>	mOpenglBlitter;
	std::shared_ptr<Directx::TBlitter>	mDirectxBlitter;
	std::shared_ptr<Directx9::TBlitter>	mDirectx9Blitter;
	std::shared_ptr<Metal::TBlitter>	mMetalBlitter;
	std::shared_ptr<Gnm::TBlitter>		mGnmBlitter;
	std::shared_ptr<TGraphRendererBase>	mGraphRenderer;
	std::shared_ptr<TAudioRendererBase>	mAudioRenderer;
	
	SoyListenerId						mOnDeviceShutdownListener;

private:
	TInstanceRef	mRef;
};



class PopMovie::TPopMovieDecoderFactory : public TVideoDecoderFactory
{
public:
	virtual std::shared_ptr<TMediaExtractor>	AllocExtractor(const TMediaExtractorParams& Params) override;
	virtual std::shared_ptr<TVideoDecoder>		Alloc(TVideoDecoderParams Params,std::map<size_t,std::shared_ptr<TPixelBufferManager>>& PixelBufferManagers,std::map<size_t,std::shared_ptr<TAudioBufferManager>>& AudioBufferManagers) override;
	virtual std::shared_ptr<TVideoDecoder>		Alloc(TVideoDecoderParams Params) override;
};

