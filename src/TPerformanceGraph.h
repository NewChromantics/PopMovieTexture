#pragma once

#if defined(ENABLE_OPENGL)
#include <SoyOpenglContext.h>
#endif
#include <SoyMath.h>
#include <mutex>

namespace PerformanceGraph
{
	static const Soy::TRgb NiceRed( 1.f, 0.22f, 0.36f );
	static const Soy::TRgb VideoGreen( 0.8f, 1.f, 0.39f );
	static const Soy::TRgb AudioGreen( 0.8f, 1.f, 0.39f );
	static const Soy::TRgb OtherGreen( 0.8f, 1.f, 0.39f );
	static const Soy::TRgb NiceBlue( 0.16f, 0.59f, 1.0f );
	static const Soy::TRgb TimeColour( 1.0f, 1.0f, 1.0f );
	static const Soy::TRgb NiceGold( 254/255.f, 197/255.f, 107/255.f );
	static const Soy::TRgb NiceTeal( 81/255.f, 197/255.f, 212/255.f );
}


namespace Opengl
{
	class TGraphRenderer;
	class TContext;
	class TTexture;
	class TRenderTarget;
	class TRenderTargetFbo;
	class TGeometry;
	class TShader;
}

namespace Directx
{
	class TGraphRenderer;
	class TContext;
	class TTexture;
	class TRenderTarget;
	class TGeometry;
	class TShader;
}

class TGraphBar
{
public:
	TGraphBar(float Progress,Soy::TRgb Colour);
	TGraphBar(Soy::Rectf Rect,Soy::TRgb Colour);
	TGraphBar()	{}
	
	Soy::Rectf	mRect;
	Soy::TRgb	mColour;
};

typedef std::function<void(const TGraphBar&,Soy::Rectf,float)> TRenderBarFunc;

class TGraphRendererBase
{
public:
	virtual ~TGraphRendererBase()	{}
		
	void	Render(std::function<vec2f()> BindRenderTarget,std::function<void(std::function<void(TRenderBarFunc)>)> RenderFunc,std::function<void()> UnbindRenderTarget,ArrayBridge<TGraphBar>& Bars,float MidLine);

	std::mutex							mRenderLock;	//	not sure this is required (due to use of atomic shared ptr's) but tracking down crash
};


class Opengl::TGraphRenderer : public TGraphRendererBase
{
public:
	TGraphRenderer(std::shared_ptr<TContext> Context);
	~TGraphRenderer();

	void	Render(TTexture& Target,Soy::TRgb ClearColour,ArrayBridge<TGraphBar>&& Bars,float MidLine);
	
	std::shared_ptr<TRenderTarget>		GetRenderTarget(TTexture& Target);
	std::shared_ptr<TGeometry>			GetGeo();
	std::shared_ptr<TShader>			GetShader();
	
	std::shared_ptr<TContext>			mContext;
	std::shared_ptr<TGeometry>			mGeo;
	std::shared_ptr<TShader>			mShader;
	std::shared_ptr<TRenderTargetFbo>	mRenderTarget;
};

class Directx::TGraphRenderer : public TGraphRendererBase
{
public:
	TGraphRenderer(std::shared_ptr<TContext> Context);
	~TGraphRenderer();

	void	Render(TTexture& Target,Soy::TRgb ClearColour,ArrayBridge<TGraphBar>&& Bars,float MidLine);

	std::shared_ptr<TRenderTarget>		GetRenderTarget(TTexture& Target);
	std::shared_ptr<TGeometry>			GetGeo();
	std::shared_ptr<TShader>			GetShader();
	
	std::shared_ptr<TContext>			mContext;
	std::shared_ptr<TGeometry>			mGeo;
	std::shared_ptr<TShader>			mShader;
	std::shared_ptr<TRenderTarget>		mRenderTarget;
};

