#include "PopMovieDecoder.h"
#include <sstream>
#include <algorithm>
#include <SortArray.h>
#include <SoyJson.h>

#if defined(TARGET_OSX)||defined(TARGET_IOS)
#include "AvfCompressor.h"
#endif

#if defined(TARGET_ANDROID)
#include "AndroidMovieDecoder.h"
#endif

const std::string TVideoDecoderParams::gProtocol_TestDecoder = "test:";
const std::string TVideoDecoderParams::gProtocol_CaptureDevice = "device:";
const std::string TVideoDecoderParams::gProtocol_AndroidSdCard = "sdcard:";
const std::string TVideoDecoderParams::gProtocol_Kinect = "kinect:";
const std::string TVideoDecoderParams::gProtocol_RawH264 = "raw:";
const std::string TVideoDecoderParams::gProtocol_Push = "push:";
const std::string TVideoDecoderParams::gProtocol_Legacy = "legacy:";		//	for old players which have been replaced, but still in the code
const std::string TVideoDecoderParams::gProtocol_Experimental = "experimental:";
const std::string TVideoDecoderParams::gProtocol_Window = "window:";
const std::string TVideoDecoderParams::gProtocol_Decklink = "decklink:";
const std::string TVideoDecoderParams::gSuffix_HLS = ".m3u8";



//	judging from old stackoverflow posts, this protocol used to work, but doesn't now. May want to support it
//		file:///android_asset/
const std::string TVideoDecoderParams::gProtocol_AndroidAssets = "apk:";
const std::string TVideoDecoderParams::gProtocol_AndroidJar = "jar:";


std::ostream& operator<<(std::ostream &out,const TPixelBufferParams& in)
{
	out << "mPopFrameSync=" << in.mPopFrameSync << ",";
	out << "mAllowPushRejection=" << in.mAllowPushRejection << ",";
	out << "mPreSeek=" << in.mPreSeek << ",";
	out << "mMaxBufferSize=" << in.mMaxBufferSize << ",";
	out << "mMinBufferSize=" << in.mMinBufferSize << ",";
	out << "mDebugFrameSkipping=" << in.mDebugFrameSkipping << ",";
	out << "mPushBlockSleepMs=" << in.mPushBlockSleepMs << ",";
	out << "mAudioSampleRate=" << in.mAudioSampleRate << ",";
	out << "mAudioChannelCount=" << in.mAudioChannelCount << ",";
	return out;
}



std::ostream& operator<<(std::ostream &out,const TVideoDecoderParams& in)
{
	out << "mFilename=" << in.mFilename << ",";
	out << in.mPixelBufferParams << ",";
	out << in.mTextureUploadParams << ",";
	out << "mAllowSlowCopy=" << in.mAllowSlowCopy << ",";
	out << "mAllowFastCopy=" << in.mAllowFastCopy << ",";
	out << "mAvfCopyBuffer=" << in.mAvfCopyBuffer << ",";
	out << "mPeekBeforeDefferedCopy=" << in.mPeekBeforeDefferedCopy << ",";
	out << "mDebugNoNewPixelBuffer=" << in.mDebugNoNewPixelBuffer << ",";
	out << "mTimecodeOffset=" << in.mTimecodeOffset << ",";
	out << "mDebugBlit=" << in.mDebugBlit << ",";
	out << "mApplyVideoTransform=" << in.mApplyVideoTransform << ",";
	out << "mForceNonPlanarOutput=" << in.mForceNonPlanarOutput << ",";
	out << "mBlitMergeYuv=" << in.mBlitMergeYuv << ",";
	out << "mClearBeforeBlit=" << in.mClearBeforeBlit << ",";
	out << "mExtractAudioStreams=" << in.mExtractAudioStreams << ",";
	out << "mDecoderUseHardwareBuffer=" << in.mDecoderUseHardwareBuffer << ",";
	out << "mBlitFatalErrors=" << in.mBlitFatalErrors << ",";
	out << "mOnlyExtractKeyframes=" << in.mOnlyExtractKeyframes << ",";
	out << "mResetInternalTimestamp=" << in.mResetInternalTimestamp << ",";
	out << "mApplyHeightPadding=" << in.mApplyHeightPadding << ",";
	out << "mApplyWidthPadding=" << in.mApplyWidthPadding << ",";
	out << "WindowIncludeBorders=" << in.mWindowIncludeBorders << ",";
	out << "mVerboseDebug=" << in.mVerboseDebug << ",";
	out << "Win7Emulation=" << in.mWin7Emulation << ",";

	out << "mLiveUseClockTime=" << in.mLiveUseClockTime << ",";
	out << "mSplitVideoPlanesIntoStreams=" << in.mSplitVideoPlanesIntoStreams << ",";
	out << "mEnableDecoderThreading=" << in.mEnableDecoderThreading << ",";
	out << "mCopyBuffersInExtraction=" << in.mCopyBuffersInExtraction << ",";
	out << "mExtractorPreDecodeSkip=" << in.mExtractorPreDecodeSkip << ",";
	out << "mAndroidSingleBufferMode=" << in.mAndroidSingleBufferMode << ",";

	out << "mDrawTimestamp=" << in.mDrawTimestamp << ",";
	out << "mDrawTimestampPosA=" << in.mDrawTimestampPosA << ",";
	out << "mDrawTimestampPosB=" << in.mDrawTimestampPosB << ",";
	
	return out;
}





TVideoDecoder::TVideoDecoder(const TVideoDecoderParams& Params,std::map<size_t,std::shared_ptr<TPixelBufferManager>>& PixelBufferManagers,std::map<size_t,std::shared_ptr<TAudioBufferManager>>& AudioBufferManagers) :
	mParams				( Params ),
	mMetaRevision		( 0 )
{
	//	gr: check what this does for perforamnce
	mPixelBufferManagers = PixelBufferManagers;
	mAudioBufferManagers = AudioBufferManagers;
	
	//	initialise start time
	//	gr: as with the extractor, may need to disallow this, or check against other things, or when we create the extractor, correct it (IF we have a player that has an extractor...)
	mPlayerTime = Params.mPixelBufferParams.mPreSeek;
}

TVideoDecoder::~TVideoDecoder()
{
	//	cleanup!
	/*
	if ( mPixelBufferManager )
	{
		mPixelBufferManager->ReleaseFrames();
		mPixelBufferManager.reset();
	}
	 */
}

std::string GetStreamMetaPrefix(size_t StreamIndex)
{
	std::stringstream PrefixStr;
	PrefixStr << "Stream" << StreamIndex << "_";
	return PrefixStr.str();
}

void TVideoDecoder::GetMeta(TJsonWriter& Json)
{
	BufferArray<TStreamMeta,10> StreamMetas;
	GetStreamMeta( GetArrayBridge(StreamMetas) );

	for ( int i=0;	i<StreamMetas.GetSize();	i++ )
	{
		auto& StreamMeta = StreamMetas[i];
		std::string Prefix = GetStreamMetaPrefix( StreamMeta.mStreamIndex );
		
		Json.Push( Prefix + "Codec", StreamMeta.mCodec );
		Json.Push( Prefix + "Duration", StreamMeta.mDuration.mTime );
		if ( StreamMeta.mEncodingBitRate != 0 )
			Json.Push( Prefix + "EncodingBitRate", StreamMeta.mEncodingBitRate );
		Json.Push( Prefix + "FramesPerSecond", StreamMeta.mFramesPerSecond );
		if ( !StreamMeta.mDescription.empty() )
			Json.Push( Prefix + "Description", StreamMeta.mDescription );
		
		//	video
		if ( StreamMeta.mPixelMeta.IsValidDimensions() )
		{
			Json.Push( Prefix + "PixelFormat", StreamMeta.mPixelMeta );
			Json.Push( Prefix + "Width", StreamMeta.mPixelMeta.GetWidth() );
			Json.Push( Prefix + "Height", StreamMeta.mPixelMeta.GetHeight() );
			Json.Push( Prefix + "Format", StreamMeta.mPixelMeta.GetFormat() );
		}
		if ( StreamMeta.mDrmProtected )
			Json.Push( Prefix + "DrmProtected", StreamMeta.mDrmProtected );
		if ( StreamMeta.mMaxKeyframeSpacing )
			Json.Push( Prefix + "MaxKeyframeSpacing", StreamMeta.mMaxKeyframeSpacing );
		if ( StreamMeta.mAverageBitRate )
			Json.Push( Prefix + "AverageBitRate", StreamMeta.mAverageBitRate );
		
		//	audio
		if ( StreamMeta.mChannelCount )
			Json.Push( Prefix + "ChannelCount", StreamMeta.mChannelCount );
		if ( StreamMeta.mAudioSampleRate )
			Json.Push( Prefix + "AudioSampleRate", StreamMeta.mAudioSampleRate );
		if ( StreamMeta.mAudioBitsPerChannel )
			Json.Push( Prefix + "AudioBitsPerChannel", StreamMeta.mAudioBitsPerChannel );

	}

	//	get meta from buffers
	auto GetBufferManagerMeta = [&Json](size_t StreamIndex,TMediaBufferManager& BufferManager)
	{
		std::string Prefix = GetStreamMetaPrefix( StreamIndex );
		BufferManager.GetMeta( Prefix, Json );
	};
	ForEachBufferManager( GetBufferManagerMeta );
}

TStreamMeta TVideoDecoder::GetStreamMeta(size_t StreamIndex)
{
	BufferArray<TStreamMeta,10> Streams;
	GetStreamMeta( GetArrayBridge( Streams ) );

	for ( int i=0;	i<Streams.GetSize();	i++ )
	{
		auto& Stream = Streams[i];
		if ( Stream.mStreamIndex != StreamIndex )
			continue;

		return Stream;
	}

	std::stringstream Error;
	Error << "Stream " << StreamIndex << " doesn't exist";
	throw Soy::AssertException( Error.str() );
}

template<typename REVERSEBUFFERMAP,typename BUFFERMAP>
void CacheStreamIndexTable(REVERSEBUFFERMAP& StreamLookup,BUFFERMAP& BufferMap)
{
	StreamLookup.Clear();

	//	get nth one, by sorted index...
	auto& Indexes = StreamLookup;
	auto Sort = [](const size_t& a,const size_t& b)
	{
		if ( a < b )	return -1;
		if ( a > b )	return 1;
		return 0;
	};
	SortArrayLambda<size_t> SortedIndexes( GetArrayBridge(Indexes), Sort );
	for ( auto& Manager : BufferMap )
	{
		SortedIndexes.Push( Manager.first );
	}
}

void TVideoDecoder::CacheStreamIndexTables()
{
	CacheStreamIndexTable( mVideoStreamIndexToStreamIndex, mPixelBufferManagers );
	CacheStreamIndexTable( mAudioStreamIndexToStreamIndex, mAudioBufferManagers );
	CacheStreamIndexTable( mTextStreamIndexToStreamIndex, mTextBufferManagers );
}


size_t TVideoDecoder::GetVideoStreamIndex(size_t VideoStreamIndex)
{
	auto& LookupMap = mVideoStreamIndexToStreamIndex;
	if ( VideoStreamIndex >= LookupMap.GetSize() )
	{
		std::stringstream Error;
		Error << "Video stream index out of range " << VideoStreamIndex << "/" << LookupMap.GetSize();
		throw Soy::AssertException( Error.str() );
	}
	return LookupMap[VideoStreamIndex];
}

size_t TVideoDecoder::GetAudioStreamIndex(size_t AudioStreamIndex)
{
	auto& LookupMap = mAudioStreamIndexToStreamIndex;
	if ( AudioStreamIndex >= LookupMap.GetSize() )
	{
		std::stringstream Error;
		Error << "Audio stream index out of range " << AudioStreamIndex << "/" << LookupMap.GetSize();
		throw Soy::AssertException( Error.str() );
	}
	return LookupMap[AudioStreamIndex];

}

size_t TVideoDecoder::GetTextStreamIndex(size_t TextStreamIndex)
{
	auto& LookupMap = mTextStreamIndexToStreamIndex;
	if ( TextStreamIndex >= LookupMap.GetSize() )
	{
		std::stringstream Error;
		Error << "Text stream index out of range " << TextStreamIndex << "/" << LookupMap.GetSize();
		throw Soy::AssertException( Error.str() );
	}
	return LookupMap[TextStreamIndex];

}


TPixelBufferManager& TVideoDecoder::GetPixelBufferManager(size_t StreamIndex)
{
	auto& BufferMap = mPixelBufferManagers;
	if ( BufferMap.find(StreamIndex) == BufferMap.end() )
	{
		std::stringstream Error;
		Error << "no such pixel buffer manager for stream " << StreamIndex;
		throw Soy::AssertException( Error.str() );
	}
	auto& Buffer = BufferMap[StreamIndex];
	if ( !Buffer )
	{
		std::stringstream Error;
		Error << "null pixel buffer manager for stream " << StreamIndex;
		throw Soy::AssertException( Error.str() );
	}
	return *Buffer;

}

TAudioBufferManager& TVideoDecoder::GetAudioBufferManager(size_t StreamIndex)
{
	auto& BufferMap = mAudioBufferManagers;
	if ( BufferMap.find(StreamIndex) == BufferMap.end() )
	{
		std::stringstream Error;
		Error << "no such audio buffer manager for stream " << StreamIndex;
		throw Soy::AssertException( Error.str() );
	}
	auto& Buffer = BufferMap[StreamIndex];
	if ( !Buffer )
	{
		std::stringstream Error;
		Error << "null audio buffer manager for stream " << StreamIndex;
		throw Soy::AssertException( Error.str() );
	}
	return *Buffer;
}

TTextBufferManager& TVideoDecoder::GetTextBufferManager(size_t StreamIndex)
{
	auto& BufferMap = mTextBufferManagers;
	if ( BufferMap.find(StreamIndex) == BufferMap.end() )
	{
		std::stringstream Error;
		Error << "no such Text buffer manager for stream " << StreamIndex;
		throw Soy::AssertException( Error.str() );
	}
	auto& Buffer = BufferMap[StreamIndex];
	if ( !Buffer )
	{
		std::stringstream Error;
		Error << "null Text buffer manager for stream " << StreamIndex;
		throw Soy::AssertException( Error.str() );
	}
	return *Buffer;
}

void TVideoDecoder::OnBufferManagersChanged(size_t StreamIndex)
{
	UpdateMetaRevision();
	CacheStreamIndexTables();
	mOnBufferManagersChanged.OnTriggered( StreamIndex );
}

std::shared_ptr<TPixelBufferManager> TVideoDecoder::AllocPixelBufferManager(const TStreamMeta& Stream)
{
	auto StreamIndex = Stream.mStreamIndex;
	auto& Buffer = mPixelBufferManagers[StreamIndex];
	if ( !Buffer )
	{
		//	take min-buffer-size from stream so we can specify it per-codec (or remove it if we know we're getting frames in-order)
		auto PixelBufferParams = mParams.mPixelBufferParams;
		static bool ForceNoBuffer = false;
		if ( !Stream.mDecodesOutOfOrder )
			PixelBufferParams.mMinBufferSize = 0;
		if ( ForceNoBuffer && PixelBufferParams.mMinBufferSize > 0 )
			PixelBufferParams.mMinBufferSize = 0;
		
		Buffer.reset( new TPixelBufferManager( PixelBufferParams ) );
		OnBufferManagersChanged( StreamIndex );
	}
	return Buffer;
}

std::shared_ptr<TAudioBufferManager> TVideoDecoder::AllocAudioBufferManager(size_t StreamIndex)
{
	auto& Buffer = mAudioBufferManagers[StreamIndex];
	if ( !Buffer )
	{
		Buffer.reset( new TAudioBufferManager(mParams.mPixelBufferParams) );
		OnBufferManagersChanged( StreamIndex );
	}
	
	return Buffer;
}

std::shared_ptr<TTextBufferManager> TVideoDecoder::AllocTextBufferManager(size_t StreamIndex)
{
	auto& Buffer = mTextBufferManagers[StreamIndex];
	if ( !Buffer )
	{
		Buffer.reset( new TTextBufferManager(mParams.mPixelBufferParams) );
		OnBufferManagersChanged( StreamIndex );
	}
	
	return Buffer;
}


TMediaBufferManager& TVideoDecoder::GetBufferManager(size_t StreamIndex)
{
	{
		auto PixelIt = mPixelBufferManagers.find( StreamIndex );
		if ( PixelIt != mPixelBufferManagers.end() )
		{
			auto pBuffer = PixelIt->second;
			if ( pBuffer )
				return *pBuffer;
		}
	}
	{
		auto PixelIt = mAudioBufferManagers.find( StreamIndex );
		if ( PixelIt != mAudioBufferManagers.end() )
		{
			auto pBuffer = PixelIt->second;
			if ( pBuffer )
				return *pBuffer;
		}
	}
	{
		auto PixelIt = mTextBufferManagers.find( StreamIndex );
		if ( PixelIt != mTextBufferManagers.end() )
		{
			auto pBuffer = PixelIt->second;
			if ( pBuffer )
				return *pBuffer;
		}
	}
	
	std::stringstream Error;
	Error << "No buffer manager for stream " << StreamIndex;
	throw Soy::AssertException( Error.str() );
}


void TVideoDecoder::ForEachBufferManager(std::function<void(size_t,TMediaBufferManager&)> FuncPerStreamIndex)
{
	for ( auto& ManagerIt : mPixelBufferManagers )
	{
		if ( !ManagerIt.second )
			continue;
		FuncPerStreamIndex( ManagerIt.first, *ManagerIt.second );
	}
	
	for ( auto& ManagerIt : mAudioBufferManagers )
	{
		if ( !ManagerIt.second )
			continue;
		FuncPerStreamIndex( ManagerIt.first, *ManagerIt.second );
	}
	
	for ( auto& ManagerIt : mTextBufferManagers )
	{
		if ( !ManagerIt.second )
			continue;
		FuncPerStreamIndex( ManagerIt.first, *ManagerIt.second );
	}
}


void TVideoDecoder::ReleaseFrames()
{
	auto ReleaseFrames = [](size_t,TMediaBufferManager& Buffer)
	{
		Buffer.ReleaseFrames();
	};
	
	ForEachBufferManager( ReleaseFrames );
}

/*
bool TVideoDecoder::PeekPixelBuffer(SoyTime Timestamp)
{
	//	if there is a fatal error, make peek fail so it gets caught
	std::string Error;
	if ( HasFatalError(Error) )
		return true;
	
	return GetPixelBufferManager().PeekPixelBuffer( Timestamp );
}


void TVideoDecoder::ReleaseFrames()
{
	if ( mPixelBufferManager )
	{
		mPixelBufferManager->ReleaseFrames();
		mPixelBufferManager.reset();
	}
}

void TVideoDecoder::PopAudioBuffer(size_t StreamIndex,ArrayBridge<float>& AudioBuffer,size_t Channels,SoyTime StartTime,SoyTime EndTime)
{
	//	grab stream
	auto StreamIt = mAudioBufferManagers.find(StreamIndex);
	if ( StreamIt == mAudioBufferManagers.end() || !StreamIt->second )
	{
		std::stringstream Error;
		Error << "No audio buffer for stream " << StreamIndex;
		throw Soy::AssertException(Error.str());
	}
	
	auto& AudioBufferManager = *StreamIt->second;
	AudioBufferManager.PopAudioData( AudioBuffer, Channels, StartTime, EndTime );
}
*/


void TVideoDecoder::SetPlayerTime(SoyTime Timestamp)
{
	mPlayerTime = Timestamp;
	
	auto SetPlayerTime = [this](size_t,TMediaBufferManager& Buffer)
	{
		Buffer.SetPlayerTime( mPlayerTime );
	};
	
	ForEachBufferManager( SetPlayerTime );
}



void TVideoDecoder::StartMovie()
{
}



TRawDecoder::TRawDecoder(const TVideoDecoderParams& Params,std::map<size_t,std::shared_ptr<TPixelBufferManager>>& PixelBufferManagers,std::map<size_t,std::shared_ptr<TAudioBufferManager>>& AudioBufferManagers,std::shared_ptr<Opengl::TContext> OpenglContext) :
	TVideoDecoder		( Params, PixelBufferManagers, AudioBufferManagers ),
	mContext			( OpenglContext )
{
}

TRawDecoder::~TRawDecoder()
{
	bool DebugDestructor = mParams.mVerboseDebug;
	
	//	stop extractors and decoders before waiting for them (may have a dependency on each other)
	if ( mExtractor )
	{
		if ( DebugDestructor )
			std::Debug << "Stopping extractor "  << mExtractor->GetThread().GetThreadName() << std::endl;
		mExtractor->Stop();
	}

	for (auto& DecoderIt : mDecoders)
	{
		auto pDecoder = DecoderIt.second;
		if ( !pDecoder )
			continue;
		
		if ( DebugDestructor )
			std::Debug << "Stopping decoder "  << pDecoder->GetThread().GetThreadName() << std::endl;
		pDecoder->Stop();
	}

	//	gr: considered putting this as a lambda to send to extractor & decoder first
	//		but just call it before destruction anyway
	if ( DebugDestructor )
		std::Debug << "Video decoder releasing frames " << std::endl;
	ReleaseFrames();

	//	destruction should wait for thread
	for (auto& DecoderIt : mDecoders)
	{
		auto pDecoder = DecoderIt.second;
		if ( !pDecoder )
			continue;
		
		if ( DebugDestructor )
			std::Debug << __func__ << " resetting decoder " << DecoderIt.first << "..." << std::endl;
		pDecoder.reset();
	}

	if ( DebugDestructor )
		std::Debug << __func__ << " resetting extractor..." << std::endl;
	mExtractor.reset();
	
	if ( DebugDestructor )
		std::Debug << __func__ << " fin." << std::endl;
}

void TRawDecoder::SetPlayerTime(SoyTime Timestamp)
{
	auto FlushFrames = [this](SoyTime FlushAfter)
	{
		auto FlushBufferManager = [FlushAfter](size_t,TMediaBufferManager& Buffer)
		{
			Buffer.FlushFrames( FlushAfter );
		};
	
		ForEachBufferManager( FlushBufferManager );
		
		//	setup fence for extractor too
		if ( mExtractor )
			mExtractor->FlushFrames( FlushAfter );
	};

	TVideoDecoder::SetPlayerTime( Timestamp );
	//	update seek on extractor
	if ( mExtractor )
	{
		mExtractor->Seek( GetPlayerTime(), FlushFrames );
	}
}

void TRawDecoder::GetMeta(TJsonWriter& Json)
{
	TVideoDecoder::GetMeta( Json );

	auto Extractor = mExtractor;

	if ( Extractor )
	{
		Extractor->GetMeta( Json );
	}
}


TVideoMeta TRawDecoder::GetMeta()
{
	if ( !mExtractor )
		return TVideoMeta();
	
	Array<TStreamMeta> Streams;
	mExtractor->GetStreams( GetArrayBridge(Streams) );
	if ( Streams.IsEmpty() )
		return TVideoMeta();
	
	TVideoMeta Meta;
	auto& StreamMeta = Streams[0];
	Meta.mDuration = StreamMeta.mDuration;
	Meta.mCodec = StreamMeta.mCodec;
	Meta.mDebug = StreamMeta.mDescription;
	
	return Meta;
}


bool TRawDecoder::HasFatalError(std::string& Error)
{
	bool HasError = false;
	
	if ( !mExtractor )
	{
		Error += "Missing extractor; ";
		HasError = true;
	}
	else
		HasError |= mExtractor->HasFatalError( Error );
	
	//	gr: work out how to deal with this now we have multiple streams... which are sometimes created deffered now
	for (auto& DecoderIt : mDecoders)
	{
		auto pDecoder = DecoderIt.second;
		if ( !pDecoder )
			continue;
		
		HasError |= pDecoder->HasFatalError( Error );
	}
	
	return HasError;
}




std::shared_ptr<TMediaDecoder> TRawDecoder::AllocVideoDecoder(const std::string& Name,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> Input,std::shared_ptr<TPixelBufferManager> Output,const TVideoDecoderParams& Params,std::shared_ptr<Platform::TMediaFormat> Format,std::shared_ptr<Opengl::TContext> OpenglContext)
{
	//	test generating format dynamically
	//	gr: only for some platforms! android requires it! move this to platform implementations!
	//	gr: we also require it for AVF for non-h264 videos which wait for SPS/PPS meta
	//		for now, lets keep it and start handling generating it (which is ideal) when we do streaming properly
	/*
#if !defined(TARGET_ANDROID)
	static bool GenerateFormat = true;
	if ( GenerateFormat )
		Format = nullptr;
#endif
	 */
	
	if ( TMediaPassThroughDecoder::HandlesCodec(Stream.mCodec) )
		return std::shared_ptr<TMediaDecoder>( new TMediaPassThroughDecoder( Name, Input, Output ) );
	
#if defined(TARGET_OSX)||defined(TARGET_IOS)
	return Avf::AllocVideoDecoder( Name, Stream, Input, Output, Params, Format );
#elif defined(TARGET_ANDROID)
	return std::shared_ptr<TMediaDecoder>( new AndroidMediaDecoder( Name, Stream, Input, Output, Format, Params, OpenglContext ) );
#elif defined(TARGET_WINDOWS)
	return std::shared_ptr<TMediaDecoder>( new TMediaPassThroughDecoder( Name, Input, Output ) );
#else
	return nullptr;
#endif
}

std::shared_ptr<TMediaDecoder> TRawDecoder::AllocAudioDecoder(const std::string& Name,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> Input,std::shared_ptr<TAudioBufferManager> Output,const TVideoDecoderParams& Params,std::shared_ptr<Platform::TMediaFormat> Format)
{
	if ( TMediaPassThroughDecoder::HandlesCodec(Stream.mCodec) )
		return std::shared_ptr<TMediaDecoder>( new TMediaPassThroughDecoder( Name, Input, Output ) );

#if defined(TARGET_OSX)||defined(TARGET_IOS)
	return Avf::AllocAudioDecoder( Name, Stream, Input, Output );
#elif defined(TARGET_ANDROID)
	return std::shared_ptr<TMediaDecoder>( new AndroidMediaDecoder( Name, Stream, Input, Output, Format, Params ) );
#elif defined(TARGET_WINDOWS)
	return std::shared_ptr<TMediaDecoder>( new TMediaPassThroughDecoder( Name, Input, Output ) );
#else
	return nullptr;
#endif
	
}



std::shared_ptr<TMediaDecoder> TRawDecoder::AllocAudioSplitDecoder(const std::string& Name,std::shared_ptr<TMediaPacketBuffer> Input,std::shared_ptr<TAudioBufferManager> Output,std::shared_ptr<TAudioBufferManager>& RealOutput,std::shared_ptr<TMediaDecoder>& RealDecoder,size_t RealChannel)
{
	return std::shared_ptr<TMediaDecoder>( new TAudioSplitChannelDecoder( Name, Input, Output, RealOutput, RealDecoder, RealChannel ) );
}

std::shared_ptr<TMediaDecoder> TRawDecoder::AllocTextDecoder(const std::string& Name,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> Input,std::shared_ptr<TTextBufferManager> Output,const TVideoDecoderParams& Params,std::shared_ptr<Platform::TMediaFormat> Format)
{
	return std::shared_ptr<TMediaDecoder>( new TMediaPassThroughDecoder( Name, Input, Output ) );
}



void TRawDecoder::AllocEncoders(const ArrayBridge<TStreamMeta>& Streams)
{
	Soy::Assert( mExtractor != nullptr, "Extractor expected" );
	auto& Extractor = *mExtractor;
	
	for ( int i=0;	i<Streams.GetSize();	i++ )
	{
		std::Debug << mParams.mFilename << " stream[" << i << "] " << Streams[i] << std::endl;
	}

	//	for fake streams (like audio split) we need an unused stream index
	size_t NewStreamIndex = 0;
	for ( int i=0;	i<Streams.GetSize();	i++ )
	{
		auto& Stream = Streams[i];
		//	indexed used, start again
		if ( Stream.mStreamIndex == NewStreamIndex )
		{
			NewStreamIndex++;
			i=0;
		}
	}
			

	//	create codec for each stream
	for ( int i=0;	i<Streams.GetSize();	i++ )
	{
		auto& Stream = Streams[i];
		std::shared_ptr<Platform::TMediaFormat> Format;
		
		std::stringstream Name;
		Name << mParams.mFilename << " stream " << Stream.mStreamIndex << " encoder";
		std::Debug << "Creating codec for " << Name.str() << ": " << Stream << std::endl;
		
		auto CurrentDecoder = mDecoders.find( Stream.mStreamIndex );
		if ( CurrentDecoder != mDecoders.end() )
		{
			std::Debug << "Already have decoder for stream " << Stream << std::endl;
			continue;
		}
		
		
		//	factory
		try
		{
			std::shared_ptr<Platform::TMediaFormat> Format = Extractor.GetStreamFormat( Stream.mStreamIndex );
			std::shared_ptr<TMediaDecoder> Decoder;
			std::shared_ptr<TMediaBufferManager> OutputBuffer;
			
			if ( SoyMediaFormat::IsAudio( Stream.mCodec ) )
			{
				//	if we're splitting the stream, then we need to allocate additional buffers and a special decoder
				//	gr: could just add on to the end of the streams and let this go, but for now, lets just explicitly allocate
				if ( mParams.mSplitAudioChannelsIntoStreams )
				{
					auto StreamInputBuffer = Extractor.AllocStreamBuffer( Stream.mStreamIndex );
					auto StreamOutputBuffer = AllocAudioBufferManager( Stream.mStreamIndex );
					OutputBuffer = StreamOutputBuffer;
					Decoder = AllocAudioDecoder( Name.str(), Stream, StreamInputBuffer, StreamOutputBuffer, mParams, Format );

					/*
					auto RealStreamInputBuffer = Extractor.AllocStreamBuffer( Stream.mStreamIndex );
					//	alloc the real decoder, to a fake buffer. That fake buffer will split on push
					//auto RealAudioBuffer = std::make_shared<TAudioBufferManager>( mParams.mPixelBufferParams );
					auto RealAudioBuffer = AllocAudioBufferManager( Stream.mStreamIndex );

					//	gr: add something here so channel count doesnt get down mixed
					auto RealDecoder = AllocAudioDecoder( Name.str(), Stream, RealStreamInputBuffer, RealAudioBuffer, mParams, Format );
					*/
					auto RealStreamInputBuffer = StreamInputBuffer;
					auto RealStreamOutputBuffer = StreamOutputBuffer;
					auto RealDecoder = Decoder;

					for ( int Channel=0;	Channel<Stream.mChannelCount;	Channel++ )
					{
						//	get the new streamindex, first channel is the original stream index
						auto ChannelStreamIndex = /*(Channel == 0) ? Stream.mStreamIndex : */NewStreamIndex++;
						auto ChannelStreamInputBuffer = Extractor.AllocStreamBuffer( ChannelStreamIndex );
						auto ChannelStreamOutputBuffer = AllocAudioBufferManager( ChannelStreamIndex );
						std::stringstream ChannelDecoderName;
						ChannelDecoderName << Name.str() << " channel " << Channel;
						auto ChannelDecoder = AllocAudioSplitDecoder( ChannelDecoderName.str(), ChannelStreamInputBuffer, ChannelStreamOutputBuffer, RealStreamOutputBuffer, RealDecoder, Channel );
						mDecoders[ChannelStreamIndex] = ChannelDecoder;

						//	just to let the normal flow continue
						if ( ChannelStreamIndex == Stream.mStreamIndex )
							Decoder = ChannelDecoder;
					}
				}
				else
				{
					auto StreamInputBuffer = Extractor.AllocStreamBuffer( Stream.mStreamIndex );
					auto StreamOutputBuffer = AllocAudioBufferManager( Stream.mStreamIndex );
					OutputBuffer = StreamOutputBuffer;
					Decoder = AllocAudioDecoder( Name.str(), Stream, StreamInputBuffer, StreamOutputBuffer, mParams, Format );
				}				
			}
			else if ( SoyMediaFormat::IsText( Stream.mCodec ) )
			{
				auto StreamInputBuffer = Extractor.AllocStreamBuffer( Stream.mStreamIndex );
				auto StreamOutputBuffer = AllocTextBufferManager( Stream.mStreamIndex );
				OutputBuffer = StreamOutputBuffer;
				Decoder = AllocTextDecoder( Name.str(), Stream, StreamInputBuffer, StreamOutputBuffer, mParams, Format );
			}
			else if ( SoyMediaFormat::IsVideo( Stream.mCodec ) )
			{
				auto StreamInputBuffer = Extractor.AllocStreamBuffer( Stream.mStreamIndex );
				auto StreamOutputBuffer = AllocPixelBufferManager( Stream );
				OutputBuffer = StreamOutputBuffer;
				Decoder = AllocVideoDecoder( Name.str(), Stream, StreamInputBuffer, StreamOutputBuffer, mParams, Format, mContext );
			}
		
			Soy::Assert( Decoder !=nullptr, "Didnt allocate decoder");

			//	apply some decoder modifications to the pixel buffer (in a generic place)
			if ( Decoder && OutputBuffer )
			{
				auto MinBufferSize = Decoder->GetMinBufferSize();
				
				//	if we're only extracting keyframes, then we don't need to wait
				if ( Extractor.mParams.mOnlyExtractKeyframes )
					MinBufferSize = 0;
				
				OutputBuffer->SetMinBufferSize( MinBufferSize );
			}

			mDecoders[Stream.mStreamIndex] = Decoder;
		}
		catch(std::exception& e)
		{
			std::stringstream Error;
			Error << "Error creating encoder for " << Stream << "; " << e.what();
			mFatalError = Error.str();
		}
	}
	
	//	now we have some encoders, start the thread
	if ( !mDecoders.empty() )
		mExtractor->Start(false);
}

void TRawDecoder::GetStreamMeta(ArrayBridge<TStreamMeta>&& Streams)
{
	if ( !mExtractor )
		return;
	
	mExtractor->GetStreams( GetArrayBridge(Streams) );
}

