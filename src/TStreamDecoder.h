#pragma once

#include "PopMovieDecoder.h"
#include <SoySocket.h>
#include <SoyStream.h>
#include <SoyHttp.h>
#include <SoyProtocol.h>
#include <SoySocketStream.h>
#include <SoyHttpConnection.h>
#include <SoySrt.h>
#include <SoyImage.h>



namespace Private
{
	//	so we can put templated constructor in source
	void		TRawFileExtractor_AllocFileReader(const TMediaExtractorParams& Params,std::shared_ptr<TStreamReader>& mFileReader,const std::function<std::shared_ptr<Soy::TReadProtocol>()>& AllocProtocol,const std::function<void(const std::string&)>& ErrorHandler);
	typedef void(&TImageReadFunc)(SoyPixelsImpl&,TStreamBuffer&);
}

class TMediaPacket_Protocol : public Soy::TReadProtocol
{
public:
	virtual void					GetPackets(ArrayBridge<std::shared_ptr<TMediaPacket>>&& Packets)
	{
		Packets.PushBack( mPacket );
	}
	
protected:
	std::shared_ptr<TMediaPacket>	mPacket;
};


class TSrtMediaPacket_Protocol : public TMediaPacket_Protocol
{
public:
	virtual TProtocolState::Type	Decode(TStreamBuffer& Buffer) override;

	static TStreamMeta				GetStreamMeta();

public:
	Srt::TFrame						mSrt;
};


class TImageFileMediaPacket_ProtocolImpl : public TMediaPacket_Protocol
{
public:
	TImageFileMediaPacket_ProtocolImpl()=delete;
	TImageFileMediaPacket_ProtocolImpl(const TImageFileMediaPacket_ProtocolImpl&)=delete;
public:
	TImageFileMediaPacket_ProtocolImpl(Private::TImageReadFunc& ReadFunction) :
		mReadFunction	( ReadFunction ),
		mTries			( 0 )
	{
	}
	
	virtual TProtocolState::Type	Decode(TStreamBuffer& Buffer) override;
	
public:
	size_t												mTries;			//	read will fail, but we cannot tell (with most formats) as to when we've got enough data... so keep trying... gotta work this out.
	std::function<void(SoyPixelsImpl&,TStreamBuffer&)>	mReadFunction;
};



template<Private::TImageReadFunc READFUNC>
class TImageFileMediaPacket_Protocol : public TImageFileMediaPacket_ProtocolImpl
{
public:
	TImageFileMediaPacket_Protocol() :
		TImageFileMediaPacket_ProtocolImpl	( *READFUNC )
	{
	}
};


//	this READS out(extracts) packet chunks int packets
class TH264ES_Protocol : public TMediaPacket_Protocol
{
public:
	virtual TProtocolState::Type	Decode(TStreamBuffer& Buffer) override;

};


//	dumb in->out protocol. see if we can optimise for this case...
class TCopyOutProtocol : public Soy::TReadProtocol
{
public:
	TCopyOutProtocol(std::shared_ptr<TStreamBuffer> Output) :
		mOutput	( Output )
	{
		Soy::Assert( mOutput!=nullptr, std::string(__func__) + " requires output stream");
	}
	
	virtual TProtocolState::Type	Decode(TStreamBuffer& Buffer) override;
	
public:
	std::shared_ptr<TStreamBuffer>	mOutput;
};





//	demuxer for a raw stream/file -> packets
class TStreamExtractor : public TMediaExtractor
{
protected:
	TStreamExtractor(const TMediaExtractorParams& Params);
	~TStreamExtractor();

	virtual std::shared_ptr<Platform::TMediaFormat>	GetStreamFormat(size_t StreamIndex) override
	{
		//	gr: may need to create formats at runtime for some platforms... depends on the encoder though...
		return nullptr;
	}
	virtual std::shared_ptr<TMediaPacket>			ReadNextPacket() override;
	virtual void									GetStreams(ArrayBridge<TStreamMeta>&& Streams) override;

	//	input from whatever stream
	virtual std::shared_ptr<TStreamReader>			AllocInputStreamReader()=0;
	TStreamBuffer&									GetPacketStream()		{	return *mPacketStream;	}
	std::shared_ptr<TStreamBuffer>					GetPacketStreamPtr()	{	return mPacketStream;	}
	
	//	converts data from stream into media packets and puts them in an intermediate packet queue (super class controls the flow)
	std::shared_ptr<TStreamReader>					AllocMediaPacketReader(std::shared_ptr<TStreamBuffer>& StreamBuffer);
	virtual std::shared_ptr<TMediaPacket_Protocol>	AllocMediaPacketProtocol()=0;
	virtual void									OnStreamPacketExtracted(std::shared_ptr<TMediaPacket>& Packet);

private:
	//	stream extractor only knows meta once we've extracted packets
	std::map<size_t,TStreamMeta>					mStreams;
	
	std::shared_ptr<TStreamReader>					mMediaPacketStreamReader;
	std::mutex										mInputPacketsLock;
	Array<std::shared_ptr<TMediaPacket>>			mInputPackets;

	std::shared_ptr<TStreamBuffer>					mPacketStream;	//	data for media packets to parse
};


//	for one-stream file formats which we can handle with protocols
//	todo: for one-shot file formats, have something on the stream readers that can be flagged that we're only
//		interested in data/being woken up when the whole file has been read (can also apply to HTTP) to save
//		multiple tries in the imaage media packet protocol
template<typename MEDIAPACKET_PROTOCOL>
class TRawFileExtractor : public TStreamExtractor
{
public:
	TRawFileExtractor(const TMediaExtractorParams& Params);
	~TRawFileExtractor();
	
protected:
	virtual std::shared_ptr<TMediaPacket_Protocol>	AllocMediaPacketProtocol() override	{	return std::shared_ptr<TMediaPacket_Protocol>( new MEDIAPACKET_PROTOCOL );	}
	virtual std::shared_ptr<TStreamReader>			AllocInputStreamReader() override	{	return mFileReader;	}
	
protected:
	std::shared_ptr<TStreamReader>		mFileReader;
};



typedef TRawFileExtractor<TSrtMediaPacket_Protocol>						TSrtFileExtractor;
typedef TRawFileExtractor<TImageFileMediaPacket_Protocol<Png::Read>>	TPngFileExtractor;
typedef TRawFileExtractor<TImageFileMediaPacket_Protocol<Bmp::Read>>	TBmpFileExtractor;
typedef TRawFileExtractor<TImageFileMediaPacket_Protocol<Tga::Read>>	TTgaFileExtractor;
typedef TRawFileExtractor<TImageFileMediaPacket_Protocol<Psd::Read>>	TPsdFileExtractor;
typedef TRawFileExtractor<TImageFileMediaPacket_Protocol<Gif::Read>>	TGifFileExtractor;



class TH264StreamMeta
{
public:
	TH264StreamMeta() :
		mCodec	( SoyMediaFormat::Invalid )
	{
	}
	
	void							GetMeta(TStreamMeta& Meta);
	
public:
	SoyMediaFormat::Type			mCodec;
	std::shared_ptr<TMediaPacket>	mSpsPacket;
	std::shared_ptr<TMediaPacket>	mPpsPacket;
};


//	demuxer for a raw stream
class TRawH264RawExtractor : public TStreamExtractor
{
public:
	TRawH264RawExtractor(const TMediaExtractorParams& Params) :
		TStreamExtractor	( Params )
	{
	}
	
protected:
	virtual std::shared_ptr<TMediaPacket_Protocol>	AllocMediaPacketProtocol() override
	{
		return std::shared_ptr<TMediaPacket_Protocol>( new TH264ES_Protocol() );
	}
	
	virtual void									GetStreams(ArrayBridge<TStreamMeta>&& Streams) override;
	
	virtual void									OnStreamPacketExtracted(std::shared_ptr<TMediaPacket>& Packet) override;
	
private:
	Array<TH264StreamMeta>				mStreamMetas;
};



//	demuxer for a raw stream
//	gr: replace with TRawFileExtractor<>
class TRawH264RawFileExtractor : public TRawH264RawExtractor
{
public:
	TRawH264RawFileExtractor(const TMediaExtractorParams& Params);
	
protected:
	virtual std::shared_ptr<TStreamReader>	AllocInputStreamReader() override
	{
		return mFileReader;
	}

	std::shared_ptr<TStreamReader>		mFileReader;
};

//	demuxer for a raw stream
//	gr: replace with better template (not a h264 base)
class TRawH264RawSocketExtractor : public TRawH264RawExtractor
{
public:
	TRawH264RawSocketExtractor(const std::string& SocketAddress,std::function<void(const SoyTime,size_t)> OnFrameExtracted) :
		TRawH264RawExtractor	( TMediaExtractorParams( SocketAddress, std::string("TRawH264RawSocketExtractor/") + SocketAddress, OnFrameExtracted, nullptr ) ),
		mSocketAddress			( SocketAddress )
	{
	}
	
protected:
	virtual std::shared_ptr<TStreamReader>			AllocInputStreamReader() override
	{
		//	this needs a socket connection before I can create the read thread... so... this needs to be able to fail?
		return mConnection->GetReadThread();
	}

	std::shared_ptr<TSocketConnection>	mConnection;
	std::string			mSocketAddress;
};




class TStreamRawDecoder : public TRawDecoder
{
public:
	TStreamRawDecoder(const TVideoDecoderParams& Params,std::map<size_t,std::shared_ptr<TPixelBufferManager>>& PixelBufferManagers,std::map<size_t,std::shared_ptr<TAudioBufferManager>>& AudioBufferManagers,std::shared_ptr<Opengl::TContext> OpenglContext,TVideoDecoderFactory& ExtractorFactory);
	
	static bool		HandlesFilename(const std::string& Filename);	//	does this handle special cases (assume it also handles generic files too)
};







//	for one-stream file formats which we can handle with protocols
template<typename MEDIAPACKET_PROTOCOL>
TRawFileExtractor<MEDIAPACKET_PROTOCOL>::TRawFileExtractor(const TMediaExtractorParams& Params) :
	TStreamExtractor	( Params )
{
	auto AllocProtocol = [this]
	{
		return std::shared_ptr<Soy::TReadProtocol>( new TCopyOutProtocol( GetPacketStreamPtr() ) );
	};
	
	auto OnError = [this](const std::string& Error)
	{
		this->OnError( Error );
	};

	Private::TRawFileExtractor_AllocFileReader( Params, mFileReader, AllocProtocol, OnError );
}





//	for one-stream file formats which we can handle with protocols
template<typename MEDIAPACKET_PROTOCOL>
TRawFileExtractor<MEDIAPACKET_PROTOCOL>::~TRawFileExtractor()
{
	mFileReader.reset();
}

