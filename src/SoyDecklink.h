#pragma once

#include <SoyMedia.h>


#if defined(TARGET_WINDOWS)
#include "Decklink\Windows\DeckLinkAPI_h.h"
#endif

namespace Decklink
{
	class TContext;
	class TExtractor;


	void		EnumDevices(std::function<void(const std::string&)> AppendName);
}



class Decklink::TContext
{
public:
	TContext();
	~TContext();

	AutoReleasePtr<IDeckLinkInput>		GetDevice(const std::string& MatchSerial);
	void								EnumDevices(std::function<void(AutoReleasePtr<IDeckLinkInput>&,const std::string&)> EnumDevice);

public:
	std::mutex							mIteratorLock;
	AutoReleasePtr<IDeckLinkIterator>	mIterator;
};


class Decklink::TExtractor : public TMediaExtractor
{
public:
	TExtractor(const TMediaExtractorParams& Params);

	virtual void									GetStreams(ArrayBridge<TStreamMeta>&& Streams) override;
	virtual std::shared_ptr<Platform::TMediaFormat>	GetStreamFormat(size_t StreamIndex) override		{	return nullptr;	}
	virtual void									GetMeta(TJsonWriter& Json) override;

protected:
	virtual std::shared_ptr<TMediaPacket>			ReadNextPacket() override;

public:
	AutoReleasePtr<IDeckLinkInput>	mInput;
};


