#include "TGifExtractor.h"
#include <SoyBase64.h>



//	for cleaner code
inline int	bits(int a)				{	return 1<<a;	}
inline int	bits(int a,int b)		{	return bits(a)|bits(b);	}
inline int	bits(int a,int b,int c)	{	return bits(a)|bits(b)|bits(c);	}


int GetPaletteSize(bool HasPalette,uint8 Flags,uint8 Bits)
{
	if ( !HasPalette )
		return 0;
	
	auto Count = Flags & Bits;
	//	docs always say +1 but this never seems to work out...
	//Count += 1;
	Count = 2 << Count;
	
	if ( Count > 256 )
	{
		std::stringstream Error;
		Error << "Mis counted palette size as " << Count;
		Soy::Assert( Count <= 256, Error.str() );
	}
	return Count;
}

bool ReadPalette(ArrayBridge<Soy::TRgba8>&& Palette,ArrayBridge<uint8>&& PaletteDataBuffer,int PaletteSize,uint8 TransparentPaletteIndex,TStreamBuffer& Buffer)
{
	//	nowt to read
	if ( PaletteSize == 0 )
		return true;
	
	auto PaletteDataSize = PaletteSize * 3;
	if ( !Buffer.Pop( PaletteDataSize, GetArrayBridge(PaletteDataBuffer) ) )
		return false;
	
	FixedRemoteArray<Soy::TRgb8> RgbPalette( reinterpret_cast<Soy::TRgb8*>(PaletteDataBuffer.GetArray()), PaletteSize );
	
	Palette.Reserve( RgbPalette.GetSize() );
	for ( int i=0;	i<RgbPalette.GetSize();	i++ )
	{
		auto& Rgb = RgbPalette[i];
		Soy::TRgba8 Rgba( Rgb.x, Rgb.y, Rgb.z, 255 );
		if ( i == TransparentPaletteIndex )
			Rgba.w = 0;
		Palette.PushBack( Rgba );
	}
	return true;
}



template<typename TYPE>
inline TYPE& ThrowFakeRef()
{
	throw Soy::AssertException("Should never be called");
}


TGifMediaPacket_Protocol::TGifMediaPacket_Protocol() :
	mGif	( ThrowFakeRef<Gif::TReader>() )
{
}


TGifMediaPacket_Protocol::TGifMediaPacket_Protocol(Gif::TReader& Reader) :
	mGif	( Reader )
{
}

TProtocolState::Type TGifMediaPacket_Protocol::Decode(TStreamBuffer& Buffer)
{
	auto Result = mGif.Decode( Buffer );
	if ( Result != TProtocolState::Finished )
		return Result;
	
	/*
	//	turn into packet
	mPacket.reset( new TMediaPacket() );
	mPacket->mTimecode = mSrt.mStart;
	mPacket->mDuration = mSrt.mEnd - mSrt.mStart;
	mPacket->mMeta.mCodec = SoyMediaFormat::Subtitle;
	
	Soy::StringToArray( mSrt.mString, GetArrayBridge( mPacket->mData ) );
	*/
	return TProtocolState::Finished;
}

void TGifMediaPacket_Protocol::GetPackets(ArrayBridge<std::shared_ptr<TMediaPacket>>&& Packets)
{
	mGif.PopPackets( Packets );
}


Gif::THeader::THeader(uint16 Width,uint16 Height,ArrayBridge<Soy::TRgba8>&& Palette,uint8 TransparentPaletteIndex) :
	mWidth		( Width ),
	mHeight		( Height ),
	mPalette	( Palette ),
	mTransparentPaletteIndex	( TransparentPaletteIndex )
{
}


Gif::TExtension::TExtension(uint8 Type,ArrayBridge<uint8>&& Data)
{
	//	some known extensions
	/*
	 //	https://www.w3.org/Graphics/GIF/spec-gif89a.txt
	 The labels used to identify labeled blocks fall
	 into three ranges : 0x00-0x7F (0-127) are the Graphic Rendering blocks,
	 excluding the Trailer (0x3B); 0x80-0xF9 (128-249) are the Control blocks;
	 0xFA-0xFF (250-255) are the Special Purpose blocks. These ranges are defined so
	 that decoders can handle block scope by appropriately identifying block labels,
	 even when the block itself cannot be processed.
	 */
	std::stringstream ExtensionName;
	switch ( Type )
	{
		case 0x01:	ExtensionName << "Plain text";	break;
		case 0xf9:	ExtensionName << "Graphics control";	break;
		case 0xfe:	ExtensionName << "Comment";	break;
		case 0xff:	ExtensionName << "Netscape2 anim";	break;
		default:	ExtensionName << "0x" << Soy::ByteToHex( Type );	break;
	}

	std::Debug << "todo: Handle gif extension " << ExtensionName.str() << " " << Soy::FormatSizeBytes(Data.GetSize()) << std::endl;
}


TProtocolState::Type Gif::TReader::DecodeHeader(TStreamBuffer& Buffer)
{
	BufferArray<uint8,6> Magic;
	BufferArray<uint8,7> Description;
	BufferArray<uint8,256*3> PaletteData;
	auto Unpop = [&](TProtocolState::Type Result)
	{
		//	in reverse!
		Buffer.UnPop( GetArrayBridge(PaletteData) );
		Buffer.UnPop( GetArrayBridge(Description) );
		Buffer.UnPop( GetArrayBridge(Magic) );
		return Result;
	};
	
	//	GIFXXX
	if ( !Buffer.Pop( 6, GetArrayBridge(Magic) ) )
		return Unpop( TProtocolState::Waiting );
	
	//	global description & palette meta
	if ( !Buffer.Pop( 7, GetArrayBridge(Description) ) )
		return Unpop( TProtocolState::Waiting );
	
	auto Width = Description[0] | (Description[1]<<8);
	auto Height = Description[2] | (Description[3]<<8);
	//	decode this byte
	//	f0 == 2
	auto Flags = Description[4];
	auto BackgroundPalIndex = Description[5];
	//auto Ratio = Description[6];

	auto HasPalette = bool_cast( Flags & bits(7) );
	//auto ColourResolution = Flags & bits(6,5,4);
	//auto Sorted = bool_cast( Flags & bits(3) );
	auto PaletteSize = GetPaletteSize( HasPalette, Flags, bits(0,1,2) );
	
	//	read palette
	BufferArray<Soy::TRgba8,256> Palette;
	if ( !ReadPalette( GetArrayBridge(Palette), GetArrayBridge(PaletteData), HasPalette ? PaletteSize : 0, BackgroundPalIndex, Buffer ) )
		return Unpop( TProtocolState::Waiting );
	
	mHeader = THeader( Width, Height, GetArrayBridge( Palette ), BackgroundPalIndex );
	
	//	success, but didn't generate any packets
	return TProtocolState::Ignore;
}


typedef struct
{
	sint16 prefix;
	uint8 first;
	uint8 suffix;
} stbi__gif_lzw;

typedef struct
{
	int w,h;
	int flags, bgindex, ratio, transparent, eflags;
	uint8  pal[256][4];
	uint8 lpal[256][4];
	stbi__gif_lzw codes[4096];
	int lflags;
	
	Soy::Rectx<uint16>	Rect;
	vec2x<int>			CurrentPos;
} stbi__gif;

static void stbi__out_gif_code(ArrayBridge<uint8>& DecompressedData,stbi__gif *g, uint16 code)
{
	//uint8 *p, *c;
	
	// recurse to decode the prefixes, since the linked-list is backwards,
	// and working backwards through an interleaved image would be nasty
	if (g->codes[code].prefix >= 0)
		stbi__out_gif_code( DecompressedData, g, g->codes[code].prefix);
	
	if (g->CurrentPos.y > g->Rect.Bottom() )
		return;
	
	auto PaletteIndex = g->codes[code].suffix;
	
	//	recursive use of this seems to go bad for main thread...
	//std::Debug << g->CurrentPos << "=" << PaletteIndex << std::endl;
	DecompressedData.PushBack( PaletteIndex );
	
	g->CurrentPos.x++;
	
	if (g->CurrentPos.x > g->Rect.Right() ) {
		g->CurrentPos.x = g->Rect.Left();
		g->CurrentPos.y ++;
		
		/*
		while (g->cur_y >= g->max_y && g->parse > 0) {
			g->step = (1 << g->parse) * g->line_size;
			g->cur_y = g->start_y + (g->step >> 1);
			--g->parse;
		}
		*/
		
	}
}


void Gif::TReader::DecodeLzw(ArrayBridge<uint8>& DecompressedData,ArrayBridge<uint8>&& LzwData,uint8 MinCodeSize,Soy::Rectx<uint16> FrameRect)
{
	static int TimerMinMs = 8;
	std::stringstream TimerName;
	TimerName << __func__ << " " << Soy::FormatSizeBytes(LzwData.GetDataSize());
	Soy::TScopeTimerPrint Timer( TimerName.str().c_str(), TimerMinMs );

	stbi__gif _gif;
	auto* g = &_gif;
	
	_gif.Rect = FrameRect;
	_gif.CurrentPos.x = _gif.Rect.x;
	_gif.CurrentPos.y = _gif.Rect.y;

	

	sint32 len, code;
	uint32 first;
	sint32 codesize, codemask, avail, oldcode, bits, valid_bits, clear;
	stbi__gif_lzw *p;
	
	auto lzw_cs = MinCodeSize;
	//lzw_cs = stbi__get8(s);
	
	//	gr: why?
	Soy::Assert( lzw_cs <= 12, "gr: min code size can't be > 12?");

	clear = 1 << lzw_cs;
	first = 1;
	codesize = lzw_cs + 1;
	codemask = (1 << codesize) - 1;
	bits = 0;
	valid_bits = 0;
	for (code = 0; code < clear; code++)
	{
		g->codes[code].prefix = -1;
		g->codes[code].first = (uint8) code;
		g->codes[code].suffix = (uint8) code;
	}
	
	// support no starting clear code
	avail = clear+2;
	oldcode = -1;
	
	len = 0;
	for(;;) {
		if (valid_bits < codesize) {
			if (len == 0) {
				len = LzwData.PopAt(0); // start new block
				if (len == 0)
					return;
			}
			--len;
			bits |= (sint32) LzwData.PopAt(0) << valid_bits;
			valid_bits += 8;
		} else {
			int32_t code = bits & codemask;
			bits >>= codesize;
			valid_bits -= codesize;
			// @OPTIMIZE: is there some way we can accelerate the non-clear path?
			if (code == clear) // clear code
			{
				codesize = lzw_cs + 1;
				codemask = (1 << codesize) - 1;
				avail = clear + 2;
				oldcode = -1;
				first = 0;
			}
			else if (code == clear + 1)	// end of stream code
			{
				LzwData.RemoveBlock(0, len);
				while ((len = LzwData.PopAt(0)) > 0)
					LzwData.RemoveBlock(0,len);
				return;
			}
			else if (code <= avail)
			{
				if (first)
					throw Soy::AssertException("no clear code");
				
				if (oldcode >= 0)
				{
					p = &g->codes[avail++];
					if (avail > 4096)
						throw Soy::AssertException("too many codes");
					p->prefix = (sint16) oldcode;
					p->first = g->codes[oldcode].first;
					p->suffix = (code == avail) ? p->first : g->codes[code].first;
				}
				else if (code == avail)
				{
					throw Soy::AssertException("illegal code in raster");
				}
				
				stbi__out_gif_code( DecompressedData, g, (uint16) code);
				
				if ((avail & codemask) == 0 && avail <= 0x0FFF) {
					codesize++;
					codemask = (1 << codesize) - 1;
				}
				
				oldcode = code;
			}
			else
			{
				throw Soy::AssertException("illegal code in raster");
			}
		}
	}
}


bool Gif::TReader::DecodeLzwBlock(ArrayBridge<uint8>&& DecompressedData,ArrayBridge<uint8>&& PoppedLzwData,TStreamBuffer& Buffer,Soy::Rectx<uint16> FrameRect)
{
	BufferArray<uint8,1> MinCodeSize;
	if ( !Buffer.Pop( 1, GetArrayBridge(MinCodeSize) ) )
		return false;
	PoppedLzwData.PushBackArray( MinCodeSize );

	
	Array<uint8> LzwData;
	
	while ( true )
	{
		BufferArray<uint8,1> BlockSize;
		if ( !Buffer.Pop( 1, GetArrayBridge(BlockSize) ) )
			return false;
		PoppedLzwData.PushBackArray( BlockSize );
		LzwData.PushBackArray( BlockSize );
		
		//	end of lzw blocks
		if ( BlockSize[0] == 0 )
			break;
		
		Array<uint8> Block;
		if ( !Buffer.Pop( BlockSize[0], GetArrayBridge(Block) ) )
			return TProtocolState::Waiting;
		PoppedLzwData.PushBackArray( Block );
		LzwData.PushBackArray( Block );
		
		//	gr: should be able to divide this
		//	decode lzw
	//	DecodeLzw( GetArrayBridge(Block), MinCodeSize[0] );
	}
	
	
	//	notify start of decode here; todo: determine timecode.
	//mOnFrameDecodeStarted.OnTriggered();
	
	DecodeLzw( DecompressedData, GetArrayBridge(LzwData), MinCodeSize[0], FrameRect );
	return true;
}


TProtocolState::Type Gif::TReader::DecodeFrame(TStreamBuffer& Buffer)
{
	BufferArray<uint8,10> FrameHeader;
	BufferArray<uint8,256*3> PaletteData;
	Array<uint8> LzwData;
	auto Unpop = [&](TProtocolState::Type Result)
	{
		//	in reverse!
		Buffer.UnPop( GetArrayBridge(LzwData) );
		Buffer.UnPop( GetArrayBridge(PaletteData) );
		Buffer.UnPop( GetArrayBridge(FrameHeader) );
		return Result;
	};
	
	if ( !Buffer.Pop( 10, GetArrayBridge(FrameHeader) ) )
		return Unpop( TProtocolState::Waiting );

	auto Type = FrameHeader[0];
	Soy::Assert( Type == 0x2c, "frame header isn't the frame header byte?" );

	auto Left = FrameHeader[1] | (FrameHeader[2]<<8);
	auto Top = FrameHeader[3] | (FrameHeader[4]<<8);
	auto Width = FrameHeader[5] | (FrameHeader[6]<<8);
	auto Height = FrameHeader[7] | (FrameHeader[8]<<8);

	auto Flags = FrameHeader[9];
	auto HasPalette = bool_cast( Flags & bits(7) );
	//auto Interlaced = bool_cast( Flags & bits(6) );
	//auto Sorted = bool_cast( Flags & bits(5) );
	//auto Reserved = bool_cast( Flags & bits(4,3) );
	auto PaletteSize = GetPaletteSize( HasPalette, Flags, bits(0,1,2) );

	//	read palette
	BufferArray<Soy::TRgba8,256> FramePalette;
	if ( !ReadPalette( GetArrayBridge(FramePalette), GetArrayBridge(PaletteData), HasPalette ? PaletteSize : 0, mHeader.mTransparentPaletteIndex, Buffer ) )
		return Unpop( TProtocolState::Waiting );
	
	//	read image
	try
	{
		Soy::Rectx<uint16> FrameRect( Left, Top, Width, Height );
		
		Array<uint8> DecompressedData;
		if ( !DecodeLzwBlock( GetArrayBridge(DecompressedData), GetArrayBridge(LzwData), Buffer, FrameRect ) )
			return Unpop( TProtocolState::Waiting );

		//	no decompressed data... not sure what that means
		Soy::Assert( !DecompressedData.IsEmpty(), "LZW decompressed zero bytes");
		
		//	as we're accumulating this, the trasnform (from rect) is kinda unused atm, but maybe for a special gif blit pass we can use it
		float3x3 Transform;

		//	turn it into an image
		SoyPixels Pixels;
		BuildFrame( Pixels, GetArrayBridge(DecompressedData), FrameRect, FramePalette );
		OnDecodedFrame( Pixels, Transform );
		
		return TProtocolState::Finished;
	}
	catch(std::exception& e)
	{
		static bool SkipFrame = true;
		std::Debug << "Exception decoding gif LZW data: " << e.what() << ( SkipFrame ? ". Skipping frame" : "" ) << std::endl;
		if ( SkipFrame )
			return TProtocolState::Ignore;
		
		throw;
	}
}


void Gif::TReader::BuildFrame(SoyPixelsImpl& Pixels,const ArrayBridge<uint8>&& PixelData,Soy::Rectx<uint16> Rect,BufferArray<Soy::TRgba8,256>& FramePalette)
{
	auto Palette = FramePalette.IsEmpty() ? this->mHeader.mPalette : FramePalette;
	Soy::Assert( !Palette.IsEmpty(), "Only have empty palettes!" );
	
	//	gr: this will be ditched for shader palettising once we can blit transparent onto prev
	
	//	rect is only part of the frame so put it in the right place
	Pixels.Init( SoyPixelsMeta( mHeader.mWidth, mHeader.mHeight, SoyPixelsFormat::RGBA ) );

	Soy::TRgba8 ClearRgba( 255, 255, 0, 255 );
	Soy::TRgba8 TransRgba( 255, 0, 255, 255 );

	//	clear in case rect is off-center
	if ( mDebugFrameRect && Rect != mHeader.GetRect() )
	{
		auto ClearRgbaArray = GetRemoteArray( &ClearRgba.x, 4 );
		Pixels.SetPixels( GetArrayBridge(ClearRgbaArray) );
	}
	
	//	clear
	
	for ( int dy=0;	dy<Rect.h;	dy++ )
	{
		for ( int dx=0;	dx<Rect.w;	dx++ )
		{
			int px = Rect.Left() + dx;
			int py = Rect.Top() + dy;
			
			auto PalIndex = PixelData[dy * Rect.w + dx];
			if ( PalIndex > Palette.GetSize() )
				PalIndex = 0;
			auto PalColour = Palette[PalIndex];
			
			if ( mDebugFrameTransparency )
			{
				if ( PalColour.w < 255 )
					PalColour = TransRgba;
			}
			
			Pixels.SetPixel( px, py, PalColour );
		}
	}

}


TProtocolState::Type Gif::TReader::DecodeExtension(TStreamBuffer& Buffer)
{
	BufferArray<uint8,2> ExtensionLabel_;
	if ( !Buffer.Pop( 2, GetArrayBridge(ExtensionLabel_) ) )
		return TProtocolState::Waiting;
	
	Array<uint8> PoppedData;
	auto Unpop = [&]
	{
		Buffer.UnPop( GetArrayBridge(PoppedData) );
		Buffer.UnPop( GetArrayBridge(ExtensionLabel_) );
	};

	auto ExtensionType = static_cast<uint8>(ExtensionLabel_[1]);

	//	seperate to popped data as we discard some bytes
	Array<uint8> ExtensionData;
	
	//	read blocks
	while ( true )
	{
		BufferArray<uint8,1> BlockLength;
		if ( !Buffer.Pop( 1, GetArrayBridge(BlockLength) ) )
		{
			Unpop();
			return TProtocolState::Waiting;
		}
		PoppedData.PushBackArray( BlockLength );
		
		//	terminator
		if ( BlockLength[0] == 0 )
			break;
		
		BufferArray<uint8,256> BlockData;
		if ( !Buffer.Pop( BlockLength[0], GetArrayBridge(BlockData) ) )
		{
			Unpop();
			return TProtocolState::Waiting;
		}
		PoppedData.PushBackArray( BlockData );
		ExtensionData.PushBackArray( BlockData );
	}

	try
	{
		TExtension Extension( ExtensionType, GetArrayBridge(ExtensionData) );
		mExtensionStack.PushBack( Extension );
	}
	catch(std::exception& e)
	{
		std::Debug << "Exception parsing gif extension; " << e.what() << std::endl;
	}
	
	//	success, but didn't generate any packets
	return TProtocolState::Ignore;
}

TProtocolState::Type Gif::TReader::Decode(TStreamBuffer& Buffer)
{
	//	read first byte
	BufferArray<uint8,1> PacketType(1);
	if ( !Buffer.Peek( GetArrayBridge(PacketType) ) )
		return TProtocolState::Waiting;
	
	
	if ( PacketType[0] == 'G' )
	{
		return DecodeHeader( Buffer );
	}
	else if ( PacketType[0] == 0x21 )	//	!
	{
		return DecodeExtension( Buffer );
	}
	else if ( PacketType[0] == 0x2c )	//	,
	{
		return DecodeFrame( Buffer );	//	;
	}
	else if ( PacketType[0] == 0x3b )
	{
		//	eof
		Buffer.Pop(1);

		//	make eof packet
		OnEof();
		return TProtocolState::Disconnect;
	}
	
	std::stringstream Error;
	Error << "Gif decode error; unknown chunk code 0x" << Soy::ByteToHex( PacketType[0] );
	throw Soy::AssertException( Error.str() );
}


void Gif::TReader::OnDecodedFrame(SoyPixelsImpl& Pixels,const float3x3& Transform)
{
	auto pPacket = std::make_shared<TMediaPacket>();
	auto& Packet = *pPacket;
	
	Packet.mPixelBuffer.reset( new TDumbPixelBuffer( Pixels, Transform ) );
	Packet.mMeta.SetPixelMeta( Pixels.GetMeta() );
	Packet.mMeta.mDuration = SoyTime( std::chrono::milliseconds(33) );
	Packet.mTimecode = mNextFrameTime;
	mNextFrameTime += std::max( SoyTime(std::chrono::milliseconds(1)), Packet.mMeta.mDuration );

	std::lock_guard<std::mutex> Lock( mFramesLock );
	mFrames.PushBack( pPacket );
}


void Gif::TReader::OnEof()
{
	auto pPacket = std::make_shared<TMediaPacket>();
	auto& Packet = *pPacket;
	
	Packet.mEof = true;
	Packet.mTimecode = mNextFrameTime;
	
	std::lock_guard<std::mutex> Lock( mFramesLock );
	mFrames.PushBack( pPacket );
}




void Gif::TReader::PopPackets(ArrayBridge<std::shared_ptr<TMediaPacket>>& Packets)
{
	std::lock_guard<std::mutex> Lock( mFramesLock );
	
	for ( int i=0;	i<mFrames.GetSize();	i++ )
		Packets.PushBack( mFrames[i] );
	
	mFrames.Clear();
}










