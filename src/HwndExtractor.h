#pragma once

#include <SoyMedia.h>


namespace Hwnd
{
	void		EnumWindows(std::function<void(const std::string&)> AppendName,std::function<bool()> Block);
}

#define INVALID_HWND	0


class TWindowHandle
{
public:
	TWindowHandle() :
		mHandle	( INVALID_HWND )
	{
	}
	bool		IsValid() const		{	return mHandle != INVALID_HWND;	}

public:
	RECT		mRect;
	HWND		mHandle;
	std::string	mName;
};

class HwndExtractor : public TMediaExtractor
{
public:
	HwndExtractor(const TMediaExtractorParams& Params);
	~HwndExtractor();

	virtual void									GetStreams(ArrayBridge<TStreamMeta>&& Streams) override;
	virtual std::shared_ptr<Platform::TMediaFormat>	GetStreamFormat(size_t StreamIndex) override		{	return nullptr;	}
	virtual void	GetMeta(TJsonWriter& Json) override;

protected:
	virtual std::shared_ptr<TMediaPacket>			ReadNextPacket() override;

	TWindowHandle	GetWindowHandle();

public:
	std::string		mWindowTitle;
	vec2x<int>		mWindowLastPos;		//	cache for meta
	SoyPixelsMeta	mWindowPixelMeta;	//	for checking when size changes

	TWindowHandle	mWindowHandle;

	std::shared_ptr<SoyPixelsImpl>	mPixelBuffer;
};


