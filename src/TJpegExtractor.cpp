#include "TJpegExtractor.h"
#include <SoyBase64.h>


namespace Jpeg
{
	void		ExtractMediaPackets(std::function<void(std::shared_ptr<TMediaPacket>)> PushPacket,Jpeg::TMeta& Meta,std::function<void(std::string&,ArrayBridge<char>&&)> OnFoundProperty);
	void		ExtractMediaPackets(std::function<void(std::shared_ptr<TMediaPacket>)> PushPacket,Jpeg::TMeta& Meta);
}


TProtocolState::Type TJpegMediaPacket_Protocol::Decode(TStreamBuffer& Buffer)
{
	//	read out meta to see if there are secret packets
	if ( !mJpegMeta )
	{
		//	gr: if this throws... maybe we're not ready at all? let the main jpeg code deal with that though
		try
		{
			mJpegMeta.reset( new Jpeg::TMeta );
			Jpeg::ReadMeta( *mJpegMeta, Buffer );
		}
		catch(std::exception& e)
		{
			mJpegMeta.reset();
			std::Debug << "Exception reading jpeg meta; " << e.what() << std::endl;
		}
	}
	
	
	//	gr: this is all mostly the same as TImageFileMediaPacket_Protocol
	
	auto pPacket = std::make_shared<TMediaPacket>();
	auto& Packet = *pPacket;
	SoyPixelsBridge<Array<uint8>> Pixels( Packet.mData, Packet.mMeta.mPixelMeta );
	
	std::string ExceptionError;
	try
	{
		auto& mReadFunction = Jpeg::Read;
		mReadFunction( Pixels, Buffer );
		Soy::Assert( Pixels.GetMeta().IsValid(), "Expected valiid pixel meta after successfull image read");
		Soy::Assert( Packet.mMeta.mPixelMeta.IsValid(), "Expected valiid pixel meta after successfull image read");
		
		//	finish packet
		Packet.mMeta.mCodec = SoyMediaFormat::FromPixelFormat( Pixels.GetFormat() );
		
		//	make all images 1ms long, 0=unknown, but we know these finish.
		Packet.mDuration = SoyTime( std::chrono::milliseconds(1) );
		Packet.mIsKeyFrame = true;
		//	gr: check this doesn't screw up the system.
		//		we want it here though so stream can determine that Duration+eof = total duration
		Packet.mEof = true;
		
		mPacket = pPacket;
		
		auto SubsequentStreamIndex = Packet.mMeta.mStreamIndex+1;
		
		auto PushPacket = [this,&SubsequentStreamIndex](std::shared_ptr<TMediaPacket> Packet)
		{
			//	bake properties
			Packet->mMeta.mStreamIndex = SubsequentStreamIndex;
			SubsequentStreamIndex++;
			
			mAdditionalPackets.PushBack( Packet );
		};
		
		//	construct more packets from meta
		if ( mJpegMeta )
			Jpeg::ExtractMediaPackets( PushPacket, *mJpegMeta );
		
		return TProtocolState::Finished;
	}
	catch (std::exception& e)
	{
		ExceptionError = e.what();
	}
	catch(...)
	{
		ExceptionError = "Unknown exception";
	}
	
	
	static size_t MaxTriesBeforeError = 1000;
	mTries++;
	//	aftrer a certain amount of tries, report a failure rather than waiting for data
	//	we should only be woken whenever the stream has MORE data so it's not like we'll spin on tries for too long (depending on the size of the file...)
	if ( mTries > MaxTriesBeforeError )
	{
		std::stringstream Error;
		Error << "Too many tries (" << mTries << "/" << MaxTriesBeforeError << ") trying to read image; " << ExceptionError;
		throw Soy::AssertException( Error.str() );
	}
	
	//	assume there's not enough data yet
	std::Debug << "Assuming not enough data yet to read image file... (" << mTries << "/" << MaxTriesBeforeError << "); Error=" << ExceptionError << std::endl;
	return TProtocolState::Waiting;
}

void TJpegMediaPacket_Protocol::GetPackets(ArrayBridge<std::shared_ptr<TMediaPacket>>&& Packets)
{
	Packets.PushBack( mPacket );
	Packets.PushBackArray( mAdditionalPackets );
}


bool IsXmlKeyChar(char c)
{
	if ( c >= '0' && c <='9' )	return true;
	if ( c >= 'A' && c <='Z' )	return true;
	if ( c >= 'a' && c <='z' )	return true;

	switch ( c )
	{
		case ':':	return true;
			
		default:
			return false;
	}
}

//	super hacky XML ripper
void RipXmlProperties(const std::string& XmlData,std::function<void(std::string&,ArrayBridge<char>&&)> OnPropertyFound)
{
	//	search for =" and then read backwards for the key and forwards for the end of the data
	int i=0;
	while ( i+1 < XmlData.size() )
	{
		if ( XmlData[i] != '=' || XmlData[i+1] != '"' )
		{
			i++;
			continue;
		}

		//	found a property with data
		//	read back
		int KeyEnd = i-1;
		int KeyStart = KeyEnd;
		while ( KeyStart > 0 )
		{
			if ( !IsXmlKeyChar( XmlData[KeyStart-1] ) )
		
				break;
			KeyStart--;
		}
		
		//	now find the end of the value
		int ValueStart = i+2;
		int ValueEnd = ValueStart;
		while ( ValueEnd+1 < XmlData.size() )
		{
			if ( XmlData[ValueEnd+1] == '"' )
				break;
			ValueEnd++;
		}

		auto KeyArray = GetRemoteArray( reinterpret_cast<const char*>(&XmlData[KeyStart]), (KeyEnd-KeyStart)+1 );
		auto ValueArray = GetRemoteArray( reinterpret_cast<const char*>(&XmlData[ValueStart]), (ValueEnd-ValueStart)+1 );
	
		auto Key = Soy::ArrayToString( GetArrayBridge(KeyArray) );
		
		OnPropertyFound( Key, GetArrayBridge(ValueArray) );
		
		//	skip over all this data
		i = ValueEnd+1;
		continue;
	}

}

extern std::shared_ptr<TMediaPacket> DecodeImagePacket(std::function<void(SoyPixelsImpl&,TStreamBuffer&)> ReadFunction,TStreamBuffer& Buffer);


std::function<void(SoyPixelsImpl&,TStreamBuffer&)> GetImageReadFunc(SoyMediaFormat::Type Codec)
{
	//	resolve overloaded Png::Read using a lambda
	auto PngOverload = [] (SoyPixelsImpl& p,TStreamBuffer& b)
	{
		Png::Read( p, b );
	};
	
	switch ( Codec )
	{
		case SoyMediaFormat::Jpeg:	return Jpeg::Read;
		case SoyMediaFormat::Png:	return PngOverload;
		case SoyMediaFormat::Bmp:	return Bmp::Read;
		case SoyMediaFormat::Tga:	return Tga::Read;
		case SoyMediaFormat::Gif:	return Gif::Read;
		
		default:
			return nullptr;
	}
}


std::shared_ptr<TMediaPacket> DecodePacketBase64(ArrayBridge<char>& Data,const std::string& MimeType)
{
	//	decode base 64
	Array<char> DecodedData;
	Base64::Decode( Data, GetArrayBridge(DecodedData) );

	//	if data is an encoded image, decode immediately
	//	gr: should this be in passthrough??
	auto Codec = SoyMediaFormat::FromMime( MimeType );
	std::function<void(SoyPixelsImpl&,TStreamBuffer&)> ImageReadFunc = GetImageReadFunc( Codec );
	
	try
	{
		if ( ImageReadFunc != nullptr )
		{
			TStreamBuffer Buffer;
			Buffer.Push( GetArrayBridge( DecodedData ) );
			auto Packet = DecodeImagePacket( ImageReadFunc, Buffer );
			if ( Packet )
			{
				return Packet;
			}
		}
	}
	catch(std::exception& e)
	{
		std::Debug << "Failed to turn " << MimeType << " data to image: " << e.what() << std::endl;
	}
	
	//	dumb data
	auto pPacket = std::make_shared<TMediaPacket>();
	auto& Packet = *pPacket;
	Packet.mData.PushBackArray( DecodedData );
	Packet.mMeta.SetMime( MimeType );
	
	return pPacket;
}

void Jpeg::ExtractMediaPackets(std::function<void(std::shared_ptr<TMediaPacket> Packet)> PushPacket,Jpeg::TMeta& Meta,std::function<void(std::string&,ArrayBridge<char>&&)> OnFoundProperty)
{
	RipXmlProperties( Meta.mXmp, OnFoundProperty );
}


void Jpeg::ExtractMediaPackets(std::function<void(std::shared_ptr<TMediaPacket> Packet)> PushPacket,Jpeg::TMeta& Meta)
{
	//	accumulate meta data as mime is in the XAP and the data is in XMP
	std::string ImageMime;
	std::string AudioMime;
	
	auto OnFoundProperty = [&](std::string& Key,ArrayBridge<char>&& Value)
	{
		if ( Key == "GImage:Mime" )
		{
			ImageMime = Soy::ArrayToString( Value );
			return;
		}
		
		if ( Key == "GImage:Data" )
		{
			auto Packet = DecodePacketBase64( Value, ImageMime );
			if ( Packet )
				PushPacket( Packet );
			return;
		}
		
		if ( Key == "GAudio:Mime" )
		{
			AudioMime = Soy::ArrayToString( Value );
			return;
		}
		
		if ( Key == "GAudio:Data" )
		{
			auto Packet = DecodePacketBase64( Value, AudioMime );
			if ( Packet )
				PushPacket( Packet );
			return;
		}
		
		static bool DebugXmpProperties = false;
		if ( DebugXmpProperties )
		{
			std::stringstream ValueStr;
			Soy::ArrayToString( Value, ValueStr );
			std::Debug << "Unhandled XMP property " << Key << "=" << ValueStr.str() << std::endl;
		}
	};
	
	ExtractMediaPackets( PushPacket, Meta, OnFoundProperty );
}




