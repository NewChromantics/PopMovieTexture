#pragma once

#include "TStreamDecoder.h"



class TPushExtractor : public TMediaExtractor
{
public:
	TPushExtractor(const TMediaExtractorParams& Params);
	~TPushExtractor();

	virtual std::shared_ptr<Platform::TMediaFormat>	GetStreamFormat(size_t StreamIndex) override;
	virtual std::shared_ptr<TMediaPacket>			ReadNextPacket() override;
	virtual void									GetStreams(ArrayBridge<TStreamMeta>&& Streams) override;

	virtual void									OnStreamPacketExtracted(std::shared_ptr<TMediaPacket>& Packet);

	void											PushBuffer(ArrayBridge<uint8_t>& Buffer,size_t StreamIndex);

private:
	std::map<size_t,TStreamMeta>					mStreams;
	std::mutex										mInputPacketsLock;
	Array<std::shared_ptr<TMediaPacket>>			mInputPackets;
};
