#pragma once

#include <SoyMedia.h>

#if defined(ENABLE_BINK)
#include "Bink\include\bink.h"
#endif

#if defined(ENABLE_GNM)
#include "SoyGnm.h"
#endif

namespace Soy
{
	class TRuntimeLibrary;
}

namespace Bink
{
	class TExtractor;
	class TFrameBuffer;

	class TTextureBufferRgba;
	class TTextureBufferPlanes;
	class TTextureBufferGnm;

	static const char*	FileExtensions[] = {".bk2",".bik"};
}


class Bink::TTextureBufferRgba
{
public:
	TTextureBufferRgba(SoyPixelsMeta Meta) :
		mRgba	( Meta )
	{
	}

	void			GetPlaneStreamMetas(ArrayBridge<SoyPixelsMeta>& Meta);
	SoyPixelsImpl&	GetPixels(struct BINK& Handle,float3x3& Transform);

	size_t			GetCurrentFrameBufferIndex()		{	return 0;	}
	size_t			GetDecodeTargetFrameBufferIndex()	{	return 0;	}

private:
	SoyPixels		mRgba;
};

//	gr: abstract this for GNM to provide pixel addresses once allocated
#if defined(ENABLE_BINK)
class Bink::TTextureBufferPlanes
{
public:
	TTextureBufferPlanes(struct BINKFRAMEBUFFERS& BufferInfo,SoyPixelsMeta VideoMeta,HBINK Handle);

	void			GetPlaneStreamMetas(ArrayBridge<SoyPixelsMeta>& Meta,bool SplitPlanesIntoStreams);
	SoyPixelsImpl&	GetPixels(size_t VideoStreamIndex,size_t BufferIndex,float3x3& Transform);
	void			GetPixels(ArrayBridge<SoyPixelsImpl*>& Textures,size_t BufferIndex,float3x3& Transform);

	SoyPixelsImpl&	AllocLuma(SoyPixelsMeta BufferMeta,SoyPixelsMeta ImageMeta);
	SoyPixelsImpl&	AllocChromaR(SoyPixelsMeta BufferMeta,SoyPixelsMeta ImageMeta);
	SoyPixelsImpl&	AllocChromaB(SoyPixelsMeta BufferMeta,SoyPixelsMeta ImageMeta);
	SoyPixelsImpl&	AllocAlpha(SoyPixelsMeta BufferMeta,SoyPixelsMeta ImageMeta);

	size_t			GetCurrentFrameBufferIndex();	//	current valid content
	size_t			GetDecodeTargetFrameBufferIndex();	//	where we will decode into

public:
	BINKFRAMEBUFFERS						mBufferInfo;
	Array<std::shared_ptr<SoyPixelsImpl>>	mLuma;
	Array<std::shared_ptr<SoyPixelsImpl>>	mChromaR;
	Array<std::shared_ptr<SoyPixelsImpl>>	mChromaB;
	Array<std::shared_ptr<SoyPixelsImpl>>	mAlpha;
};
#endif

#if defined(ENABLE_BINK)
class Bink::TExtractor : public TMediaExtractor
{
public:
	TExtractor(const TMediaExtractorParams& Params);
	~TExtractor();

	virtual void									GetStreams(ArrayBridge<TStreamMeta>&& Streams) override;
	virtual std::shared_ptr<Platform::TMediaFormat>	GetStreamFormat(size_t StreamIndex) override		{	return nullptr;	}
	virtual void									GetMeta(TJsonWriter& Json) override;

	void											GetPlaneStreamMetas(ArrayBridge<SoyPixelsMeta>&& Metas,bool SplitPlanesIntoStreams);

	void											GetBufferPixels(ArrayBridge<SoyPixelsImpl*>& Textures,float3x3& Transform,size_t VideoStreamIndex,size_t BufferIndex);
	void											GetBufferPixels(ArrayBridge<SoyPixelsImpl*>&& Textures,float3x3& Transform,size_t VideoStreamIndex,size_t BufferIndex)	{	GetBufferPixels( Textures, Transform, VideoStreamIndex, BufferIndex );	}

protected:
	virtual std::shared_ptr<TMediaPacket>			ReadNextPacket() override;
	virtual bool									CanSleep() override;

	std::shared_ptr<TMediaPacket>					DecodeNextPacket();
	SoyPixelsMeta									GetVideoMeta();

	bool											IsPixelBufferStreamLocked(size_t BufferIndex);
	size_t											GetCurrentFrameBufferIndex();
	size_t											GetDecodeTargetFrameBufferIndex();

public:
	std::mutex										mHandleLock;
	HBINK											mHandle;
	std::shared_ptr<Soy::TRuntimeLibrary>			mBinkDll;
	Array<U32>										mThreads;

	std::map<size_t,std::map<size_t,bool>>			mPixelBufferStreamLocks;
	Array<std::shared_ptr<TMediaPacket>>			mPacketQueue;	//	gr: reuse this as locks?
	std::shared_ptr<TTextureBufferRgba>				mBufferRgba;
	std::shared_ptr<TTextureBufferPlanes>			mBufferPlanes;
};
#endif



/*
	Due to the way bink works (one frame at a time), make this a super dumb interface back to the
*/
#if defined(ENABLE_BINK)
class Bink::TFrameBuffer : public TPixelBuffer
{
public:
	TFrameBuffer(TExtractor& Extractor,size_t VideoStreamIndex,size_t BufferIndex,std::function<void()> OnUnlock);
	~TFrameBuffer();

	virtual void		Lock(ArrayBridge<Opengl::TTexture>&& Textures,Opengl::TContext& Context,float3x3& Transform) override		{}
	virtual void		Lock(ArrayBridge<Directx::TTexture>&& Textures,Directx::TContext& Context,float3x3& Transform) override		{}
	virtual void		Lock(ArrayBridge<Metal::TTexture>&& Textures,Metal::TContext& Context,float3x3& Transform) override			{}
	virtual void		Lock(ArrayBridge<Gnm::TTexture>&& Textures,Gnm::TContext& Context,float3x3& Transform) override;
	virtual void		Lock(ArrayBridge<SoyPixelsImpl*>&& Textures,float3x3& Transform) override;
	virtual void		Unlock() override;

protected:
	std::function<void()>	mOnUnlock;
	TExtractor&				mExtractor;
	size_t					mVideoStreamIndex;
	size_t					mBufferIndex;
};
#endif