#include "TStreamDecoder.h"
#include "AvfCompressor.h"
#include <SoyH264.h>
#include "TDecoderPlaylist.h"

#if defined(TARGET_ANDROID)
#include <SoyJava.h>
#endif


void Private::TRawFileExtractor_AllocFileReader(const TMediaExtractorParams& Params,std::shared_ptr<TStreamReader>& mFileReader,const std::function<std::shared_ptr<Soy::TReadProtocol>()>& AllocProtocol,const std::function<void(const std::string&)>& ErrorHandler)
{
#if defined(TARGET_ANDROID)
	try
	{
		mFileReader.reset( new Java::TApkFileStreamReader_ProtocolLambda( Params.mFilename, AllocProtocol ) );
	}
	catch(std::exception& e)
	{
		std::Debug << "Failed to create android asset fd. Trying normal; " << e.what() << std::endl;
	}
#endif
	
	if ( !mFileReader )
		mFileReader.reset( new TFileStreamReader_ProtocolLambda<>( Params.mFilename, AllocProtocol ) );
	
	mFileReader->mOnError.AddListener( ErrorHandler );

	mFileReader->Start();
}


TRawH264RawFileExtractor::TRawH264RawFileExtractor(const TMediaExtractorParams& Params) :
	TRawH264RawExtractor	( Params )
{
	auto AllocProtocol = [this]
	{
		return std::shared_ptr<Soy::TReadProtocol>( new TCopyOutProtocol( GetPacketStreamPtr() ) );
	};
/*
#if defined(TARGET_ANDROID)
	try
	{
		mFileReader.reset( new Java::TFileStreamReader_ProtocolLambda( Filename, AllocProtocol ) );
		
	}
	catch(std::exception& e)
	{
		std::Debug << "Failed to create android asset fd. Trying normal; " << e.what() << std::endl;
	}
#endif
	*/
	if ( !mFileReader )
		mFileReader.reset( new TFileStreamReader_ProtocolLambda<>( Params.mFilename, AllocProtocol ) );
	
	mFileReader->Start();

}


TStreamRawDecoder::TStreamRawDecoder(const TVideoDecoderParams& Params,std::map<size_t,std::shared_ptr<TPixelBufferManager>>& PixelBufferManagers,std::map<size_t,std::shared_ptr<TAudioBufferManager>>& AudioBufferManagers,std::shared_ptr<Opengl::TContext> Context,TVideoDecoderFactory& ExtractorFactory) :
	TRawDecoder			( Params, PixelBufferManagers, AudioBufferManagers, Context )
{
	auto OnFrameExtracted = [this](const SoyTime Time,size_t StreamIndex)
	{
		//	we may not have allocated a buffer manager so this may be dropped
		try
		{
			this->GetBufferManager( StreamIndex ).mOnFrameExtracted.OnTriggered( Time );
		}
		catch(std::exception& e)
		{
			//	do nothing
			static bool DebugOnFrameExtractedFailed = true;
			if ( DebugOnFrameExtractedFailed )
			{
				std::Debug << "TStreamRawDecoder:: OnFrameExtraced lambda exception; " << e.what() << std::endl;
			}
		}
	};

	//	allocate extractor
	TMediaExtractorParams ExtractorParams( Params.mFilename, Params.mFilename, OnFrameExtracted, nullptr );
	ExtractorParams.mReadAheadMs = Params.mExtractAheadMs;
	ExtractorParams.mDiscardOldFrames = Params.mPixelBufferParams.mPopFrameSync;
	ExtractorParams.mForceNonPlanarOutput = Params.mForceNonPlanarOutput;
	ExtractorParams.mForceYuvColourFormat = Params.mForceYuvColourFormat;
	ExtractorParams.mExtractAudioStreams = Params.mExtractAudioStreams;
	ExtractorParams.mExtractVideoStreams = Params.mExtractVideoStreams;
	ExtractorParams.mExtractDepthStreams = Params.mExtractDepthStreams;
	ExtractorParams.mExtractSkeletonStreams = Params.mExtractSkeletonStreams;
	ExtractorParams.mExtractAlpha = Params.mExtractAlpha;
	ExtractorParams.mApplyHeightPadding = Params.mApplyHeightPadding;
	ExtractorParams.mApplyWidthPadding = Params.mApplyWidthPadding;
	ExtractorParams.mInitialTime = GetPlayerTime();
	ExtractorParams.mOnlyExtractKeyframes = Params.mOnlyExtractKeyframes;
	ExtractorParams.mResetInternalTimestamp = Params.mResetInternalTimestamp;
	ExtractorParams.mAudioSampleRate = Params.mPixelBufferParams.mAudioSampleRate;
	ExtractorParams.mAudioChannelCount = Params.mPixelBufferParams.mAudioChannelCount;
	ExtractorParams.mWindowIncludeBorders = Params.mWindowIncludeBorders;
	ExtractorParams.mWin7Emulation = Params.mWin7Emulation;
	ExtractorParams.mVerboseDebug = Params.mVerboseDebug;
	ExtractorParams.mSplitAudioChannelsIntoStreams = Params.mSplitAudioChannelsIntoStreams;
	ExtractorParams.mDecoderUseHardwareBuffer = Params.mDecoderUseHardwareBuffer;
	ExtractorParams.mAllowPushRejection = Params.mPixelBufferParams.mAllowPushRejection;
	ExtractorParams.mEnableDecoderThreading = Params.mEnableDecoderThreading;
	ExtractorParams.mSplitVideoPlanesIntoStreams = Params.mSplitVideoPlanesIntoStreams;
	ExtractorParams.mPeekBeforeDefferedCopy = Params.mPeekBeforeDefferedCopy;
	ExtractorParams.mLiveUseClockTime = Params.mLiveUseClockTime;
	ExtractorParams.mCopyBuffersInExtraction = Params.mCopyBuffersInExtraction;
	ExtractorParams.mExtractorPreDecodeSkip = Params.mExtractorPreDecodeSkip;


	mExtractor = ExtractorFactory.AllocExtractor( ExtractorParams );
	Soy::Assert( mExtractor!=nullptr, "Failed to allocate an extractor");

	//	might be known instantly. need a better solution as I want extractors to start immediately...
	Array<TStreamMeta> Streams;
	mExtractor->GetStreams( GetArrayBridge(Streams) );
	
	auto OnStreamsChanged = [this](const ArrayBridge<TStreamMeta>& Streams)
	{
		//	when extractor reports meta as changed, update meta revision (changes inside a stream)
		UpdateMetaRevision();
		AllocEncoders(Streams);
	};
	
	//	gr: listen regardless
	mExtractor->mOnStreamsChanged.AddListener( OnStreamsChanged );
	
	if ( !Streams.IsEmpty() )
		AllocEncoders( GetArrayBridge(Streams ) );
}



TStreamExtractor::TStreamExtractor(const TMediaExtractorParams& Params) :
	TMediaExtractor		( Params ),
	mPacketStream		( new TStreamBuffer )
{
	//	alloc reader as soon as some data enters. This helps us get around the pure virtual call here
	auto AllocMediaPacketStreamReader = [this](bool&)
	{
		auto OnMediaPacketExtracted = [this](std::shared_ptr<Soy::TReadProtocol>& Packet)
		{
#if defined(ENABLE_RTTI)
			auto MediaPacketPtr = std::dynamic_pointer_cast<TMediaPacket_Protocol>( Packet );
#else
			auto MediaPacketPtr = std::static_pointer_cast<TMediaPacket_Protocol>( Packet );
#endif
			Array<std::shared_ptr<TMediaPacket>> Packets;
			MediaPacketPtr->GetPackets( GetArrayBridge(Packets) );
			for ( int p=0;	p<Packets.GetSize();	p++ )
			{
				this->OnStreamPacketExtracted( Packets[p] );
			}
		};
		

		if ( mMediaPacketStreamReader )
			return;
		mMediaPacketStreamReader = AllocMediaPacketReader( mPacketStream );

		//	catch all packet outputs
		mMediaPacketStreamReader->mOnDataRecieved.AddListener( OnMediaPacketExtracted );
		mMediaPacketStreamReader->Start();
	};
	mPacketStream->mOnDataPushed.AddListener( AllocMediaPacketStreamReader );
}

TStreamExtractor::~TStreamExtractor()
{
	mMediaPacketStreamReader.reset();
	mPacketStream.reset();
}

void TStreamExtractor::GetStreams(ArrayBridge<TStreamMeta>&& Streams)
{
	for ( auto& Meta : mStreams )
	{
		Streams.PushBack( Meta.second );
	}
}


std::shared_ptr<TMediaPacket> TStreamExtractor::ReadNextPacket()
{
	if ( mInputPackets.IsEmpty() )
		return nullptr;
	
	std::lock_guard<std::mutex> Lock(mInputPacketsLock);
	return mInputPackets.PopAt(0);
}

void TStreamExtractor::OnStreamPacketExtracted(std::shared_ptr<TMediaPacket>& Packet)
{
	Soy::Assert( Packet !=nullptr, "TStreamExtractor::OnPacketDecoded; Expected packet" );

	//	first bit of stream info
	auto StreamIndex = Packet->mMeta.mStreamIndex;
	if ( mStreams.find(StreamIndex) == mStreams.end() )
	{
		mStreams[StreamIndex] = Packet->mMeta;
		mStreams[StreamIndex].mDuration = SoyTime();
		OnStreamsChanged();
	}

	//	update duration if we know when it finishes
	if ( Packet->mEof )
	{
		auto& Duration = mStreams[StreamIndex].mDuration.mTime;
		if ( Duration == 0 )
		{
			//	note: EOF needs a time really :/ maybe we can automatically catch this by recording last frame time for each stream... which may need to come from the buffer?
			//	1=corrected
			if ( Packet->mTimecode.mTime <= 1 )
				std::Debug << "Warning, EOF packet has no timecode." << std::endl;
			
			Duration = Packet->mTimecode.mTime + Packet->mDuration.mTime;
			//	gr: for images, timecode of 1 and frameduration of 1, we want a total duration of 1 really. MAYBE that's the right calculation for movies too?
			if ( Duration > 0 )
				Duration--;

			//	just in case we have no duration, or odd time etc.. make sure the duration is not zero (unknown)
			if ( Duration == 0 )
				Duration = 1;
		}
	}
	
	mInputPacketsLock.lock();
	mInputPackets.PushBack( Packet );
	mInputPacketsLock.unlock();
	
	//	wake up the TMediaExtractor to grab new packets
	OnPacketExtracted( Packet );
}


std::shared_ptr<TStreamReader> TStreamExtractor::AllocMediaPacketReader(std::shared_ptr<TStreamBuffer>& StreamBuffer)
{
	//	buffer is externally updated
	auto Read = [StreamBuffer]
	{
		//	eof
		if ( !StreamBuffer )
		{
			std::Debug << __func__ << " read returning eof as null stream buffer" << std::endl;
			return false;
		}

		if ( StreamBuffer->HasEndOfStream() )
		{
			std::Debug << __func__ << " read returning eof as stream buffer has eof" << std::endl;
			return false;
		}

		return true;
	};
	
	auto Shutdown = []
	{
		//	todo: stop parent
	};
	
	auto AllocProtocol = [this]
	{
		auto Protocol = AllocMediaPacketProtocol();
		return Protocol;
	};
	
	return std::shared_ptr<TStreamReader>( new TStreamReader_Impl( mPacketStream, Read, Shutdown, AllocProtocol, "MediaPacket stream reader" ) );
}


TProtocolState::Type TCopyOutProtocol::Decode(TStreamBuffer& Buffer)
{
	Array<char> Data;
	while ( Buffer.GetBufferedSize() > 0 )
	{
		Data.Clear();
		static size_t LengthMax = 1024*1024*1;
		size_t Length = std::min( LengthMax, Buffer.GetBufferedSize() );
		if ( !Buffer.Pop( Length, GetArrayBridge(Data) ) )
			break;
		
		mOutput->Push( GetArrayBridge(Data) );
	}
	
	return TProtocolState::Finished;
}


void TRawH264RawExtractor::OnStreamPacketExtracted(std::shared_ptr<TMediaPacket>& Packet)
{
	bool Changed = false;
	
	//	look to see if we can construct our streams
	if ( Packet->mMeta.mCodec == SoyMediaFormat::H264_PPS_ES )
	{
		if ( mStreamMetas.IsEmpty() )
			mStreamMetas.PushBack();

		Changed |= (mStreamMetas[0].mPpsPacket == nullptr);
		mStreamMetas[0].mPpsPacket = Packet;
	}
	else if ( Packet->mMeta.mCodec == SoyMediaFormat::H264_SPS_ES )
	{
		if ( mStreamMetas.IsEmpty() )
			mStreamMetas.PushBack();
		
		Changed |= (mStreamMetas[0].mSpsPacket == nullptr);
		mStreamMetas[0].mSpsPacket = Packet;
	}
	else if ( mStreamMetas.IsEmpty() )
	{
		mStreamMetas.PushBack();
		mStreamMetas[0].mCodec = Packet->mMeta.mCodec;
		Changed |= true;
	}

	//	reconstruct stream and notify
	if ( Changed && mStreamMetas.GetSize() > 0 && mStreamMetas[0].mSpsPacket && mStreamMetas[0].mPpsPacket )
	{
		mStreamMetas[0].mCodec = SoyMediaFormat::H264_ES;
		Array<TStreamMeta> Streams;
		GetStreams( GetArrayBridge(Streams) );
		OnStreamsChanged( GetArrayBridge(Streams) );
	}
	
	TStreamExtractor::OnStreamPacketExtracted( Packet );
}

void TRawH264RawExtractor::GetStreams(ArrayBridge<TStreamMeta>&& Streams)
{
	for ( int i=0;	i<mStreamMetas.GetSize();	i++ )
	{
		TStreamMeta Meta;
		mStreamMetas[i].GetMeta( Meta );
		Streams.PushBack( Meta );
	}
}



TProtocolState::Type TH264ES_Protocol::Decode(TStreamBuffer& IncomingBuffer)
{
	//	eat until we find the [first] NALU header
	while ( true )
	{
		//	this check is inside the loop, when parent clears out on shutdown, we end up with no data
		if ( IncomingBuffer.GetBufferedSize() < 5 )
			return TProtocolState::Waiting;
		
		Array<char> Chunk;
		auto EatLength = std::min<size_t>( 1024*2, IncomingBuffer.GetBufferedSize() );
		if ( !IncomingBuffer.Pop( EatLength, GetArrayBridge(Chunk) ) )
		{
			IncomingBuffer.UnPop( GetArrayBridge( Chunk ) );
			return TProtocolState::Waiting;
		}
		
		//	look for start of nalu
		ssize_t PacketNaluStart = -1;
		size_t PacketDataStart = 0;
		{
			size_t NaluSize = 0;
			size_t HeaderSize = 0;
			auto Chunk8 = GetRemoteArray( reinterpret_cast<const uint8*>(Chunk.GetArray()), Chunk.GetDataSize() );
			PacketNaluStart = H264::FindNaluStartIndex( GetArrayBridge(Chunk8), NaluSize, HeaderSize );
			PacketDataStart = PacketNaluStart + HeaderSize;
		}
		
		//	looking for first nalu
		if ( !mPacket )
		{
			if ( PacketNaluStart < 0 )
			{
				//	discard all data and carry on
				//std::Debug << "TH264ES_Protocol: Discarding " << Soy::FormatSizeBytes(Chunk.GetDataSize()) << std::endl;
				continue;
			}
			else
			{
				//	start of NALU, remove garbage before NALU header
				//std::Debug << "TH264ES_Protocol: Found start of NALU, discarding " << Soy::FormatSizeBytes(NaluStart) << std::endl;
				Chunk.RemoveBlock( 0, PacketNaluStart );
	
				//	re-search for next nalu (offset from the start or we'll just match it again)
				//	gr: be careful, SPS is only 6/7 bytes including the nalu header
				auto SearchOffset = PacketDataStart - size_cast<ssize_t>(PacketNaluStart);
				if ( SearchOffset > Chunk.GetDataSize() )
					Soy::Assert( SearchOffset <= Chunk.GetDataSize(), "Search offset too far into chunk");
				size_t NaluSize = 0;
				auto Chunk8 = GetRemoteArray( reinterpret_cast<const uint8*>(Chunk.GetArray()+SearchOffset), Chunk.GetDataSize()-SearchOffset );
				size_t HeaderSize;
				PacketNaluStart = H264::FindNaluStartIndex( GetArrayBridge(Chunk8), NaluSize, HeaderSize );
				
				if ( SearchOffset == 0 )
					Soy::Assert( PacketNaluStart!=0, "Found nal in same place again" );

				if ( PacketNaluStart >= 0 )
					PacketNaluStart += SearchOffset;
				
				mPacket.reset( new TMediaPacket() );
				mPacket->mMeta.mCodec = SoyMediaFormat::H264_ES;
			}
		}
		
		Soy::Assert( mPacket != nullptr, "Should have a packet allocated here");
		
		//	not found next packet yet, store the data
		if ( PacketNaluStart < 0 )
		{
			mPacket->mData.PushBackArray( Chunk );
			continue;
		}
		
		//	save the packet, unpop the following data
		auto ThisPacket = GetRemoteArray( Chunk.GetArray(), PacketNaluStart );
		auto NextPacket = GetRemoteArray( &Chunk[PacketNaluStart], Chunk.GetSize()-PacketNaluStart );
		//std::Debug << "TH264ES_Protocol: Found NALU packet (" << Soy::FormatSizeBytes(ThisPacket.GetDataSize()) << ") unpopping " << Soy::FormatSizeBytes(NextPacket.GetDataSize()) << std::endl;
		mPacket->mData.PushBackArray( GetArrayBridge( ThisPacket ) );

		//	get the real format of the data (detect SPS/PPS)
		H264::ResolveH264Format( mPacket->mMeta.mCodec, GetArrayBridge(mPacket->mData) );
		
		IncomingBuffer.UnPop( GetArrayBridge( NextPacket ) );
		break;
	}
	
	return TProtocolState::Finished;
}

void TH264StreamMeta::GetMeta(TStreamMeta& Meta)
{
	Meta.mCodec = mCodec;
	
	if ( mSpsPacket )
	{
		Meta.mSps.Copy( mSpsPacket->mData );
		static bool KeepNaluByte = true;	//	avf compressor format generation requires this
		H264::RemoveHeader( mSpsPacket->mMeta.mCodec, GetArrayBridge(Meta.mSps), KeepNaluByte );
		
		auto SpsNoNaluByte = GetRemoteArray( Meta.mSps.GetArray()+(KeepNaluByte), Meta.mSps.GetDataSize()-KeepNaluByte );
		auto Params = H264::ParseSps( GetArrayBridge(SpsNoNaluByte) );
		Meta.mPixelMeta = SoyPixelsMeta( Params.mWidth, Params.mHeight, SoyPixelsFormat::Invalid );
	}

	if ( mPpsPacket )
	{
		Meta.mPps.Copy( mPpsPacket->mData );
		static bool KeepNaluByte = true;	//	avf compressor format generation requires this
		H264::RemoveHeader( mPpsPacket->mMeta.mCodec, GetArrayBridge(Meta.mPps), KeepNaluByte );
	}
}



TProtocolState::Type TSrtMediaPacket_Protocol::Decode(TStreamBuffer& Buffer)
{
	auto Result = mSrt.Decode( Buffer );
	if ( Result != TProtocolState::Finished )
		return Result;
	
	//	turn into packet
	mPacket.reset( new TMediaPacket() );
	mPacket->mTimecode = mSrt.mStart;
	mPacket->mDuration = mSrt.mEnd - mSrt.mStart;
	mPacket->mMeta.mCodec = SoyMediaFormat::Subtitle;
	
	Soy::StringToArray( mSrt.mString, GetArrayBridge( mPacket->mData ) );
	
	return TProtocolState::Finished;
}


TStreamMeta TSrtMediaPacket_Protocol::GetStreamMeta()
{
	TStreamMeta Meta;
	Meta.mCodec = SoyMediaFormat::Subtitle;
	return Meta;
}


std::shared_ptr<TMediaPacket> DecodeImagePacket(std::function<void(SoyPixelsImpl&,TStreamBuffer&)> ReadFunction,TStreamBuffer& Buffer)
{
	auto pPacket = std::make_shared<TMediaPacket>();
	auto& Packet = *pPacket;
	SoyPixelsBridge<Array<uint8>> Pixels( Packet.mData, Packet.mMeta.mPixelMeta );
	
	ReadFunction( Pixels, Buffer );
	Soy::Assert( Pixels.GetMeta().IsValid(), "Expected valiid pixel meta after successfull image read");
	Soy::Assert( Packet.mMeta.mPixelMeta.IsValid(), "Expected valiid pixel meta after successfull image read");
		
	//	finish packet
	Packet.mMeta.mCodec = SoyMediaFormat::FromPixelFormat( Pixels.GetFormat() );
		
	//	make all images 1ms long, 0=unknown, but we know these finish.
	Packet.mDuration = SoyTime( std::chrono::milliseconds(1) );
	Packet.mIsKeyFrame = true;
	//	gr: check this doesn't screw up the system.
	//		we want it here though so stream can determine that Duration+eof = total duration
	Packet.mEof = true;
	
	return pPacket;
}


TProtocolState::Type TImageFileMediaPacket_ProtocolImpl::Decode(TStreamBuffer& Buffer)
{
	Soy::Assert( mReadFunction!=nullptr, "TImageFileMediaPacket_ProtocolImpl Expected read function");

	std::string ExceptionError;
	try
	{
		mPacket = DecodeImagePacket( mReadFunction, Buffer );
		return TProtocolState::Finished;
	}
	catch (std::exception& e)
	{
		ExceptionError = e.what();
	}
	catch(...)
	{
		ExceptionError = "Unknown exception";
	}
	
	static size_t MaxTriesBeforeError = 1000;
	mTries++;
	//	aftrer a certain amount of tries, report a failure rather than waiting for data
	//	we should only be woken whenever the stream has MORE data so it's not like we'll spin on tries for too long (depending on the size of the file...)
	if ( mTries > MaxTriesBeforeError )
	{
		std::stringstream Error;
		Error << "Too many tries (" << mTries << "/" << MaxTriesBeforeError << ") trying to read image; " << ExceptionError;
		throw Soy::AssertException( Error.str() );
	}

	//	assume there's not enough data yet
	std::Debug << "Assuming not enough data yet to read image file... (" << mTries << "/" << MaxTriesBeforeError << "); Error=" << ExceptionError << std::endl;
	return TProtocolState::Waiting;
}
