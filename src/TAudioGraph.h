#pragma once

#if defined(ENABLE_OPENGL)
#include <SoyOpenglContext.h>
#endif
#include <mutex>
#include <heaparray.hpp>

namespace Opengl
{
	class TAudioRenderer;
	class TContext;
	class TTexture;
	class TShader;
	class TRenderTarget;
	class TRenderTargetFbo;
	class TGeometry;
}
namespace Directx
{
	class TAudioRenderer;
	class TContext;
	class TTexture;
	class TRenderTarget;
	class TGeometry;
	class TShader;
}

class TAudioRendererBase
{
public:
	virtual ~TAudioRendererBase()	{}

	std::mutex							mRenderLock;	//	not sure this is required (due to use of atomic shared ptr's) but tracking down crash
};

class TAudioGraphData
{
public:
	TAudioGraphData() :
		mCurrentTime	( -1 )
	{
	}
	
	Array<float>	mSamples;		//	will be padded out up to mCurrentTime's index
	int				mCurrentTime;
};

#if defined(ENABLE_OPENGL)
class Opengl::TAudioRenderer : public TAudioRendererBase
{
public:
	TAudioRenderer(std::shared_ptr<TContext> Context);
	~TAudioRenderer();

	void	Render(TTexture& Target,const TAudioGraphData& AudioData);
	
	TTexture&	GetAudioDataTexture(const ArrayBridge<float>&& SampleCount);		//	re/alloc texture for this data
	
	std::shared_ptr<TRenderTarget>		GetRenderTarget(TTexture& Target);
	std::shared_ptr<TGeometry>			GetGeo();
	std::shared_ptr<TShader>			GetShader();
	
	std::shared_ptr<TContext>			mContext;
	std::shared_ptr<TGeometry>			mGeo;
	std::shared_ptr<TShader>			mShader;
	std::shared_ptr<TRenderTargetFbo>	mRenderTarget;

	std::shared_ptr<TTexture>			mAudioDataTexture;
	Array<uint8>						mAudioPixelsDataBuffer;

};
#endif

#if defined(ENABLE_DIRECTX)
class Directx::TAudioRenderer : public TAudioRendererBase
{
public:
	TAudioRenderer(std::shared_ptr<TContext> Context);
	~TAudioRenderer();

	void	Render(TTexture& Target);

	std::shared_ptr<TRenderTarget>		GetRenderTarget(TTexture& Target);
	std::shared_ptr<TGeometry>			GetGeo();
	std::shared_ptr<TShader>			GetShader();
	
	std::shared_ptr<TContext>			mContext;
	std::shared_ptr<TGeometry>			mGeo;
	std::shared_ptr<TShader>			mShader;
	std::shared_ptr<TRenderTarget>		mRenderTarget;
};
#endif


