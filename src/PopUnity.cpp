#include "PopUnity.h"
#include "TSourceManager.h"
#include <SoyJson.h>

#if defined(ENABLE_GNM)
#include "SoyGnm.h"
#endif

#if defined(ENABLE_METAL)
#include <SoyMetal.h>
#endif

#if defined(ENABLE_DIRECTX9)
#include <SoyDirectx9.h>
#endif

namespace Unity
{
	template<class TTEXTURE>
	TTEXTURE			GetTexture(NativeTexturePtr TexturePtr);
	template<class TTEXTURE>
	TTEXTURE			GetTexture(NativeTexturePtr TexturePtr,const SoyPixelsMeta& Meta)
	{
		return GetTexture<TTEXTURE>( TexturePtr );
	}
}

#if defined(ENABLE_OPENGL)
template<>
Opengl::TTexture Unity::GetTexture(Unity::NativeTexturePtr TexturePtr,const SoyPixelsMeta& Meta)	
{
	GLenum Type = GL_TEXTURE_2D;
	return Opengl::TTexture( TexturePtr, Meta, Type );
}
#endif

#if defined(ENABLE_DIRECTX)
template<>
Directx::TTexture Unity::GetTexture(Unity::NativeTexturePtr TexturePtr)	
{
	return Directx::TTexture( static_cast<ID3D11Texture2D*>(TexturePtr) );
}
#endif

#if defined(ENABLE_DIRECTX9)
template<>
Directx9::TTexture Unity::GetTexture(Unity::NativeTexturePtr TexturePtr)	
{
	return Directx9::TTexture( static_cast<IDirect3DTexture9*>(TexturePtr) );
}
#endif

#if defined(ENABLE_GNM)
template<>
Gnm::TTexture Unity::GetTexture(Unity::NativeTexturePtr TexturePtr)	
{
	return Gnm::TTexture( TexturePtr );
}
#endif

#if defined(ENABLE_METAL)
template<>
Metal::TTexture Unity::GetTexture(Unity::NativeTexturePtr TexturePtr)	
{
	return Metal::TTexture( TexturePtr );
}
#endif


template<class TTEXTURE,class TJOB,class TCONTEXT,typename ... ARGS>
void PushJob(std::shared_ptr<PopMovie::TInstance>& Instance,Unity::NativeTexturePtr TextureId,const SoyPixelsMeta& Meta,TCONTEXT& Context,ARGS ... Args)
{
	auto Texture = Unity::GetTexture<TTEXTURE>( TextureId, Meta );
	std::shared_ptr<PopWorker::TJob> Job( new TJOB( Instance, Texture, Args... ) );
	Context.PushJob( Job );
}


template<class TJOB,typename ... ARGS>
void PushPlatformJob(std::shared_ptr<PopMovie::TInstance>& Instance,Unity::NativeTexturePtr TextureId,const SoyPixelsMeta& Meta,ARGS ... Args)
{
#if defined(ENABLE_OPENGL)
	if ( Unity::HasOpenglContext() )
	{
		PushJob<Opengl::TTexture,TJOB>( Instance, TextureId, Meta, Unity::GetOpenglContext(), Args... );
		return;
	}
#endif

#if defined(ENABLE_DIRECTX)
	if ( Unity::HasDirectxContext() )
	{
		PushJob<Directx::TTexture,TJOB>( Instance, TextureId, Meta, Unity::GetDirectxContext(), Args...  );
		return;
	}
#endif

#if defined(ENABLE_DIRECTX9)
	if ( Unity::HasDirectx9Context() )
	{
		PushJob<Directx9::TTexture,TJOB>( Instance, TextureId, Meta, Unity::GetDirectx9Context(), Args...  );
		return;
	}
#endif

#if defined(ENABLE_GNM)
	if ( Unity::HasGnmContext() )
	{
		PushJob<Gnm::TTexture,TJOB>( Instance, TextureId, Meta, Unity::GetGnmContext(), Args...  );
		return;
	}
#endif

#if defined(ENABLE_METAL)
	if ( Unity::HasMetalContext() )
	{
		PushJob<Metal::TTexture,TJOB>( Instance, TextureId, Meta, Unity::GetMetalContext(), Args...  );
		return;
	}
#endif

	throw Soy::AssertException("No graphics context");
}


template<typename TTEXTURE>
class TPlatformTexture
{
public:
	TPlatformTexture()
	{
	}
	TPlatformTexture(TTEXTURE& Texture) :
		mTexture	( Texture )
	{
	}

	bool			operator==(const TTEXTURE& Texture) const	{	return mTexture == Texture;	}
	bool			operator!=(const TTEXTURE& Texture) const	{	return !(mTexture == Texture);	}

protected:
	TTEXTURE		mTexture;
};

class TDummy
{
};

class TPlatformTextureContainer : 
#if defined(ENABLE_OPENGL)
	public TPlatformTexture<Opengl::TTexture>,
#endif
#if defined(ENABLE_DIRECTX)
	public TPlatformTexture<Directx::TTexture>,
#endif
#if defined(ENABLE_DIRECTX9)
	public TPlatformTexture<Directx9::TTexture>,
#endif
#if defined(ENABLE_GNM)
	public TPlatformTexture<Gnm::TTexture>,
#endif
#if defined(ENABLE_METAL)
	public TPlatformTexture<Metal::TTexture>,
#endif
	public TDummy
{
public:
#if defined(ENABLE_OPENGL)
	TPlatformTextureContainer(Opengl::TTexture& Texture) :	TPlatformTexture<Opengl::TTexture>	( Texture )	{}
#endif
#if defined(ENABLE_DIRECTX)
	TPlatformTextureContainer(Directx::TTexture& Texture) :	TPlatformTexture<Directx::TTexture>	( Texture )	{}
#endif
#if defined(ENABLE_DIRECTX9)
	TPlatformTextureContainer(Directx9::TTexture& Texture) :	TPlatformTexture<Directx9::TTexture>	( Texture )	{}
#endif
#if defined(ENABLE_GNM)
	TPlatformTextureContainer(Gnm::TTexture& Texture) :	TPlatformTexture<Gnm::TTexture>	( Texture )	{}
#endif
#if defined(ENABLE_METAL)
	TPlatformTextureContainer(Metal::TTexture& Texture) :	TPlatformTexture<Metal::TTexture>	( Texture )	{}
#endif

	template<typename TTEXTURE>
	TTEXTURE&	GetTexture()	{	return TPlatformTexture<TTEXTURE>::mTexture;	}
};


class TDefferedCopy : public PopWorker::TJob, public TPlatformTextureContainer
{
public:
	template<class TTEXTURE>
	TDefferedCopy(std::shared_ptr<PopMovie::TInstance>& Instance,TTEXTURE& Texture,size_t StreamIndex) :
		mInstance		( Instance ),
		mStreamIndex	( StreamIndex ),
		TPlatformTextureContainer	( Texture )
	{
		auto& pDecoder = Instance->mDecoder;
		if ( pDecoder )
			if ( pDecoder->mParams.mCopyFrameAtUpdate )
				mCopyTimestamp = Instance->mTimestamp;
	}
	
	virtual void		Run() override
	{
		if ( !mInstance )
			return;
		
#if defined(ENABLE_OPENGL)
		if ( GetTexture<Opengl::TTexture>().IsValid() )
			mInstance->UpdateTexture( GetTexture<Opengl::TTexture>(), mStreamIndex, mCopyTimestamp );
#endif

#if defined(ENABLE_DIRECTX)
		if ( GetTexture<Directx::TTexture>().IsValid() )
			mInstance->UpdateTexture( GetTexture<Directx::TTexture>(), mStreamIndex, mCopyTimestamp );
#endif

#if defined(ENABLE_DIRECTX9)
		if ( GetTexture<Directx9::TTexture>().IsValid() )
			mInstance->UpdateTexture( GetTexture<Directx9::TTexture>(), mStreamIndex, mCopyTimestamp );
#endif

#if defined(ENABLE_GNM)
		if ( GetTexture<Gnm::TTexture>().IsValid() )
			mInstance->UpdateTexture( GetTexture<Gnm::TTexture>(), mStreamIndex, mCopyTimestamp );
#endif

#if defined(ENABLE_METAL)
		if ( GetTexture<Metal::TTexture>().IsValid() )
			mInstance->UpdateTexture( GetTexture<Metal::TTexture>(), mStreamIndex, mCopyTimestamp );
#endif
		
		//	gr: throw here if no work done?
	}

	std::shared_ptr<PopMovie::TInstance>	mInstance;
	size_t					mStreamIndex;
	SoyTime					mCopyTimestamp;

};


class TDefferedUpdatePerformanceGraph : public PopWorker::TJob, public TPlatformTextureContainer
{
public:
	template<class TTEXTURE>
	TDefferedUpdatePerformanceGraph(std::shared_ptr<PopMovie::TInstance>& Instance,TTEXTURE& Texture,size_t StreamIndex,bool LocalisedGraph) :
		mInstance		( Instance ),
		TPlatformTextureContainer	( Texture ),
		mStreamIndex	( StreamIndex ),
		mLocalisedGraph	( LocalisedGraph )
	{
	}
	
	virtual void		Run() override
	{
		if ( !mInstance )
			return;
		
#if defined(ENABLE_OPENGL)	
		if ( GetTexture<Opengl::TTexture>().IsValid() )
			mInstance->UpdatePerformanceGraphTexture( GetTexture<Opengl::TTexture>(), mStreamIndex, mLocalisedGraph );
#endif

#if defined(ENABLE_DIRECTX)
		if ( GetTexture<Directx::TTexture>().IsValid() )
			mInstance->UpdatePerformanceGraphTexture( GetTexture<Directx::TTexture>(), mStreamIndex, mLocalisedGraph );
#endif
		
#if defined(ENABLE_GNM)
		if ( GetTexture<Gnm::TTexture>().IsValid() )
			mInstance->UpdatePerformanceGraphTexture( GetTexture<Gnm::TTexture>(), mStreamIndex, mLocalisedGraph );
#endif
		
#if defined(ENABLE_METAL)
		if ( GetTexture<Metal::TTexture>().IsValid() )
			mInstance->UpdatePerformanceGraphTexture( GetTexture<Metal::TTexture>(), mStreamIndex, mLocalisedGraph );
#endif
	}
	
	bool									mLocalisedGraph;
	std::shared_ptr<PopMovie::TInstance>	mInstance;
	size_t									mStreamIndex;
};


class TDefferedUpdateMemoryGraph : public PopWorker::TJob, public TPlatformTextureContainer
{
public:
	template<class TTEXTURE>
	TDefferedUpdateMemoryGraph(std::shared_ptr<PopMovie::TInstance>& Instance,TTEXTURE& Texture) :
		mInstance					( Instance ),
		TPlatformTextureContainer	( Texture )
	{
	}
	
	virtual void		Run() override
	{
		if ( !mInstance )
			return;

#if defined(ENABLE_OPENGL)
		if ( GetTexture<Opengl::TTexture>().IsValid() )
			mInstance->UpdateMemoryGraphTexture( GetTexture<Opengl::TTexture>() );
#endif

#if defined(ENABLE_DIRECTX)
		 if ( GetTexture<Directx::TTexture>().IsValid() )
			mInstance->UpdateMemoryGraphTexture( GetTexture<Directx::TTexture>() );
#endif
		
#if defined(ENABLE_GNM)
		if ( GetTexture<Gnm::TTexture>().IsValid() )
			mInstance->UpdateMemoryGraphTexture( GetTexture<Gnm::TTexture>() );
#endif
		
#if defined(ENABLE_METAL)
		if ( GetTexture<Metal::TTexture>().IsValid() )
			mInstance->UpdateMemoryGraphTexture( GetTexture<Metal::TTexture>() );
#endif
	}
	
public:
	std::shared_ptr<PopMovie::TInstance>	mInstance;
};


class TDefferedUpdateAudioTexture : public PopWorker::TJob, public TPlatformTextureContainer
{
public:
	template<class TTEXTURE>
	TDefferedUpdateAudioTexture(std::shared_ptr<PopMovie::TInstance>& Instance,TTEXTURE& Texture,size_t StreamIndex) :
		mInstance					( Instance ),
		TPlatformTextureContainer	( Texture ),
		mStreamIndex				( StreamIndex )
	{
	}
	
	virtual void		Run() override
	{
		if ( !mInstance )
			return;
		
#if defined(ENABLE_OPENGL)
		if ( GetTexture<Opengl::TTexture>().IsValid() )
			mInstance->UpdateAudioTexture( GetTexture<Opengl::TTexture>(), mStreamIndex );
#endif

#if defined(ENABLE_DIRECTX)
		if ( GetTexture<Directx::TTexture>().IsValid() )
			mInstance->UpdateAudioTexture( GetTexture<Directx::TTexture>(), mStreamIndex );
#endif
		
#if defined(ENABLE_GNM)
		if ( GetTexture<Gnm::TTexture>().IsValid() )
			mInstance->UpdateAudioTexture( GetTexture<Gnm::TTexture>(), mStreamIndex );
#endif
		
#if defined(ENABLE_METAL)
		if ( GetTexture<Metal::TTexture>().IsValid() )
			mInstance->UpdateAudioTexture( GetTexture<Metal::TTexture>(), mStreamIndex );
#endif
	}
	
	std::shared_ptr<PopMovie::TInstance>	mInstance;
	size_t									mStreamIndex;
};




bool HasBit(Unity::uint ParamBits,TPluginParams::PopMovieFlags Param)
{
	auto Masked = (ParamBits & Param);
	return Masked != 0;
}
bool HasBit(Unity::uint ParamBits,TPluginParams::PopMovieFlags2 Param)
{
	auto Masked = (ParamBits & Param);
	return Masked != 0;
}

TVideoDecoderParams MakeVideoDecoderParams(Unity::uint ParamBits,Unity::uint ParamBits2,Unity::uint PreSeekMs,Unity::uint AudioSampleRate,Unity::uint AudioChannelCount,const char* Filename)
{
	//	override params
	static bool ForceDebugParams = false;
	if ( ForceDebugParams )
	{
		ParamBits |= TPluginParams::DebugFrameSkipping;
		ParamBits |= TPluginParams::DebugNoNewPixelBuffer;
		ParamBits |= TPluginParams::DebugRenderThreadCallback;
	}
	
	//	global params
	Unity::mDebugPluginEvent = HasBit( ParamBits, TPluginParams::DebugRenderThreadCallback);
	Unity::mMinTimerMs = HasBit( ParamBits, TPluginParams::DebugTimers ) ? 1 : 9;
	Unity::mVerboseUpdateFunctions = HasBit( ParamBits, TPluginParams::VerboseDebug );

	//	decoder params
	TVideoDecoderParams Params;
	
	Params.mFilename = Filename;
	Params.mPixelBufferParams.mPreSeek = SoyTime( std::chrono::milliseconds(PreSeekMs) );
	Params.mPixelBufferParams.mAudioSampleRate = AudioSampleRate;
	Params.mPixelBufferParams.mAudioChannelCount = AudioChannelCount;

	Params.mPixelBufferParams.mAllowPushRejection = HasBit( ParamBits, TPluginParams::SkipPushFrames);
	Params.mPixelBufferParams.mPopFrameSync = HasBit( ParamBits, TPluginParams::SkipPopFrames);
	Params.mAllowSlowCopy = HasBit( ParamBits, TPluginParams::AllowSlowCopy);
	Params.mAllowFastCopy = HasBit( ParamBits, TPluginParams::AllowFastCopy);
	Params.mResetInternalTimestamp = HasBit( ParamBits, TPluginParams::ResetInternalTimestamp);
	Params.mApplyVideoTransform = HasBit( ParamBits, TPluginParams::ApplyVideoTransform);
	Params.mPeekBeforeDefferedCopy = HasBit( ParamBits, TPluginParams::PeekBeforeDefferedCopy);
	Params.mPixelBufferParams.mPopNearestFrame = HasBit( ParamBits, TPluginParams::PopNearestFrame );
	
	Params.mDebugBlit = HasBit( ParamBits, TPluginParams::DebugBlit);
	Params.mPixelBufferParams.mDebugFrameSkipping = HasBit( ParamBits, TPluginParams::DebugFrameSkipping);
	Params.mDebugNoNewPixelBuffer = HasBit( ParamBits, TPluginParams::DebugNoNewPixelBuffer);
	
	Params.mDecoderUseHardwareBuffer = HasBit( ParamBits, TPluginParams::DecoderUseHardwareBuffer );
	Params.mBlitFatalErrors = HasBit( ParamBits, TPluginParams::BlitFatalErrors );
	Params.mForceNonPlanarOutput = HasBit( ParamBits, TPluginParams::ForceNonPlanarOutput );

	Params.mBlitMergeYuv = HasBit( ParamBits, TPluginParams::BlitMergeYuv );
	Params.mClearBeforeBlit = HasBit( ParamBits, TPluginParams::ClearBeforeBlit );
	Params.mExtractAudioStreams = HasBit( ParamBits, TPluginParams::EnableAudioStreams );
	Params.mExtractVideoStreams = HasBit( ParamBits2, TPluginParams::EnableVideoStreams );
	Params.mExtractDepthStreams = HasBit( ParamBits2, TPluginParams::EnableDepthStreams );
	Params.mExtractSkeletonStreams = HasBit( ParamBits2, TPluginParams::EnableSkeletonStreams );
	Params.mExtractAlpha = HasBit( ParamBits2, TPluginParams::EnableAlpha );
	Params.mCopyFrameAtUpdate = HasBit( ParamBits, TPluginParams::CopyFrameAtUpdateTime );
	Params.mOnlyExtractKeyframes = HasBit( ParamBits, TPluginParams::OnlyExtractKeyframes );
	Params.mApplyHeightPadding = HasBit( ParamBits, TPluginParams::ApplyHeightPadding );
	Params.mApplyWidthPadding = HasBit( ParamBits2, TPluginParams::ApplyWidthPadding );
	Params.mWindowIncludeBorders = HasBit( ParamBits, TPluginParams::WindowIncludeBorders );
	Params.mVerboseDebug = HasBit( ParamBits, TPluginParams::VerboseDebug );
	Params.mWin7Emulation = HasBit( ParamBits, TPluginParams::Win7Emulation );
	Params.mSplitAudioChannelsIntoStreams = HasBit( ParamBits, TPluginParams::SplitAudioChannelsIntoStreams );
	Params.mSplitVideoPlanesIntoStreams = HasBit( ParamBits2, TPluginParams::SplitVideoPlanesIntoStreams );
	Params.mLiveUseClockTime = HasBit( ParamBits2, TPluginParams::UseLiveTimestampsForStreaming );
	Params.mEnableDecoderThreading = HasBit( ParamBits2, TPluginParams::EnableDecoderThreading );
	Params.mCopyBuffersInExtraction = HasBit( ParamBits2, TPluginParams::CopyBuffersInExtraction );
	Params.mExtractorPreDecodeSkip = HasBit( ParamBits2, TPluginParams::ExtractorPreDecodeSkip );
	Params.mAndroidSingleBufferMode = HasBit( ParamBits2, TPluginParams::AndroidSingleBufferMode );
	Params.mDrawTimestamp = HasBit( ParamBits2, TPluginParams::DrawTimestamp );
	
	Params.mTextureUploadParams.mAllowOpenglConversion = HasBit( ParamBits, TPluginParams::AllowGpuColourConversion);
	Params.mTextureUploadParams.mAllowCpuConversion = HasBit( ParamBits, TPluginParams::AllowCpuColourConversion);
	Params.mTextureUploadParams.mAllowClientStorage = HasBit( ParamBits, TPluginParams::PixelClientStorage);
	Params.mTextureUploadParams.mGenerateMipMaps = HasBit( ParamBits, TPluginParams::GenerateMipMaps );
	Params.mTextureUploadParams.mStretch = HasBit( ParamBits, TPluginParams::StretchImage );
	
	{
		bool ForceYuvFull = HasBit( ParamBits2, TPluginParams::ForceYuvRangeFull );
		bool ForceYuvNtsc = HasBit( ParamBits2, TPluginParams::ForceYuvRangeNtsc );
		bool ForceYuvSmptec = HasBit( ParamBits2, TPluginParams::ForceYuvRangeSmptec );
		int ForceCount = ForceYuvFull + ForceYuvNtsc + ForceYuvSmptec;

		if ( ForceCount > 1 )
		{
			std::Debug << "Warning, conflicting options; ";
			if ( ForceYuvFull )
				std::Debug << "ForceYuvRangeFull ";
			if ( ForceYuvNtsc )
				std::Debug << "ForceYuvNtsc ";
			if ( ForceYuvSmptec )
				std::Debug << "ForceYuvSmptec ";
			std::Debug << std::endl;
		}
		
		if ( ForceYuvFull )
			Params.mForceYuvColourFormat = SoyPixelsFormat::Luma_Full;
		if ( ForceYuvNtsc )
			Params.mForceYuvColourFormat = SoyPixelsFormat::Luma_Ntsc;
		if ( ForceYuvSmptec )
			Params.mForceYuvColourFormat = SoyPixelsFormat::Luma_Smptec;
	}


#if defined(TARGET_WINDOWS) && defined(ENABLE_CUDA)
	Params.mAllowFastCopy = false;
	Params.mAllowSlowCopy = true;
	Params.mBufferFrameSize = 1000;
#endif

	//	memory constrained a bit more on ios. ES2 crashes with 10
	//	(though ES3 doesn't as much, I'm sure this is a co-inciedence as paging is surely independent from gfx)
#if defined(TARGET_IOS)
	Params.mPixelBufferParams.mMaxBufferSize = 5;
#endif


	//	some warnings
	if ( Params.mForceNonPlanarOutput && Params.mSplitVideoPlanesIntoStreams )
	{
		std::Debug << "Warning, conflicting options; ForceNonPlanarOutput and SplitVideoPlanesIntoStreams" << std::endl;
	}
	
	return Params;
}


template<class TTEXTURE,class TCONTEXT>
void TryUpdateTexture(std::shared_ptr<PopMovie::TInstance> pInstance,TVideoDecoder& Decoder,Unity::NativeTexturePtr TextureId,const SoyPixelsMeta& Meta,size_t VideoStreamIndex,TTEXTURE& Texture,TCONTEXT& Context)
{
	auto& Instance = *pInstance;

	auto StreamIndex = Decoder.GetVideoStreamIndex( VideoStreamIndex );

	//	immediate copy
	if ( Context.HasMultithreadAccess() )
	{
		Instance.UpdateTexture( Texture, StreamIndex, SoyTime() );
		return;
	}

	bool PeekBeforeDefferedCopy = Decoder.mParams.mPeekBeforeDefferedCopy;
	if ( PeekBeforeDefferedCopy )
		if ( !pInstance->HasNewPixelBuffer(StreamIndex) )
			return;

	//	re-use job when possible
	auto& DefferedCopyJob = Instance.mDefferedCopyJobs[StreamIndex];
	if ( DefferedCopyJob )
	{
		auto& DefferedCopyJob_ = static_cast<TDefferedCopy&>( *DefferedCopyJob );

		//	texture has changed, make a new job
		if ( DefferedCopyJob_ != Texture )
			DefferedCopyJob.reset();
	}

	if ( !DefferedCopyJob )
		DefferedCopyJob.reset( new TDefferedCopy( pInstance, Texture, StreamIndex ) );

	Context.PushJob( DefferedCopyJob );
}

#if defined(ENABLE_OPENGL)
void TryUpdateOpenglTexture(std::shared_ptr<PopMovie::TInstance> pInstance,TVideoDecoder& Decoder,Unity::NativeTexturePtr TextureId,const SoyPixelsMeta& Meta,size_t VideoStreamIndex)
{
	auto Texture = Unity::GetTexture<Opengl::TTexture>( TextureId, Meta );
	auto& Context = Unity::GetOpenglContext();
	TryUpdateTexture( pInstance, Decoder, TextureId, Meta, VideoStreamIndex, Texture, Context );
}
#endif

#if defined(ENABLE_DIRECTX)
void TryUpdateDirectxTexture(std::shared_ptr<PopMovie::TInstance> pInstance,TVideoDecoder& Decoder,Unity::NativeTexturePtr TextureId,const SoyPixelsMeta& Meta,size_t VideoStreamIndex)
{
	auto Texture = Unity::GetTexture<Directx::TTexture>( TextureId );
	auto& Context = Unity::GetDirectxContext();
	TryUpdateTexture( pInstance, Decoder, TextureId, Meta, VideoStreamIndex, Texture, Context );
}
#endif

#if defined(ENABLE_DIRECTX9)
void TryUpdateDirectx9Texture(std::shared_ptr<PopMovie::TInstance> pInstance,TVideoDecoder& Decoder,Unity::NativeTexturePtr TextureId,const SoyPixelsMeta& Meta,size_t VideoStreamIndex)
{
	auto Texture = Unity::GetTexture<Directx9::TTexture>( TextureId );
	auto& Context = Unity::GetDirectx9Context();
	TryUpdateTexture( pInstance, Decoder, TextureId, Meta, VideoStreamIndex, Texture, Context );
}
#endif

#if defined(ENABLE_GNM)
void TryUpdateGnmTexture(std::shared_ptr<PopMovie::TInstance> pInstance,TVideoDecoder& Decoder,Unity::NativeTexturePtr TextureId,const SoyPixelsMeta& Meta,size_t VideoStreamIndex)
{
	auto Texture = Unity::GetTexture<Gnm::TTexture>( TextureId );
	auto& Context = Unity::GetGnmContext();
	TryUpdateTexture( pInstance, Decoder, TextureId, Meta, VideoStreamIndex, Texture, Context );
}
#endif

#if defined(ENABLE_METAL)
void TryUpdateMetalTexture(std::shared_ptr<PopMovie::TInstance> pInstance,TVideoDecoder& Decoder,Unity::NativeTexturePtr TextureId,const SoyPixelsMeta& Meta,size_t VideoStreamIndex)
{
	auto Texture = Unity::GetTexture<Metal::TTexture>( TextureId );
	auto& Context = Unity::GetMetalContext();
	TryUpdateTexture( pInstance, Decoder, TextureId, Meta, VideoStreamIndex, Texture, Context );
}
#endif


__export Unity::uint	PopMovie_Alloc(const char* Filename,Unity::uint ParamBits,Unity::uint ParamBits2,Unity::uint PreSeekMs,Unity::uint AudioSampleRate,Unity::uint AudioChannelCount,char* ErrorBuffer,Unity::uint ErrorBufferSize)
{
	try
	{
		ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	
		auto Params = MakeVideoDecoderParams( ParamBits, ParamBits2, PreSeekMs, AudioSampleRate, AudioChannelCount, Filename );
		std::Debug << "Plugin params: " << Params << ", MinTimerMs=" << Unity::mMinTimerMs << std::endl;
	
		auto Instance = PopMovie::Alloc( Params );
		if ( !Instance )
			return 0;
		
		return Instance->GetRef().GetInt64();
	}
	catch ( std::exception& e )
	{
		std::Debug << "Failed to allocate movie: " << e.what() << std::endl;
		Soy::StringToBuffer( e.what(), ErrorBuffer, ErrorBufferSize );
	}
	catch (...)
	{
		std::Debug << "Failed to allocate movie: Unknown exception in " << __func__ << std::endl;
		Soy::StringToBuffer( "Unknown exception", ErrorBuffer, ErrorBufferSize );
	}
	
	return 0;
}

__export bool	PopMovie_Free(Unity::uint Instance)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	return PopMovie::Free( PopMovie::TInstanceRef( Instance ) );
}

__export bool		PopMovie_SetTime(Unity::uint Instance,Unity::uint TimeMs)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
		return false;
	
	try
	{
		//	update timestamp
		pInstance->SetTimestamp( SoyTime( std::chrono::milliseconds(TimeMs) ) );
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
		return false;
	}
	
	return true;
}


__export Unity::uint PopMovie_GetDurationMs(Unity::uint Instance)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
		return 0;
	
	try
	{
		auto Meta = pInstance->GetMeta();
		auto DurationUl = size_cast<Unity::uint>( Meta.mDuration.mTime );
		return DurationUl;
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
		return 0;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
		return 0;
	}
}

__export Unity::uint PopMovie_GetLastFrameCopiedMs(Unity::uint Instance,Unity::sint VideoStreamIndex)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
		return 0;
	
	try
	{
		auto TimeUl = size_cast<Unity::uint>( pInstance->GetVideoPerformanceStats(VideoStreamIndex).mLastCopiedTime.mTime );
		return TimeUl;
	}
	catch ( std::exception& e )
	{
		if ( Unity::mVerboseUpdateFunctions )
			std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
		return 0;
	}
	catch (...)
	{
		if ( Unity::mVerboseUpdateFunctions )
			std::Debug << "Unknown exception in " << __func__ << std::endl;
		return 0;
	}
}


__export Unity::sint PopMovie_GetMetaRevision(Unity::uint Instance)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
		return -1;

	try
	{
		return pInstance->GetMetaRevision();
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
		return -1;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
		return -1;
	}
}

__export void PopMovie_RegisterOnTextureCopied(Unity::uint Instance,Unity::uint VideoStreamIndex,void* CallbackPtr)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
		return;

	try
	{
		auto Callback = reinterpret_cast<PopMovie::TOnFrameCopiedCallbackFunc>( CallbackPtr );
		return pInstance->RegisterOnTextureCopied( VideoStreamIndex, Callback );
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
	}
}


__export void PopMovie_UnregisterOnTextureCopied(Unity::uint Instance,Unity::uint VideoStreamIndex,void* CallbackPtr)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
		return;

	try
	{
		auto Callback = reinterpret_cast<PopMovie::TOnFrameCopiedCallbackFunc>( CallbackPtr );
		return pInstance->UnregisterOnTextureCopied( VideoStreamIndex, Callback );
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
	}
}


__export const char* PopMovie_GetMetaJson(Unity::uint Instance)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
		return nullptr;
	
	try
	{
		//	generate meta
		TJsonWriter Json;
		pInstance->GetMeta( Json );
		auto& StringManager = PopMovie::GetExportStringManager();
		
		auto* ExportString = StringManager.Lock( Json.GetString() );
		return ExportString;
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
		return nullptr;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
		return nullptr;
	}
}

__export void			PopMovie_ReleaseMetaJsonString(const char* String)
{
	try
	{
		auto& StringManager = PopMovie::GetExportStringManager();
		StringManager.Unlock( String );
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
	}
}



static bool PopMovie_UpdateTexture(Unity::uint Instance,Unity::NativeTexturePtr TextureId,SoyPixelsMeta Meta,size_t VideoStreamIndex)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
	{
		std::Debug << "Instance " << Instance << " not found" << std::endl;
		return false;
	}
	
	if ( !pInstance->mDecoder )
		return false;
	auto& Decoder = *pInstance->mDecoder;
	
	try
	{
#if defined(ENABLE_OPENGL)
		if ( Unity::HasOpenglContext() )
		{
			TryUpdateOpenglTexture( pInstance, Decoder, TextureId, Meta, VideoStreamIndex );
			return true;
		}
#endif
#if defined(ENABLE_DIRECTX)
		if ( Unity::HasDirectxContext() )
		{
			TryUpdateDirectxTexture( pInstance, Decoder, TextureId, Meta, VideoStreamIndex );
			return true;
		}
#endif
#if defined(ENABLE_DIRECTX9)
		if ( Unity::HasDirectx9Context() )
		{
			TryUpdateDirectx9Texture( pInstance, Decoder, TextureId, Meta, VideoStreamIndex );
			return true;
		}
#endif
#if defined(ENABLE_GNM)
		if ( Unity::HasGnmContext() )
		{
			TryUpdateGnmTexture( pInstance, Decoder, TextureId, Meta, VideoStreamIndex );
			return true;
		}
#endif
#if defined(ENABLE_METAL)
		if ( Unity::HasMetalContext() )
		{
			TryUpdateMetalTexture( pInstance, Decoder, TextureId, Meta, VideoStreamIndex );
			return true;
		}
#endif
		throw Soy::AssertException("No graphics context");
	}
	catch ( std::exception& e )
	{
		if ( Unity::mVerboseUpdateFunctions )
			std::Debug << "Exception in UpdateTexture: " << e.what() << std::endl;
		return false;
	}
	catch (...)
	{
		if ( Unity::mVerboseUpdateFunctions )
			std::Debug << "Unknown exception in UpdateTexture" << std::endl;
		return false;
	}
	
	return false;
}


__export bool PopMovie_UpdateTexture2D(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::Texture2DPixelFormat::Type PixelFormat,Unity::sint StreamIndex)
{
	SoyPixelsMeta Meta( Width, Height, Unity::GetPixelFormat( PixelFormat ) );
	return PopMovie_UpdateTexture( Instance, TextureId, Meta, size_cast<size_t>(StreamIndex) );
}

__export bool PopMovie_UpdateRenderTexture(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::RenderTexturePixelFormat::Type PixelFormat,Unity::sint StreamIndex)
{
	SoyPixelsMeta Meta( Width, Height, Unity::GetPixelFormat( PixelFormat ) );
	return PopMovie_UpdateTexture( Instance, TextureId, Meta, size_cast<size_t>(StreamIndex) );
}


static bool PopMovie_UpdatePerformanceGraphTexture(Unity::uint Instance,Unity::NativeTexturePtr TextureId,SoyPixelsMeta Meta,size_t StreamIndex,bool LocalisedGraph)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
	{
		std::Debug << __func__ << " Instance " << Instance << " not found" << std::endl;
		return false;
	}
	
	try
	{
		//	get type and return false if no such stream
		auto Format = pInstance->GetStreamFormat( StreamIndex );
		if ( Format == SoyMediaFormat::Invalid )
			return false;

		PushPlatformJob<TDefferedUpdatePerformanceGraph>( pInstance, TextureId, Meta, StreamIndex, LocalisedGraph );
		return true;
	}
	catch ( std::exception& e )
	{
		if ( Unity::mVerboseUpdateFunctions )
			std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
		return false;
	}
	catch (...)
	{
		if ( Unity::mVerboseUpdateFunctions )
			std::Debug << "Unknown exception in " << __func__ << std::endl;
		return false;
	}
}


__export void PopMovie_GetPerformanceStats(Unity::uint Instance,Unity::uint* DataBuffer,Unity::uint DataBufferSize)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );

	if ( !DataBuffer || DataBufferSize == 0 )
	{
		std::Debug << __func__ << " Null/" << DataBufferSize << " data buffer supplied." << std::endl;
		return;
	}
	
	auto Data = GetRemoteArray( DataBuffer, DataBufferSize );
	PopMovie::TPerformanceStats::InitialiseBuffer( GetArrayBridge(Data) );
	
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
	{
		std::Debug << __func__ << " Instance " << Instance << " not found" << std::endl;
	}
	
	try
	{
		pInstance->UpdatePerformanceData( GetArrayBridge(Data) );
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
	}
}



static bool PopMovie_UpdateMemoryGraphTexture(Unity::uint Instance,Unity::NativeTexturePtr TextureId,SoyPixelsMeta Meta)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
	{
		std::Debug << __func__ << " Instance " << Instance << " not found" << std::endl;
		return false;
	}

	try
	{
		PushPlatformJob<TDefferedUpdateMemoryGraph>( pInstance, TextureId, Meta );
		return true;
	}
	catch ( std::exception& e )
	{
		if ( Unity::mVerboseUpdateFunctions )
			std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
		return false;
	}
	catch (...)
	{
		if ( Unity::mVerboseUpdateFunctions )
			std::Debug << "Unknown exception in " << __func__ << std::endl;
		return false;
	}
}


static bool PopMovie_UpdateAudioTexture(Unity::uint Instance,Unity::NativeTexturePtr TextureId,SoyPixelsMeta Meta,size_t AudioStreamIndex)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
	{
		std::Debug << __func__ << " Instance " << Instance << " not found" << std::endl;
		return false;
	}
	
	try
	{
		//	convert audio stream index to Real stream index
		auto StreamIndex = pInstance->ResolveAudioStreamIndex( AudioStreamIndex );

		PushPlatformJob<TDefferedUpdateAudioTexture>( pInstance, TextureId, Meta, StreamIndex );
		return true;
	}
	catch ( std::exception& e )
	{
		if ( Unity::mVerboseUpdateFunctions )
			std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
		return false;
	}
	catch (...)
	{
		if ( Unity::mVerboseUpdateFunctions )
			std::Debug << "Unknown exception in " << __func__ << std::endl;
		return false;
	}
}


__export void PopMovie_DebugMemoryStats()
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );

	//	don't throw plz
	try
	{
		Array<SoyMem::THeapMeta> Heaps;
		SoyMem::GetHeapMetas( GetArrayBridge( Heaps ) );

		std::stringstream out;
		out << "Memory heaps x" << Heaps.GetSize() << ": ";
		size_t TotalAlloc = 0;
		for ( int i=0;	i<Heaps.GetSize();	i++ )
		{
			auto& Heap = Heaps[i];
			out << Heap << " ";
			TotalAlloc += Heap.mAllocBytes;
		}
		out << "Total: " << Soy::FormatSizeBytes( TotalAlloc );
		std::Debug << out.str() << std::endl;
	}
	catch (std::exception& e)
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
	}
}


__export bool PopMovie_GetAudioBuffer(Unity::uint Instance,float* AudioBuffer,Unity::uint AudioBufferSize,Unity::uint Channels,Unity::uint SampleRate,Unity::uint StartTimeMs,Unity::sint StreamIndex)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
	{
		std::Debug << __func__ << " Instance " << Instance << " not found" << std::endl;
		return false;
	}

	auto AudioBufferArray = GetRemoteArray( AudioBuffer, AudioBufferSize );
	
	static bool TestMinimal = false;
	if ( TestMinimal )
	{
		GetArrayBridge(AudioBufferArray).SetAll(0);
		return true;
	}
	
	try
	{
		//	gr: dont let unity tell us what to get
		//SoyTime StartTime( static_cast<uint64>(StartTimeMs) );
		SoyTime StartTime;
		
		//	avoid alloc
		static TAudioBufferBlock AudioBlock;
		AudioBlock.mChannels = Channels;
		AudioBlock.mFrequency = SampleRate;
		AudioBlock.mStartTime = StartTime;
		AudioBlock.mData.SetSize(AudioBufferSize);
		pInstance->PopAudioBuffer( StreamIndex, AudioBlock );

		AudioBufferArray.Copy( AudioBlock.mData );

		return true;
	}
	catch ( std::exception& e )
	{
		GetArrayBridge(AudioBufferArray).SetAll(0);
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
		return false;
	}
	catch (...)
	{
		GetArrayBridge(AudioBufferArray).SetAll(0);
		std::Debug << "Unknown exception in " << __func__ << std::endl;
		return false;
	}
}

__export Unity::sint PopMovie_UpdateString(Unity::uint Instance,char* Buffer,Unity::uint BufferSize,Unity::boolean SkipOldData,Unity::uint TextStreamIndex)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
		return -1;
	
	try
	{
		std::stringstream Output;
		if ( !pInstance->PopText( Output, TextStreamIndex, bool_cast(SkipOldData) ) )
			return 0;
		
		auto OutputStr = Output.str();
		Unity::sint OrigLength = size_cast<Unity::sint>( OutputStr.length() );
		Soy::StringToBuffer( OutputStr, Buffer, BufferSize );
		return OrigLength;
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
		return -1;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
		return -1;
	}
}

__export const char*	PopMovie_GetFrameText(Unity::uint Instance, Unity::uint TimeMs, Unity::uint TextStreamIndex)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
		return nullptr;

	try
	{
		std::stringstream Output;
		SoyTime Time = SoyTime( std::chrono::milliseconds(TimeMs) );
		pInstance->GetText(Output, Time, TextStreamIndex);
		
		auto OutputStr = Output.str();

		//	generate meta
		auto& StringManager = PopMovie::GetExportStringManager();
		auto* ExportString = StringManager.Lock( OutputStr );
		return ExportString;
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
	}
	return nullptr;
}

__export void			PopMovie_ReleaseFrameText(const char* String)
{
	try
	{
		auto& StringManager = PopMovie::GetExportStringManager();
		StringManager.Unlock( String );
	}
	catch ( std::exception& e )
	{
		std::Debug << __func__ << " exception: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::Debug << __func__ << " unknown exception" << std::endl;
	}

}


__export bool PopMovie_UpdatePerformanceGraphTexture2D(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::Texture2DPixelFormat::Type PixelFormat,Unity::sint StreamIndex,Unity::boolean LocalisedGraph)
{
	SoyPixelsMeta Meta( Width, Height, Unity::GetPixelFormat( PixelFormat ) );
	return PopMovie_UpdatePerformanceGraphTexture( Instance, TextureId, Meta, StreamIndex, bool_cast(LocalisedGraph) );
}

__export bool PopMovie_UpdatePerformanceGraphRenderTexture(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::RenderTexturePixelFormat::Type PixelFormat,Unity::sint StreamIndex,Unity::boolean LocalisedGraph)
{
	SoyPixelsMeta Meta( Width, Height, Unity::GetPixelFormat( PixelFormat ) );
	return PopMovie_UpdatePerformanceGraphTexture( Instance, TextureId, Meta, StreamIndex, bool_cast(LocalisedGraph) );
}

__export bool PopMovie_UpdateMemoryGraphTexture2D(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::Texture2DPixelFormat::Type PixelFormat)
{
	SoyPixelsMeta Meta( Width, Height, Unity::GetPixelFormat( PixelFormat ) );
	return PopMovie_UpdateMemoryGraphTexture( Instance, TextureId, Meta );
}

__export bool PopMovie_UpdateMemoryGraphRenderTexture(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::RenderTexturePixelFormat::Type PixelFormat)
{
	SoyPixelsMeta Meta( Width, Height, Unity::GetPixelFormat( PixelFormat ) );
	return PopMovie_UpdateMemoryGraphTexture( Instance, TextureId, Meta );
}

__export bool PopMovie_UpdateAudioRenderTexture(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::RenderTexturePixelFormat::Type PixelFormat,Unity::sint AudioStreamIndex)
{
	SoyPixelsMeta Meta( Width, Height, Unity::GetPixelFormat( PixelFormat ) );
	return PopMovie_UpdateAudioTexture( Instance, TextureId, Meta, AudioStreamIndex );
}

__export bool PopMovie_UpdateAudioTexture2D(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::Texture2DPixelFormat::Type PixelFormat,Unity::sint AudioStreamIndex)
{
	SoyPixelsMeta Meta( Width, Height, Unity::GetPixelFormat( PixelFormat ) );
	return PopMovie_UpdateAudioTexture( Instance, TextureId, Meta, AudioStreamIndex );
}


__export Unity::sint PopMovie_GetPluginEventId()
{
	return Unity::GetPluginEventId();
}

bool Unity::IsDebugPluginEventEnabled()
{
	return mDebugPluginEvent;
}

int Unity::GetPluginEventId()
{
	return mEventId;
}


__export void			PopMovie_EnumSourceDirectory(char* Directory)
{
	try
	{
		auto& SourceManager = PopMovie::GetSourceManager();
		SourceManager.EnumDirectory( Directory );
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
	}
}

__export void PopMovie_EnumSourcesClear()
{
	auto pSourceManager = PopMovie::GetSourceManagerPtr();
	if ( !pSourceManager )
		return;
	
	try
	{
		pSourceManager->Clear();
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
	}
}

//	zero = no such file (eof)
//	-1 = error
__export Unity::sint	PopMovie_EnumSource(char* Buffer,Unity::uint BufferSize,Unity::uint Index)
{
	try
	{
		auto& SourceManager = PopMovie::GetSourceManager();

		std::string Filename;
		if ( !SourceManager.GetFilename( Index, Filename ) )
			return 0;

		Unity::sint OrigLength = size_cast<Unity::sint>( Filename.length() );
		Soy::StringToBuffer( Filename, Buffer, BufferSize );
		return OrigLength;
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
		return -1;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
		return -1;
	}
}




__export const char*	PopMovie_PopDebugString()
{
	try
	{
		return Unity::PopDebugString();
	}
	catch(std::exception& e)
	{
		//	gr: this could get a bit recursive
		std::Debug << __func__ << " exception " << e.what() << std::endl;
		return nullptr;
	}
}

__export void	PopMovie_ReleaseDebugString(const char* String)
{
	try
	{
		Unity::ReleaseDebugString( String );
	}
	catch(std::exception& e)
	{
		//	gr: this could get a bit recursive
		std::Debug << __func__ << " exception " << e.what() << std::endl;
	}
}

__export void			PopMovie_ReleaseAllExports()
{
	try
	{
		Unity::ReleaseDebugStringAll();
	}
	catch(std::exception& e)
	{
		//	gr: this could get a bit recursive
		std::Debug << __func__ << " exception " << e.what() << std::endl;
	}
}


__export void PopMovie_DebugPrint()
{
	Platform::DebugPrint("Soy::Platform::DebugPrint(PopMovie_DebugPrint)");
	std::Debug << __func__ << std::endl;
}

class TThreadObject
{
public:
	std::thread		mThread;
};
bool global = true;

std::shared_ptr<bool> SharedBool;

__export void			PopMovie_DebugThread()
{
	Platform::DebugPrint("PopMovie_DebugThread");
	auto* pThread = new std::thread;
	delete pThread;
	Platform::DebugPrint("thread deleted");

	{
		Platform::DebugPrint("volatile bool x;");
		volatile bool x = global;
		x = !x;
		global = x;
	}
	Platform::DebugPrint("volatile bool done");

	{
		Platform::DebugPrint("creating thread object");
		TThreadObject ThreadObject;
	}
	Platform::DebugPrint("done thread object");

	{
		Platform::DebugPrint("creating soythread object");
		TThreadObject ThreadObject;
	}
	Platform::DebugPrint("done soythread object");


	{
		Platform::DebugPrint("creating shared_ptr object");
		SharedBool.reset( new bool() );
		*SharedBool = true;
	}
	Platform::DebugPrint("done shared_ptr object");

	Platform::DebugPrint("PopMovie_DebugThread Done");
}

__export void PopMovie_DebugThrowCatch()
{
	Platform::DebugPrint("PopMovie_DebugThrowCatch: entry");
	try
	{
		Platform::DebugPrint("PopMovie_DebugThrowCatch: throwing...");
		throw Soy::AssertException("Assertion");
	}
	catch(std::exception& e)
	{
		Platform::DebugPrint("PopMovie_DebugThrowCatch: catch");
		Platform::DebugPrint("exception:");
		Platform::DebugPrint( e.what() );
	}
	catch(...)
	{
		Platform::DebugPrint("PopMovie_DebugThrowCatch: catch(...)" );
	}
	Platform::DebugPrint("PopMovie_DebugThrowCatch: post catch");
}

__export void PopMovie_DebugTestAlloc()
{
	Platform::DebugPrint("PopMovie_DebugTestAlloc");
	try
	{
		Platform::DebugPrint("PopMovie_DebugTestAlloc new...");
		auto* x = new int;

		if ( x == nullptr )
		{
			Platform::DebugPrint("PopMovie_DebugTestAlloc x==null");
		}
		else
		{
			Platform::DebugPrint("PopMovie_DebugTestAlloc delete...");
			delete x;
		}
	}
	catch(std::exception& e)
	{
		Platform::DebugPrint("PopMovie_DebugTestAlloc catch");
		Platform::DebugPrint("exception:");
		Platform::DebugPrint( e.what() );
	}
	catch(...)
	{
		Platform::DebugPrint("PopMovie_DebugTestAlloc catch(...)");
	}
	Platform::DebugPrint("PopMovie_DebugTestAlloc end");
}

__export bool PopMovie_PushRawBuffer(Unity::uint Instance,uint8_t* Buffer,Unity::uint BufferSize,Unity::sint StreamIndex)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	auto pInstance = PopMovie::GetInstance( PopMovie::TInstanceRef(Instance) );
	if ( !pInstance )
		return false;

	try
	{
		auto BufferArray = GetRemoteArray( Buffer, BufferSize );
		pInstance->PushStreamBuffer( GetArrayBridge(BufferArray), size_cast<size_t>(StreamIndex) );
		return true;
	}
	catch ( std::exception& e )
	{
		std::Debug << "Exception in " << __func__ << ": " << e.what() << std::endl;
	}
	catch (...)
	{
		std::Debug << "Unknown exception in " << __func__ << std::endl;
	}
	return false;
}

