#include "TAudioGraph.h"
#include "PopUnity.h"
#include "TBlitter.h"
#include "PopMovieDecoder.h"
#include "SoyWave.h"
#include "TPerformanceGraph.h"




namespace Opengl
{
	const char*	BlitFragShader_AudioGraph =
#include "BlitAudioGraph.glsl.frag"
	;
}

namespace Directx
{
	const char*	BlitFragShader_AudioGraph =
#include "BlitAudioGraph.hlsl.frag"
	;
}




#if defined(ENABLE_OPENGL)
Opengl::TAudioRenderer::TAudioRenderer(std::shared_ptr<Opengl::TContext> Context) :
	mContext	( Context )
{
	Soy::Assert( mContext!=nullptr, "Graph renderer requires opengl context");
}
#endif

#if defined(ENABLE_OPENGL)
Opengl::TAudioRenderer::~TAudioRenderer()
{
	//	wait for render to finish
	std::lock_guard<std::mutex> Lock(mRenderLock );

	//	deffer object shutdown
	PopWorker::DeferDelete( mContext, mGeo );
    PopWorker::DeferDelete( mContext, mShader );
	PopWorker::DeferDelete( mContext, mRenderTarget );
	
	PopWorker::DeferDelete( mContext, mAudioDataTexture );

	mContext.reset();
}
#endif


#if defined(ENABLE_OPENGL)
void Opengl::TAudioRenderer::Render(TTexture &Target,const TAudioGraphData& AudioData)
{
	//	render
	auto RenderTarget = GetRenderTarget( Target );
	auto BlitGeo = GetGeo();
	auto BlitShader = GetShader();
	
	Soy::Assert( RenderTarget!=nullptr, "Failed to get render target for blit");
	Soy::Assert( BlitGeo!=nullptr, "Failed to get geo for blit");
	Soy::Assert( BlitShader!=nullptr,"Failed to get shader for blit");

	Soy::TRgb ClearColour = PerformanceGraph::NiceBlue;
	Soy::TRgb TimeColour = PerformanceGraph::TimeColour;
	Soy::TRgb AheadColour = PerformanceGraph::AudioGreen;
	Soy::TRgb BehindColour = PerformanceGraph::NiceRed;
	Soy::TRgb ErrorColour = PerformanceGraph::NiceRed;
	
	auto& AudioDataTexture = GetAudioDataTexture( GetArrayBridge(AudioData.mSamples) );
	
	RenderTarget->Bind();
	static bool DoClearDepth = false;
	static bool DoClearColour = true;
	static bool DoClearStencil = false;
	if ( DoClearDepth )		Opengl::ClearDepth();
	if ( DoClearColour )	Opengl::ClearColour( ClearColour, 1 );
	if ( DoClearStencil )	Opengl::ClearStencil();
	{
		auto Shader = BlitShader->Bind();
		Shader.SetUniform("AudioData", AudioDataTexture );
		Shader.SetUniform("AudioDataWidth", size_cast<int>(AudioDataTexture.GetWidth()) );
		Shader.SetUniform("SampleCount", size_cast<int>(AudioData.mSamples.GetSize()) );
		Shader.SetUniform("TimeIndex", size_cast<int>(AudioData.mCurrentTime) );
		Shader.SetUniform("TimeColour", TimeColour );
		Shader.SetUniform("AheadColour", AheadColour );
		Shader.SetUniform("BehindColour", BehindColour );
		Shader.SetUniform("ErrorColour", ErrorColour );
		BlitGeo->Draw();
	}
	RenderTarget->Unbind();
}
#endif

#if defined(ENABLE_OPENGL)
Opengl::TTexture& Opengl::TAudioRenderer::GetAudioDataTexture(const ArrayBridge<float>&& AudioSamples)
{
	//	gotta work out max width the data texture can be
	static size_t MaxWidth = 2048;
	
	//	work out what padding we might need
	auto DataWidth = std::max<size_t>( 1, AudioSamples.GetSize() );
	if ( mAudioDataTexture )
		DataWidth = std::max( mAudioDataTexture->GetWidth(), DataWidth );

	static uint8 PadValue = 0;
	auto& PixelsData = mAudioPixelsDataBuffer;
	PixelsData.SetSize( 0 );
	PixelsData.Reserve( AudioSamples.GetSize() );
	
	//	todo: pre-pad if the audio is before any sample
	
	Wave::ConvertSamples( AudioSamples, GetArrayBridge(PixelsData) );
	
	//	pad the rest
	for ( size_t i=PixelsData.GetSize();	i<DataWidth;	i++ )
		PixelsData.PushBack(PadValue);
	
	//	re-sample
	if ( PixelsData.GetSize() > MaxWidth )
	{
		//	gotta remove every nth sample
		float Skipf = floorf( PixelsData.GetSize() / static_cast<float>(MaxWidth) );
		int Skip = std::max( 1, (int)(Skipf-0.5f) );
	
		//	work backwards,
		//	keep ME, and delete the N after
		for ( float Indexf=PixelsData.GetSize()-Skipf;	Indexf>0;	Indexf-=Skipf )
		{
			//	round down the index
			auto Index = (int)Indexf;
			Index = std::min( Index, size_cast<int>(PixelsData.GetSize())-1 );
			Index = std::max( Index, 0 );
		
			if ( Index + Skip < PixelsData.GetSize() )
				PixelsData.RemoveBlock( Index+1, Skip );
			
			//	culled enough
			if ( PixelsData.GetSize() <= MaxWidth )
				break;
		}
		
		//	may still have excess
		if ( PixelsData.GetSize() > MaxWidth )
		{
			PixelsData.SetSize( MaxWidth );
		}
		
	}
		
		
	
	SoyPixelsMeta PixelsMeta( PixelsData.GetSize(), 1, SoyPixelsFormat::Greyscale );
	SoyPixelsRemote Pixels( GetArrayBridge(PixelsData), PixelsMeta );
	
	//	need to realloc texture if it's bigger than before
	if ( mAudioDataTexture )
	{
		if ( mAudioDataTexture->GetMeta() != Pixels.GetMeta() )
			mAudioDataTexture.reset();
	}
	if ( !mAudioDataTexture )
	{
		mAudioDataTexture.reset( new Opengl::TTexture( Pixels.GetMeta(), GL_TEXTURE_2D ) );
	}
	
	//	fill data
	mAudioDataTexture->Write( Pixels );
	
	return *mAudioDataTexture;
}
#endif


#if defined(ENABLE_OPENGL)
std::shared_ptr<Opengl::TRenderTarget> Opengl::TAudioRenderer::GetRenderTarget(TTexture& Target)
{
	if ( mRenderTarget && mRenderTarget->mTexture != Target )
		mRenderTarget.reset();
	
	if ( mRenderTarget )
		return mRenderTarget;

	mRenderTarget.reset( new TRenderTargetFbo( Target ) );
	return mRenderTarget;
}
#endif

#if defined(ENABLE_OPENGL)
std::shared_ptr<Opengl::TGeometry> Opengl::TAudioRenderer::GetGeo()
{
	if ( mGeo )
		return mGeo;
	
	//	for each part of the vertex, add an attribute to describe the overall vertex
	SoyGraphics::TGeometryVertex Vertex;
	Array<uint8> MeshData;
	Array<size_t> Indexes;
	Soy::TBlitter::GetGeo( Vertex, GetArrayBridge(MeshData), GetArrayBridge(Indexes), false );
	
	mGeo.reset( new TGeometry( GetArrayBridge(MeshData), GetArrayBridge(Indexes), Vertex ) );
	return mGeo;
}
#endif

#if defined(ENABLE_OPENGL)
std::shared_ptr<Opengl::TShader> Opengl::TAudioRenderer::GetShader()
{
	if ( mShader )
		return mShader;
	auto Geo = GetGeo();
	if ( !Geo )
		return nullptr;
	
	mShader.reset( new TShader( BlitVertShader, BlitFragShader_AudioGraph, Geo->mVertexDescription, "AudioGraphShader", *mContext ) );
	return mShader;
}
#endif
