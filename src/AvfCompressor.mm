#include "AvfCompressor.h"

#include <CoreMedia/CMBase.h>
#include <VideoToolbox/VTBase.h>
#include <CoreFoundation/CoreFoundation.h>
#include <CoreVideo/CoreVideo.h>
#include <CoreMedia/CMSampleBuffer.h>
#include <CoreMedia/CMFormatDescription.h>
#include <CoreMedia/CMTime.h>
#include <VideoToolbox/VTSession.h>
#include <VideoToolbox/VTCompressionProperties.h>
#include <VideoToolbox/VTCompressionSession.h>
#include <VideoToolbox/VTDecompressionSession.h>
#include <VideoToolbox/VTErrors.h>
#include <SoyH264.h>
#include "AvfPixelBuffer.h"
#include <SoyWave.h>


class AvfCompressor::TSession
{
public:
	TSession(VTDecompressionSessionRef Session) :
	mSession	( Session )
	{
	}
	~TSession();
	
public:
	VTDecompressionSessionRef mSession;
};



std::shared_ptr<TMediaDecoder> Avf::AllocVideoDecoder(const std::string& Name,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> Input,std::shared_ptr<TPixelBufferManager> Output,const TVideoDecoderParams& Params,std::shared_ptr<Platform::TMediaFormat> Format)
{
	return std::shared_ptr<TMediaDecoder>( new AvfMediaDecoder( Name, Stream, Input, Output, Format, Params ) );
}

std::shared_ptr<TMediaDecoder> Avf::AllocAudioDecoder(const std::string& Name,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> Input,std::shared_ptr<TAudioBufferManager> Output)
{
	return std::shared_ptr<TMediaDecoder>( new AvfAudioDecoder( Name, Stream, Input, Output ) );
}



void NSDataToArray(NSData* Data,ArrayBridge<uint8>&& Array)
{
	auto DataSize = Data.length;
	auto DataArray = GetRemoteArray( (uint8*)Data.bytes, DataSize );
	Array.PushBackArray( DataArray );
}

NSData* ArrayToNSData(const ArrayBridge<uint8>&& Array)
{
	void* Data = (void*)Array.GetArray();
	return [[NSData alloc] initWithBytesNoCopy:Data length:Array.GetDataSize() freeWhenDone:false];
}



AvfCompressor::TSession::~TSession()
{
	if ( mSession )
	{
		VTDecompressionSessionInvalidate( mSession );
		CFRelease( mSession );
		mSession = nullptr;
	}
}


void OnDecompress(void* DecompressionContext,void* SourceContext,OSStatus Status,VTDecodeInfoFlags Flags,CVImageBufferRef ImageBuffer,CMTime PresentationTimeStamp,CMTime PresentationDuration)
{
	if ( !DecompressionContext )
	{
		std::Debug << "OnDecompress missing context" << std::endl;
		return;
	}
	
	AvfMediaDecoder& Encoder = *reinterpret_cast<AvfMediaDecoder*>( DecompressionContext );
	SoyTime Timecode = Soy::Platform::GetTime( PresentationTimeStamp );

	try
	{
		Avf::IsOkay( Status, "OnDecompress" );
		Encoder.OnDecodedFrame( ImageBuffer, Timecode );
	}
	catch (std::exception& e)
	{
		Encoder.OnDecodeError( e.what(), Timecode );
	}
}

void ReadPixelBuffer(CVImageBufferRef ImageBuffer,ArrayBridge<uint8>&& Data)
{
	//	lock pixels & copy
	auto Result = CVPixelBufferLockBaseAddress( ImageBuffer, kCVPixelBufferLock_ReadOnly );
	Avf::IsOkay( Result, "CVPixelBufferLockBaseAddress" );
	auto* Pixels = static_cast<UInt8*>( CVPixelBufferGetBaseAddress(ImageBuffer) );
	Soy::Assert( Pixels, "Missing pixels from CVPixelBufferGetBaseAddress");
	auto DataSize = CVPixelBufferGetDataSize( ImageBuffer );
	Data.SetSize( DataSize );
	memcpy( Data.GetArray(), Pixels, DataSize );
	Result = CVPixelBufferUnlockBaseAddress( ImageBuffer, kCVPixelBufferLock_ReadOnly );
	Avf::IsOkay( Result, "CVPixelBufferUnlockBaseAddress" );
}

void WritePixelBuffer(CVImageBufferRef ImageBuffer,const ArrayBridge<uint8>&& Data)
{
	auto DataSize = CVPixelBufferGetDataSize( ImageBuffer );
	Soy::Assert( DataSize == Data.GetDataSize(), "Image buffer and buffer size mismatch");

	//	lock pixels & copy
	auto Result = CVPixelBufferLockBaseAddress( ImageBuffer, 0x0 );
	Avf::IsOkay( Result, "CVPixelBufferLockBaseAddress" );
	auto* Pixels = static_cast<UInt8*>( CVPixelBufferGetBaseAddress(ImageBuffer) );
	Soy::Assert( Pixels, "Missing pixels from CVPixelBufferGetBaseAddress");
	memcpy( Pixels, Data.GetArray(), DataSize );
	Result = CVPixelBufferUnlockBaseAddress( ImageBuffer, kCVPixelBufferLock_ReadOnly );
	Avf::IsOkay( Result, "CVPixelBufferUnlockBaseAddress" );
}

/*
CMVideoFormatDescriptionRef GetVideoFormatDescription(const TStreamMeta& Stream)
{
	CFAllocatorRef Allocator = nil;
	auto InputCodec = GetCMVideoCodec( Stream.mCodec );
	CFDictionaryRef Extensions = nullptr;
	
	CMVideoFormatDescriptionRef InputFormat = nullptr;
	auto Result = CMVideoFormatDescriptionCreate( Allocator, InputCodec, Stream.mPixelMeta.GetWidth(), Stream.mPixelMeta.GetHeight(), Extensions, &InputFormat );
	AvfCompressor::IsOkay( Result, "CMVideoFormatDescriptionCreate" );
	
	return InputFormat;
}
*/

AvfMediaDecoder::AvfMediaDecoder(const std::string& ThreadName,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> InputBuffer,std::shared_ptr<TPixelBufferManager> OutputBuffer,std::shared_ptr<Platform::TMediaFormat> FormatDesc,const TVideoDecoderParams& Params) :
	TMediaDecoder		( ThreadName, InputBuffer, OutputBuffer ),
	mDecoderRenderer	( new AvfDecoderRenderer() ),
	mFormatDesc			( FormatDesc ),
	mStream				( Stream )
{
	//	refer to this later re: creating description & annexb vs avcc headesr
	//	http://stackoverflow.com/questions/24039345/decoding-h264-videotoolkit-api-fails-with-error-8971-in-vtdecompressionsessionc
	
	
	

	auto StartDecoder = [this,Params](bool&)
	{
		//	clear old session
		if ( mSession )
		{
			mSession.reset();
		}
		
		//	alloc format from stream if we don't have one
		if ( !mFormatDesc )
		{
			auto InputFormat = Avf::GetFormatDescription( mStream );
			mFormatDesc.reset( new Platform::TMediaFormat( InputFormat ) );
			
			static bool UseRevisedStream = false;
			auto RevisedStream = Avf::GetStreamMeta( InputFormat );
			std::Debug << "Got stream from generated input format; " << RevisedStream << std::endl;
			
			if ( UseRevisedStream )
				mStream = RevisedStream;
		}
		
		
		auto& Stream = mStream;
		CFAllocatorRef Allocator = nil;
		Soy::Assert( mFormatDesc!=nullptr, "Format missing" );
		Soy::Assert( Stream.mPixelMeta.IsValidDimensions(), "AvfMediaDecoder needs valid dimensions" );
	
		if ( !Stream.mSps.IsEmpty() )
		{
			try
			{
				auto SpsParams = H264::ParseSps( GetArrayBridge(Stream.mSps) );
				std::Debug << "Starting decode of H264: " << SpsParams.mProfile << " " << SpsParams.mLevel << " " << SpsParams.mWidth << "x" << SpsParams.mHeight << " (warning: SPS parsing not valid)" << std::endl;
			}
			catch(std::exception& e)
			{
				std::Debug << "Warning error in SPS parser; " << e.what() << std::endl;
			}
		}
		// Set the pixel attributes for the destination buffer
		CFMutableDictionaryRef destinationPixelBufferAttributes = CFDictionaryCreateMutable(
																							Allocator, // CFAllocatorRef allocator
																							0,    // CFIndex capacity
																							&kCFTypeDictionaryKeyCallBacks,
																							&kCFTypeDictionaryValueCallBacks);
		
		SInt32 Width = size_cast<SInt32>( Stream.mPixelMeta.GetWidth() );
		SInt32 Height = size_cast<SInt32>( Stream.mPixelMeta.GetHeight() );
		
		CFDictionarySetValue(destinationPixelBufferAttributes,kCVPixelBufferWidthKey, CFNumberCreate(NULL, kCFNumberSInt32Type, &Width));
		CFDictionarySetValue(destinationPixelBufferAttributes, kCVPixelBufferHeightKey, CFNumberCreate(NULL, kCFNumberSInt32Type, &Height));

		bool OpenglCompatible = Params.mDecoderUseHardwareBuffer;
		CFDictionarySetValue(destinationPixelBufferAttributes, kCVPixelBufferOpenGLCompatibilityKey, OpenglCompatible ? kCFBooleanTrue : kCFBooleanFalse );

		OSType destinationPixelType = 0;
		
		if ( Params.mForceNonPlanarOutput )
		{
			destinationPixelType = kCVPixelFormatType_32BGRA;
		}
		else
	#if defined(TARGET_IOS)
		//	to get ios to use an opengl texture, we need to explicitly set the format to RGBA.
		//	None (auto) creates a non-iosurface compatible texture
		if ( OpenglCompatible )
		{
			//destinationPixelType = kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange;
			//destinationPixelType = kCVPixelFormatType_24RGB;
			destinationPixelType = kCVPixelFormatType_32BGRA;
		}
		else	//	for CPU copies we prefer bi-planar as it comes out faster and we merge in shader. though costs TWO texture uploads...
	#endif
		{
			//	favour bi-plane so we can merge with shader rather than have OS do it in CPU
			destinationPixelType = kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange;
			//destinationPixelType = kCVPixelFormatType_24RGB;
		}
		
		if ( destinationPixelType != 0 )
		{
			CFDictionarySetValue(destinationPixelBufferAttributes,kCVPixelBufferPixelFormatTypeKey, CFNumberCreate(NULL, kCFNumberSInt32Type, &destinationPixelType));
		}

		
		// Set the Decoder Parameters
		CFMutableDictionaryRef decoderParameters = CFDictionaryCreateMutable(
																			 NULL, // CFAllocatorRef allocator
																			 0,    // CFIndex capacity
																			 &kCFTypeDictionaryKeyCallBacks,
																			 &kCFTypeDictionaryValueCallBacks);
		
		static bool AllowDroppedFrames = false;
		CFDictionarySetValue(decoderParameters,kVTDecompressionPropertyKey_RealTime, AllowDroppedFrames ? kCFBooleanTrue : kCFBooleanFalse );

		
		VTDecompressionSessionRef Session = nullptr;
		const VTDecompressionOutputCallbackRecord callback = { OnDecompress, this };
		auto Result = VTDecompressionSessionCreate( Allocator, mFormatDesc->mDesc, decoderParameters, destinationPixelBufferAttributes, &callback, &Session );
		

		CFRelease(destinationPixelBufferAttributes);
		CFRelease(decoderParameters);
		
		Avf::IsOkay( Result, "TDecompressionSessionCreate" );
		Soy::Assert( Session !=nullptr, "Failed to create decompression session");
		
		mSession.reset( new AvfCompressor::TSession( Session ) );
	};
	
	
	mOnStreamChanged.AddListener( StartDecoder );
	
	
	//	already have a format, can start(and fail) immediately
	if ( mFormatDesc )
	{
		bool Dummy;
		mOnStreamChanged.OnTriggered( Dummy );
	}
	
	
	//	if we don't have a format yet, we need to wait for the header packets
	//	todo: expand for other formats
	//	if we don't have a native format, we need to allocate one from the meta we have
	if ( !mFormatDesc )
	{
		if ( !mStream.mPps.IsEmpty() && !mStream.mSps.IsEmpty() )
		{
			bool Dummy;
			mOnStreamChanged.OnTriggered( Dummy );
			/*
			auto InputFormat = GetFormatDescription( mStream );
			mFormatDesc.reset( new Platform::TMediaFormat( InputFormat ) );
			
			auto RevisedStream = GetStreamMeta( InputFormat );
			std::Debug << "Got stream from generated input format; " << RevisedStream << std::endl;
			mStream = RevisedStream;
			 */
		}
	}
	
	
	Start();

}

AvfMediaDecoder::~AvfMediaDecoder()
{
	WaitToFinish();
}


bool AvfMediaDecoder::ProcessPacket(const TMediaPacket& Packet)
{
	//	still looking for SPS/PPS to setup format/session
	if ( !mFormatDesc )
	{
		Soy::Assert( mSession==nullptr, "Expected no session" );
		if ( !mSpsPacket && Packet.mMeta.mCodec == SoyMediaFormat::H264_SPS_ES )
		{
			mSpsPacket.reset( new TMediaPacket( Packet ) );
		}
		else if ( !mPpsPacket && Packet.mMeta.mCodec == SoyMediaFormat::H264_PPS_ES )
		{
			mPpsPacket.reset( new TMediaPacket( Packet ) );
		}
		else
		{
			mQueuedPackets.PushBack( Packet );
			static int QueueWarningSize = 10;
			if ( mQueuedPackets.GetSize() >= QueueWarningSize )
				std::Debug << "Warning: AvfMediaDecoder has queued " << mQueuedPackets.GetSize() << " waiting for SPS/PPS from codec " << Packet.mMeta.mCodec << std::endl;
		}
		
		//	got missing data
		if ( mSpsPacket && mPpsPacket )
		{
			mStream.mSps.Copy( mSpsPacket->mData );
			mStream.mPps.Copy( mPpsPacket->mData );
			bool Dummy;
			mOnStreamChanged.OnTriggered(Dummy);
			mSpsPacket.reset();
			mPpsPacket.reset();
			mQueuedPackets.Clear();
		}
		return true;
	}
	
	
	
	
	
	
	Soy::Assert( mSession!=nullptr, "Session expected" );

	//	create buffer from packet
	CFAllocatorRef Allocator = nullptr;
	
	CMSampleBufferRef SampleBuffer = nullptr;
	
	Array<uint8> AvccData;
	
	//	if packet is an image
	//	h264 = buffer, not image
	bool IsPixels = false;
//	if ( SoyMediaFormat::IsVideo(Packet.mMeta.mCodec) )
	if ( IsPixels )
	{
		/*
		 auto FormatDesc = GetVideoFormatDescription( Packet.mMeta );

		CFDictionaryRef PixelBufferAttribs = nullptr;
		CVPixelBufferRef PixelBuffer = nullptr;
		OSType PixelFormatType = GetPixelFormat
		auto Result = CVPixelBufferCreate( Allocator, Packet.mMeta.mPixelMeta.GetWidth(), Packet.mMeta.mPixelMeta.GetHeight(), PixelFormatType, PixelBufferAttribs, &PixelBuffer );
		AvfCompressor::IsOkay( Result, "CVPixelBufferCreate" );
		WritePixelBuffer( PixelBuffer, GetArrayBridge(Packet.mData) );
		
		CMSampleTimingInfo SampleTiming;
		SampleTiming.decodeTimeStamp = Soy::Platform::GetTime( Packet.mDecodeTimecode );
		SampleTiming.duration = Soy::Platform::GetTime( Packet.mDuration );
		SampleTiming.presentationTimeStamp = Soy::Platform::GetTime( Packet.mTimecode );
		
		Result = CMSampleBufferCreateForImageBuffer( Allocator, PixelBuffer, true, Callback, nullptr, FormatDesc, &SampleTiming, &SampleBuffer );
		AvfCompressor::IsOkay( Result, "CMSampleBufferCreateForImageBuffer" );
		 */
	}
	else
	{
		uint32_t SubBlockSize = 0;
		CMBlockBufferFlags Flags = 0;
		CMBlockBufferRef BlockBuffer = nullptr;
		auto Result = CMBlockBufferCreateEmpty( Allocator, SubBlockSize, Flags, &BlockBuffer );
		Avf::IsOkay( Result, "CMBlockBufferCreateEmpty" );

		//	for now, just convert. we can speed this up in future by doing 2 explicit buffer appends
		//	gr: using a buffer slows this down??
		//auto& AvccData = Packet.mData;
		AvccData.Copy( Packet.mData );
		
		if ( SoyMediaFormat::IsH264( Packet.mMeta.mCodec ) )
		{
			SoyMediaFormat::Type AvccFormat = Packet.mMeta.mCodec;
			H264::ConvertToFormat( AvccFormat, SoyMediaFormat::H264_32, GetArrayBridge( AvccData ) );
			
			static bool ReconvertTest = false;
			if ( ReconvertTest )
			{
				H264::ConvertToFormat( AvccFormat, SoyMediaFormat::H264_ES, GetArrayBridge( AvccData ) );
				H264::ConvertToFormat( AvccFormat, SoyMediaFormat::H264_32, GetArrayBridge( AvccData ) );
			}
			
			//	gr: due to AUD annexb packets (joins) we can produce empty AVCC packets, safely step over these
			//		we COULD omit them earlier (for cross platform purposes), but cope here anyway
			if ( AvccData.IsEmpty() )
			{
				std::Debug << "Skipping empty AVCC packet" << std::endl;
				return true;
			}
			
			//std::Debug << "AVCC data x" << AvccData.GetDataSize() << "; " << Soy::DataToHexString( GetArrayBridge(AvccData), 20 ) << std::endl;
			
			//	extract first byte
			//	gr: this will always fail as its avcc - correct for offset if we still care about this debug output
			/*
			try
			{
				H264NaluContent::Type Content;
				H264NaluPriority::Type Priority;
				H264::DecodeNaluByte( AvccData[0], Content, Priority );
				std::Debug << "Avcc nalu; " << Content << ", " << Priority << std::endl;
			}
			catch(std::exception& e)
			{
				std::Debug << "Error decoding nalu byte; " << e.what() << std::endl;
			}
			 */
		}
		
		//	gr: when you pass memory to a block buffer, it only bloody frees it. make sure kCFAllocatorNull is the "allocator" for the data
		//		also means of course, for async decoding the data could go out of scope. May explain the wierd MACH__O error that came from the decoder?
		void* Data = (void*)AvccData.GetArray();
		auto DataSize = AvccData.GetDataSize();
		size_t Offset = 0;

		Result = CMBlockBufferAppendMemoryBlock( BlockBuffer,
												Data,
												DataSize,
												kCFAllocatorNull,
												nullptr,
												Offset,
												DataSize-Offset,
												Flags );

		Avf::IsOkay( Result, "CMBlockBufferAppendMemoryBlock" );

	
		//CMFormatDescriptionRef Format = GetFormatDescription( Packet.mMeta );
		//CMFormatDescriptionRef Format = Packet.mFormat->mDesc;
		auto Format = Packet.mFormat ? Packet.mFormat->mDesc : mFormatDesc->mDesc;
		if ( !VTDecompressionSessionCanAcceptFormatDescription( mSession->mSession, Format ) )
		{
			std::Debug << "VTDecompressionSessionCanAcceptFormatDescription failed" << std::endl;
			//	gr: maybe re-create session here with... new format? (save the packet's format to mFormatDesc?)
			//bool Dummy;
			//mOnStreamChanged.OnTriggered( Dummy );
		}
	
		int NumSamples = 1;
		BufferArray<size_t,1> SampleSizes;
		SampleSizes.PushBack( AvccData.GetDataSize() );
		BufferArray<CMSampleTimingInfo,1> SampleTimings;
		auto& FrameTiming = SampleTimings.PushBack();
		FrameTiming.duration = Soy::Platform::GetTime( Packet.mDuration );
		FrameTiming.presentationTimeStamp = Soy::Platform::GetTime( Packet.mTimecode );
		FrameTiming.decodeTimeStamp = Soy::Platform::GetTime( Packet.mDecodeTimecode );

		static bool ForcedTimecode = false;
		if ( ForcedTimecode )
		{
			static SoyTime Timestamp( std::chrono::milliseconds(1001) );
			Timestamp += 30;
			FrameTiming.presentationTimeStamp = Soy::Platform::GetTime( Timestamp );
			FrameTiming.decodeTimeStamp = Soy::Platform::GetTime( Timestamp );
		}
		
		Result = CMSampleBufferCreate(	Allocator,
										BlockBuffer,
										true,
										nullptr,	//	callback
										nullptr,	//	callback context
										Format,
										NumSamples,
										SampleTimings.GetSize(),
										SampleTimings.GetArray(),
										SampleSizes.GetSize(),
										SampleSizes.GetArray(),
										&SampleBuffer );
		Avf::IsOkay( Result, "CMSampleBufferCreateReady" );

		//	sample buffer now has a reference to the block
		CFRelease( BlockBuffer );
	}
	
	
	Soy::Assert( SampleBuffer, "Failed to create sample buffer for decoder");
	
	//VTDecodeFrameFlags Flags = kVTDecodeFrame_EnableAsynchronousDecompression;
	VTDecodeFrameFlags Flags = 0;
	VTDecodeInfoFlags FlagsOut = 0;
	
	SoyTime DecodeDuration;
	auto OnFinished = [&DecodeDuration](SoyTime Timer)
	{
		DecodeDuration = Timer;
	};
	
	bool RecreateStream = false;
	{
		OnDecodeFrameSubmitted( Packet.mTimecode );
		
		//std::Debug << "decompressing " << Packet.mTimecode << "..." << std::endl;
		Soy::TScopeTimer Timer("VTDecompressionSessionDecodeFrame", 1, OnFinished, true );
		auto Result = VTDecompressionSessionDecodeFrame( mSession->mSession, SampleBuffer, Flags, nullptr, &FlagsOut );
		Timer.Stop();
		//std::Debug << "Decompress " << Packet.mTimecode << " took " << DecodeDuration << "; error=" << (int)Result << std::endl;
		Avf::IsOkay( Result, "VTDecompressionSessionDecodeFrame", false );

		static int FakeInvalidateSessionCounter = 0;
		static int FakeInvalidateSessionOnCount = -1;
		if ( ++FakeInvalidateSessionCounter == FakeInvalidateSessionOnCount )
		{
			FakeInvalidateSessionCounter = 0;
			Result = kVTInvalidSessionErr;
		}
		
		switch ( Result )
		{
			//	no error
			case 0:
				break;
				
			//	gr: if we pause for ages without eating a frame, we can get this...
			//		because for somereason the decoder thread is still trying to decode stuff??
			case MACH_RCV_TIMED_OUT:
				std::Debug << "Decompression MACH_RCV_TIMED_OUT..." << std::endl;
				break;
		
			//	gr: restoring iphone app sometimes gives us malfunction, sometimes invalid session.
			//		guessing invalid session is if it's been put to sleep properly or OS has removed some resources
			case kVTInvalidSessionErr:
			case kVTVideoDecoderMalfunctionErr:
			{
				//  gr: need to re-create session. Session dies when app sleeps and restores
				std::stringstream Error;
				Error << "Lost decompression session; " << Avf::GetString(Result);
				OnDecodeError( Error.str(), Packet.mTimecode );
				//	make errors visible for debugging
				//std::this_thread::sleep_for( std::chrono::milliseconds(1000));
				RecreateStream = true;
			}
			break;
				
				
			default:
			{
				static bool RecreateOnDecompressionError = false;
				std::Debug << "some decompression error; " << Avf::GetString(Result) << std::endl;
				if ( RecreateOnDecompressionError )
					RecreateStream = true;
			}
			break;
		}
		
		//	gr: do we NEED to make sure all referecnes are GONE here? as the data in block buffer is still in use if >1?
		//auto SampleCount = CFGetRetainCount( SampleBuffer );
		CFRelease( SampleBuffer );
	}
	
	if ( RecreateStream )
	{
		Soy::TScopeTimerPrint Timer("Recreating decompression session", 1);
		bool Dummy;
		mOnStreamChanged.OnTriggered(Dummy);
		
		//	gr: if packet was a keyframe, maybe return false to re-process the packet so immediate next frame is not scrambled
		if ( Packet.mIsKeyFrame )
		{
			std::Debug << "Returning keyframe back to buffer after session recreation" << std::endl;
			return false;
		}
	}
	
	return true;
}

void AvfMediaDecoder::OnDecodeError(const std::string& Error,SoyTime Timecode)
{
	GetPixelBufferManager().mOnFrameDecodeFailed.OnTriggered( Timecode );

	static bool StoreErrors = true;
	if ( StoreErrors )
		mFatalError << Error;
}

void AvfMediaDecoder::OnDecodedFrame(void* ImageBufferRef,SoyTime Timecode)
{
	Soy::StringStreamClear(mFatalError);

	auto& Output = GetPixelBufferManager();
	Output.CorrectDecodedFrameTimestamp( Timecode );
	Output.mOnFrameDecoded.OnTriggered( Timecode );
	
	Soy::Assert( ImageBufferRef, "ImageBuffer missing" );

	CVImageBufferRef ImageBuffer = reinterpret_cast<CVImageBufferRef>( ImageBufferRef );

	//	gr: seem to need an extra retain. find out what's releaseing this twice despite retain below
	//auto RetainCount = CFGetRetainCount( ImageBuffer );
	//std::Debug << "On decoded frame, retain count=" << RetainCount << std::endl;
	CFRetain( ImageBuffer );
	
	TPixelBufferFrame Frame;
	Frame.mTimestamp = Timecode;

	Frame.mPixels.reset( new CVPixelBuffer( ImageBuffer, true, mDecoderRenderer, mStream.GetTransform() ) );
	
	//	bail out once thread is stopped
	auto Block = [this]
	{
		return IsWorking();
	};
	Output.PushPixelBuffer( Frame, Block );


	//auto FinalRetainCount = CFGetRetainCount( ImageBuffer );
	//std::Debug << "Final retain count=" << FinalRetainCount << std::endl;
}



AvfAsset::AvfAsset(const std::string& Filename,std::function<bool(const TStreamMeta&)> FilterTrack)
{
	//	alloc asset
	auto Url = GetUrl( Filename );
	
	NSDictionary *Options = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
	AVURLAsset* Asset = [[AVURLAsset alloc] initWithURL:Url options:Options];
	
	if ( !Asset )
		throw Soy::AssertException("Failed to create asset");
	
	if ( !Asset.readable )
		std::Debug << "Warning: Asset " << Filename << " reported as not-readable" << std::endl;
	
	mAsset.Retain( Asset );
	
	if ( !FilterTrack )
		FilterTrack = [](const TStreamMeta&){	return true;	};
	
	LoadTracks( FilterTrack );
}


NSURL* AvfAsset::GetUrl(const std::string& Filename)
{
	NSString* UrlString = Soy::StringToNSString( Filename );
	NSError *err;
	
	//	try as file which we can test for immediate fail
	NSURL* Url = [[NSURL alloc]initFileURLWithPath:UrlString];
	if ([Url checkResourceIsReachableAndReturnError:&err] == NO)
	{
		//	FILE is not reachable.
		
		//	try as url
		Url = [[NSURL alloc]initWithString:UrlString];
		
		/*	gr: throw this error IF we KNOW it's a file we're trying to reach and not an url.
		 check for ANY scheme?
		 std::stringstream Error;
		 Error << "Failed to reach file from url: " << mParams.mFilename << "; " << Soy::NSErrorToString(err);
		 throw Soy::AssertException( Error.str() );
		 */
	}
	
	return Url;
}

void AvfAsset::LoadTracks(const std::function<bool(const TStreamMeta&)>& FilterTrack)
{
	AVAsset* Asset = mAsset.mObject;
	Soy::Assert( Asset, "Asset expected" );
		
	Soy::TSemaphore LoadTracksSemaphore;
	__block Soy::TSemaphore& Semaphore = LoadTracksSemaphore;
	
	auto OnCompleted = ^
	{
		NSError* err = nil;
		auto Status = [Asset statusOfValueForKey:@"tracks" error:&err];
		if ( Status != AVKeyValueStatusLoaded)
		{
			std::stringstream Error;
			Error << "Error loading tracks: " << Soy::NSErrorToString(err);
			Semaphore.OnFailed( Error.str().c_str() );
			return;
		}
		
		Semaphore.OnCompleted();
	};
	
	//	load tracks
	NSArray* Keys = [NSArray arrayWithObjects:@"tracks",@"playable",@"hasProtectedContent",@"duration",@"preferredTransform",nil];
	[Asset loadValuesAsynchronouslyForKeys:Keys completionHandler:OnCompleted];
	
	LoadTracksSemaphore.Wait();
	
	//	grab duration
	//	gr: not valid here with AVPlayer!
	//	http://stackoverflow.com/a/7052147/355753
	auto Duration = Soy::Platform::GetTime( Asset.duration );

	//	get meta for each track
	NSArray* Tracks = [Asset tracks];
	for ( int t=0;	t<[Tracks count];	t++ )
	{
		//	get and retain track
		AVAssetTrack* Track = [Tracks objectAtIndex:t];
		if ( !Track )
			continue;

		TStreamMeta TrackMeta;

		//	extract meta from track
		NSArray* FormatDescriptions = [Track formatDescriptions];
		std::shared_ptr<Platform::TMediaFormat> PlatformFormat;
		
		std::stringstream MetaDebug;
		if ( !FormatDescriptions )
		{
			MetaDebug << "Format descriptions missing";
		}
		else if ( FormatDescriptions.count == 0 )
		{
			MetaDebug << "Format description count=0";
		}
		else
		{
			//	use first description, warn if more
			if ( FormatDescriptions.count > 1 )
			{
				MetaDebug << "Found mulitple(" << FormatDescriptions.count << ") format descriptions. ";
			}
			
			id DescElement = FormatDescriptions[0];
			CMFormatDescriptionRef Desc = (__bridge CMFormatDescriptionRef)DescElement;
			
			
			TrackMeta = Avf::GetStreamMeta( Desc );

			//	save format
			PlatformFormat.reset( new Platform::TMediaFormat( Desc ) );
		}
		
		TrackMeta.mStreamIndex = t;
		TrackMeta.mEncodingBitRate = Track.estimatedDataRate;

		//	grab the transform from the video track
		TrackMeta.mPixelMeta.DumbSetWidth( Track.naturalSize.width );
		TrackMeta.mPixelMeta.DumbSetHeight( Track.naturalSize.height );
		TrackMeta.mTransform = Soy::MatrixToVector( Track.preferredTransform, vec2f(TrackMeta.mPixelMeta.GetWidth(),TrackMeta.mPixelMeta.GetHeight()) );

		TrackMeta.mDuration = Duration;
		
		//	skip tracks
		if ( !FilterTrack(TrackMeta) )
			continue;
		
		mStreamFormats[t] = PlatformFormat;
		mStreams.PushBack( TrackMeta );
	}
	
}


AvfMediaExtractor::AvfMediaExtractor(const TMediaExtractorParams& Params) :
	TMediaExtractor		( Params ),
	mAudioSampleRate	( Params.mAudioSampleRate )
{
	auto TrackFilter = [Params](const TStreamMeta& Stream)
	{
		if ( !Params.mExtractAudioStreams )
		{
			if ( SoyMediaFormat::IsAudio( Stream.mCodec ) )
				return false;
		}
		return true;
	};

	mAsset.reset( new AvfAsset( Params.mFilename, TrackFilter ) );
	
	CreateAssetReader();
	
	//	start the extractor thread automatically so we get first frames asap
	//	gr: maybe not?
	Start();
}

AvfMediaExtractor::~AvfMediaExtractor()
{
}

void AvfMediaExtractor::Stop()
{
	TMediaExtractor::Stop();
	DeleteAssetReader();
}

void AvfMediaExtractor::CreateAssetReader()
{
	SoyTime StartingTime = GetSeekTime();
	std::lock_guard<std::mutex> Lock( mAssetReaderLock );
	
	//	delete old stuff
	DeleteAssetReader();

	auto* Asset = mAsset->mAsset.mObject;
	
	//	alloc asset reader
	NSError* error = nil;
	auto* AssetReader = [AVAssetReader assetReaderWithAsset:Asset error:&error];
	if ( error )
	{
		std::stringstream Error;
		Error << "failed to create asset reader: " << Soy::NSErrorToString( error );
		throw Soy::AssertException( Error.str() );
	}
	
	mAssetReader.Retain( AssetReader );
	
	auto& Streams = mAsset->mStreams;
	
	NSArray* Tracks = [Asset tracks];
	
	for ( int i=0;	i<Streams.GetSize();	i++ )
	{
		auto Stream = Streams[i];
		auto TrackIndex = Stream.mStreamIndex;
		Soy::Assert( TrackIndex < Tracks.count, "stream index out of range" );
		AVAssetTrack* Track = [Tracks objectAtIndex:TrackIndex];
		Soy::Assert( Track, "stream missing" );
		
		AVAssetReaderTrackOutput* TrackOutput = nullptr;
		
		//	with AVFoundation we make things easiest by immediately mixing the track in the extractor;
		//	gr: see if this adds unnecessary CPU expense WRT scanning through and dropping packets.
		//		ideally we'll have a cross platform audio decoder eventually...
		//	http://stackoverflow.com/questions/9364227/how-to-correctly-read-decoded-pcm-samples-on-ios-using-avassetreader-currentl
		if ( SoyMediaFormat::IsAudio( Stream.mCodec ) )
		{
			auto SampleRate = mAudioSampleRate;
			static size_t DefaultSampleRate = 44100;
			if ( SampleRate <= 0 )
			{
				std::Debug << __func__ << " desired audio sample rate of " << SampleRate << ", defaulting to " << DefaultSampleRate << std::endl;
				SampleRate = DefaultSampleRate;
			}
			else
			{
				std::Debug << __func__ << " setting AVF to decode audio to " << SampleRate << std::endl;
			}
			float SampleRatef = SampleRate;
			int BitDepth = 16;
			/*
			 audioReadSettings = [[NSMutableDictionary alloc] init];
			 [audioReadSettings setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM]
			 forKey:AVFormatIDKey];
			 [audioReadSettings setValue:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
			 [audioReadSettings setValue:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
			 [audioReadSettings setValue:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
			 [audioReadSettings setValue:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsNonInterleaved];
			 [audioReadSettings setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
			 */
			NSDictionary *outputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
											[NSNumber numberWithInt:kAudioFormatLinearPCM], AVFormatIDKey,
											[NSNumber numberWithFloat:SampleRatef], AVSampleRateKey,
											[NSNumber numberWithInt:BitDepth], AVLinearPCMBitDepthKey,
											[NSNumber numberWithBool:NO], AVLinearPCMIsNonInterleaved,
											[NSNumber numberWithBool:NO], AVLinearPCMIsFloatKey,
											[NSNumber numberWithBool:NO], AVLinearPCMIsBigEndianKey,
											nil];
			auto* AudioTrackOutput = [AVAssetReaderAudioMixOutput assetReaderAudioMixOutputWithAudioTracks:[NSArray arrayWithObject:Track]
																							 audioSettings:outputSettings];
			TrackOutput = (AVAssetReaderTrackOutput*)AudioTrackOutput;
		}
		else
		{
			TrackOutput = [AVAssetReaderTrackOutput assetReaderTrackOutputWithTrack:Track outputSettings:nil];
		}
		
		if ( !TrackOutput || ![mAssetReader.mObject canAddOutput:TrackOutput])
		{
			std::stringstream Error;
			Error << "Cannot add output of track " << Stream;
			throw Soy::AssertException( Error.str() );
		}
		[mAssetReader.mObject addOutput:TrackOutput];
		
		auto& TrackPtr = mTracks.PushBack();
		TrackPtr.reset( new ObjcPtr<AVAssetTrack>( Track ) );
		auto& TrackOutputPtr = mTrackOutputs.PushBack();
		TrackOutputPtr.reset( new ObjcPtr<AVAssetReaderTrackOutput>( TrackOutput ) );
	}
	
	SoyTime InitialSeekTime( StartingTime );
	if ( InitialSeekTime.IsValid() )
	{
		std::Debug << "Reader initial seek to " << InitialSeekTime << std::endl;
		auto StartTime = Soy::Platform::GetTime( InitialSeekTime );
		CMTimeRange TimeRange = CMTimeRangeMake( StartTime, kCMTimePositiveInfinity );
		mAssetReader.mObject.timeRange = TimeRange;
	}
	
	//	start reading
	if ( [mAssetReader.mObject startReading] == NO )
	{
		std::stringstream Error;
		Error << "Failed to startReading asset: " << Soy::NSErrorToString( [mAssetReader.mObject error] );
		throw Soy::AssertException( Error.str() );
	}

	//	gr: may need to flag this as "not real"
	//	to stop recreating the reader every OnSeek, reset the mLastReadTime
	//	really this might get overwritten, so we need an ignore thing in ReadNextPacket like MFdecoders
	mLastReadTime = StartingTime;
}

void AvfMediaExtractor::DeleteAssetReader()
{
	//	need to do this in case TMediaExtractor thread is waiting on copyNextSample
	if ( mAssetReader )
	{
		[mAssetReader.mObject cancelReading];
		mAssetReader.Release();
		mTrackOutputs.Clear();
	}
}


std::shared_ptr<Platform::TMediaFormat> AvfMediaExtractor::GetStreamFormat(size_t StreamIndex)
{
	return mAsset ? mAsset->GetStreamFormat( StreamIndex ) : nullptr;
}

void AvfMediaExtractor::GetStreams(ArrayBridge<TStreamMeta>&& Streams)
{
	if ( !mAsset )
		return;
	
	Streams.PushBackArray( mAsset->mStreams );
}



std::shared_ptr<TMediaPacket> AvfMediaExtractor::ReadNextPacket()
{
	std::lock_guard<std::mutex> Lock( mAssetReaderLock );
	Soy::Assert( mAssetReader, "Asset reader expected");
	auto* AssetReader = mAssetReader.mObject;
	
	if ( AssetReader.status == AVAssetReaderStatusFailed )
	{
		//	gr: got an... invalid operation? here. Which kept the reader stuck. (after app sleep & restore).
		//		Need to handle this case and recreate the reader to recover.
		//		todo: find the specific error and handle it. (or handle all faileds?)
		std::stringstream Error;
		Error << "Asset reader error: " << Soy::NSErrorToString( [AssetReader error] );
		throw Soy::AssertException( Error.str() );
	}
	

	//	fill the next-packet list for each stream
	for ( int i=0;	i<mTrackOutputs.GetSize();	i++ )
	{
		auto& NextPacket = mNextPacketPerStream[i];
		if ( NextPacket )
			continue;
		
		NextPacket = ReadNextPacket(i);
		if ( !NextPacket )
			continue;

		//	update read time with any non-zero time
		auto Timecode = NextPacket->GetSortingTimecode();
		if ( Timecode.IsValid() )
			mLastReadTime = Timecode;
		
		//std::Debug << "Read next packet for stream " << i << " format: " << NextPacket->mMeta.mCodec << std::endl;
	}
	
	SoyTime OldestPacketTime;
	int OldestPacketStreamIndex = -1;
	
	//	find oldest packet
	for ( auto it=mNextPacketPerStream.begin();	it!=mNextPacketPerStream.end();	it++ )
	{
		auto& pPacket = it->second;
		auto Index = it->first;
		if ( !pPacket )
			continue;
		
		auto Timecode = pPacket->GetSortingTimecode();
		
		//	select oldest or first
		if ( OldestPacketStreamIndex == -1 || Timecode < OldestPacketTime )
		{
			OldestPacketStreamIndex = size_cast<int>(Index);
			OldestPacketTime = Timecode;
		}
	}
	
	//	nothing ready
	if ( OldestPacketStreamIndex == -1 )
		return nullptr;
	
	//	pop oldest
	auto pPacket = mNextPacketPerStream[OldestPacketStreamIndex];
	mNextPacketPerStream[OldestPacketStreamIndex] = nullptr;
	
	return pPacket;
}


std::shared_ptr<TMediaPacket> AvfMediaExtractor::ReadNextPacket(size_t StreamIndex)
{
	if ( StreamIndex >= mTrackOutputs.GetSize() )
		return nullptr;
	
	auto* Track = mTrackOutputs[StreamIndex]->mObject;
	CMSampleBufferRef SampleBuffer = [Track copyNextSampleBuffer];
	
	//	no new frame
	if ( !SampleBuffer )
	{
		Soy::Assert( mAssetReader, "Asset reader expected");
		auto* AssetReader = mAssetReader.mObject;
		
		//	no frame and finished decoding == eof
		//	need to do this for each stream?
		if ( AssetReader.status == AVAssetReaderStatusReading )
			return nullptr;
		
		std::shared_ptr<TMediaPacket> pPacket( new TMediaPacket() );
		pPacket->mEof = true;
		return pPacket;
	}
	
	//	clean up any buffers we don't use
	auto SkipBuffer = []
	{
		//	buffer doesn't seem to be continuing
		//CMSampleBufferInvalidate( Buffer );
		//	gr: this is sometimes crashing, refer back to old code to see if we're missing a retain
		//CFRelease(SampleBuffer);
	};
	
	//	gr: how do I hadnle this?
	if ( !CMSampleBufferDataIsReady( SampleBuffer ) )
	{
		std::Debug << "Don't know what to do with not-ready buffer?" << std::endl;
		SkipBuffer();
		return nullptr;
	}
	
	
	CMTime PresentationTimestampCm = CMSampleBufferGetPresentationTimeStamp(SampleBuffer);
	auto PresentationTimestamp = Soy::Platform::GetTime(PresentationTimestampCm);
	CMTime DecodeTimestampCm = CMSampleBufferGetDecodeTimeStamp(SampleBuffer);
	CMTime SampleDurationCm = CMSampleBufferGetDuration(SampleBuffer);
	bool IsKeyframe = Avf::IsKeyframe( SampleBuffer, true );
	auto DecodeTimestamp = Soy::Platform::GetTime(DecodeTimestampCm);
	auto SampleDuration = Soy::Platform::GetTime(SampleDurationCm);

	//	gr: these don't seem to exist for the shark video... maybe they will with more OOO videos
	static bool UsePresentationAsDecode = false;
	if ( UsePresentationAsDecode )
		DecodeTimestamp = PresentationTimestamp;
	
	//	I get a packet (or two?) at the right timecode, but then a load starting from 0
	//	could be this? (but for vidoe???) http://stackoverflow.com/a/13280305/355753
	//	lets just ditch anything outside the dictated timerange
	
	{
		Soy::Assert( mAssetReader, "Asset reader expected");
		auto* AssetReader = mAssetReader.mObject;
		auto InitialTimestamp = Soy::Platform::GetTime(AssetReader.timeRange.start);
		
		//	note: should use .IsValid() but will not handle packets with timecode of 0 (start)
		if ( PresentationTimestamp < InitialTimestamp )
		{
			static bool DropPacketsOutOfRange = false;
			if ( DropPacketsOutOfRange )
			{
				std::Debug << __func__ << " dropping packet with PresentationTimestamp (" << PresentationTimestamp << ") outside of timerange of reader (" << InitialTimestamp << ")" << std::endl;
				SkipBuffer();
				return nullptr;
			}
			else
			{
				std::Debug << __func__ << " packet with PresentationTimestamp (" << PresentationTimestamp << ") outside of timerange of reader (" << InitialTimestamp << ") not dropped" << std::endl;
			}
		}
	}
	
	static bool IncrementByTimerange = false;
	if ( IncrementByTimerange )
	{
		Soy::Assert( mAssetReader, "Asset reader expected");
		auto* AssetReader = mAssetReader.mObject;
		auto InitialTimestamp = Soy::Platform::GetTime(AssetReader.timeRange.start);
		if ( PresentationTimestamp.IsValid() )
			PresentationTimestamp += InitialTimestamp;
		if ( DecodeTimestamp.IsValid() )
			DecodeTimestamp += InitialTimestamp;
	}
	
	static bool Debug_IsKeyFrame = false;
	if ( Debug_IsKeyFrame && IsKeyframe )
		std::Debug << "Frame " << PresentationTimestamp << " is keyframe!" << std::endl;
	
	//	gr: this needs to be more clever. like hold onto keyframe if the target time uses this keyframe...
	//		this applies to all platforms though so something generic would be good
	//	gr: if timestamp is "invalid", then push it through
	//	note: Invalid (CMTIME_IS_INVALID) is handled by the conversion func, but maybe 0 is a valid presentation timestamp? maybe the conversion should throw...
	if ( PresentationTimestamp.IsValid() )
	{
		//	gr: here we should check for the seek latch
		if ( !CanPushPacket( PresentationTimestamp, StreamIndex, IsKeyframe ) )
		{
			if ( !IsKeyframe )
			{
				std::Debug << "Skipping extracted timecode " << PresentationTimestamp << std::endl;
				SkipBuffer();
				return nullptr;
			}
			else
			{
				std::Debug << "Not skipped extracted frame as it's a keyframe!" << std::endl;
			}
		}
	}
	
	//	push sample buffer as new packet
	std::shared_ptr<TMediaPacket> pPacket( new TMediaPacket() );
	auto& Packet = *pPacket;

	//	gr: trying to remove the dependeancy on the OS formats
	static bool GenerateFormatInDecompressor = true;
	if ( !GenerateFormatInDecompressor )
	{
		Packet.mFormat.reset( new Platform::TMediaFormat( CMSampleBufferGetFormatDescription(SampleBuffer) ) );
	}
	

	//	get bytes, either blocks of data or a CVImageBuffer
	{
		CMBlockBufferRef BlockBuffer = CMSampleBufferGetDataBuffer( SampleBuffer );
		CVImageBufferRef ImageBuffer = CMSampleBufferGetImageBuffer( SampleBuffer );

		if ( BlockBuffer )
		{
			//	read bytes from block
			auto DataSize = CMBlockBufferGetDataLength( BlockBuffer );
			Packet.mData.SetSize( DataSize );
			auto Result = CMBlockBufferCopyDataBytes( BlockBuffer, 0, Packet.mData.GetDataSize(), Packet.mData.GetArray() );
			Avf::IsOkay( Result, "CMBlockBufferCopyDataBytes" );
		}
		else if ( ImageBuffer )
		{
			ReadPixelBuffer( ImageBuffer, GetArrayBridge(Packet.mData) );
		}
		else
		{
			//	h265, which doesn't decode on OSX, gives a null format (as well as not being able to extract packet data)
			if ( !Packet.mFormat )
			{
				std::Debug << "Failed to get BlockBuffer or ImageBuffer, failed to decode packet. Dropped." << std::endl;
				SkipBuffer();
				return nullptr;
			}

			//	gr: I think this is data-less, but format contains PPS/SPS data
			std::Debug << "Failed to get BlockBuffer or ImageBuffer... passing on raw packet format." << std::endl;

			//throw Soy::AssertException("Failed to get BlockBuffer or ImageBuffer.");
		}
		
		if ( BlockBuffer )
			CFRelease( BlockBuffer );
		
		if ( ImageBuffer )
			CFRelease( ImageBuffer );
	}

	//	copy meta
	Packet.mMeta = Avf::GetStreamMeta( CMSampleBufferGetFormatDescription(SampleBuffer) );
	Packet.mMeta.mStreamIndex = StreamIndex;
	
	//	for audio only?
	Packet.mMeta.mAudioSampleCount = CMSampleBufferGetNumSamples( SampleBuffer );
	/*
	CMSampleBufferGetAudioBufferListWithRetainedBlockBuffer(sampleBufferRef,
															NULL,
															&audioBufferList,
															sizeof(audioBufferList),
															NULL,
															NULL,
															0,
															&blockBuffer);
	
	int data_length = floorf(numSamplesInBuffer * 1.0f);
	
	int j = 0;
	
	for (int bufferCount=0; bufferCount < audioBufferList.mNumberBuffers; bufferCount++) {
		SInt16* samples = (SInt16 *)audioBufferList.mBuffers[bufferCount].mData;
		for (int i=0; i < numSamplesInBuffer; i++) {
			dataBuffer[j] = samples[i];
			j++;
		}
	}
	*/
	

	Packet.mTimecode = PresentationTimestamp;
	Packet.mDecodeTimecode = DecodeTimestamp;
	Packet.mDuration = SampleDuration;
	//GetPixelBufferManager().mOnFrameFound.OnTriggered( Timestamp );
	
	Packet.mIsKeyFrame = IsKeyframe;
	
	//	detect keyframe ourselves if h264 data
	if ( !Packet.mIsKeyFrame && SoyMediaFormat::IsH264( Packet.mMeta.mCodec ) )
	{
		//	gr: as of now, this doesn't work on non-nalu data. h264 code needs updating
		Packet.mIsKeyFrame = H264::IsKeyframe( Packet.mMeta.mCodec, GetArrayBridge(Packet.mData) );
	}

//	std::Debug << "Decoded packet: " << Packet.mTimecode << " format=" << Packet.mFormat << " size=" << Soy::FormatSizeBytes(Packet.mData.GetDataSize()) << std::endl;


	//	gr: missing?
	//	CMSampleBufferInvalidate( SampleBuffer );
	//	gr: this is sometimes crashing, refer back to old code to see if we're missing a retain
	//CFRelease(SampleBuffer);
	
	return pPacket;
}

bool AvfMediaExtractor::CanSeek()
{
	static bool AllowSeek = true;
	return AllowSeek;
}

bool AvfMediaExtractor::OnSeek()
{
	if ( !CanSeek() )
		return false;
	
	static bool DebugSeek = false;
	
	//	this may need to be at least KeyFrameDifference ms
	static int MinSeekDiffForwardMs = 1000;
	static int MinSeekDiffBackwardMs = -1000;
	auto SeekTime = GetSeekTime();
	
	auto SeekRealTime = GetExtractorRealTimecode( SeekTime );
	auto Diff = SeekRealTime.GetDiff( mLastReadTime );
	
	if ( Diff == 0 )
		return false;
	if ( Diff > MinSeekDiffBackwardMs && Diff < MinSeekDiffForwardMs )
	{
		if ( DebugSeek )
			std::Debug << "Skipping seek, difference only " << Diff << "/(" << MinSeekDiffBackwardMs << "..." << MinSeekDiffForwardMs << ")" << std::endl;
		return false;
	}

	//	recreate reader at the specific time
	CreateAssetReader();
	
	return true;
}

AvfAudioDecoder::AvfAudioDecoder(const std::string& ThreadName,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> InputBuffer,std::shared_ptr<TAudioBufferManager> OutputBuffer) :
	TMediaDecoder	( ThreadName, InputBuffer, OutputBuffer )
{
	Start();
}


//	note: this currently casts away const-ness (because of GetRemoteArray)
template<typename NEWTYPE,typename OLDTYPE>
inline FixedRemoteArray<NEWTYPE> CastArray(const ArrayBridge<OLDTYPE>&& Array)
{
	auto OldDataSize = Array.GetDataSize();
	//auto OldElementSize = Array.GetElementSize();
	auto NewElementSize = sizeof(NEWTYPE);
	
	auto NewElementCount = OldDataSize / NewElementSize;
	auto* NewData = reinterpret_cast<const NEWTYPE*>( Array.GetArray() );
	return GetRemoteArray( NewData, NewElementCount );
}

	
bool AvfAudioDecoder::ProcessPacket(const TMediaPacket& Packet)
{
	OnDecodeFrameSubmitted( Packet.mTimecode );

	auto& Output = GetAudioBufferManager();

	TAudioBufferBlock AudioBlock;
	AudioBlock.mChannels = Packet.mMeta.mChannelCount;
	AudioBlock.mFrequency = Packet.mMeta.mAudioSampleRate;
	AudioBlock.mStartTime = Packet.mTimecode;
	
	//	convert to float audio
	if ( Packet.mMeta.mCodec == SoyMediaFormat::PcmLinear_16 )
	{
		auto Data16 = CastArray<sint16>( GetArrayBridge(Packet.mData) );
		Wave::ConvertSamples( GetArrayBridge(Data16), GetArrayBridge(AudioBlock.mData) );
		Output.mOnFrameDecoded.OnTriggered( Packet.mTimecode );
		Output.PushAudioBuffer( AudioBlock );
	}
	else if ( Packet.mMeta.mCodec == SoyMediaFormat::PcmLinear_8 )
	{
		auto Data8 = CastArray<sint8>( GetArrayBridge(Packet.mData) );
		Wave::ConvertSamples( GetArrayBridge(Data8), GetArrayBridge(AudioBlock.mData) );
		Output.mOnFrameDecoded.OnTriggered( Packet.mTimecode );
		Output.PushAudioBuffer( AudioBlock );
	}
	else if ( Packet.mMeta.mCodec == SoyMediaFormat::PcmLinear_float )
	{
		auto Data8 = CastArray<float>( GetArrayBridge(Packet.mData) );
		Wave::ConvertSamples( GetArrayBridge(Data8), GetArrayBridge(AudioBlock.mData) );
		Output.mOnFrameDecoded.OnTriggered( Packet.mTimecode );
		Output.PushAudioBuffer( AudioBlock );
	}
	else
	{
		Output.mOnFrameDecodeFailed.OnTriggered( Packet.mTimecode );
		std::stringstream Error;
		Error << __func__ << " cannot handle " << Packet.mMeta.mCodec;
		throw Soy::AssertException( Error.str() );
	}
	
	return true;
}


void AvfAudioDecoder::ConvertPcmLinear16ToPcmFloat(const ArrayBridge<sint16>&& Input,ArrayBridge<float>&& Output)
{
	for ( int i=0;	i<Input.GetSize();	i++ )
	{
		float Samplef;
		Wave::ConvertSample( Input[i], Samplef );
		static float Gain = 1.f;
		Samplef *= Gain;
		Output.PushBack( Samplef );
	}
}


