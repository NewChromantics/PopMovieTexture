#pragma once

/*
	umbrella header for framework;
 https://developer.apple.com/library/mac/documentation/MacOSX/Conceptual/BPFrameworks/Tasks/IncludingFrameworks.html
 
*/

//	all the relevent headers for osx framework

//	meta classes
#include "PopMovieDecoder.h"

//	c++ instances
#include "PopMovieTexture.h"


//	C interfaces
#include "PopUnity.h"

/*
namespace PopMovieDecoder
{
	//	accessor to the decoder factory
	std::shared_ptr<TVideoDecoder>	AllocDecoder(TVideoDecoderParams Params);
};
*/
