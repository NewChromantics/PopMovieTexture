﻿using UnityEngine;
using System.Collections;



/// <summary>
///		PopMovieAutoSubtitles automatically syncs up with a PopMovieSimple component
///		and tries to load a neighbouring .srt file. It then pulls out subtitle strings for
///		the current frame and sets the text on an output UI text object
/// </summary>
[AddComponentMenu("PopMovie/PopMovieAutoSubtitles")]
public class PopMovieAutoSubtitles : MonoBehaviour {

	public PopMovieSimple		Movie;
	public PopMovie				Instance;
	public UnityEngine.UI.Text	SubtitlesTarget;
	public float				SubtitlesTimeOffsetSecs = 0;
	public PopMovieParams		Parameters;


	bool CreateInstance()
	{
		if (Movie == null)
			return false;

		//	look for subtitle file that goes with the movie filename
		var MovieFilename = Movie.Filename;
		//	remove extension
		var ExtPos = MovieFilename.LastIndexOf('.');
		if (ExtPos == -1)
		{
			Debug.LogWarning ("PopMovieAutoSubtitles could not determine subtitle filename from " + MovieFilename);
			return false;
		}
		var SubtitleFilename = MovieFilename.Substring (0,ExtPos);
		SubtitleFilename += ".srt";

		try
		{
			//	gr: it is important that reset internal timestamp is not applied 
			Parameters.ResetInternalTimestamp = false;

			Instance = new PopMovie( SubtitleFilename, Parameters, Movie.MovieTime );
		}
		catch(System.Exception e) {
			Debug.LogWarning("PopMovieAutoSubtitles failed to create movie: " + e.Message );
			return false;
		}

		return true;
	}

	void Awake () {

		if (!CreateInstance ()) {
			gameObject.SetActive (false);
			return;
		}
	}
	
	void Update () {
	
		if (Instance == null)
			return;

		Instance.SetTime (Movie.MovieTime + SubtitlesTimeOffsetSecs);

		//	get current subtitles
		var FrameSubtitle = Instance.GetText();
		if (FrameSubtitle != null) {
			if (SubtitlesTarget != null) {
				SubtitlesTarget.text = FrameSubtitle;
			} else if (FrameSubtitle != null) {
				Debug.Log ("Subtitle: " + FrameSubtitle);
			}
		}
	}
}

