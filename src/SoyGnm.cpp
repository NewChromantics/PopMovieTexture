#include "SoyGnm.h"
#include <SoyAssert.h>
#include <SoyPixels.h>
#include <SoyMath.h>
#include <SoyUnity.h>

#if !defined(ENABLE_GNM)
#error Cannot compile GNM support without ENABLE_GNM
#endif

using namespace sce::Gnm;

namespace Gnm
{
	SoyPixelsFormat::Type	GetFormat(DataFormat Format);
	SoyPixelsFormat::Type	GetFormat(RenderTargetFormat Format);
}



#include <Unity/IUnityGraphicsPS4.h>
/*
#include "Unity/IUnityGraphicsPS4.h"
#include "PS4/geommath.h"

extern "C" char _binary_VS_sb_start[];
extern "C" char _binary_PS_sb_start[];

sce::Gnmx::ShaderInfo	VS_info, PS_info;
sce::Gnmx::InputResourceOffsets VS_SRO, PS_SRO;

void *VS_fetchShader = NULL;
uint32_t VS_shaderModifier = 0;



void EnsurePS4ResourcesAreCreated()
{

	{
		sce::Gnmx::parseShader(&VS_info, _binary_VS_sb_start);

		VS_fetchShader = memalign(sce::Gnm::kAlignmentOfFetchShaderInBytes, sce::Gnmx::computeVsFetchShaderSize(VS_info.m_vsShader));	// TODO: this should be garlic memory
		sce::Gnmx::generateVsFetchShader(VS_fetchShader, &VS_shaderModifier, VS_info.m_vsShader, NULL);

		void * gpumem = memalign(sce::Gnm::kAlignmentOfShaderInBytes, VS_info.m_gpuShaderCodeSize);	// TODO: this should be garlic memory
		memcpy(gpumem, VS_info.m_gpuShaderCode, VS_info.m_gpuShaderCodeSize);
		VS_info.m_vsShader->patchShaderGpuAddress(gpumem);

		sce::Gnmx::generateInputResourceOffsetTable(&VS_SRO, sce::Gnm::kShaderStageVs, VS_info.m_vsShader);
	}

	{
		sce::Gnmx::parseShader(&PS_info, _binary_PS_sb_start);
		void * gpumem = memalign(256, PS_info.m_gpuShaderCodeSize);	// TODO: this should be garlic memory
		memcpy(gpumem, PS_info.m_gpuShaderCode, PS_info.m_gpuShaderCodeSize);
		PS_info.m_psShader->patchShaderGpuAddress(gpumem);

		sce::Gnmx::generateInputResourceOffsetTable(&PS_SRO, sce::Gnm::kShaderStagePs, PS_info.m_vsShader);

	}

}


// A dynamic vertex buffer just to demonstrate how to handle D3D9 device resets.
//static IDirect3DVertexBuffer9* g_D3D9DynamicVB;

static void DoEventGraphicsDevicePS4(UnityGfxDeviceEventType eventType)
{
	// Create or release a small dynamic vertex buffer depending on the event type.
	switch (eventType) {
		case kUnityGfxDeviceEventInitialize:
		{
			//IUnityGraphicsPS4* iPS4 = s_UnityInterfaces->Get<IUnityGraphicsPS4>();
			//g_gfxc = (sce::Gnmx::LightweightGfxContext *)iPS4->GetGfxContext();

			EnsurePS4ResourcesAreCreated();



	}
}

#endif // #if SUPPORT_PS4

/*

static void DoRendering (const float* worldMatrix, const float* identityMatrix, float* projectionMatrix, const MyVertex* verts)
{
// Does actual rendering of a simple triangle

#if SUPPORT_PS4
// PS4 case
if (s_DeviceType == kUnityGfxRendererPS4 )
{
IUnityGraphicsPS4* iPS4 = s_UnityInterfaces->Get<IUnityGraphicsPS4>();
sce::Gnmx::LightweightGfxContext *gfxc = (sce::Gnmx::LightweightGfxContext *)iPS4->GetGfxContext();


sce::Gnm::RenderTarget *renderTarget = (sce::Gnm::RenderTarget *)iPS4->GetCurrentRenderTarget(0);
sce::Gnm::DepthRenderTarget *depthRenderTarget = (sce::Gnm::DepthRenderTarget *)iPS4->GetCurrentDepthRenderTarget();


class ShaderConstants
{
public:
Matrix4Unaligned m_worldMatrix;
Matrix4Unaligned m_projectionMatrix;
};

int vertexCount = 3;
const int numBuffers = 2;
static sce::Gnm::Buffer VertexBuffers[numBuffers];

// Tweak the projection matrix a bit to make it match what identity projection would do in D3D case.
projectionMatrix[10] = 2.0f;
projectionMatrix[14] = -1.0f;

sce::Gnm::Buffer constBuffer;
static MyVertex *GPUVertexBuffer = NULL;
if (GPUVertexBuffer == NULL)
{
// The vertices need to be in allocated GPU mem, but as the vertices aren't changing in this demo we can just copy them the first time
int buffersize = vertexCount  * sizeof(MyVertex);
GPUVertexBuffer = (MyVertex *)iPS4->AllocateGPUMemory(buffersize, sce::Gnm::kAlignmentOfBufferInBytes);
memcpy(GPUVertexBuffer, verts, buffersize);

VertexBuffers[0].initAsVertexBuffer((void *)&GPUVertexBuffer->x, sce::Gnm::kDataFormatR32G32B32Float, sizeof(MyVertex), vertexCount);
VertexBuffers[0].setResourceMemoryType(sce::Gnm::kResourceMemoryTypeRO);
VertexBuffers[1].initAsVertexBuffer((void *)&GPUVertexBuffer->color, sce::Gnm::kDataFormatR8G8B8A8Unorm, sizeof(MyVertex), vertexCount);
VertexBuffers[1].setResourceMemoryType(sce::Gnm::kResourceMemoryTypeRO);

printf("renderTarget %dx%d\n", renderTarget->getWidth(), renderTarget->getHeight());

}

gfxc->pushMarker("Native Rendering Plugin", 0x00ff00);

gfxc->setShaderType(sce::Gnm::kShaderTypeGraphics);
gfxc->setActiveShaderStages(sce::Gnm::kActiveShaderStagesVsPs);
gfxc->setVsShader(VS_info.m_vsShader, VS_shaderModifier, VS_fetchShader, &VS_SRO);
gfxc->setPsShader(PS_info.m_psShader, &PS_SRO);

// as the shader constants change every frame we need to continually allocate memory for it ... using the command buffer is ideal
ShaderConstants *constants = (ShaderConstants*)gfxc->allocateFromCommandBuffer(sizeof(ShaderConstants), sce::Gnm::kEmbeddedDataAlignment4);
memcpy(&constants->m_projectionMatrix, projectionMatrix, sizeof(constants->m_projectionMatrix));
memcpy(&constants->m_worldMatrix, worldMatrix, sizeof(constants->m_worldMatrix));
constBuffer.initAsConstantBuffer(constants, sizeof(ShaderConstants));
gfxc->setConstantBuffers(sce::Gnm::kShaderStageVs, 0, 1, &constBuffer);

gfxc->setVertexBuffers(sce::Gnm::kShaderStageVs, 0, numBuffers, VertexBuffers);

gfxc->setPrimitiveType(sce::Gnm::kPrimitiveTypeTriList);
gfxc->drawIndexAuto(3);

gfxc->popMarker();

// update native texture from code
if (g_TexturePointer)
{
sce::Gnm::Texture *Texture = (sce::Gnm::Texture *)(g_TexturePointer);

// convert texture so that it will be rendered linearly ... this avoids having to re-tile it (but renders slower)
Texture->setTileMode(sce::Gnm::kTileModeDisplay_LinearAligned);

FillTextureFromCode(Texture->getWidth(), Texture->getHeight(), Texture->getHeight() * 4, (unsigned char *)Texture->getBaseAddress());
}

}
#endif
*/

sce::Gnm::DataFormat Gnm::GetFormat(SoyGraphics::TElementType::Type Format)
{
	switch( Format )
	{
		case SoyGraphics::TElementType::Float:	return kDataFormatR32Float;
		case SoyGraphics::TElementType::Float2:	return kDataFormatR32G32Float;
		case SoyGraphics::TElementType::Float3:	return kDataFormatR32G32B32Float;
		case SoyGraphics::TElementType::Float4:	return kDataFormatR32G32B32A32Float;
//		case SoyGraphics::TElementType::Int:	return kDataFormatR32G32B32A32Int;
	}

	std::stringstream Error;
	Error << "Unhandled Uniformat type " << Format << " to PS4 dataformat";
	throw Soy::AssertException( Error.str() );
}


SoyPixelsFormat::Type Gnm::GetFormat(RenderTargetFormat Format)
{
	switch ( Format )
	{
		default:
		case kRenderTargetFormatInvalid:	return SoyPixelsFormat::Invalid;
		case kRenderTargetFormat8:			return SoyPixelsFormat::Greyscale;
		case kRenderTargetFormat8_8:		return SoyPixelsFormat::GreyscaleAlpha;
		case kRenderTargetFormat8_8_8_8:	return SoyPixelsFormat::RGBA;
	}
}

SoyPixelsFormat::Type Gnm::GetFormat(DataFormat Format)
{
	return GetFormat( Format.getRenderTargetFormat() );
}



Gnm::TTexture::TTexture(void* TexturePtr) :
	mTexture		( reinterpret_cast<sce::Gnm::Texture*>(TexturePtr) )
{
	Soy::Assert( TexturePtr!=nullptr, "Texture is null");
	//	gr: verify texture with os
}
	
void Gnm::TTexture::Write(const SoyPixelsImpl& Pixels)
{
	static bool VerboseDebug = false;
	//	setup texture for linear reading (as we're writing linear)
	//	unity: // convert texture so that it will be rendered linearly ... this avoids having to re-tile it (but renders slower)
	
	//	gr: just in case there's some sync changing mode...
	auto TileMode = mTexture->getTileMode();
	if ( TileMode != sce::Gnm::kTileModeDisplay_LinearAligned )
		mTexture->setTileMode( sce::Gnm::kTileModeDisplay_LinearAligned );
	
	auto TargetPixels = GetPixels();
	if ( VerboseDebug )
		std::Debug << "PS4 copying " << Pixels.GetMeta() << " into texture " << TargetPixels.GetMeta() << std::endl;
	bool FlipSource = false;
	bool FlipDestination = true;

	ofScopeTimerWarning Timer("PS4 texture blit", 2 );
	TargetPixels.Copy( Pixels, TSoyPixelsCopyParams(true,true,true,FlipSource,FlipDestination) );
}

SoyPixelsMeta Gnm::TTexture::GetMeta() const
{
	auto& Texture = *mTexture;

	auto w = Texture.getWidth();
	auto h = Texture.getHeight();
	auto Format = GetFormat( Texture.getDataFormat() );

	//	pad if its padded internally
	bool Padded = Texture.isPaddedToPow2();
	if ( Padded )
	{
		w = SoyMath::GetNextPower2( w );
		h = SoyMath::GetNextPower2( h );
	}

	SoyPixelsMeta Meta( w, h, Format );

	//	verify pitch
	//	gr: pitch on PS4 is [padded]width, not stride
	auto Pitch = Texture.getPitch();
	if ( Pitch != Meta.GetWidth() )
	{
		std::Debug << "Warning: PS4 texture pitch(" << Pitch << ",  padded=" << Padded << ") (gr: not stride) doesn't match width " << Meta << std::endl;
	}
	
	return Meta;
}

SoyPixelsRemote Gnm::TTexture::GetPixels()
{
	auto& Texture = *mTexture;
	auto Meta = GetMeta();
	auto* Data = reinterpret_cast<uint8*>( Texture.getBaseAddress() );
	auto DataSize = Meta.GetDataSize();
	return SoyPixelsRemote( Data, DataSize, Meta );
}


void Gnm::IterateContext( Gnm::TContext& Context)
{
	Context.Iteration();
}


std::shared_ptr<Gnm::TContext> Gnm::AllocContext(void* DevicePtr,std::function<void*(size_t)> GpuAlloc,std::function<bool(void*)> GpuFree)
{
	auto* Device = reinterpret_cast<sce::Gnmx::LightweightGfxContext*>( DevicePtr );
	std::shared_ptr<Gnm::TContext> Context( new TContext( Device, GpuAlloc, GpuFree ) );
	return Context;
}



Gnm::TContext::TContext(sce::Gnmx::LightweightGfxContext* Context,std::function<void*(size_t)> GpuAlloc,std::function<bool(void*)> GpuFree) :
	mContext	( Context ),
	mHeap		( GpuAlloc, GpuFree )
{
	if ( mContext == nullptr )
		std::Debug << "Warning sce::Gnmx::LightweightGfxContext from unity is null" << std::endl;
	//Soy::Assert( mContext!=nullptr, "Missing Gnmx context");
}

void Gnm::TContext::Iteration()		
{
	//	look for context
	if ( !mContext )
	{
		std::Debug << "Gnm context is null, Trying to get PS4 context..." << std::endl;
		mContext = reinterpret_cast<sce::Gnmx::LightweightGfxContext*>( Unity::GetPlatformDeviceContext() );
	}

	Flush(*this);	
}

sce::Gnmx::LightweightGfxContext& Gnm::TContext::GetContext()
{
	if ( !mContext )
		throw Soy::AssertException("GNM context not yet acquired");
	return *mContext;
}



/*
void EnsurePS4ResourcesAreCreated()
{
	
	{
		sce::Gnmx::parseShader(&VS_info, _binary_VS_sb_start);
		
		VS_fetchShader = memalign(sce::Gnm::kAlignmentOfFetchShaderInBytes, sce::Gnmx::computeVsFetchShaderSize(VS_info.m_vsShader));	// TODO: this should be garlic memory
		sce::Gnmx::generateVsFetchShader(VS_fetchShader, &VS_shaderModifier, VS_info.m_vsShader, NULL);
		
		void * gpumem = memalign(sce::Gnm::kAlignmentOfShaderInBytes, VS_info.m_gpuShaderCodeSize);	// TODO: this should be garlic memory
		memcpy(gpumem, VS_info.m_gpuShaderCode, VS_info.m_gpuShaderCodeSize);
		VS_info.m_vsShader->patchShaderGpuAddress(gpumem);
		
		sce::Gnmx::generateInputResourceOffsetTable(&VS_SRO, sce::Gnm::kShaderStageVs, VS_info.m_vsShader);
	}
	
	{
		sce::Gnmx::parseShader(&PS_info, _binary_PS_sb_start);
		void * gpumem = memalign(256, PS_info.m_gpuShaderCodeSize);	// TODO: this should be garlic memory
		memcpy(gpumem, PS_info.m_gpuShaderCode, PS_info.m_gpuShaderCodeSize);
		PS_info.m_psShader->patchShaderGpuAddress(gpumem);
		
		sce::Gnmx::generateInputResourceOffsetTable(&PS_SRO, sce::Gnm::kShaderStagePs, PS_info.m_vsShader);
		
	}
	
}

*/

Gnm::TGpuHeap::TGpuHeap(std::function<void*(size_t)> GpuAlloc,std::function<bool(void*)> GpuFree) :
	prmem::Heap	( false, false, "GNM GPU heap", 0, false ),
	mAlloc		( GpuAlloc ),
	mFree		( GpuFree )
{
}


void* Gnm::TGpuHeap::RealAllocImpl(size_t Size)
{
	auto* Allocation = mAlloc( Size );
	if ( !Allocation )
	{
		std::stringstream Error;
		Error << "GPU failed to allocate " << Size << " bytes";
		throw Soy::AssertException( Error.str() );
	}
	return Allocation;
}

bool Gnm::TGpuHeap::CanFreeImpl(void* Object,size_t Size)
{
	return true;
}

bool Gnm::TGpuHeap::RealFreeImpl(void* Object,size_t Size)
{
	std::Debug << "GPU free(" << Size << ")" << std::endl;
	return mFree( Object );
}

