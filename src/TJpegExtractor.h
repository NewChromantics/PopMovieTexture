#pragma once

#include "TStreamDecoder.h"



class TJpegMediaPacket_Protocol : public TMediaPacket_Protocol
{
public:
	TJpegMediaPacket_Protocol() :
		mTries		( 0 )
	{
	}
	virtual TProtocolState::Type	Decode(TStreamBuffer& Buffer) override;

	virtual void					GetPackets(ArrayBridge<std::shared_ptr<TMediaPacket>>&& Packets) override;
	
public:
	std::shared_ptr<Jpeg::TMeta>			mJpegMeta;
	size_t									mTries;
	Array<std::shared_ptr<TMediaPacket>>	mAdditionalPackets;		//	packets for VRjpegs which have embedded stuff. could also list other meta in a text stream
};



//	jpeg has a special extractor in case there is hidden stuff in the exif/comments like the google cardboard
//	based on ~TRawFileExtractor
class TJpegFileExtractor : public TRawFileExtractor<TJpegMediaPacket_Protocol>
{
public:
	TJpegFileExtractor(const TMediaExtractorParams& Params) :
		TRawFileExtractor	( Params )
	{
	}
};

