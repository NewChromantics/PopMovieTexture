#include "PopMovieTexture.h"
#include <sstream>
#include "PopMovieDecoder.h"
#include <RemoteArray.h>
#include "TestMovieDecoder.h"
#include "TDecoderPlaylist.h"
#include "TStreamDecoder.h"
#include "TPerformanceGraph.h"
#include "TAudioGraph.h"
#include "SoyMpegts.h"
#include "PopUnity.h"
#include "SoyFilesystem.h"
#include "TSourceManager.h"
#include "TJpegExtractor.h"
#include "TGifExtractor.h"
#include "TPushExtractor.h"
#include <SoyJson.h>

#if defined(TARGET_WINDOWS)
#include "SoyDecklink.h"
#endif

#if defined(ENABLE_OPENGL)
#include "TBlitterOpengl.h"
#endif

#if defined(TARGET_ANDROID)
#include "AndroidMovieDecoder.h"
#endif

#if defined(TARGET_OSX) || defined(TARGET_IOS)
#include "AvfMovieDecoder.h"
#include "AvfCompressor.h"
#include "AvfVideoCapture.h"
#endif

#if defined(TARGET_WINDOWS) && defined(ENABLE_CUDA)
#include "CudaMovieDecoder.h"
#endif

#if defined(ENABLE_DIRECTX)
#include "TBlitterDirectx.h"
#endif

#if defined(ENABLE_DIRECTX9)
#include "TBlitterDirectx9.h"
#endif

#if defined(TARGET_WINDOWS)
#include "MfDecoder.h"
#include "MfCapture.h"
#include "HwndExtractor.h"
#endif

#if defined(TARGET_OSX)
#include "SoyKinect.h"
#include "CGWindowExtractor.h"
#endif

#if defined(ENABLE_METAL)
#include "TBlitterMetal.h"
#endif

#if defined(ENABLE_GNM)
#include "SoyGnm.h"
#include "TBlitterGnm.h"
#endif

//	gr: always included for the file extensions
#include "SoyBink.h"

#define POPPIXELBUFFER_TIMER_MIN	3
#define MAX_PIXEL_BUFFER_PLANES		4

namespace Soy
{
	template<typename TYPE>
	TYPE	ArrayMax(const ArrayBridge<TYPE>&& Values);

	template<typename TYPE>
	TYPE	ArrayMin(const ArrayBridge<TYPE>&& Values);
}


template<typename TYPE>
inline TYPE Soy::ArrayMax(const ArrayBridge<TYPE>&& Values)
{
	if ( Values.IsEmpty() )
		return TYPE();
	
	auto Max = Values[0];
	for ( int i=1;	i<Values.GetSize();	i++ )
		Max = std::max( Max, Values[i] );
	return Max;
}

template<typename TYPE>
inline TYPE Soy::ArrayMin(const ArrayBridge<TYPE>&& Values)
{
	if ( Values.IsEmpty() )
		return TYPE();
	
	auto Min = Values[0];
	for ( int i=1;	i<Values.GetSize();	i++ )
		Min = std::min( Min, Values[i] );
	return Min;
}


namespace PopMovie
{
	std::mutex											gInstancesLock;
	std::vector<std::shared_ptr<PopMovie::TInstance> >	gInstances;
	PopMovie::TPopMovieDecoderFactory					gVideoDecoderFactory;
	std::shared_ptr<TSourceManager>						gSourceManager;
	
	std::shared_ptr<TExportManager<std::string,char>>	gExportStringManager;

	
	void				AllocShutdownCallback();
	SoyListenerId		mOnUnityShutdownListener;
};


TExportManager<std::string,char>& PopMovie::GetExportStringManager()
{
	if ( !gExportStringManager )
		gExportStringManager.reset( new TExportManager<std::string,char>(100) );
	return *gExportStringManager;
}


void PopMovie::AllocShutdownCallback()
{
	if ( mOnUnityShutdownListener.IsValid() )
		return;
	
	auto Shutdown = [](bool&)
	{
#if defined(TARGET_OSX)
		Kinect::FreeContext();
#endif
	};

	//	catch shutdown
	mOnUnityShutdownListener = Unity::GetOnDeviceShutdown().AddListener( Shutdown );
}


std::shared_ptr<PopMovie::TInstance> PopMovie::Alloc(const TVideoDecoderParams& Params)
{
	static std::atomic<Unity::uint> gInstanceRefCounter( 1000 );
	gInstanceRefCounter++;

	//	try to alloc, if this fails it'll throw
	std::shared_ptr<TInstance> pInstance;
	{
		ofScopeTimerWarning Timer("PopMovie::Alloc alloc instance", Unity::mMinTimerMs );
		pInstance.reset( new TInstance( TInstanceRef(gInstanceRefCounter) ,Params) );
	}
	{
		ofScopeTimerWarning Timer("PopMovie::Alloc instances lock", Unity::mMinTimerMs );
		gInstancesLock.lock();
		gInstances.push_back( pInstance );
		gInstancesLock.unlock();
	}

	return pInstance;
}

std::shared_ptr<PopMovie::TInstance> PopMovie::GetInstance(TInstanceRef Instance)
{
	for ( int i=0;	i<gInstances.size();	i++ )
	{
		auto& pInstance = gInstances[i];
		if ( pInstance->GetRef() == Instance )
			return pInstance;
	}
	return std::shared_ptr<PopMovie::TInstance>();
}

bool PopMovie::Free(TInstanceRef InstanceRef)
{
	//	shutdown can take a while, so pop the instance before deleting it
	//	(and consider putting it on some shutdown thread
	std::shared_ptr<PopMovie::TInstance> Instance;
	
	gInstancesLock.lock();
	for ( int i=0;	i<gInstances.size();	i++ )
	{
		auto& pInstance = gInstances[i];
		if ( pInstance->GetRef() != InstanceRef )
			continue;
		Instance = pInstance;
		gInstances.erase( gInstances.begin()+ i );
		break;
	}
	gInstancesLock.unlock();
	
	if ( !Instance )
		return false;
	
	//	consider putting this on another thread so mono doesn't have to wait, but may need to keep an eye on it so it gets cleaned up when the app shutsdown
	//	this does appear to be on a different thread (garbage collector) though, so may be okay (seems okay so far)
	Instance->Shutdown();
	Instance.reset();
	
	return true;
}

extern std::string GetStreamMetaPrefix(size_t StreamIndex);

size_t PopMovie::TInstance::GetMetaRevision()
{
	auto Decoder = mDecoder;
	Soy::Assert( Decoder != nullptr, "Missing decoder");
	return Decoder->GetMetaRevision();
}

void PopMovie::TInstance::GetMeta(TJsonWriter& Json)
{
	Json.Push("CurrentTimeMs", mTimestamp.mTime );

	auto Decoder = mDecoder;
	if ( Decoder )
	{
		Decoder->GetMeta( Json );
	}
	
#if defined(ENABLE_OPENGL)
	auto OpenglBlitter = mOpenglBlitter;
	if ( OpenglBlitter )
		OpenglBlitter->GetMeta( Json );
#endif
	
#if defined(ENABLE_DIRECTX)
	auto DirectxBlitter = mDirectxBlitter;
	if ( DirectxBlitter )
		DirectxBlitter->GetMeta( Json );
#endif
	
	for ( auto it=mPerformanceStats.begin();	it!=mPerformanceStats.end();	it++ )
	{
		auto& Stats = it->second;
		auto& StreamIndex = it->first;
		auto Prefix = GetStreamMetaPrefix(StreamIndex);
		Stats.GetMeta( Prefix, Json );
	}
}

SoyMediaFormat::Type PopMovie::TInstance::GetStreamFormat(size_t StreamIndex)
{
	auto pDecoder = mDecoder;
	if ( !pDecoder )
		return SoyMediaFormat::Invalid;

	try
	{
		auto Meta = pDecoder->GetStreamMeta(StreamIndex);
		return Meta.mCodec;
	}
	catch(...)
	{
		return SoyMediaFormat::Invalid;
	}
}

bool PopMovie::TInstance::HasNewPixelBuffer(size_t StreamIndex)
{
	if ( !mDecoder )
		return false;

	auto& Buffer = mDecoder->GetPixelBufferManager( StreamIndex );
	return Buffer.PeekPixelBuffer( mTimestamp );
}

void PopMovie::TInstance::OnFrameCopied(SoyTime Time,size_t StreamIndex)
{
	std::pair<SoyTime,size_t> TimeAndStream( Time, StreamIndex );
	mOnFrameCopied.OnTriggered( TimeAndStream );

	//	todo: convert these callbacks to SoyEvent listeners to reduce the locks
	if ( !mOnFrameCopiedCallbacks.IsEmpty() )
	{
		std::lock_guard<std::mutex> Lock( mOnFrameCopiedCallbacksLock );
		try
		{
			for ( int i=0;	i<mOnFrameCopiedCallbacks.GetSize();	i++ )
			{
				auto& Callback = mOnFrameCopiedCallbacks[i];
				if ( Callback.mStreamIndex != StreamIndex )
					continue;

				Callback.Call( Time );
			}
		}
		catch(std::exception& e)
		{
			std::Debug << "Exception calling OnFrameCopied callbacks; " << e.what() << std::endl;
		}
	}
}

void PopMovie::TInstance::RegisterOnTextureCopied(size_t VideoStreamIndex,TOnFrameCopiedCallbackFunc CallbackFunc)
{
	auto StreamIndex = ResolveVideoStreamIndex( VideoStreamIndex );

	TOnFrameCopiedCallback Callback( CallbackFunc, StreamIndex );

	std::lock_guard<std::mutex> Lock( mOnFrameCopiedCallbacksLock );
	mOnFrameCopiedCallbacks.PushBack( Callback );
}

void PopMovie::TInstance::UnregisterOnTextureCopied(size_t VideoStreamIndex,TOnFrameCopiedCallbackFunc MatchCallback)
{
	std::lock_guard<std::mutex> Lock( mOnFrameCopiedCallbacksLock );
	
	for ( int i=mOnFrameCopiedCallbacks.GetSize()-1;	i>=0;	i-- )
	{
		auto& Callback = mOnFrameCopiedCallbacks[i];
		if ( Callback.mCallback != MatchCallback )
			continue;

		mOnFrameCopiedCallbacks.RemoveBlock( i, 1 );
	}
}


//-------------------------------------
//	returns if texture changed. throw on error. No data/no path is not an error!
//-------------------------------------
#if defined(ENABLE_OPENGL)
bool PopMovie::TInstance::UpdateTexture(Opengl::TTexture& Texture,size_t StreamIndex,SoyTime CopyTimestamp)
{
	Soy::TScopeTimer UpdateTextureTimer = GetUpdateTextureTotalTimer(StreamIndex);
	
	if ( !Soy::Assert( Texture.IsValid(), "Invalid texture supplied" ) )
		return false;
	
	//	save ptr for thread safety
	auto pDecoder = mDecoder;
	if ( !pDecoder )
		return false;
	auto& Decoder = *pDecoder;
	
	//	allocate the blitter
	if ( !mOpenglBlitter )
	{
		mOpenglBlitter.reset( new Opengl::TBlitter(nullptr) );
	}
	auto& Blitter = *mOpenglBlitter;
	auto& Context = Unity::GetOpenglContext();
	auto& PixelBufferManager = Decoder.GetPixelBufferManager(StreamIndex);
	
	//	check for [deffered] fatal errors
	//	gr: will handle this differently later, but for now display an error shader
	std::string FatalError;
	if ( Decoder.HasFatalError( FatalError ) )
	{
		//	flush out frames
		PixelBufferManager.ReleaseFrames();
		
		if ( Decoder.mParams.mBlitFatalErrors )
		{
			//std::Debug << "Fatal error: " << FatalError << std::endl;
			Blitter.BlitError( Texture, FatalError, Context );
		}
		return true;
	}
	
	ofScopeTimerWarning PopPixelBuffer_Timer("Decoder::PopPixelBuffer",POPPIXELBUFFER_TIMER_MIN);
	SoyTime PixelTimestamp = CopyTimestamp.IsValid() ? CopyTimestamp : mTimestamp;
	auto PixelBuffer = PixelBufferManager.PopPixelBuffer( PixelTimestamp );
	PopPixelBuffer_Timer.Stop();
	
	//	no new pixel buffer
	if ( !PixelBuffer )
	{
		if ( Decoder.mParams.mDebugNoNewPixelBuffer )
			std::Debug << "No new pixel buffer for " << mTimestamp << std::endl;
		return false;
	}
	
	if ( Decoder.mParams.mDebugNoNewPixelBuffer )
		std::Debug << "Blitting pixel buffer (" << PixelTimestamp << ") for " << mTimestamp << std::endl;

	
	//	set transform
	Blitter.SetUseTestBlitShader( Decoder.mParams.mDebugBlit );
	Blitter.SetMergeYuv( Decoder.mParams.mBlitMergeYuv );
	Blitter.SetClearBeforeBlit( Decoder.mParams.mClearBeforeBlit );
	
	//	error to display if we fast & slow paths fail and we do a last-chance error-blit
	std::stringstream LastPathError;
	
	if ( Decoder.mParams.mAllowFastCopy )
	{
		try
		{
			auto& Context = Unity::GetOpenglContext();
			BufferArray<Opengl::TTexture,MAX_PIXEL_BUFFER_PLANES> Textures;
			float3x3 Transform;
			{
				Soy::TScopeTimer LockTimer = GetUpdateTextureLockTimer(StreamIndex);
				PixelBuffer->Lock( GetArrayBridge(Textures), Context, Transform );
			}
			if ( !Textures.IsEmpty() )
			{
				{
					Soy::TScopeTimer BlitTimer = GetUpdateTextureBlitTimer(StreamIndex);
					Blitter.SetTransform( Decoder.mParams.mApplyVideoTransform ? Transform : float3x3() );
					Blitter.BlitTexture( Texture, GetArrayBridge(Textures), Context, Decoder.mParams.mTextureUploadParams );
					if ( Decoder.mParams.mDrawTimestamp )
						Blitter.BlitNumber( Texture, Texture, PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
				}
				OnFrameCopied( PixelTimestamp, StreamIndex );
				PixelBuffer->Unlock();
				return true;
			}
		}
		catch(std::exception& e)
		{
			LastPathError << "FastCopy failed: " << e.what();
			PixelBuffer->Unlock();
		}
	}
	
	if ( Decoder.mParams.mAllowSlowCopy )
	{
		try
		{
			float3x3 Transform;
			BufferArray<SoyPixelsImpl*,MAX_PIXEL_BUFFER_PLANES> Textures;
			{
				Soy::TScopeTimer LockTimer = GetUpdateTextureLockTimer(StreamIndex);
				PixelBuffer->Lock( GetArrayBridge(Textures), Transform );
			}
			//	opengl path
			auto& Context = Unity::GetOpenglContext();
			if ( !Textures.IsEmpty() )
			{
				BufferArray<const SoyPixelsImpl*,MAX_PIXEL_BUFFER_PLANES> Textures_Const( Textures );
				{
					Soy::TScopeTimer BlitTimer = GetUpdateTextureBlitTimer(StreamIndex);
					Blitter.SetTransform( Decoder.mParams.mApplyVideoTransform ? Transform : float3x3() );
					Blitter.BlitTexture( Texture, GetArrayBridge(Textures_Const), Context, Decoder.mParams.mTextureUploadParams );
					if ( Decoder.mParams.mDrawTimestamp )
						Blitter.BlitNumber( Texture, Texture, PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
				}
				OnFrameCopied( PixelTimestamp, StreamIndex );
				PixelBuffer->Unlock();
				return true;
			}
		}
		catch(std::exception& e)
		{
			LastPathError << "SlowCopy failed: " << e.what();
			PixelBuffer->Unlock();
		}
	}

	//	if we reached here, fast failed, and slow failed.
	//	try and draw the error shader as a last chance
	if ( Decoder.mParams.mBlitFatalErrors )
	{
		auto& Context = Unity::GetOpenglContext();
		try
		{
			std::string Error = LastPathError.str();
			if ( Error.empty() )
			{
				LastPathError << "Fast & slow path blit of " << PixelTimestamp << " failed";
				Error = LastPathError.str();
			}
			Blitter.BlitError( Texture, Error, Context );
			if ( Decoder.mParams.mDrawTimestamp )
				Blitter.BlitNumber( Texture, Texture, PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
			std::Debug << Error << std::endl;
			OnFrameCopied( PixelTimestamp, StreamIndex );
			return true;
		}
		catch(std::exception& e)
		{
			std::Debug << "Last-path blit-error failed: " << e.what() << std::endl;
		}
	}
	return false;
}
#endif



//-------------------------------------
//	returns if texture changed. throw on error. No data/no path is not an error!
//-------------------------------------
#if defined(ENABLE_METAL)
bool PopMovie::TInstance::UpdateTexture(Metal::TTexture& Texture,size_t StreamIndex,SoyTime CopyTimestamp)
{
	namespace Renderer = Metal;
	Soy::TScopeTimer UpdateTextureTimer = GetUpdateTextureTotalTimer(StreamIndex);
	
	if ( !Soy::Assert( Texture.IsValid(), "Invalid texture supplied" ) )
		return false;
	
	//	save ptr for thread safety
	auto pDecoder = mDecoder;
	if ( !pDecoder )
		return false;
	auto& Decoder = *pDecoder;
	
	//	allocate the blitter
	if ( !mMetalBlitter )
	{
		mMetalBlitter.reset( new Renderer::TBlitter(nullptr) );
	}
	auto& Blitter = *mMetalBlitter;
	auto& Context = Unity::GetMetalContext();
	auto& PixelBufferManager = Decoder.GetPixelBufferManager(StreamIndex);
	
	//	check for [deffered] fatal errors
	//	gr: will handle this differently later, but for now display an error shader
	std::string FatalError;
	if ( Decoder.HasFatalError( FatalError ) )
	{
		//	flush out frames
		PixelBufferManager.ReleaseFrames();
		
		if ( Decoder.mParams.mBlitFatalErrors )
		{
			//std::Debug << "Fatal error: " << FatalError << std::endl;
			Blitter.BlitError( Texture, FatalError, Context );
		}
		return true;
	}
	
	ofScopeTimerWarning PopPixelBuffer_Timer("Decoder::PopPixelBuffer",POPPIXELBUFFER_TIMER_MIN);
	SoyTime PixelTimestamp = CopyTimestamp.IsValid() ? CopyTimestamp : mTimestamp;
	auto PixelBuffer = PixelBufferManager.PopPixelBuffer( PixelTimestamp );
	PopPixelBuffer_Timer.Stop();
	
	//	no new pixel buffer
	if ( !PixelBuffer )
	{
		if ( Decoder.mParams.mDebugNoNewPixelBuffer )
			std::Debug << "No new pixel buffer for " << mTimestamp << std::endl;
		return false;
	}
	
	if ( Decoder.mParams.mDebugNoNewPixelBuffer )
		std::Debug << "Blitting pixel buffer (" << PixelTimestamp << ") for " << mTimestamp << std::endl;
	
	
	//	set transform
	Blitter.SetUseTestBlitShader( Decoder.mParams.mDebugBlit );
	Blitter.SetMergeYuv( Decoder.mParams.mBlitMergeYuv );
	Blitter.SetClearBeforeBlit( Decoder.mParams.mClearBeforeBlit );

	//	error to display if we fast & slow paths fail and we do a last-chance error-blit
	std::stringstream LastPathError;
	
	if ( Decoder.mParams.mAllowFastCopy )
	{
		try
		{
			BufferArray<Renderer::TTexture,MAX_PIXEL_BUFFER_PLANES> Textures;
			float3x3 Transform;
			{
				Soy::TScopeTimer LockTimer = GetUpdateTextureLockTimer(StreamIndex);
				PixelBuffer->Lock( GetArrayBridge(Textures), Context, Transform );
			}
			if ( !Textures.IsEmpty() )
			{
				{
					Soy::TScopeTimer BlitTimer = GetUpdateTextureBlitTimer(StreamIndex);
					Blitter.SetTransform( Decoder.mParams.mApplyVideoTransform ? Transform : float3x3() );
					Blitter.BlitTexture( Texture, GetArrayBridge(Textures), Context, Decoder.mParams.mTextureUploadParams );
					if ( Decoder.mParams.mDrawTimestamp )
						Blitter.BlitNumber( Texture, Texture, PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
				}
				OnFrameCopied( PixelTimestamp, StreamIndex );
				PixelBuffer->Unlock();
				return true;
			}
		}
		catch(std::exception& e)
		{
			LastPathError << "FastCopy failed: " << e.what();
			PixelBuffer->Unlock();
		}
	}
	
	if ( Decoder.mParams.mAllowSlowCopy )
	{
		try
		{
			float3x3 Transform;
			BufferArray<SoyPixelsImpl*,MAX_PIXEL_BUFFER_PLANES> Textures;
			{
				Soy::TScopeTimer LockTimer = GetUpdateTextureLockTimer(StreamIndex);
				PixelBuffer->Lock( GetArrayBridge(Textures), Transform );
			}
			//	opengl path
			if ( !Textures.IsEmpty() )
			{
				BufferArray<const SoyPixelsImpl*,MAX_PIXEL_BUFFER_PLANES> Textures_Const( Textures );
				{
					Soy::TScopeTimer BlitTimer = GetUpdateTextureBlitTimer(StreamIndex);
					Blitter.SetTransform( Decoder.mParams.mApplyVideoTransform ? Transform : float3x3() );
					Blitter.BlitTexture( Texture, GetArrayBridge(Textures_Const), Context, Decoder.mParams.mTextureUploadParams );
					if ( Decoder.mParams.mDrawTimestamp )
						Blitter.BlitNumber( Texture, Texture, PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
				}
				OnFrameCopied( PixelTimestamp, StreamIndex );
				PixelBuffer->Unlock();
				return true;
			}
		}
		catch(std::exception& e)
		{
			LastPathError << "SlowCopy failed: " << e.what();
			PixelBuffer->Unlock();
		}
	}
	
	//	if we reached here, fast failed, and slow failed.
	//	try and draw the error shader as a last chance
	if ( Decoder.mParams.mBlitFatalErrors )
	{
		try
		{
			std::string Error = LastPathError.str();
			if ( Error.empty() )
			{
				LastPathError << "Fast & slow path blit of " << PixelTimestamp << " failed";
				Error = LastPathError.str();
			}
			Blitter.BlitError( Texture, Error, Context );
			if ( Decoder.mParams.mDrawTimestamp )
				Blitter.BlitNumber( Texture, Texture, PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
			std::Debug << Error << std::endl;
			OnFrameCopied( PixelTimestamp, StreamIndex );
			return true;
		}
		catch(std::exception& e)
		{
			std::Debug << "Last-path blit-error failed: " << e.what() << std::endl;
		}
	}
	
	return false;
}
#endif


//-------------------------------------
//	returns if texture changed. throw on error. No data/no path is not an error!
//-------------------------------------
#if defined(ENABLE_DIRECTX)
bool PopMovie::TInstance::UpdateTexture(Directx::TTexture& Texture,size_t StreamIndex,SoyTime CopyTimestamp)
{
	Soy::TScopeTimer UpdateTextureTimer = GetUpdateTextureTotalTimer(StreamIndex);

	if ( !Soy::Assert( Texture.IsValid(), "Invalid texture supplied" ) )
		return false;
	
	//	save ptr for thread safety
	auto pDecoder = mDecoder;
	if ( !pDecoder )
		return false;
	auto& Decoder = *pDecoder;

	//	allocate the blitter
	if ( !mDirectxBlitter )
		mDirectxBlitter.reset( new Directx::TBlitter(nullptr) );
	auto& Blitter = *mDirectxBlitter;
	auto& Context = Unity::GetDirectxContext();

	//	check for [deffered] fatal errors
	//	gr: will handle this differently later, but for now display an error shader
	std::string FatalError;
	if ( Decoder.HasFatalError( FatalError ) )
	{
		//	flush out frames
		Decoder.GetPixelBufferManager(StreamIndex).ReleaseFrames();

		if ( Decoder.mParams.mBlitFatalErrors )
		{
			//std::Debug << "Fatal error: " << FatalError << std::endl;
			Blitter.BlitError( Texture, FatalError, Context );
		}
		return true;
	}
	
	ofScopeTimerWarning PopPixelBuffer_Timer("Decoder::PopPixelBuffer",POPPIXELBUFFER_TIMER_MIN);

	SoyTime PixelTimestamp = CopyTimestamp.IsValid() ? CopyTimestamp : mTimestamp;
	auto PixelBuffer = Decoder.GetPixelBufferManager(StreamIndex).PopPixelBuffer( PixelTimestamp );
	PopPixelBuffer_Timer.Stop();
	
	//	no new pixel buffer
	if ( !PixelBuffer )
	{
		if ( Decoder.mParams.mDebugNoNewPixelBuffer )
			std::Debug << "No new pixel buffer for " << PixelTimestamp << std::endl;
		return false;
	}
	
	if ( Decoder.mParams.mDebugNoNewPixelBuffer )
		std::Debug << "Blitting pixel buffer (" << PixelTimestamp << ") for " << mTimestamp << std::endl;

	//	set transform
	Blitter.SetUseTestBlitShader( Decoder.mParams.mDebugBlit );
	Blitter.SetMergeYuv( Decoder.mParams.mBlitMergeYuv );
	Blitter.SetClearBeforeBlit( Decoder.mParams.mClearBeforeBlit );

	//	error to display if we fast & slow paths fail and we do a last-chance error-blit
	std::stringstream LastPathError;

	if ( Decoder.mParams.mAllowFastCopy )
	{
		BufferArray<Directx::TTexture,MAX_PIXEL_BUFFER_PLANES> Textures;
		float3x3 Transform;
		{
			Soy::TScopeTimer LockTimer = GetUpdateTextureLockTimer(StreamIndex);
			PixelBuffer->Lock( GetArrayBridge(Textures), Context, Transform );
		}
		if ( !Textures.IsEmpty() )
		{
			try
			{
				//	gr: note: no use of transform here (or watermark blit?)
				//		should probably force this through the blitter
				std::Debug << "Gr: force this through blitter" << std::endl;
				{
					Soy::TScopeTimer BlitTimer = GetUpdateTextureBlitTimer(StreamIndex);
					Texture.Write( Textures[0], Context );
					if ( Decoder.mParams.mDrawTimestamp )
						Blitter.BlitNumber( Textures[0], Textures[0], PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
				}
				OnFrameCopied(PixelTimestamp,StreamIndex);
				PixelBuffer->Unlock();
				return true;
			}
			catch(std::exception& e)
			{
				LastPathError << "Directx texture->texture copy failed: " << e.what() << ". ";
				PixelBuffer->Unlock();
			}
		}
	}

	//	use blitter (copy to temporary dynamic texture, then GPU copy, for when we don't have write access to the texture)
	//	gr: in opengl this has all been merged into the blitter
	//	gr: code above currently creates blitter regardless (like opengl), maybe remove this implicit creation/dependency
	//if ( mDirectxBlitter )
	if ( true )
	{
		auto& Context = Unity::GetDirectxContext();
		BufferArray<SoyPixelsImpl*,MAX_PIXEL_BUFFER_PLANES> Textures;
		float3x3 Transform;
		{
			Soy::TScopeTimer LockTimer = GetUpdateTextureLockTimer(StreamIndex);
			PixelBuffer->Lock( GetArrayBridge(Textures), Transform );
		}
		if ( !Textures.IsEmpty() )
		{
			try
			{
				BufferArray<const SoyPixelsImpl*,MAX_PIXEL_BUFFER_PLANES> Textures_Const( Textures );
				{
					Soy::TScopeTimer BlitTimer = GetUpdateTextureBlitTimer(StreamIndex);
					Blitter.SetTransform( Decoder.mParams.mApplyVideoTransform ? Transform : float3x3() );
					Blitter.BlitTexture(Texture, GetArrayBridge(Textures_Const), Context );
					if ( Decoder.mParams.mDrawTimestamp )
						Blitter.BlitNumber( Texture, Texture, PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
				}
				OnFrameCopied(PixelTimestamp, StreamIndex);
				PixelBuffer->Unlock();
				return true;
			}
			catch ( std::exception& e )
			{
				LastPathError << "Blit " << Textures.GetSize() << "x pixels failed: " << e.what() << ". ";
				PixelBuffer->Unlock();
			}
		}
	}

	if ( Decoder.mParams.mAllowSlowCopy )
	{
		BufferArray<SoyPixelsImpl*,MAX_PIXEL_BUFFER_PLANES> Textures;
		float3x3 Transform;
		{
			Soy::TScopeTimer LockTimer = GetUpdateTextureLockTimer(StreamIndex);
			PixelBuffer->Lock( GetArrayBridge(Textures), Transform );
		}
		auto& Context = Unity::GetDirectxContext();
		if ( !Textures.IsEmpty() )
		{
			try
			{
				//	todo; handle mulitple textures
				//	gr: this needs to go through the blitter too
				std::Debug << "gr: this needs to go through the blitter" << std::endl;
				{
					Soy::TScopeTimer BlitTimer = GetUpdateTextureBlitTimer(StreamIndex);
					Texture.Write( *Textures[0], Context );
					if ( Decoder.mParams.mDrawTimestamp )
						Blitter.BlitNumber( Texture, Texture, PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
				}
				OnFrameCopied(PixelTimestamp,StreamIndex);
				PixelBuffer->Unlock();
				return true;
			}
			catch(std::exception& e)
			{
				LastPathError << "Directx pixels->texture copy failed: " << e.what() << ". ";
				PixelBuffer->Unlock();
			}
		}
	}

	//	if we reached here, fast failed, and slow failed.
	//	try and draw the error shader as a last chance
	if ( Decoder.mParams.mBlitFatalErrors )
	{
		auto& Context = Unity::GetDirectxContext();
		try
		{
			std::string Error = LastPathError.str();
			if ( Error.empty() )
			{
				LastPathError << "Fast & slow path blit of " << PixelTimestamp << " failed";
				Error = LastPathError.str();
			}
			Blitter.BlitError( Texture, Error, Context );
			if ( Decoder.mParams.mDrawTimestamp )
				Blitter.BlitNumber( Texture, Texture, PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
			OnFrameCopied(PixelTimestamp,StreamIndex);
			return true;
		}
		catch(std::exception& e)
		{
			std::Debug << "Last-path blit-error failed: " << e.what() << std::endl;
		}
	}
	return false;
}
#endif


//-------------------------------------
//	returns if texture changed. throw on error. No data/no path is not an error!
//-------------------------------------
#if defined(ENABLE_DIRECTX9)
bool PopMovie::TInstance::UpdateTexture(Directx9::TTexture& Texture,size_t StreamIndex,SoyTime CopyTimestamp)
{
	Soy::TScopeTimer UpdateTextureTimer = GetUpdateTextureTotalTimer(StreamIndex);

	if ( !Soy::Assert( Texture.IsValid(), "Invalid texture supplied" ) )
		return false;

	//	save ptr for thread safety
	auto pDecoder = mDecoder;
	if ( !pDecoder )
		return false;
	auto& Decoder = *pDecoder;

	//	allocate the blitter
	if ( !mDirectx9Blitter )
		mDirectx9Blitter.reset( new Directx9::TBlitter(nullptr) );
	auto& Blitter = *mDirectx9Blitter;
	auto& Context = Unity::GetDirectx9Context();

	//	check for [deffered] fatal errors
	//	gr: will handle this differently later, but for now display an error shader
	std::string FatalError;
	if ( Decoder.HasFatalError( FatalError ) )
	{
		//	flush out frames
		Decoder.GetPixelBufferManager(StreamIndex).ReleaseFrames();

		if ( Decoder.mParams.mBlitFatalErrors )
		{
			//std::Debug << "Fatal error: " << FatalError << std::endl;
			Blitter.BlitError( Texture, FatalError, Context );
		}
		return true;
	}

	ofScopeTimerWarning PopPixelBuffer_Timer("Decoder::PopPixelBuffer",POPPIXELBUFFER_TIMER_MIN);

	SoyTime PixelTimestamp = CopyTimestamp.IsValid() ? CopyTimestamp : mTimestamp;
	auto PixelBuffer = Decoder.GetPixelBufferManager(StreamIndex).PopPixelBuffer( PixelTimestamp );
	PopPixelBuffer_Timer.Stop();

	//	no new pixel buffer
	if ( !PixelBuffer )
	{
		if ( Decoder.mParams.mDebugNoNewPixelBuffer )
			std::Debug << "No new pixel buffer for " << PixelTimestamp << std::endl;
		return false;
	}

	if ( Decoder.mParams.mDebugNoNewPixelBuffer )
		std::Debug << "Blitting pixel buffer (" << PixelTimestamp << ") for " << mTimestamp << std::endl;

	//	set transform
	Blitter.SetUseTestBlitShader( Decoder.mParams.mDebugBlit );
	Blitter.SetMergeYuv( Decoder.mParams.mBlitMergeYuv );
	Blitter.SetClearBeforeBlit( Decoder.mParams.mClearBeforeBlit );

	//	error to display if we fast & slow paths fail and we do a last-chance error-blit
	std::stringstream LastPathError;

	if ( Decoder.mParams.mAllowFastCopy )
	{
		BufferArray<Directx9::TTexture,2> Textures;
		float3x3 Transform;
		{
			Soy::TScopeTimer LockTimer = GetUpdateTextureLockTimer(StreamIndex);
			PixelBuffer->Lock( GetArrayBridge(Textures), Context, Transform );
		}
		if ( !Textures.IsEmpty() )
		{
			try
			{
				//	gr: note: no use of transform here (or watermark blit?)
				//		should probably force this through the blitter
				std::Debug << "Gr: force this through blitter" << std::endl;
				{
					Soy::TScopeTimer BlitTimer = GetUpdateTextureBlitTimer(StreamIndex);
					Texture.Write( Textures[0], Context );
					if ( Decoder.mParams.mDrawTimestamp )
						Blitter.BlitNumber( Textures[0], Textures[0], PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
				}
				OnFrameCopied(PixelTimestamp,StreamIndex);
				PixelBuffer->Unlock();
				return true;
			}
			catch(std::exception& e)
			{
				LastPathError << "Directx texture->texture copy failed: " << e.what() << ". ";
				PixelBuffer->Unlock();
			}
		}
	}

	//	use blitter (copy to temporary dynamic texture, then GPU copy, for when we don't have write access to the texture)
	//	gr: in opengl this has all been merged into the blitter
	//	gr: code above currently creates blitter regardless (like opengl), maybe remove this implicit creation/dependency
	//if ( mDirectxBlitter )
	if ( true )
	{
		auto& Context = Unity::GetDirectx9Context();
		BufferArray<SoyPixelsImpl*,4> Textures;
		float3x3 Transform;
		{
			Soy::TScopeTimer LockTimer = GetUpdateTextureLockTimer(StreamIndex);
			PixelBuffer->Lock( GetArrayBridge(Textures), Transform );
		}
		if ( !Textures.IsEmpty() )
		{
			try
			{
				BufferArray<const SoyPixelsImpl*,4> Textures_Const( Textures );
				{
					Soy::TScopeTimer BlitTimer = GetUpdateTextureBlitTimer(StreamIndex);
					Blitter.SetTransform( Decoder.mParams.mApplyVideoTransform ? Transform : float3x3() );
					Blitter.BlitTexture(Texture, GetArrayBridge(Textures_Const), Context );
					if ( Decoder.mParams.mDrawTimestamp )
						Blitter.BlitNumber( Texture, Texture, PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
				}
				OnFrameCopied(PixelTimestamp, StreamIndex);
				PixelBuffer->Unlock();
				return true;
			}
			catch ( std::exception& e )
			{
				LastPathError << "Blit " << Textures.GetSize() << "x pixels failed: " << e.what() << ". ";
				PixelBuffer->Unlock();
			}
		}
	}

	if ( Decoder.mParams.mAllowSlowCopy )
	{
		BufferArray<SoyPixelsImpl*,2> Textures;
		float3x3 Transform;
		{
			Soy::TScopeTimer LockTimer = GetUpdateTextureLockTimer(StreamIndex);
			PixelBuffer->Lock( GetArrayBridge(Textures), Transform );
		}
		auto& Context = Unity::GetDirectx9Context();
		if ( !Textures.IsEmpty() )
		{
			try
			{
				//	todo; handle mulitple textures
				//	gr: this needs to go through the blitter too
				std::Debug << "gr: this needs to go through the blitter" << std::endl;
				{
					Soy::TScopeTimer BlitTimer = GetUpdateTextureBlitTimer(StreamIndex);
					Texture.Write( *Textures[0], Context );
					if ( Decoder.mParams.mDrawTimestamp )
						Blitter.BlitNumber( Texture, Texture, PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
				}
				OnFrameCopied(PixelTimestamp,StreamIndex);
				PixelBuffer->Unlock();
				return true;
			}
			catch(std::exception& e)
			{
				LastPathError << "Directx pixels->texture copy failed: " << e.what() << ". ";
				PixelBuffer->Unlock();
			}
		}
	}

	//	if we reached here, fast failed, and slow failed.
	//	try and draw the error shader as a last chance
	if ( Decoder.mParams.mBlitFatalErrors )
	{
		auto& Context = Unity::GetDirectx9Context();
		try
		{
			std::string Error = LastPathError.str();
			if ( Error.empty() )
			{
				LastPathError << "Fast & slow path blit of " << PixelTimestamp << " failed";
				Error = LastPathError.str();
			}
			Blitter.BlitError( Texture, Error, Context );
			if ( Decoder.mParams.mDrawTimestamp )
				Blitter.BlitNumber( Texture, Texture, PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
			OnFrameCopied(PixelTimestamp,StreamIndex);
			return true;
		}
		catch(std::exception& e)
		{
			std::Debug << "Last-path blit-error failed: " << e.what() << std::endl;
		}
	}
	return false;
}
#endif

std::shared_ptr<TPixelBuffer> PopMovie::TInstance::PopPixelBuffer(TVideoDecoder& Decoder,SoyTime& PixelTimestamp,size_t StreamIndex)
{
	auto& PixelBufferManager = Decoder.GetPixelBufferManager(StreamIndex);

	ofScopeTimerWarning PopPixelBuffer_Timer("Decoder::PopPixelBuffer",POPPIXELBUFFER_TIMER_MIN);
	if ( !PixelTimestamp.IsValid() )
		PixelTimestamp = mTimestamp;
	auto PixelBuffer = PixelBufferManager.PopPixelBuffer( PixelTimestamp );
	PopPixelBuffer_Timer.Stop();

	//	no new pixel buffer
	if ( !PixelBuffer )
	{
		if ( Decoder.mParams.mDebugNoNewPixelBuffer )
			std::Debug << "No new pixel buffer for " << mTimestamp << std::endl;
		return nullptr;
	}

	if ( Decoder.mParams.mDebugNoNewPixelBuffer )
		std::Debug << "Blitting pixel buffer (" << PixelTimestamp << ") for " << mTimestamp << std::endl;

	return PixelBuffer;
}


#if defined(ENABLE_GNM)
bool PopMovie::TInstance::UpdateTexture(Gnm::TTexture& Texture,size_t StreamIndex,SoyTime CopyTimestamp)
{
	Soy::TScopeTimer UpdateTextureTimer = GetUpdateTextureTotalTimer(StreamIndex);

	if ( !Soy::Assert( Texture.IsValid(), "Invalid texture supplied" ) )
		return false;

	//	save ptr for thread safety
	auto pDecoder = mDecoder;
	if ( !pDecoder )
		return false;
	auto& Decoder = *pDecoder;

	SoyTime PixelTimestamp = CopyTimestamp;
	auto PixelBuffer = PopPixelBuffer( Decoder, PixelTimestamp, StreamIndex );
	if ( !PixelBuffer )
		return false;

	auto& LastPathError = std::Debug;

	//	test mode
	if (Decoder.mParams.mDebugBlit )
	{
		try
		{
			auto& Context = Unity::GetGnmContext();
			if ( !mGnmBlitter )
				mGnmBlitter.reset( new Gnm::TBlitter(Decoder.mParams) );
			auto& Blitter = *mGnmBlitter;
			Blitter.BlitUnityTest( Texture, Context );
			//Blitter.BlitTest( Texture, Context );
			return true;
		}
		catch(std::exception& e)
		{
			std::Debug << "Exception with test blitter: " << e.what() << std::endl;
		}
	}

	if ( Decoder.mParams.mAllowFastCopy )
	{
		try
		{
			auto& Context = Unity::GetGnmContext();

			//	allocate the blitter
			if ( !mGnmBlitter )
				mGnmBlitter.reset( new Gnm::TBlitter(Decoder.mParams) );
			auto& Blitter = *mGnmBlitter;

			float3x3 Transform;
			BufferArray<Gnm::TTexture,MAX_PIXEL_BUFFER_PLANES> Textures;
			{
				Soy::TScopeTimer LockTimer = GetUpdateTextureLockTimer(StreamIndex);
				PixelBuffer->Lock( GetArrayBridge(Textures), Context, Transform );
			}
			try
			{
				if ( !Textures.IsEmpty() )
				{
					{
						Soy::TScopeTimer BlitTimer = GetUpdateTextureBlitTimer(StreamIndex);
						Blitter.SetTransform( Transform );
						Blitter.BlitTexture( Texture, GetArrayBridge(Textures), Context );
						if ( Decoder.mParams.mDrawTimestamp )
							Blitter.BlitNumber( Textures[0], Textures[0], PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
					}
					OnFrameCopied(PixelTimestamp,StreamIndex);
					PixelBuffer->Unlock();
					return true;
				}
			}
			catch(...)
			{
				PixelBuffer->Unlock();
				throw;
			}
		}
		catch(std::exception& e)
		{
			LastPathError << "Gnm fast texture blit failed: " << e.what() << ". ";
		}
	}

	if ( Decoder.mParams.mAllowSlowCopy )
	{
		try
		{
			float3x3 Transform;
			BufferArray<SoyPixelsImpl*,MAX_PIXEL_BUFFER_PLANES> Textures;
			{
				Soy::TScopeTimer LockTimer = GetUpdateTextureLockTimer(StreamIndex);
				PixelBuffer->Lock( GetArrayBridge(Textures), Transform );
			}
			if ( !Textures.IsEmpty() )
			{
				if ( Textures.GetSize() > 1 )
					std::Debug << "Warning GNM copying only 1/" << Textures.GetSize() << " pixel buffer planes" << std::endl;
				{
					Soy::TScopeTimer BlitTimer = GetUpdateTextureBlitTimer(StreamIndex);
					Texture.Write( *Textures[0] );
					if ( Decoder.mParams.mDrawTimestamp )
						Blitter.BlitNumber( Textures[0], Textures[0], PixelTimestamp.GetTime(), Decoder.mParams.mDrawTimestampPosA, Decoder.mParams.mDrawTimestampPosB, Context );
				}
				OnFrameCopied( PixelTimestamp, StreamIndex );
				PixelBuffer->Unlock();
				return true;
			}
		}
		catch(std::exception& e)
		{
			LastPathError << "SlowCopy failed: " << e.what();
			PixelBuffer->Unlock();
		}
	}

	return false;
}
#endif



bool PopMovie::TInstance::UpdateTexture(const std::function<void(SoyPixelsImpl&,SoyTime)>& FrameCallback,size_t StreamIndex)
{
	//	save ptr for thread safety
	auto pDecoder = mDecoder;
	if ( !pDecoder )
		return false;
	auto& Decoder = *pDecoder;
	
	ofScopeTimerWarning PopPixelBuffer_Timer("Decoder::PopPixelBuffer",POPPIXELBUFFER_TIMER_MIN);
	SoyTime PixelTimestamp = mTimestamp;
	auto PixelBuffer = Decoder.GetPixelBufferManager(StreamIndex).PopPixelBuffer( PixelTimestamp );
	PopPixelBuffer_Timer.Stop();
	
	//	no new pixel buffer
	if ( !PixelBuffer )
	{
		if ( Decoder.mParams.mDebugNoNewPixelBuffer )
			std::Debug << "No new pixel buffer for " << mTimestamp << std::endl;
		return false;
	}
	
	if ( Decoder.mParams.mDebugNoNewPixelBuffer )
		std::Debug << "Blitting pixel buffer (" << PixelTimestamp << ") for " << mTimestamp << std::endl;

	//	lock pixels, callback and unlock
	try
	{
		float3x3 Transform;
		BufferArray<SoyPixelsImpl*,MAX_PIXEL_BUFFER_PLANES> Textures;
		{
			Soy::TScopeTimer LockTimer = GetUpdateTextureLockTimer(StreamIndex);
			PixelBuffer->Lock( GetArrayBridge(Textures), Transform );
		}
		Soy::Assert( !Textures.IsEmpty(), "Pixel buffer has no raw pixels/support");
		auto Texture0 = Textures[0];
		Soy::Assert( Texture0!=nullptr, "Pixel buffer returned null pixels");

		//	do callback
		{
			Soy::TScopeTimer BlitTimer = GetUpdateTextureBlitTimer(StreamIndex);
			FrameCallback( *Texture0, PixelTimestamp );
			if ( Decoder.mParams.mDrawTimestamp )
			{
				//	maybe a manual drawing?
				std::Debug << "Timestamp drawing not implemented for non-gpu textures" << std::endl;
			}
		}
		PixelBuffer->Unlock();
	}
	catch (std::exception& e)
	{
		std::Debug << "Exception with raw pixel callback; " << e.what() << std::endl;
		PixelBuffer->Unlock();
		throw;
	}
	
	return true;
}


Soy::TScopeTimer PopMovie::TInstance::GetUpdateTextureTotalTimer(size_t StreamIndex)
{
	auto UpdateValue = [this,StreamIndex](SoyTime Duration)
	{
		mPerformanceStats[StreamIndex].mLastUpdateTextureTotalDuration = Duration;
	};
	return Soy::TScopeTimer("Update Texture", 0, UpdateValue, true );
}

Soy::TScopeTimer PopMovie::TInstance::GetUpdateTextureLockTimer(size_t StreamIndex)
{
	auto UpdateValue = [this,StreamIndex](SoyTime Duration)
	{
		mPerformanceStats[StreamIndex].mLastUpdateTextureLockDuration = Duration;
	};
	return Soy::TScopeTimer("Lock pixel buffer", 0, UpdateValue, true );
}

Soy::TScopeTimer PopMovie::TInstance::GetUpdateTextureBlitTimer(size_t StreamIndex)
{
	auto UpdateValue = [this,StreamIndex](SoyTime Duration)
	{
		mPerformanceStats[StreamIndex].mLastUpdateTextureBlitDuration = Duration;
	};
	return Soy::TScopeTimer("Blit pixel buffer", 0, UpdateValue, true );
}


void PopMovie::TInstance::GetPerformanceGraphData(ArrayBridge<TGraphBar>&& Bars,Soy::TRgb& BackgroundColour,float& PlayerTimeProgress,size_t StreamIndex,bool LocalisedGraph)
{
	auto VideoMeta = GetMeta();
	auto Stats = mPerformanceStats.at(StreamIndex);

	//	gr: colour to type of stream
	auto StreamFormat = GetStreamFormat( StreamIndex );

	static std::chrono::milliseconds PadAheadMs(60);
	static std::chrono::milliseconds PadBehindMs(200);
	SoyTime PadAhead(PadAheadMs);
	SoyTime PadBehind(PadBehindMs);
	
	//	generate min/max
	SoyTime TimeMin;
	SoyTime TimeMax;
	{
		Array<SoyTime> AllTimes;
		if ( !LocalisedGraph )
		{
			if ( VideoMeta.mDuration.IsValid() )
				AllTimes.PushBack( VideoMeta.mDuration );
			if ( VideoMeta.mKnownDuration.IsValid() )
				AllTimes.PushBack( VideoMeta.mKnownDuration );
		}
		
		//	with & without ahead pad
		AllTimes.PushBack( mTimestamp );
		AllTimes.PushBack( Stats.mLastExtractedTime );
		AllTimes.PushBack( Stats.mLastDecodeSubmittedTime );
		AllTimes.PushBack( Stats.mLastDecodedTime );
		AllTimes.PushBack( Stats.mLastBufferedTime );
		AllTimes.PushBack( Stats.mLastCopiedTime );
		
		static std::chrono::milliseconds TooHighWarningTimecode( 99999999 );
		
		//  catch invalid values
		if ( Stats.mLastExtractedTime > SoyTime(TooHighWarningTimecode) )			std::Debug << "mLastExtractedTime=" << Stats.mLastExtractedTime << std::endl;
		if ( Stats.mLastDecodeSubmittedTime > SoyTime(TooHighWarningTimecode) )	std::Debug << "mLastDecodeSubmittedTime=" << Stats.mLastDecodeSubmittedTime << std::endl;
		if ( Stats.mLastDecodedTime > SoyTime(TooHighWarningTimecode) )			std::Debug << "mLastDecodedTime=" << Stats.mLastDecodedTime << std::endl;
		if ( Stats.mLastBufferedTime > SoyTime(TooHighWarningTimecode) )			std::Debug << "mLastBufferedTime=" << Stats.mLastBufferedTime << std::endl;
		if ( Stats.mLastCopiedTime > SoyTime(TooHighWarningTimecode) )			std::Debug << "mLastCopiedTime=" << Stats.mLastCopiedTime << std::endl;
		
		TimeMax = Soy::ArrayMax( GetArrayBridge(AllTimes) );
		TimeMin = Soy::ArrayMin( GetArrayBridge(AllTimes) );
	}

	//	pad ahead
	TimeMax = TimeMax + PadAhead;
	
	//	pad behind
	if ( TimeMin > PadBehind )
		TimeMin = SoyTime( TimeMin - PadBehind );
	else
		TimeMin = SoyTime();
	
	//	align back if not localised
	if ( !LocalisedGraph )
	{
		TimeMin = SoyTime();
	}
	
	//	each stat as progress
	Array<float> Progresses;
	PlayerTimeProgress = Soy::Range( mTimestamp, TimeMin, TimeMax );
	Progresses.PushBack( PlayerTimeProgress );
	Progresses.PushBack( Soy::Range( Stats.mLastExtractedTime, TimeMin, TimeMax ) );
	Progresses.PushBack( Soy::Range( Stats.mLastDecodeSubmittedTime, TimeMin, TimeMax ) );
	Progresses.PushBack( Soy::Range( Stats.mLastDecodedTime, TimeMin, TimeMax ) );
	Progresses.PushBack( Soy::Range( Stats.mLastBufferedTime, TimeMin, TimeMax ) );
	Progresses.PushBack( Soy::Range( Stats.mLastCopiedTime, TimeMin, TimeMax ) );

	
	BackgroundColour = PerformanceGraph::NiceBlue;
	
	for ( int i=0;	i<Progresses.GetSize();	i++ )
	{
		bool Ahead = (Progresses[i] >= PlayerTimeProgress);

		auto Green = PerformanceGraph::OtherGreen;
		if ( SoyMediaFormat::IsVideo( StreamFormat ) )
			Green = PerformanceGraph::VideoGreen;
		if ( SoyMediaFormat::IsAudio( StreamFormat ) )
			Green = PerformanceGraph::AudioGreen;

		Bars.PushBack( TGraphBar(Progresses[i], Ahead ? Green : PerformanceGraph::NiceRed ) );
	}
}


void PopMovie::TInstance::GetMemoryGraphData(ArrayBridge<TGraphBar>&& Bars,Soy::TRgb& BackgroundColour,float& PlayerTimeProgress)
{
	//	enum memory stats
	Array<SoyMem::THeapMeta> Heaps;
	SoyMem::GetHeapMetas( GetArrayBridge( Heaps ) );

	//	scale everything down for floating point errors
	static size_t ByteScalar = 1;
	//	show a line at X mb
	static size_t MeasureLineBytes = 1024 * 1024 * 10;

	Array<size_t> HeapAllocs;
	size_t MinAlloc = 0;
	size_t MaxAlloc = MeasureLineBytes;
	for ( int h=0;	h<Heaps.GetSize();	h++ )
	{
		auto& Heap = Heaps[h];
		auto HeapAlloc = Heap.mAllocBytes / ByteScalar;
		HeapAllocs.PushBack( HeapAlloc );

		MaxAlloc = std::max( MaxAlloc, HeapAlloc );
	}

	//	each stat as progress
	PlayerTimeProgress = Soy::Range( MeasureLineBytes/ByteScalar, MinAlloc, MaxAlloc );
	for ( int h=0;	h<HeapAllocs.GetSize();	h++ )
	{
		auto HeapAlloc = HeapAllocs[h];
		auto HeapAllocf = Soy::Range( HeapAlloc, MinAlloc, MaxAlloc );
		Bars.PushBack( TGraphBar( HeapAllocf, PerformanceGraph::NiceGold ) );
	}
	
	BackgroundColour = PerformanceGraph::NiceRed;
}

#if defined(ENABLE_OPENGL)
void PopMovie::TInstance::UpdateMemoryGraphTexture(Opengl::TTexture& Texture)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );

	auto OpenglContext = Unity::GetOpenglContextPtr();
	if ( !mGraphRenderer )
	{
		mGraphRenderer.reset( new Opengl::TGraphRenderer(OpenglContext) );
	}
	//	capture for thread safety
	auto pGraphRenderer = mGraphRenderer;
	if ( !pGraphRenderer )
		return;
#if defined(TARGET_ANDROID)
	auto& GraphRenderer = static_cast<Opengl::TGraphRenderer&>(*pGraphRenderer);
#else
	auto& GraphRenderer = dynamic_cast<Opengl::TGraphRenderer&>(*pGraphRenderer);
#endif
	
	Array<TGraphBar> Bars;
	Soy::TRgb BackgroundColour;
	float PlayerTimeProgress = 0;
	GetMemoryGraphData( GetArrayBridge(Bars), BackgroundColour, PlayerTimeProgress );
	
	{
		ofScopeTimerWarning Timer("MemoryGraphRenderer.Render", Unity::mMinTimerMs );
		GraphRenderer.Render( Texture, BackgroundColour, GetArrayBridge( Bars ), PlayerTimeProgress );
	}
}
#endif


#if defined(ENABLE_DIRECTX)
void PopMovie::TInstance::UpdateMemoryGraphTexture(Directx::TTexture& Texture)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );

	auto DirectxContext = Unity::GetDirectxContextPtr();
	if ( !mGraphRenderer )
	{
		mGraphRenderer.reset( new Directx::TGraphRenderer(DirectxContext) );
	}
	//	capture for thread safety
	auto pGraphRenderer = mGraphRenderer;
	if ( !pGraphRenderer )
		return;
	auto& GraphRenderer = dynamic_cast<Directx::TGraphRenderer&>(*pGraphRenderer);
				
	Array<TGraphBar> Bars;
	Soy::TRgb BackgroundColour;
	float PlayerTimeProgress = 0;
	GetMemoryGraphData( GetArrayBridge(Bars), BackgroundColour, PlayerTimeProgress );
	
	{
		ofScopeTimerWarning Timer("MemoryGraphRenderer.Render", Unity::mMinTimerMs );
		GraphRenderer.Render( Texture, BackgroundColour, GetArrayBridge( Bars ), PlayerTimeProgress );
	}
}
#endif


#if defined(ENABLE_GNM)
void PopMovie::TInstance::UpdateMemoryGraphTexture(Gnm::TTexture& Texture)
{
	std::Debug << __func__ << " todo" << std::endl;
}
#endif

#if defined(ENABLE_METAL)
void PopMovie::TInstance::UpdateMemoryGraphTexture(Metal::TTexture& Texture)
{
	std::Debug << __func__ << " todo" << std::endl;
}
#endif


#if defined(ENABLE_OPENGL)
void PopMovie::TInstance::UpdatePerformanceGraphTexture(Opengl::TTexture& Texture,size_t StreamIndex,bool LocalisedGraph)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );

	auto OpenglContext = Unity::GetOpenglContextPtr();
	if ( !mGraphRenderer )
	{
		mGraphRenderer.reset( new Opengl::TGraphRenderer(OpenglContext) );
	}
	//	capture for thread safety
	auto pGraphRenderer = mGraphRenderer;
	if ( !pGraphRenderer )
		return;
#if defined(TARGET_ANDROID)
	auto& GraphRenderer = static_cast<Opengl::TGraphRenderer&>(*pGraphRenderer);
#else
	auto& GraphRenderer = dynamic_cast<Opengl::TGraphRenderer&>(*pGraphRenderer);
#endif
	
	Array<TGraphBar> Bars;
	Soy::TRgb BackgroundColour;
	float PlayerTimeProgress = 0;
	GetPerformanceGraphData( GetArrayBridge(Bars), BackgroundColour, PlayerTimeProgress, StreamIndex, LocalisedGraph );
	
	{
		ofScopeTimerWarning Timer("GraphRenderer.Render", Unity::mMinTimerMs );
		GraphRenderer.Render( Texture, BackgroundColour, GetArrayBridge( Bars ), PlayerTimeProgress );
	}
}
#endif


#if defined(ENABLE_DIRECTX)
void PopMovie::TInstance::UpdatePerformanceGraphTexture(Directx::TTexture& Texture,size_t StreamIndex,bool LocalisedGraph)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );

	auto DirectxContext = Unity::GetDirectxContextPtr();
	if ( !mGraphRenderer )
	{
		mGraphRenderer.reset( new Directx::TGraphRenderer(DirectxContext) );
	}
	//	capture for thread safety
	auto pGraphRenderer = mGraphRenderer;
	if ( !pGraphRenderer )
		return;
	auto& GraphRenderer = dynamic_cast<Directx::TGraphRenderer&>(*pGraphRenderer);
				
	Array<TGraphBar> Bars;
	Soy::TRgb BackgroundColour;
	float PlayerTimeProgress = 0;
	GetPerformanceGraphData( GetArrayBridge(Bars), BackgroundColour, PlayerTimeProgress, StreamIndex, LocalisedGraph );
	
	{
		ofScopeTimerWarning Timer("GraphRenderer.Render", Unity::mMinTimerMs );
		GraphRenderer.Render( Texture, BackgroundColour, GetArrayBridge( Bars ), PlayerTimeProgress );
	}
}
#endif


#if defined(ENABLE_GNM)
void PopMovie::TInstance::UpdatePerformanceGraphTexture(Gnm::TTexture& Texture,size_t StreamIndex,bool LocalisedGraph)
{
	std::Debug << __func__ << " todo" << std::endl;
}
#endif

#if defined(ENABLE_METAL)
void PopMovie::TInstance::UpdatePerformanceGraphTexture(Metal::TTexture& Texture,size_t StreamIndex,bool LocalisedGraph)
{
	std::Debug << __func__ << " todo" << std::endl;
}
#endif


#if defined(ENABLE_OPENGL)
void PopMovie::TInstance::UpdateAudioTexture(Opengl::TTexture& Texture,size_t StreamIndex)
{
	ofScopeTimerWarning Timer(__func__, Unity::mMinTimerMs );
	
	auto Decoder = mDecoder;
	if ( !Decoder )
		return;
	
	auto OpenglContext = Unity::GetOpenglContextPtr();
	if ( !mAudioRenderer )
	{
		mAudioRenderer.reset( new Opengl::TAudioRenderer(OpenglContext) );
	}
	//	capture for thread safety
	auto pRenderer = mAudioRenderer;
	if ( !pRenderer )
		return;
#if defined(TARGET_ANDROID)
	auto& Renderer = static_cast<Opengl::TAudioRenderer&>(*pRenderer);
#else
	auto& Renderer = dynamic_cast<Opengl::TAudioRenderer&>(*pRenderer);
#endif

	SoyTime StreamTime;
	/*
	{
		auto it = mAudioStreamTime.find(StreamIndex);
		if ( it != mAudioStreamTime.end() )
			StreamTime = it->second;
	}
	 */
	
	//	get data to render0
	//	gr: this dictates how much audio data to see. Let the user specify it (in secs for them)
	//		we also are limited to the texture width, but maybe the platform code can re-sample
	static int SampleCount = 1024;
	auto& AudioBuffer = Decoder->GetAudioBufferManager( StreamIndex );

	TAudioBufferBlock AudioDataBlock;
	AudioDataBlock.mStartTime = ( StreamTime.IsValid() && StreamTime < mTimestamp ) ? StreamTime : mTimestamp;
	
	AudioDataBlock.mChannels = AudioBuffer.GetChannels();
	AudioDataBlock.mFrequency = AudioBuffer.GetFrequency();
	AudioDataBlock.mData.SetSize( SampleCount );
	
	try
	{
		static bool ClipStart = false;
		static bool ClipEnd = true;
		static bool Debug = false;
		static bool Cull = false;
		static bool PadTail = true;
		if ( !AudioBuffer.GetAudioBuffer( AudioDataBlock, Debug, PadTail, ClipStart, ClipEnd, Cull ) )
		{
			AudioDataBlock.mData.SetAll(0);
		}
	}
	catch(std::exception& e)
	{
		std::Debug << "Error getting audio data for graph; " << e.what() << std::endl;
		AudioDataBlock.mData.SetAll(0);
	}

	TAudioGraphData AudioData;
	float Timef = Soy::Range( StreamTime, AudioDataBlock.GetStartTime(), AudioDataBlock.GetEndTime() );
	AudioData.mCurrentTime = Timef * AudioData.mSamples.GetSize();
	AudioData.mSamples.Copy( AudioDataBlock.mData );
	
	{
		ofScopeTimerWarning Timer("AudioRenderer.Render", Unity::mMinTimerMs );
		Renderer.Render( Texture, AudioData );
	}
}
#endif

#if defined(ENABLE_DIRECTX)
void PopMovie::TInstance::UpdateAudioTexture(Directx::TTexture& Texture,size_t StreamIndex)
{
	std::Debug << __func__ << " todo" << std::endl;
}
#endif

#if defined(ENABLE_DIRECTX9)
void PopMovie::TInstance::UpdateAudioTexture(Directx9::TTexture& Texture,size_t StreamIndex)
{
	std::Debug << __func__ << " todo" << std::endl;
}
#endif

#if defined(ENABLE_GNM)
void PopMovie::TInstance::UpdateAudioTexture(Gnm::TTexture& Texture,size_t StreamIndex)
{
	std::Debug << __func__ << " todo" << std::endl;
}
#endif

#if defined(ENABLE_METAL)
void PopMovie::TInstance::UpdateAudioTexture(Metal::TTexture& Texture,size_t StreamIndex)
{
	std::Debug << __func__ << " todo" << std::endl;
}
#endif


void PopMovie::TInstance::PopAudioBuffer(size_t AudioStreamIndex,TAudioBufferBlock& Output)
{
	auto Decoder = mDecoder;
	if ( Decoder == nullptr )
		throw Soy::AssertException("PopAudioBuffer: Decoder expected");

	//	convert audio stream index to Real stream index
	auto StreamIndex = Decoder->GetAudioStreamIndex( AudioStreamIndex );

	
	auto& AudioBuffer = Decoder->GetAudioBufferManager( StreamIndex );
	AudioBuffer.PopNextAudioData( GetArrayBridge(Output.mData), true );
}

bool PopMovie::TInstance::PopText(std::stringstream& Output,size_t TextStreamIndex,bool SkipOldText)
{
	auto Decoder = mDecoder;
	Soy::Assert( Decoder!=nullptr, "PopText: Decoder expected");
	
	//	convert audio stream index to Real stream index
	auto StreamIndex = Decoder->GetTextStreamIndex( TextStreamIndex );
	
	auto& Buffer = Decoder->GetTextBufferManager( StreamIndex );
	SoyTime OutputTimestamp = Buffer.PopBuffer( Output, mTimestamp, SkipOldText );
	
	if ( !OutputTimestamp.IsValid() )
		return false;

	//	some data popped!
	OnFrameCopied( OutputTimestamp, StreamIndex );
	
	return true;
}

void PopMovie::TInstance::GetText(std::stringstream& Output,const SoyTime& Time,size_t TextStreamIndex)
{
	auto Decoder = mDecoder;
	Soy::Assert( Decoder!=nullptr, "PopText: Decoder expected");

	//	convert audio stream index to Real stream index
	auto StreamIndex = Decoder->GetTextStreamIndex( TextStreamIndex );

	auto& Buffer = Decoder->GetTextBufferManager( StreamIndex );
	SoyTime StartTime = Time;
	SoyTime EndTime;
	Buffer.GetBuffer( Output, StartTime, EndTime );

	//	some data popped!
	if ( StartTime.IsValid() )
	{
		OnFrameCopied(StartTime, StreamIndex);
	}
	else
	{
		//	no data on this frame, but don't error...
	}

}

void PopMovie::TInstance::PushStreamBuffer(ArrayBridge<uint8_t>&& Buffer,size_t StreamIndex)
{
	//	get the push extractor (will throw if wrong type!)
	auto pDecoder = mDecoder;
#if defined(ENABLE_RTTI)
	auto& RawDecoder = dynamic_cast<TRawDecoder&>( *pDecoder );
#else
	//	gr: maybe need a solution for android.
	auto& RawDecoder = reinterpret_cast<TRawDecoder&>( *pDecoder );
#endif
	
	auto& PushExtractor = RawDecoder.GetExtractor<TPushExtractor>();

	PushExtractor.PushBuffer( Buffer, StreamIndex );
}


void PopMovie::TInstance::SetTimestamp(SoyTime Timestamp)
{
	mTimestamp = Timestamp;
	
	//	send to decoder for pre-emptive frame discard or to seek
	if ( mDecoder )
	{
		try
		{
			mDecoder->SetPlayerTime( Timestamp );
		}
		catch(std::exception& e)
		{
			std::Debug << "Exception caught in Decoder::SetPlayerTime: " << e.what() << std::endl;
		}
	}

}


const PopMovie::TPerformanceStats& PopMovie::TInstance::GetVideoPerformanceStats(size_t VideoStreamIndex)
{
	//	get proper stream index
	size_t StreamIndex = 0;

	auto Decoder = mDecoder;
	if ( !Decoder )
		throw Soy::AssertException("Cannot get proper stream index (No decoder)");
	StreamIndex = Decoder->GetVideoStreamIndex( VideoStreamIndex );
	
	//	if the stream index is valid (above hasn't thrown) then allow this function to instance it in the map
	return mPerformanceStats.at(StreamIndex);
}


PopMovie::TInstance::TInstance(const TInstanceRef& Ref,TVideoDecoderParams Params) :
	mRef		( Ref )
{
	mDecoder = PopMovie::gVideoDecoderFactory.Alloc( Params );
	if ( !mDecoder )
		throw Soy::AssertException("Failed to create decoder");

	
	//	init listeners for buffer managers when they are created
	auto BindBufferManagerStats = [=](size_t StreamIndex,TMediaBufferManager& Manager)
	{
		BindStatsCallbacks( Params, StreamIndex );
	};
	auto BindBufferManagerStats2 = [=](size_t StreamIndex)
	{
		BindStatsCallbacks( Params, StreamIndex );
	};
	mDecoder->mOnBufferManagersChanged.AddListener( BindBufferManagerStats2 );
	//	do initial ones in case they've been added by another thread
	mDecoder->ForEachBufferManager( BindBufferManagerStats );
	
	
	//	catch device shutdowns
	{
		auto OnDeviceShutdown = [this](bool&)
		{
			this->OnDeviceShutdown();
		};
		this->mOnDeviceShutdownListener = Unity::GetOnDeviceShutdown().AddListener(OnDeviceShutdown);
	}
	
	mDecoder->StartMovie();
}


PopMovie::TInstance::~TInstance()
{
	Unity::GetOnDeviceShutdown().RemoveListener( mOnDeviceShutdownListener );
}

void PopMovie::TInstance::Shutdown()
{
	//	gr: no more explicit shutdown, decoder should have a pointer to any context it needs for a deffered shutdown
	if ( mDecoder )
		mDecoder.reset();
	
	//	deffered shutdown of opengl blitter
	if ( mOpenglBlitter )
	{
	#if defined(ENABLE_OPENGL)
		PopWorker::DeferDelete( Unity::GetOpenglContextPtr(), mOpenglBlitter );
	#endif
	}

	mGraphRenderer.reset();
	mAudioRenderer.reset();
}

void PopMovie::TInstance::OnDeviceShutdown()
{
	mOpenglBlitter.reset();
	
	if ( mDecoder )
	{
		mDecoder->OnDeviceShutdown();
	}
}

void PopMovie::TInstance::BindStatsCallbacks(const TVideoDecoderParams& Params,size_t StreamIndex)
{
	auto UpdateLastCopied = [=](std::pair<SoyTime,size_t>& Frame)
	{
		mPerformanceStats[StreamIndex].mLastCopiedTime = Frame.first;
	};
	mOnFrameCopied.AddListener( UpdateLastCopied );
	
	if ( !mDecoder )
		return;
	
	auto& PixelBufferManager = mDecoder->GetBufferManager(StreamIndex);
	
	//	bind debug
	bool DebugFrameSkipping = Params.mPixelBufferParams.mDebugFrameSkipping;

	auto OnPopSkipped = [this,DebugFrameSkipping,StreamIndex](const ArrayBridge<SoyTime>& Frames)
	{
		if ( DebugFrameSkipping )
		{
			std::Debug << "Decoded frame skipped x" << Frames.GetSize() << "... ";
			for ( int i=0;	i<Frames.GetSize();	i++ )
				std::Debug << Frames[i] << " ";
			std::Debug << " Now: " << this->mTimestamp;
			std::Debug << std::endl;
		}
		mPerformanceStats[StreamIndex].mFramesPopSkipped += Frames.GetSize();
	};
	
	auto OnFrameDecoded = [this,DebugFrameSkipping,StreamIndex](const SoyTime& Frame)
	{
		if ( DebugFrameSkipping )
			std::Debug << "Frame decoded " << Frame << " Now: " << this->mTimestamp << std::endl;
		mPerformanceStats[StreamIndex].mFramesDecoded ++;
	};

	auto OnPushSkipped = [this,DebugFrameSkipping,StreamIndex](const SoyTime& Frame)
	{
		if ( DebugFrameSkipping )
			std::Debug << "Redundant decoded frame skipped " << Frame << " Now: " << this->mTimestamp << std::endl;
		mPerformanceStats[StreamIndex].mFramesPushSkipped ++;
	};
	
	auto OnPushFailed = [this,DebugFrameSkipping,StreamIndex](const SoyTime& Frame)
	{
		if ( DebugFrameSkipping )
			std::Debug << "Dropped frame " << Frame << " during push (buffer full)" << " Now: " << this->mTimestamp << std::endl;
		mPerformanceStats[StreamIndex].mFramesPushFailed++;
	};
	
	//	gr: have had crashes accessing the performance stats map, so if we init it here we should only get that once
	//		because these BindStatsCallbacks are generally only done serially, I think we're safe...
	mPerformanceStats[StreamIndex] = TPerformanceStats();
	
	PixelBufferManager.mOnFrameDecoded.AddListener( OnFrameDecoded );
	PixelBufferManager.mOnFramePopSkipped.AddListener( OnPopSkipped );
	PixelBufferManager.mOnFramePushSkipped.AddListener( OnPushSkipped );
	PixelBufferManager.mOnFramePushFailed.AddListener( OnPushFailed );
	
	//	bind stats
	PixelBufferManager.mOnFrameExtracted.AddListener( [=](const SoyTime& Frame){ mPerformanceStats[StreamIndex].mLastExtractedTime = Frame;	}	);
	PixelBufferManager.mOnFrameDecoded.AddListener( [=](const SoyTime& Frame){ mPerformanceStats[StreamIndex].mLastDecodedTime = Frame;	}	);
	PixelBufferManager.mOnFrameDecodeSubmission.AddListener( [=](const SoyTime& Frame){ mPerformanceStats[StreamIndex].mLastDecodeSubmittedTime = Frame;	}	);
	PixelBufferManager.mOnFramePushed.AddListener( [=](const SoyTime& Frame){ mPerformanceStats[StreamIndex].mLastBufferedTime = Frame;	}	);
}


TVideoMeta PopMovie::TInstance::GetMeta()
{
	if ( !mDecoder )
		return TVideoMeta();
	
	auto Meta = mDecoder->GetMeta();
	return Meta;
}

bool PopMovie::TInstance::GetFatalError(std::string& Error)
{
	if ( !mDecoder )
	{
		Error = "No decoder";
		return true;
	}

	return mDecoder->HasFatalError( Error );
}


size_t PopMovie::TInstance::ResolveAudioStreamIndex(size_t AudioStreamIndex)
{
	auto pDecoder = mDecoder;
	Soy::Assert( pDecoder !=nullptr, "ResolveAudioStreamIndex: No decoder");
	return pDecoder->GetAudioStreamIndex( AudioStreamIndex );
}

size_t PopMovie::TInstance::ResolveVideoStreamIndex(size_t VideoStreamIndex)
{
	auto pDecoder = mDecoder;
	Soy::Assert( pDecoder !=nullptr, "ResolveVideoStreamIndex: No decoder");
	return pDecoder->GetVideoStreamIndex( VideoStreamIndex );
}

size_t PopMovie::TInstance::ResolveTextStreamIndex(size_t TextStreamIndex)
{
	auto pDecoder = mDecoder;
	Soy::Assert( pDecoder !=nullptr, "ResolveTextStreamIndex: No decoder");
	return pDecoder->GetTextStreamIndex( TextStreamIndex );
}



void PopMovie::TInstance::UpdatePerformanceData(ArrayBridge<Unity::uint>&& Data)
{
	for ( auto StreamStats : mPerformanceStats )
	{
		StreamStats.second.PushToBuffer( Data, StreamStats.first, mTimestamp );
	}
}



//	accessor to the decoder factory
std::shared_ptr<TVideoDecoder> PopMovieDecoder::AllocDecoder(TVideoDecoderParams Params)
{
	auto Decoder = PopMovie::gVideoDecoderFactory.Alloc( Params );
	return Decoder;
}



std::shared_ptr<TVideoDecoder> PopMovie::TPopMovieDecoderFactory::Alloc(TVideoDecoderParams Params)
{
	std::map<size_t,std::shared_ptr<TPixelBufferManager>> PixelBufferManagers;
	std::map<size_t,std::shared_ptr<TAudioBufferManager>> AudioBufferManagers;

	return Alloc( Params, PixelBufferManagers, AudioBufferManagers );
}

std::shared_ptr<TVideoDecoder> PopMovie::TPopMovieDecoderFactory::Alloc(TVideoDecoderParams Params,std::map<size_t,std::shared_ptr<TPixelBufferManager>>& PixelBufferManagers,std::map<size_t,std::shared_ptr<TAudioBufferManager>>& AudioBufferManagers)
{
	//	gr: re-work these special video decoders to just be an extractor and have specific encoder allocations
	
	if ( Soy::StringTrimLeft( Params.mFilename, TVideoDecoderParams::gProtocol_TestDecoder, false ) )
	{
		return std::shared_ptr<TVideoDecoder>( new TestVideoDecoder( Params, PixelBufferManagers, AudioBufferManagers ) );
	}
	

	//	kinect's
	if ( Soy::StringTrimLeft( Params.mFilename, TVideoDecoderParams::gProtocol_Kinect, false ) )
	{
#if defined(TARGET_OSX)
		return std::shared_ptr<TVideoDecoder>( new Kinect::TDeviceDecoder( Params, PixelBufferManagers, AudioBufferManagers, Unity::GetOpenglContextPtr() ) );
#else
		throw Soy::AssertException("This platform currently doesn't support kinects");
#endif
	}
	

#if defined(TARGET_WINDOWS) && defined(ENABLE_CUDA)
	{
		return std::shared_ptr<TVideoDecoder>( new CudaVideoDecoder(Params, PixelBufferManagers, AudioBufferManagers, Unity::GetCudaContext() ) );
	}
#endif
	
	return std::shared_ptr<TVideoDecoder>( new TStreamRawDecoder( Params, PixelBufferManagers, AudioBufferManagers, Unity::GetOpenglContextPtr(), *this ) );
}


std::shared_ptr<TMediaExtractor> PopMovie::TPopMovieDecoderFactory::AllocExtractor(const TMediaExtractorParams& Params)
{
	std::string Filename = Params.mFilename;

	if ( Soy::StringTrimLeft( Filename, TVideoDecoderParams::gProtocol_Window, false ) )
	{
#if defined(TARGET_WINDOWS)
		TMediaExtractorParams NewParams( Filename, Params );
		return std::shared_ptr<TMediaExtractor>( new HwndExtractor( NewParams ) );
#elif defined(TARGET_OSX)
		TMediaExtractorParams NewParams( Filename, Params );
		return std::shared_ptr<TMediaExtractor>( new CGWindowExtractor( NewParams ) );
#else
		throw Soy::AssertException("This platform doesn't support window capture");
#endif
	}

	//	check for video capture sources
	if ( Soy::StringTrimLeft( Filename, TVideoDecoderParams::gProtocol_CaptureDevice, false ) )
	{
		//	gr: the fact I'm using opengl here, means MAYBE this shouldn't be an extractor afterall, bleh
#if defined(TARGET_IOS) || defined(TARGET_OSX)
		TMediaExtractorParams NewParams( Filename, Params );
		return Platform::AllocCaptureExtractor( NewParams, Unity::GetOpenglContextPtr() );
#elif defined(TARGET_WINDOWS)
		TMediaExtractorParams NewParams( Filename, Params );
		return std::shared_ptr<TMediaExtractor>( new MediaFoundation::TCaptureExtractor( NewParams ) );
#else
		throw Soy::AssertException("This platform currently doesn't support any capture devices");
#endif
	}
	
	if ( Soy::StringTrimLeft( Filename, TVideoDecoderParams::gProtocol_RawH264, false ) )
	{
		return std::shared_ptr<TMediaExtractor>( new TRawH264RawFileExtractor( Params ) );
	}

	if ( Soy::StringTrimLeft( Filename, TVideoDecoderParams::gProtocol_Push, false ) )
	{
		TMediaExtractorParams NewParams( Filename, Params );
		return std::shared_ptr<TMediaExtractor>( new TPushExtractor( NewParams ) );
	}

	if ( Soy::StringEndsWith( Params.mFilename, Bink::FileExtensions, false ) )
	{
	#if defined(ENABLE_BINK)
		return std::shared_ptr<TMediaExtractor>( new Bink::TExtractor( Params ) );
	#else
		throw Soy::AssertException("This platform doesn't support RAD bink movies");
	#endif
	}

	//	gr: this needs changing, it should be a generic file EXTRACTOR and a .srt DECODER
	if ( Soy::StringEndsWith( Params.mFilename, Srt::FileExtensions, false ) )
	{
		return std::shared_ptr<TMediaExtractor>( new TSrtFileExtractor( Params ) );
	}
	
	if ( Soy::StringEndsWith( Params.mFilename, Png::FileExtensions, false ) )
	{
		return std::shared_ptr<TMediaExtractor>( new TPngFileExtractor( Params ) );
	}
	
	if ( Soy::StringEndsWith( Params.mFilename, Jpeg::FileExtensions, false ) )
	{
		return std::shared_ptr<TMediaExtractor>( new TJpegFileExtractor( Params ) );
	}
	
	if ( Soy::StringEndsWith( Params.mFilename, Gif::FileExtensions, false ) )
	{
		if ( Soy::StringTrimLeft( Filename, TVideoDecoderParams::gProtocol_Experimental, false ) )
		{
			TMediaExtractorParams NewParams( Filename, Params );
			return std::shared_ptr<TMediaExtractor>( new TAnimatedGifFileExtractor( NewParams ) );
		}
		return std::shared_ptr<TMediaExtractor>( new TGifFileExtractor( Params ) );
	}
	
	if ( Soy::StringEndsWith( Params.mFilename, Bmp::FileExtensions, false ) )
	{
		return std::shared_ptr<TMediaExtractor>( new TBmpFileExtractor( Params ) );
	}
	
	if ( Soy::StringEndsWith( Params.mFilename, Tga::FileExtensions, false ) )
	{
		return std::shared_ptr<TMediaExtractor>( new TTgaFileExtractor( Params ) );
	}
	
	if ( Soy::StringEndsWith( Params.mFilename, Psd::FileExtensions, false ) )
	{
		return std::shared_ptr<TMediaExtractor>( new TPsdFileExtractor( Params ) );
	}
	
	if ( Soy::StringEndsWith( Params.mFilename, TVideoDecoderParams::gSuffix_HLS, false ) )
	{
		return std::shared_ptr<TMediaExtractor>( new TM3u8Extractor( Params, *this ) );
	}

	/* experimental
	if ( Soy::StringBeginsWith( Params.mFilename, "http://", false ) && Soy::StringEndsWith( Params.mFilename, ".ts", false ) )
	{
		return std::shared_ptr<TMediaExtractor>( new Mpegts::TRawSocketExtractor( Params ) );
	}
	
	if ( Soy::StringEndsWith( Params.mFilename, ".ts", false ) )
	{
		return std::shared_ptr<TMediaExtractor>( new Mpegts::TRawFileExtractor( Params ) );
	}
	*/
	//	OS specific defaults
#if defined(TARGET_IOS) || defined(TARGET_OSX)
	{
		return std::shared_ptr<TMediaExtractor>( new AvfMediaExtractor( Params ) );
	}
#endif
	
#if defined(TARGET_ANDROID)
	{
		return std::shared_ptr<TMediaExtractor>( new AndroidMediaExtractor( Params ) );
	}
#endif

#if defined(TARGET_WINDOWS)
	{
		return std::shared_ptr<TMediaExtractor>( new MfFileExtractor( Params ) );
	}
#endif

	return nullptr;
}


std::shared_ptr<TSourceManager> PopMovie::GetSourceManagerPtr()
{
	return gSourceManager;
}


TSourceManager& PopMovie::GetSourceManager()
{
	if ( !gSourceManager )
	{
		gSourceManager.reset( new TSourceManager );
		
		//	catch DLL shutdown
		auto Cleanup = [](bool&)
		{
			gSourceManager.reset();
		};
		Unity::GetOnDeviceShutdown().AddListener( Cleanup );
	}
	
	return *gSourceManager;
}




void PopMovie::EnumSources(std::function<void(const std::string&,const std::string&)> _AppendFilename,const ArrayBridge<std::string>&& Directories,std::function<bool()> Block,std::function<void()> Throttle)
{
	auto AppendFilenameThrottled = [=](const std::string& a,const std::string& b)
	{
		_AppendFilename( a, b );
		
		if ( Throttle )
			Throttle();
	};
	
	AllocShutdownCallback();

	
	//	build this from our different extractors
	Array<std::string> IncludeFilenameExtensions;
	Array<std::string> ExcludeFilenameExtensions;
	Unity::GetSystemFileExtensions( GetArrayBridge(ExcludeFilenameExtensions) );
	Platform::GetSystemFileExtensions( GetArrayBridge(ExcludeFilenameExtensions) );

	//	todo: match this up with the raw decoder factory
	Soy::PushStringArray( GetArrayBridge(IncludeFilenameExtensions), Srt::FileExtensions );
	Soy::PushStringArray( GetArrayBridge(IncludeFilenameExtensions), Png::FileExtensions );
	Soy::PushStringArray( GetArrayBridge(IncludeFilenameExtensions), Jpeg::FileExtensions );
	Soy::PushStringArray( GetArrayBridge(IncludeFilenameExtensions), Gif::FileExtensions );
	Soy::PushStringArray( GetArrayBridge(IncludeFilenameExtensions), Bmp::FileExtensions );
	Soy::PushStringArray( GetArrayBridge(IncludeFilenameExtensions), Tga::FileExtensions );
	Soy::PushStringArray( GetArrayBridge(IncludeFilenameExtensions), Psd::FileExtensions );
	IncludeFilenameExtensions.PushBack( TVideoDecoderParams::gSuffix_HLS );
#if defined(TARGET_IOS)||defined(TARGET_OSX)
	Avf::GetFileExtensions( GetArrayBridge(IncludeFilenameExtensions) );
#elif defined(TARGET_ANDROID)
	Android::GetMediaFileExtensions( GetArrayBridge(IncludeFilenameExtensions) );
#elif defined(TARGET_WINDOWS)
	MediaFoundation::GetMediaFileExtensions( GetArrayBridge(IncludeFilenameExtensions) );
#endif

	auto AppendFileFunc = [&](const std::string& Filename,const std::string& Prefix)
	{
		if ( !IncludeFilenameExtensions.IsEmpty() )
			if ( !Soy::StringEndsWith( Filename, GetArrayBridge(IncludeFilenameExtensions), false ) )
				return;
		if ( !ExcludeFilenameExtensions.IsEmpty() )
			if ( Soy::StringEndsWith( Filename, GetArrayBridge(ExcludeFilenameExtensions), false ) )
				return;
		
		AppendFilenameThrottled( Filename, Prefix );
	};

	for ( int d=0;	d<Directories.GetSize();	d++ )
	{
		auto Directory = Directories[d];
		
		//	fiddle with android special directories
#if defined(TARGET_ANDROID)
		if ( Soy::StringTrimLeft( Directory, TVideoDecoderParams::gProtocol_AndroidSdCard, true ) )
		{
			try
			{
				auto AndroidSdCardPath = Platform::GetSdCardDirectory();
				Directory.insert( 0, AndroidSdCardPath );
			}
			catch(std::exception& e)
			{
				std::Debug << "Failed to get android sd card root; " << e.what() << std::endl;
				continue;
			}
		}
#endif
		
		Platform::EnumFiles( Directory, [&](const std::string& Filename) {	AppendFileFunc( Filename, std::string() );	} );
	}

#if defined(TARGET_IOS)||defined(TARGET_OSX)
	Platform::EnumCaptureDevices( [&](const std::string& Filename) {	AppendFilenameThrottled( Filename, TVideoDecoderParams::gProtocol_CaptureDevice );	} );
#endif
	
#if defined(TARGET_OSX)
	//	gr: temporarily disabled until I can establish if this will happily run without libusb/libfreenect on the sys
	//Kinect::EnumDevices([&](const std::string& Filename) {	AppendFilenameThrottled( Filename, TVideoDecoderParams::gProtocol_Kinect );	} );
#endif

#if defined(TARGET_OSX)
	Platform::EnumWindows( [&](const std::string& Filename) {	AppendFilenameThrottled( Filename, TVideoDecoderParams::gProtocol_Window );	} );
#endif

#if defined(TARGET_WINDOWS)
	Decklink::EnumDevices( [&](const std::string& Filename) {	AppendFilenameThrottled( Filename, TVideoDecoderParams::gProtocol_Decklink );	} );
#endif

#if defined(TARGET_ANDROID)
#endif
	
#if defined(TARGET_WINDOWS)
	MediaFoundation::EnumCaptureDevices( [&](const std::string& Filename) {	AppendFilenameThrottled( Filename, TVideoDecoderParams::gProtocol_CaptureDevice );	} );
	
	Hwnd::EnumWindows( [&](const std::string& Filename) {	AppendFilenameThrottled( Filename, TVideoDecoderParams::gProtocol_Window );	}, Block );
#endif

}


void PopMovie::TPerformanceStats::GetMeta(const std::string& Prefix,TJsonWriter& Json)
{
	Json.Push( Prefix + "FramesDecoded", mFramesDecoded );
	Json.Push( Prefix + "FramesPopSkipped", mFramesPopSkipped );
	Json.Push( Prefix + "FramesPushSkipped", mFramesPushSkipped );
	Json.Push( Prefix + "FramesPushFailed", mFramesPushFailed );
	
	Json.Push( Prefix + "FramesSkippedTotal", mFramesPopSkipped+mFramesPushSkipped+mFramesPushFailed );
	Json.Push( Prefix + "LastExtractedTime", mLastExtractedTime.mTime );
	Json.Push( Prefix + "LastDecodeSubmittedTime", mLastDecodeSubmittedTime.mTime );
	Json.Push( Prefix + "LastDecodedTime", mLastDecodedTime.mTime );
	Json.Push( Prefix + "LastBufferedTime", mLastBufferedTime.mTime );
	Json.Push( Prefix + "LastCopiedTime", mLastCopiedTime.mTime );

}

void PopMovie::TPerformanceStats::InitialiseBuffer(ArrayBridge<Unity::uint>&& Data)
{
	int Version = 4;
	int TotalSize = 2;

	Data[0] = Version;
	Data[1] = TotalSize;
}

void PopMovie::TPerformanceStats::PushToBuffer(ArrayBridge<Unity::uint>& Data,size_t StreamIndex,SoyTime StreamTime)
{
	//	add n bits of data and increase length
	auto& DataLength = Data[1];
	
	auto Push = [&](Unity::uint v)
	{
		//	gr: oob access doesn't throw at the moment, so check bounds manually
		int Index = std::min<int>( DataLength, Data.GetSize()-1 );
		DataLength++;
		Data[Index] = v;
	};
	
	int BlockSize = 1 + 1 + 13;
	Push( BlockSize );
	Push( StreamIndex );

	//	gr: make sure this matches c#
	Push( StreamTime.GetTime() );
	Push( this->mLastExtractedTime.GetTime() );
	Push( this->mLastDecodeSubmittedTime.GetTime() );
	Push( this->mLastDecodedTime.GetTime() );
	Push( this->mLastBufferedTime.GetTime() );
	Push( this->mLastCopiedTime.GetTime() );
	Push( this->mFramesDecoded );
	Push( this->mFramesPopSkipped );
	Push( this->mFramesPushSkipped );
	Push( this->mFramesPushFailed );
	Push( this->mLastUpdateTextureTotalDuration.GetTime() );
	Push( this->mLastUpdateTextureLockDuration.GetTime() );
	Push( this->mLastUpdateTextureBlitDuration.GetTime() );
	
}



#if defined(TARGET_PS4)
//	gr: copied these from examples. Stub seems to be being created anyway
// We need to provide an export to force the expected stub library to be generated
__declspec (dllexport) void dummy()
{
}

// entry function called upon start processing 
int module_start(size_t argc, const void*argv)
{
	return 0;
}

// entry function called upon stop processing 
int module_stop(size_t argc, const void*argv)
{
	return 0;
}

#endif


void PopMovie::TOnFrameCopiedCallback::Call(SoyTime FrameMs)
{
	if ( mCallback == nullptr )
		throw Soy::AssertException("Null OnFrameCopiedCallback callback");

	auto Ms = size_cast<Unity::uint>( FrameMs.GetMilliSeconds().count() );
	auto& Callback = *mCallback;

	try
	{
		Callback( Ms );
	}
	catch(std::exception& e)
	{
		throw;
	}
	catch(...)
	{
		//	catch deadly exceptions
		throw Soy::AssertException("Unknown Exception calling OnFrameCopiedCallback");
	}
}

