#pragma once

#if defined(ENABLE_OPENGL)
#include <SoyOpenglContext.h>
#endif
#include "TBlitter.h"
#include <SoyPool.h>


namespace Opengl
{
	class TBlitter;
}



class Opengl::TBlitter : public Soy::TBlitter
{
public:
	TBlitter(std::shared_ptr<TPool<TTexture>> TexturePool);
	~TBlitter();

	virtual void	GetMeta(TJsonWriter& Json) override;

	void			BlitTexture(TTexture& Target,ArrayBridge<TTexture>&& Source,TContext& Context,const SoyGraphics::TTextureUploadParams& UploadParams,std::shared_ptr<TShader> OverrideShader=nullptr,std::function<void(TShaderState&)> SetUniforms=nullptr);
	void			BlitTexture(TTexture& Target,ArrayBridge<TTexture>&& Source,TContext& Context,const SoyGraphics::TTextureUploadParams& UploadParams,const char* OverrideShader,std::function<void(TShaderState&)> SetUniforms=nullptr);
	void			BlitTexture(TTexture& Target,ArrayBridge<const SoyPixelsImpl*>&& Source,TContext& Context,const SoyGraphics::TTextureUploadParams& UploadParams,const char* OverrideShader=nullptr,std::function<void(TShaderState&)> SetUniforms=nullptr);
	void			BlitError(TTexture& Target,const std::string& Error,TContext& Context);
	void			BlitNumber(TTexture& Target,TTexture& Source,uint16_t Number,vec2f DrawPosA,vec2f DrawPosB,TContext& Context);
	
	std::shared_ptr<TRenderTarget>			GetRenderTarget(TTexture& Target,TContext& Context);
	std::shared_ptr<TGeometry>				GetGeo(TContext& Context);
	std::shared_ptr<TShader>				GetShader(ArrayBridge<TTexture>& Sources,TContext& Context);

	std::shared_ptr<TShader>				GetShader(const std::string& Name,const char* Source,TContext& Context);
	std::shared_ptr<TShader>				GetBackupShader(TContext& Context);		//	shader for when a shader doesn't compile
	std::shared_ptr<TShader>				GetErrorShader(TContext& Context);
	std::shared_ptr<TShader>				GetFontShader(TContext& Context);

	std::shared_ptr<TTexture>				GetTempTexturePtr(SoyPixelsMeta Meta,TContext& Context,GLenum Mode);
	TTexture&								GetTempTexture(SoyPixelsMeta Meta,TContext& Context,GLenum Mode);

protected:
	TPool<TTexture>&						GetTexturePool();

public:
	std::shared_ptr<TPool<TTexture>>		mTempTextures;	
	std::shared_ptr<TRenderTargetFbo>		mRenderTarget;
	std::shared_ptr<TGeometry>				mBlitQuad;
	std::map<const char*,std::shared_ptr<TShader>>	mBlitShaders;	//	shader mapped to it's literal content address
};


