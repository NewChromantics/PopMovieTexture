/*

	PopMovie is the raw instance & C interface to the plugin


	Please report bugs and send feature requests to the github page
	https://github.com/NewChromantics/PopMovieTexture_Release/issues

	or email help@popmovie.xyz

*/
using UnityEngine;
using System.Collections;					// required for Coroutines
using System.Runtime.InteropServices;		// required for DllImport
using System;								// requred for IntPtr
using System.Text;
using System.Collections.Generic;

/// <summary>
/// Low-level options for PopMovie instances. Some settings are platform specific.
///	Most can be left as-is, but can be used to work around specific bugs, performance
///	issues, or just enable debug features.
/// </summary>
[Serializable]
public class  PopMovieParams
{
	[Tooltip("skip queuing old frames after decoding if in the past")]
	public bool				SkipPushFrames = true;

	[Tooltip("skip old frames when possible when presenting latest (synchronisation)")]
	public bool				SkipPopFrames = true;

	[Tooltip("allow colour conversion on graphics driver - often done in CPU (depends on platform/driver)")]
	public bool				AllowGpuColourConversion = true;	

	[Tooltip("allow internal colour conversion on CPU. Slow!")]
	public bool				AllowCpuColourConversion = false;

	[Tooltip("allow CPU->GPU pixel copying")]
	public bool				AllowSlowCopy = true;	

	[Tooltip("allow GPU->GPU copying where availible")]
	public bool				AllowFastCopy = true;

	[Tooltip("on OSX allow storing pixels on host/cpu rather than uploading to GPU (faster CPU copy)")]
	public bool				PixelClientStorage = false;

	[Tooltip("if the input image is smaller than the texture and we aren't doing a GPU->GPU copy, then stretch to fill the texture. Can be slower on some platforms and a manual UV remap will be faster")]
	public bool				StretchImage = true;

	[Tooltip("print out messages when we drop/skip frames for various reasons")]
	public bool				DebugFrameSkipping = false; 

	[Tooltip("don't queue a copy (for when we do copies on the graphics thread) if RIGHT NOW there is no new frame. Can reduce wasted time on graphics thread")]
	public bool				PeekBeforeDefferedCopy = true;

	[Tooltip("print out when there is no frame to pop. Useful if frames aren't appearing with no error")]
	public bool				DebugNoNewPixelBuffer = false;

	[Tooltip("turn on to show that unity is calling the plugin's graphics thread callback (essential for multithreaded rendering, and often is a problem with staticcly linked plugins - like ios)")]
	public bool				DebugRenderThreadCallback = false;

	[Tooltip("if your source video's timestamps don't start at 0, this resets them so the first frame becomes 0")]
	public bool				ResetInternalTimestamp = true;

	[Tooltip("use the test shader for blitting. This is just to make sure the internal blit/shader pipeline is working")]
	public bool				DebugBlit = false;

	[Tooltip("apply the transform found inside the video")]
	public bool				ApplyVideoTransform = true;

	[Tooltip("generate mip maps onto textures. Mostly for dev debugging, but some platforms don't require this and can be an optimisation when targetting limited devices/platforms")]
	public bool				GenerateMipMaps = false;

	[Tooltip("instead of getting the LATEST frame for NOW, get the CLOSEST. So frame 97 will get pixels for 99, instead of 91.")]
	public bool				PopNearestFrame = false;

	[Tooltip("show how long different functions are taking (slow functions will stall the renderthread)")]
	public bool				DebugTimers = false;

	[Tooltip("for android this uses a SurfaceTexture to decode pixels to")]
	public bool				DecoderUseHardwareBuffer = true;

	[Tooltip("on error (during decoding/rendering) if there is an error, blit our error display to the texture. (later this will include the message in GetFatalError) If off, the texture is left unchanged from the last blit")]
	public bool				BlitFatalErrors = true;

	[Tooltip("ios/osx; force decoder to output straight to RGB. Slower in most cases as the YUV->RGB conversion is done on CPU instead of GPU, but it's done OFF the render thread, saves a bit of GPU time and in some cases we may be doing YUV conversion wrong and the OS presumably will do it right")]
	public bool				ForceNonPlanarOutput = false;

	[Tooltip("Force a YUV colour source to decode as Full-range")]
	public bool				ForceYuvRangeFull = false;

	[Tooltip("Force a YUV colour source to decode as NTSC-range")]
	public bool				ForceYuvRangeNtsc = false;

	[Tooltip("Force a YUV colour source to decode as SMPTEC-range")]
	public bool				ForceYuvRangeSmptec = false;

	[Tooltip("usual YUV conversion. Turn off for debugging (will only render the Luma Y channel, and only applies to bi-planar formats)")]
	public bool				MergeYuv = true;

	[Tooltip("Clear texture before blitting. Can cost GPU time on some platforms, use for debugging")]
	public bool				ClearBeforeBlit = true;

	[Tooltip("Enable audio stream extraction/decompression)")]
	public bool				EnableAudioStreams = true;

	[Tooltip("Enable video stream extraction (for kinect)")]
	public bool				 EnableVideoStreams = true;

	[Tooltip("Enable depth video stream extraction (for kinect)")]
	public bool				EnableDepthStreams = true;

	[Tooltip("Enable skeleton text stream extraction (for kinect)")]
	public bool				EnableSkeletonStreams = true;

	[Tooltip("Enable alpha channel where applicable")]
	public bool				EnableAlpha = true;

	[Tooltip("Copy frame based on the time when UpdateTexture is called")]
	public bool				CopyFrameAtUpdateTime = false;

	[Tooltip("Extractors will only submit keyframes for decoding. For Debug.")]
	public bool				OnlyExtractKeyframes = false;

	[Tooltip("Some platforms require height padding with YUV formats. This turns it off for debugging")]
	public bool				ApplyHeightPadding = true;

	[Tooltip("Some platforms require width padding with YUV formats. This turns it off for debugging")]
	public bool				ApplyWidthPadding = true;

	[Tooltip("For window: capture, should we include window borders")]
	public bool				WindowIncludeBorders = true;

	[Tooltip("LOTS of additional Debug output. It's possible this can slow down the audio thread so can lead to noise, and Debug.log is slow so can harm FPS")]
	public bool				VerboseDebug = false;

    [Tooltip("Windows only: Force decoding down some win7 specific paths to detect bugs")]
    public bool             Windows7Emulation = false;

	[Tooltip("Treat each channel in the audio as a seperate stream to send different channels to different AudioSources")]
	public bool				SplitAudioChannelsIntoStreams = false;

	[Tooltip("Split seperate YUV video planes as seperate streams")]
	public bool				SplitVideoPlanesIntoStreams = false;

	[Tooltip("Enable multithreadeding for specific decoders")]
	public bool				EnableDecoderThreading = true;

	[Tooltip("For webcams and streaming devices, use a live timestamp instead of the REAL timestamp to allow pausing and automatic catchup, but timestamps wont be camera-accurate")]
	public bool				UseLiveTimestampsForStreaming = true;

	[Tooltip("For some cases, copy buffers at extraction rather than in decode")]
	public bool				CopyBuffersInExtraction = false;

	[Tooltip("Skip packet extraction early where applicable")]
	public bool				ExtractorPreDecodeSkip = false;

	[Tooltip("Android only: allocate SurfaceTexture in single-buffer mode")]
	public bool				AndroidSingleBufferMode = false;

	[Tooltip("Draw frame timestamps onto texture")]
	public bool				DrawTimestamps = false;
}


[Serializable]
public class PopMovieMeta
{
	[Tooltip("Last time submitted to SetTime()")]
	public int		CurrentTimeMs = 0;

	[Tooltip("Duration of movie. 0 if unknown - eg. streaming")]
	public int		DurationMs = 0;

	[Tooltip("Known duration of movie. Last timecode if Duration unknown")]
	public int		KnownDurationMs = 0;

	[Tooltip("Not all videos/platforms/sources can seek")]
	public bool		CanSeekBackwards = false;

	//	for more streams, they follow the same variable naming convention.
	//	There may be more (many debug) entries than this in the Json.
	//	Use the Inspector to see what all other variables exist (these are just the commonly used ones)

	public string	Stream0_Codec;
	public string	Stream0_Type;
	public int		Stream0_Duration = 0;
	public int		Stream0_Width = 0;
	public int		Stream0_Height = 0;
	public int		Stream0_EncodingBitRate = 0;
	public string	Stream0_Error;
	public int[]	Stream0_NextFrameTime;
	public int		Stream0_LastCopiedTime = 0;
	public int		Stream0_LastExtractedTime = 0;
	public int		Stream0_LastDecodedTime = 0;
	public int		Stream0_FramesDecoded = 0;
	public int		Stream0_FramesSkippedTotal = 0;

	//	stream 1 is commonly audio
	public string	Stream1_Codec;
	public int		Stream1_Duration = 0;
	public int		Stream1_Width = 0;
	public int		Stream1_Height = 0;
	public int		Stream1_EncodingBitRate = 0;
	public string	Stream1_Error;
	public int		Stream1_LastCopiedTime = 0;
	public int		Stream1_LastExtractedTime = 0;
	public int		Stream1_LastDecodedTime = 0;
	public int		Stream1_FramesDecoded = 0;
	public int		Stream1_FramesSkippedTotal = 0;
}

//	performance statistics
public class PopMoviePerformance
{
	public class StreamPerformance
	{
		//	ms of the last packet we handled (note: buffered can come in out of order)
		//	use this to work out if extraction, decoding, or something else is falling behind. 
		//	apart from LastCopiedTime, they should all be ahead of Time
		public uint		Time;						//	time of the stream when snapshot was taken
		public uint		LastExtractedTime;			//	basically last known timecode
		public uint		LastDecodeSubmittedTime;
		public uint		LastDecodedTime;			//	last decoded, even if discarded
		public uint		LastBufferedTime;			//	oldest time in the buffer
		public uint		LastCopiedTime;				//	last one copied

		//	ever growing numbers
		public uint		FramesDecoded;
		public uint		FramesPopSkipped;
		public uint		FramesPushSkipped;
		public uint		FramesPushFailed;

		//	profiler durations
		public uint		UpdateTextureTotalDuration;
		public uint		UpdateTextureLockDuration;	//	time spent grabbing data from OS/Decoder buffers
		public uint		UpdateTextureBlitDuration;	//	time spent doing internal blit (YUV merge etc)
	}

	public Dictionary<int,StreamPerformance>	Streams = new Dictionary<int,StreamPerformance>();
}

/// <summary>
/// PopMovie represents an instance in the plugin. This doesn't auto-update, but provides an interface
///	to all the low-level functionality.
/// </summary>
public class PopMovie
{
#if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
	private const string PluginName = "PopMovieTextureOsx";
#elif UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
	private const string PluginName = "PopMovieTexture";
#elif UNITY_ANDROID
	private const string PluginName = "PopMovieTexture";
#elif UNITY_PS4
	private const string PluginName = "PopMovieTexture";
#elif UNITY_IOS
	private const string PluginName = "__Internal";
#endif

	///	<Summary>useful for android where file.IO.Exists() doesn't work.
	///	if the file is in the APK, it's not required (PopMovie will insert apk: if no prefix)
	///	Unity also changes this path for auto-detected OBB file, so if the app is split, this is required
	///	other platforms don't REQUIRE it as it will find the file in streaming assets, but will works</Summary>
	public const string	FilenamePrefix_StreamingAssets = "streamingassets:";

	///	<Summary>android only - explicitly load from APK (see streamingassets:)</Summary>
	public const string	FilenamePrefix_Apk = "apk:";

	///	<Summary>android only - explicit path relative to the SD card root, doesn't always match Application.persistentDataPath</Summary>
	public const string	FilenamePrefix_Sdcard = "sdcard:";

	///	<Summary>OS video sources (internal cameras, webcams, TV cards etc). Invalid name will output debug with a list of options</Summary>
	public const string	FilenamePrefix_Device = "device:";

	///	<Summary>Runtime generated debug data source</Summary>
	public const string	FilenamePrefix_Test = "test:";

	///	<Summary>Some systems have legacy decoders (mostly using OS media players which have bugs or sync issues)</Summary>
	public const string	FilenamePrefix_Legacy = "legacy:";

	///	<Summary>Some formats have experimental implementations. Currently applies only to .gifs</Summary>
	public const string	FilenamePrefix_Experimental = "experimental:";

	///	<Summary>internal testing</Summary>
	public const string	FilenamePrefix_Raw = "raw:";
	
	///	<Summary>BlackMagic capture cards using the Decklink interface</Summary>
	public const string FilenamePrefix_Decklink= "decklink:";

	///	<Summary>Source is pushed from c# PushBuffer functions</Summary>
	public const string	FilenamePrefix_Push = "push:";

	//	gr: after a huge amount of headaches... we're passing a bitfield for params. NOT a struct
	//	gr: cannot use bit 31 on OSX (convert() fails). (enum is signed 32 on osx, 64/unsigned32 on windows?)
	private enum PopMovieFlags
	{
		None						= 0,
		SkipPushFrames				= 1<<0,
		SkipPopFrames				= 1<<1,
		AllowGpuColourConversion	= 1<<2,
		AllowCpuColourConversion	= 1<<3,
		AllowSlowCopy				= 1<<4,
		AllowFastCopy				= 1<<5,
		PixelClientStorage			= 1<<6,
		DebugFrameSkipping			= 1<<7,
		PeekBeforeDefferedCopy		= 1<<8,
		VerboseDebug				= 1<<9,
		Win7Emulation				= 1<<10,
		DebugNoNewPixelBuffer		= 1<<11,
		DebugRenderThreadCallback	= 1<<12,
		ResetInternalTimestamp		= 1<<13,
		DebugBlit					= 1<<14,
		ApplyVideoTransform			= 1<<15,
		GenerateMipMaps				= 1<<16,
		SplitAudioChannelsIntoStreams	= 1<<17,
		PopNearestFrame				= 1<<18,
		DebugTimers					= 1<<19,
		StretchImage				= 1<<20,
		DecoderUseHardwareBuffer	= 1<<21,
		BlitFatalErrors				= 1<<22,
        ForceNonPlanarOutput		= 1<<23,
        MergeYuv					= 1<<24,
		EnableAudioStreams			= 1<<25,
		CopyFrameAtUpdateTime		= 1<<26,
		OnlyExtractKeyframes		= 1<<27,
		ApplyHeightPadding			= 1<<28,
		WindowIncludeBorders		= 1<<29,
	};

	private enum PopMovieFlags2
	{
		None							= 0,
		EnableVideoStreams				= 1 << 0,
		EnableDepthStreams				= 1 << 1,
		EnableSkeletonStreams			= 1 << 2,
		SplitVideoPlanesIntoStreams		= 1 << 3,
		EnableDecoderThreading			= 1 << 4,
		UseLiveTimestampsForStreaming	= 1 << 5,
		CopyBuffersInExtraction			= 1 << 6,
		ExtractorPreDecodeSkip			= 1 << 7,
		EnableAlpha						= 1 << 8,
		ApplyWidthPadding				= 1 << 9,
		ClearBeforeBlit					= 1 << 10,
		ForceYuvRangeFull				= 1 << 11,
		ForceYuvRangeNtsc				= 1 << 12,
		ForceYuvRangeSmptec				= 1 << 13,
		AndroidSingleBufferMode			= 1 << 14,
		DrawTimestamps					= 1 << 15,
		Unused16						= 1 << 16,
		Unused17						= 1 << 17,
		Unused18						= 1 << 18,
		Unused19						= 1 << 19,
		Unused20						= 1 << 20,
		Unused21						= 1 << 21,
		Unused22						= 1 << 22,
		Unused23 						= 1 << 23,
		Unused24 						= 1 << 24,
		Unused25 						= 1 << 25,
		Unused26 						= 1 << 26,
		Unused27 						= 1 << 27,
		Unused28 						= 1 << 28,
		Unused29 						= 1 << 29,
	};
	//	add your own callbacks here to catch debug output (eg. to a GUI)
	public static System.Action<string>		DebugCallback;
	public static bool 						EnableDebugLog 
	{
		get {
			return DebugCallbackWrapper != null;
		}
		set {
			if (value == false) {
				DebugCallback -= DebugCallbackWrapper;
				DebugCallbackWrapper = null;
			} else {
				if (DebugCallbackWrapper == null) {
					DebugCallbackWrapper = (string Message) => {
						Debug.Log (Message);
					};
					DebugCallback += DebugCallbackWrapper;
				}
			}
		}

	}

	//	internal wrapper to allow global toggling via inspector - this is essentially our "is enabled" bool
	private static System.Action<string>	DebugCallbackWrapper;

    private uint							mInstance = 0;
	private static int						mPluginEventId = 0;
	private uint							mLastSetTime = 0;		//	last time sent to the plugin. Remove this when we provide something from the plugin.
	private int								mAudioSampleRate = AudioSettings.outputSampleRate;	//	cannot read this on an audio thread, (where GetAudioBuffer will typically be called) so we have to cache it ourselves

	//	gr: we need to keep delegates around as the marshall function becomes invalid if the GC collects it, so just keep a private list
	private delegate void OnTextureCopiedDelegate(uint FrameMs);
	private List<OnTextureCopiedDelegate>	mOnTextureCopiedDelegates = new List<OnTextureCopiedDelegate>();


	[DllImport (PluginName, CallingConvention=CallingConvention.Cdecl)]
	private static extern uint		PopMovie_Alloc(string Filename,uint Params,uint Params2,uint PreSeekMs,uint AudioSampleRate,uint AudioChannelCount,StringBuilder ErrorBuffer,uint ErrorBufferSize);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern bool		PopMovie_Free(uint Instance);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern bool		PopMovie_UpdateTexture2D(uint Instance,System.IntPtr TextureId,int Width,int Height,TextureFormat textureFormat,int VideoStreamIndex);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern bool		PopMovie_UpdateRenderTexture(uint Instance,System.IntPtr TextureId,int Width,int Height,RenderTextureFormat textureFormat,int VideoStreamIndex);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern bool		PopMovie_GetAudioBuffer(uint Instance,float[] AudioBufferFloats,uint AudioBufferFloatCount,uint Channels,uint AudioSampleRate,uint StartTimeMs,int AudioStreamIndex);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern bool		PopMovie_PushRawBuffer(uint Instance,byte[] Buffer,uint BufferCount,int StreamIndex);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern bool		PopMovie_SetTime(uint Instance, uint TimeMs);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern uint		PopMovie_GetDurationMs(uint Instance);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern uint		PopMovie_GetLastFrameCopiedMs(uint Instance,int VideoStreamIndex);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern bool		PopMovie_UpdatePerformanceGraphRenderTexture(uint Instance,System.IntPtr TextureId,int Width,int Height,RenderTextureFormat textureFormat,int VideoStreamIndex,bool LocalisedGraph);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
    private static extern bool      PopMovie_UpdatePerformanceGraphTexture2D(uint Instance, System.IntPtr TextureId, int Width, int Height,TextureFormat textureFormat,int VideoStreamIndex, bool LocalisedGraph);

	[DllImport(PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern bool		PopMovie_UpdateMemoryGraphRenderTexture(uint Instance, System.IntPtr TextureId, int Width, int Height, RenderTextureFormat textureFormat);

	[DllImport(PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern bool		PopMovie_UpdateMemoryGraphTexture2D(uint Instance, System.IntPtr TextureId, int Width, int Height, TextureFormat textureFormat);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern bool		PopMovie_UpdateAudioRenderTexture(uint Instance,System.IntPtr TextureId,int Width,int Height,RenderTextureFormat textureFormat,int AudioStreamIndex);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern bool      PopMovie_UpdateAudioTexture2D(uint Instance, System.IntPtr TextureId, int Width, int Height,TextureFormat textureFormat,int AudioStreamIndex);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	public static extern int		PopMovie_GetPluginEventId();

	[DllImport (PluginName, CallingConvention=CallingConvention.Cdecl)]
	private static extern int		PopMovie_UpdateString(uint Instance,StringBuilder Buffer,uint BufferSize,bool SkipOldData,uint TextStreamIndex);

	[DllImport(PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern System.IntPtr	PopMovie_GetFrameText(uint Instance, uint TimeMs,uint TextStreamIndex);

	[DllImport(PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern void		PopMovie_ReleaseFrameText(System.IntPtr String);

	[DllImport (PluginName, CallingConvention=CallingConvention.Cdecl)]
	private static extern int       PopMovie_EnumSource(StringBuilder Buffer,uint BufferSize,uint SourceIndex);

	[DllImport (PluginName, CallingConvention=CallingConvention.Cdecl)]
	private static extern void		PopMovie_EnumSourceDirectory(string Directory);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	public static extern void		PopMovie_EnumSourcesClear();

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern System.IntPtr	PopMovie_PopDebugString();

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern void		PopMovie_ReleaseDebugString(System.IntPtr String);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	public static extern void		PopMovie_ReleaseAllExports();

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern int		PopMovie_GetMetaRevision(uint Instance);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern System.IntPtr	PopMovie_GetMetaJson(uint Instance);

	[DllImport (PluginName, CallingConvention = CallingConvention.Cdecl)]
	private static extern void		PopMovie_ReleaseMetaJsonString(System.IntPtr String);

	[DllImport(PluginName, CallingConvention = CallingConvention.Cdecl)]
	public static extern void		PopMovie_DebugPrint();

	[DllImport(PluginName, CallingConvention = CallingConvention.Cdecl)]
	public static extern void		PopMovie_DebugThrowCatch();

	[DllImport(PluginName, CallingConvention = CallingConvention.Cdecl)]
	public static extern void		PopMovie_DebugTestAlloc();

	[DllImport(PluginName, CallingConvention = CallingConvention.Cdecl)]
	public static extern void		PopMovie_DebugThread();

	[DllImport(PluginName, CallingConvention = CallingConvention.Cdecl)]
	public static extern void		PopMovie_RegisterOnTextureCopied(uint Instance,uint VideoStreamIndex,IntPtr Callback);

	[DllImport(PluginName, CallingConvention = CallingConvention.Cdecl)]
	public static extern void		PopMovie_UnregisterOnTextureCopied(uint Instance,uint VideoStreamIndex,IntPtr Callback);

	[DllImport(PluginName, CallingConvention = CallingConvention.Cdecl)]
	public static extern void		PopMovie_GetPerformanceStats(uint Instance,uint[] Data,uint DataSize);


	/// <summary>
	/// Instance constructor. If the movie fails instantly (eg. non-existant file/device) this will throw an exception
	/// <param name="Filename">Filename to the movie. This can be a fully qualified path, or just a filename. If it's just a filename, it will try and resolve this to StreamingAssets or persistent data.
	///		See the FilenamePrefix_ members for alternatives for reading from webcam devices, out of APK's and OBB's for android, and more
	///		</param>
	/// </summary>
	/// <param name="Params">Low-level decoding options. Defaults can be used (new PopMovieParams())
	///		</param>
	/// <param name="InitialTime">For platforms and files that support it, you can specify an initial time to save a re-seek (where applicable) with the first SetTime() call
	///		</param>
	public PopMovie(string Filename,PopMovieParams Params,float InitialTime=0)
	{
		try
		{
			//	gr: have to do this here, rather than variable init or we don't get exception errors (which we want to identify plugin problems)
			mPluginEventId = PopMovie_GetPluginEventId ();
		}
		catch(System.Exception e)
		{
			throw new System.Exception("Error initialising PopMovieTexture plugin (" + PluginName + ") DLL may not be installed properly: " + e.GetType().Name + "/" + e.Message );
		}

		PopMovieFlags ParamFlags = 0;
		ParamFlags |= Params.AllowCpuColourConversion	? PopMovieFlags.AllowCpuColourConversion : PopMovieFlags.None;
		ParamFlags |= Params.AllowFastCopy				? PopMovieFlags.AllowFastCopy : PopMovieFlags.None;
		ParamFlags |= Params.AllowGpuColourConversion	? PopMovieFlags.AllowGpuColourConversion : PopMovieFlags.None;
		ParamFlags |= Params.AllowSlowCopy				? PopMovieFlags.AllowSlowCopy : PopMovieFlags.None;
		ParamFlags |= Params.ApplyVideoTransform		? PopMovieFlags.ApplyVideoTransform : PopMovieFlags.None;
		ParamFlags |= Params.DebugBlit					? PopMovieFlags.DebugBlit : PopMovieFlags.None;
		ParamFlags |= Params.DebugFrameSkipping			? PopMovieFlags.DebugFrameSkipping : PopMovieFlags.None;
		ParamFlags |= Params.DebugNoNewPixelBuffer 		? PopMovieFlags.DebugNoNewPixelBuffer : PopMovieFlags.None;
		ParamFlags |= Params.DebugRenderThreadCallback	? PopMovieFlags.DebugRenderThreadCallback : PopMovieFlags.None;
		ParamFlags |= Params.PeekBeforeDefferedCopy		? PopMovieFlags.PeekBeforeDefferedCopy : PopMovieFlags.None;
		ParamFlags |= Params.PixelClientStorage			? PopMovieFlags.PixelClientStorage : PopMovieFlags.None;
		ParamFlags |= Params.ResetInternalTimestamp		? PopMovieFlags.ResetInternalTimestamp : PopMovieFlags.None;
		ParamFlags |= Params.SkipPopFrames				? PopMovieFlags.SkipPopFrames : PopMovieFlags.None;
		ParamFlags |= Params.SkipPushFrames 			? PopMovieFlags.SkipPushFrames : PopMovieFlags.None;
		ParamFlags |= Params.GenerateMipMaps 			? PopMovieFlags.GenerateMipMaps : PopMovieFlags.None;
		ParamFlags |= Params.PopNearestFrame			? PopMovieFlags.PopNearestFrame : PopMovieFlags.None;
		ParamFlags |= Params.DebugTimers				? PopMovieFlags.DebugTimers : PopMovieFlags.None;
		ParamFlags |= Params.StretchImage				? PopMovieFlags.StretchImage : PopMovieFlags.None;
		ParamFlags |= Params.DecoderUseHardwareBuffer	? PopMovieFlags.DecoderUseHardwareBuffer : PopMovieFlags.None;
		ParamFlags |= Params.BlitFatalErrors			? PopMovieFlags.BlitFatalErrors : PopMovieFlags.None;
		ParamFlags |= Params.ForceNonPlanarOutput		? PopMovieFlags.ForceNonPlanarOutput : PopMovieFlags.None;
		ParamFlags |= Params.MergeYuv					? PopMovieFlags.MergeYuv : PopMovieFlags.None;
		ParamFlags |= Params.EnableAudioStreams			? PopMovieFlags.EnableAudioStreams : PopMovieFlags.None;
		ParamFlags |= Params.CopyFrameAtUpdateTime		? PopMovieFlags.CopyFrameAtUpdateTime : PopMovieFlags.None;
		ParamFlags |= Params.OnlyExtractKeyframes		? PopMovieFlags.OnlyExtractKeyframes : PopMovieFlags.None;
		ParamFlags |= Params.ApplyHeightPadding			? PopMovieFlags.ApplyHeightPadding : PopMovieFlags.None;
		ParamFlags |= Params.WindowIncludeBorders		? PopMovieFlags.WindowIncludeBorders : PopMovieFlags.None;
		ParamFlags |= Params.VerboseDebug				? PopMovieFlags.VerboseDebug : PopMovieFlags.None;
        ParamFlags |= Params.Windows7Emulation          ? PopMovieFlags.Win7Emulation : PopMovieFlags.None;
		ParamFlags |= Params.SplitAudioChannelsIntoStreams	? PopMovieFlags.SplitAudioChannelsIntoStreams : PopMovieFlags.None;

		PopMovieFlags2 ParamFlags2 = 0;
		ParamFlags2 |= Params.EnableVideoStreams			? PopMovieFlags2.EnableVideoStreams : PopMovieFlags2.None;
		ParamFlags2 |= Params.EnableDepthStreams			? PopMovieFlags2.EnableDepthStreams : PopMovieFlags2.None;
		ParamFlags2 |= Params.EnableSkeletonStreams			? PopMovieFlags2.EnableSkeletonStreams : PopMovieFlags2.None;
		ParamFlags2 |= Params.SplitVideoPlanesIntoStreams	? PopMovieFlags2.SplitVideoPlanesIntoStreams : PopMovieFlags2.None;
		ParamFlags2 |= Params.EnableDecoderThreading		? PopMovieFlags2.EnableDecoderThreading : PopMovieFlags2.None;
		ParamFlags2 |= Params.UseLiveTimestampsForStreaming	? PopMovieFlags2.UseLiveTimestampsForStreaming : PopMovieFlags2.None;
		ParamFlags2 |= Params.CopyBuffersInExtraction		? PopMovieFlags2.CopyBuffersInExtraction : PopMovieFlags2.None;
		ParamFlags2 |= Params.ExtractorPreDecodeSkip 		? PopMovieFlags2.ExtractorPreDecodeSkip : PopMovieFlags2.None;
		ParamFlags2 |= Params.EnableAlpha 					? PopMovieFlags2.EnableAlpha : PopMovieFlags2.None;
		ParamFlags2 |= Params.ApplyWidthPadding				? PopMovieFlags2.ApplyWidthPadding : PopMovieFlags2.None;
		ParamFlags2 |= Params.ClearBeforeBlit				? PopMovieFlags2.ClearBeforeBlit : PopMovieFlags2.None;
		ParamFlags2 |= Params.ForceYuvRangeFull				? PopMovieFlags2.ForceYuvRangeFull : PopMovieFlags2.None;
		ParamFlags2 |= Params.ForceYuvRangeNtsc				? PopMovieFlags2.ForceYuvRangeNtsc : PopMovieFlags2.None;
		ParamFlags2 |= Params.ForceYuvRangeSmptec			? PopMovieFlags2.ForceYuvRangeSmptec : PopMovieFlags2.None;
		ParamFlags2 |= Params.AndroidSingleBufferMode		? PopMovieFlags2.AndroidSingleBufferMode : PopMovieFlags2.None;
		ParamFlags2 |= Params.DrawTimestamps				? PopMovieFlags2.DrawTimestamps : PopMovieFlags2.None;

		uint ParamFlags_32 = Convert.ToUInt32 (ParamFlags);
		uint ParamFlags2_32 = Convert.ToUInt32 (ParamFlags2);
		string FinalFilename = GetVideoFilename (Filename);

		//	gr: through trial and error, windows 7, unity 5.20f3 doesn't modify the buffer if the buffersize is over N(not 641 as first thought)
		StringBuilder ErrorStringBuffer = new StringBuilder(256);

		uint AudioSampleRate = (uint)AudioSettings.outputSampleRate;

		//	gr: need to properly handle quad, 5.1 etc etc
		//	http://docs.unity3d.com/ScriptReference/AudioSpeakerMode.html
		//	currently just let raw stream data through if we're not set to mono/stereo
		uint AudioChannelCount = 0;
		if (AudioSettings.speakerMode == AudioSpeakerMode.Mono)
			AudioChannelCount = 1;
		if (AudioSettings.speakerMode == AudioSpeakerMode.Stereo)
			AudioChannelCount = 2;


		uint InitialTimeMs = (uint)(Mathf.Max (0, InitialTime) * 1000.0f);
		mInstance = PopMovie_Alloc ( FinalFilename, ParamFlags_32, ParamFlags2_32, InitialTimeMs, AudioSampleRate, AudioChannelCount, ErrorStringBuffer, (uint)ErrorStringBuffer.Capacity );

		//	throw an exception if allocation fails
		if (mInstance == 0)
		{
			string AllocError = ErrorStringBuffer.ToString();
			if ( AllocError.Length == 0 )
				AllocError = "No error detected";
			throw new System.Exception("Failed to allocate PopMovieTexture: " + AllocError);
		}
	}


	~PopMovie()
	{
		Free();
	}

	/// <Summary>Free the instance explicitly to release memory. This otherwise will only be called during garbage collection.
	///		</Summary>
	public void Free()
	{
		PopMovie_Free (mInstance);
		FlushDebug ();
	}

	public bool IsAllocated()
	{
		return mInstance != 0;
	}

	/// <summary>Copy the latest frame (dictated by SetTime()) to the specified texture. With and without
	///		multithreaded rendering this will queue up a command which will do the copy. So the texture won't
	///		be immediately changed. You can check if a frame has been copied with GetLastCopiedTime()
	///	</summary>
	///	<return>Returns false if the instance is deallocated, the StreamIndex is out of range, or if there is an
	///		internal problem. It does not dictate if a frame will be copied or not.
	///	</return>
	///	<param name="StreamIndex">Allows you to copy from the Nth video-stream instead of just the first</param>
	public bool UpdateTexture(Texture Target,int StreamIndex=0)
	{
        bool Result = false;
		Update ();

		if (Target is RenderTexture) {
			RenderTexture Target_rt = Target as RenderTexture;
			Result = PopMovie_UpdateRenderTexture(mInstance, PopTexturePtrCache.GetNativeTexturePtr(Target_rt), Target.width, Target.height, Target_rt.format, StreamIndex);
		} else if (Target is Texture2D) {
			Texture2D Target_2d = Target as Texture2D;
			Result = PopMovie_UpdateTexture2D(mInstance, PopTexturePtrCache.GetNativeTexturePtr(Target_2d), Target.width, Target.height, Target_2d.format, StreamIndex);
		} else {
			Debug.LogError ("Texture is not a RenderTexture or Texture2D. Unhandled type. email help@popmovie.xyz");
		}
		FlushDebug ();
        return Result;
	}
	

	/// <summary>Queues a command to copy a graph with internal memory usage to the specified texture
	///	</summary>
	public bool UpdateMemoryGraphTexture(Texture Target)
	{
		bool Result = false;
		if (Target is RenderTexture)
		{
			RenderTexture Target_rt = Target as RenderTexture;
			Result = PopMovie_UpdateMemoryGraphRenderTexture(mInstance, PopTexturePtrCache.GetNativeTexturePtr(Target_rt), Target.width, Target.height, Target_rt.format );
		}
		else if (Target is Texture2D)
		{
			Texture2D Target_2d = Target as Texture2D;
			Result = PopMovie_UpdateMemoryGraphTexture2D(mInstance, PopTexturePtrCache.GetNativeTexturePtr(Target_2d), Target.width, Target.height, Target_2d.format );
		}
		else
		{
			Debug.LogError("Texture is not a RenderTexture or Texture2D. Unhandled type. email help@popmovie.xyz");
		}

		FlushDebug();
		return Result;
	}

	/// <summary>Queues a command to copy a graph extraction & decoding performance information to the specified texture
	///	</summary>
	///	<param name="StreamIndex">Unlike the other functions, the stream index is at a global level. Typically 0 is video,
	///		1 is audio, but the true indexes are dictated by the file.
	///	</param>
	///	<param name="LocalisedGraph">A localised graph zooms the graph in to the min & max relevant times. false will show
	///		the graphs relative to the entire video duration
	///	</param>
	public bool UpdatePerformanceGraphTexture(Texture Target,int StreamIndex=0,bool LocalisedGraph=true)
	{
        bool Result = false;
        if (Target is RenderTexture)
        {
			RenderTexture Target_rt = Target as RenderTexture;
	Result = PopMovie_UpdatePerformanceGraphRenderTexture(mInstance, PopTexturePtrCache.GetNativeTexturePtr(Target_rt), Target.width, Target.height, Target_rt.format, StreamIndex, LocalisedGraph);
		} else if (Target is Texture2D) {
			Texture2D Target_2d = Target as Texture2D;
	Result = PopMovie_UpdatePerformanceGraphTexture2D(mInstance, PopTexturePtrCache.GetNativeTexturePtr(Target_2d), Target.width, Target.height, Target_2d.format, StreamIndex, LocalisedGraph);
		} else {
			Debug.LogError ("Texture is not a RenderTexture or Texture2D. Unhandled type. email help@popmovie.xyz");
		}

        FlushDebug();
        return Result;
    }

	/// <summary>Queues a command to copy the latest audio data as a waveform to the specified texture
	///	</summary>
	public bool UpdateAudioTexture(Texture Target,int AudioStreamIndex=0)
	{
		bool Result = false;
		if (Target is RenderTexture)
		{
			RenderTexture Target_rt = Target as RenderTexture;
			Result = PopMovie_UpdateAudioRenderTexture(mInstance, PopTexturePtrCache.GetNativeTexturePtr(Target_rt), Target.width, Target.height, Target_rt.format, AudioStreamIndex );
		} else if (Target is Texture2D) {
			Texture2D Target_2d = Target as Texture2D;
			Result = PopMovie_UpdateAudioTexture2D(mInstance, PopTexturePtrCache.GetNativeTexturePtr(Target_2d), Target.width, Target.height, Target_2d.format, AudioStreamIndex );
		} else {
			Debug.LogError ("Texture is not a RenderTexture or Texture2D. Unhandled type. email help@popmovie.xyz");
		}

		FlushDebug();
		return Result;
	}


	/// <summary>Extract audio data for this buffer (typically used with OnAudioFilter on an AudioSource to push audio to Unity's sound system)
	///	</summary>
	//	<todo>move as much calculations as we can into this func (eg. start/end time from sample rate etc)</todo>
	public bool GetAudioBuffer(float[] AudioData,int Channels,uint StartTimeMs,int StreamIndex=0)
	{
		bool Result = PopMovie_GetAudioBuffer( mInstance, AudioData, (uint)AudioData.Length, (uint)Channels, (uint)mAudioSampleRate, StartTimeMs, StreamIndex );
        FlushDebug();
        return Result;
    }

	/// <summary>Get the text for this frame from a text stream (eg. subtitles). Returns null on error, but could return an empty string
	///	</summary>
	public string GetText(uint TextStreamIndex=0)
	{
		uint StartTimeMs = GetTimeMs();

		System.IntPtr StringPtr = PopMovie_GetFrameText(mInstance, StartTimeMs, TextStreamIndex );
		//	internal error
		if ( StringPtr == System.IntPtr.Zero )
			return null;

		try
		{
			string Str = Marshal.PtrToStringAnsi(StringPtr);
			PopMovie_ReleaseFrameText(StringPtr );
			return Str;
		}
		catch
		{
			PopMovie_ReleaseFrameText(StringPtr );
			return null;
		}
	}


	/// <summary>Extract the latest string data (relative to SetTime()) from the source. Not all sources will have text, this is typically used for meta data and subtitle(.srt) files
	///	</summary>
	///	<param name="SkipOldStrings">If true then we only return string data relevent to the current time. eg. for subtitles it will only return the text for this frame.
	///		If false, all accumulated text up until now will be returned
	///	</param>
	public String UpdateString(bool SkipOldStrings,int TextStreamIndex=0,int BufferSize=1000)
	{
		StringBuilder StringBuffer = new StringBuilder(BufferSize);
		var OrigLength = PopMovie_UpdateString( mInstance, StringBuffer, (uint)StringBuffer.Capacity, SkipOldStrings, (uint)TextStreamIndex );

		//	error/no string
		if ( OrigLength <= 0 )
			return null;

		//	warning if we clipped!
		if ( OrigLength > StringBuffer.Capacity )
			Debug.LogWarning("String from PopMovieTexture was clipped; " + StringBuffer.Capacity + "/" + OrigLength );

		return StringBuffer.ToString();
	}

	//	last time we sent to the movie player
	public uint GetTimeMs()
	{
		return mLastSetTime;
	}

	public float GetTime()
	{
		return (float)GetTimeMs () / 1000.0f;
	}

	public void SetTime(float TimeSecs)
	{
		//	no negatives
		TimeSecs = Mathf.Max (0, TimeSecs);
		SetTime ((uint)(TimeSecs * 1000.0f));
	}

	public void SetTime(uint TimeMs)
	{
		PopMovie_SetTime ( mInstance, TimeMs);
		mLastSetTime = TimeMs;
	}

	/// <summary>Known duration of the movie. This will return 0 if the duration is unknown (for example, streaming/camera video)
	///	</summary>
	public uint GetDurationMs()
	{
		var DurationMs = PopMovie_GetDurationMs (mInstance);
		return DurationMs;
	}

	/// <summary>Known duration of the movie. This will return 0 if the duration is unknown (for example, streaming/camera video)
	///	</summary>
	public float GetDuration()
	{
		uint DurationMs = GetDurationMs ();

		//	convert to float
		float DurationSec = (float)DurationMs / 1000.0f;
		return DurationSec;
	}

	/// <summary>Returns the time of the last frame copied to a texture. Useful for detecting if textures have been updated (on the
	///		render thread), or haven't been modified yet. 0 indicates nothing has been written. 1 is always the first frame.
	///	</summary>
	public uint GetLastFrameCopiedMs(int VideoStreamIndex=0)
	{
		var LastFrameMs = PopMovie_GetLastFrameCopiedMs(mInstance, VideoStreamIndex);
		return LastFrameMs;
	}

	/// <summary>Returns the time of the last frame copied to a texture. Useful for detecting if textures have been updated (on the
	///		render thread), or haven't been modified yet. 0 indicates nothing has been written. 1ms (0.001f) is always the first frame.
	///	</summary>
    public float GetLastFrameCopied(int VideoStreamIndex=0)
	{
		uint LastFrameMs = GetLastFrameCopiedMs(VideoStreamIndex);

		//	convert to float
		float LastFrameSec = (float)LastFrameMs / 1000.0f;
		return LastFrameSec;
	}

	/// <summary>Flush all debug text the global callback delegate/event
	///	</summary>
	public static void FlushDebug()
	{
		FlushDebug( DebugCallback );
	}

	/// <summary>Flush all debug text to a callback function
	///	</summary>
	public static void FlushDebug(System.Action<string> Callback)
	{
		int MaxFlushPerFrame = 100;
		int i = 0 ;
		while ( i++ < MaxFlushPerFrame )
		{
			System.IntPtr StringPtr = PopMovie_PopDebugString();

			//	no more strings to pop
			if ( StringPtr == System.IntPtr.Zero )
				break;

			try
			{
				string Str = Marshal.PtrToStringAnsi(StringPtr);
				if ( Callback != null )
					Callback.Invoke( Str );
				PopMovie_ReleaseDebugString( StringPtr );
			}
			catch
			{
				PopMovie_ReleaseDebugString( StringPtr );
				throw;
			}
		}
	}


	/// <summary>Call update() regularly to make sure the plugin executes render thread commands and prints out latest debug
	///	</summary>
	public void Update()
	{
		//	this can only be accessed on the main/mono thread, so we cache it here
		if ( mAudioSampleRate == 0 )
			mAudioSampleRate = AudioSettings.outputSampleRate;
		GL.IssuePluginEvent (mPluginEventId);
		FlushDebug();
	}

	static bool StringStartsWith(string String,string[] Exclusions)
	{
		foreach ( var e in Exclusions )
		{
			if ( String.StartsWith(e) )
				return true;
		}
		return false;
	}

	/// <summary>GetVideoFilename() attempts to resolve a basic filename to fully qualified filenames or platform specific
	///		protocols to ensure correct reading. For example on Android this should resolve a filename to the latest OBB expansion,
	///		or in the APK (which has different access than just using StreamingAssets).
	///	</summary>
	static string GetVideoFilename(string AssetFilename)
	{
		//	see constants FilenamePrefix_ for options

		//	resolve local filenames automatically regardless of prefix
		string Prefix = null;
		char[] Delin = ":".ToCharArray ();
		string[] Parts = AssetFilename.Split(Delin,2);
		if ( Parts.Length > 1 )
		{
			Prefix = Parts[0] + ":";
			AssetFilename = Parts[1];
		}

		//	android cannot read streaming assets file-exists, so if there is no prefix, we ASSUME it is a file in streaming assets
		//	http://answers.unity3d.com/questions/210909/android-streamingassets-file-access.html
#if UNITY_ANDROID && !UNITY_EDITOR
		if ( Parts.Length == 1 )
		{
			Prefix = FilenamePrefix_Apk;
		}
#endif

		//	auto-resolve streaming assets to a raw path
		//	android: will auto resolve OBB files and path will be prefixed with jar:file://
		if (Prefix == FilenamePrefix_StreamingAssets) {
			Prefix = null;
			if (!AssetFilename.StartsWith ("/"))
				AssetFilename = "/" + AssetFilename;
			AssetFilename = Application.streamingAssetsPath + AssetFilename;
		}

        try
		{
			String PersistentFilename = System.IO.Path.Combine(Application.persistentDataPath, AssetFilename);
			String StreamingFilename = System.IO.Path.Combine(Application.streamingAssetsPath, AssetFilename);

			if (System.IO.File.Exists (StreamingFilename))
				AssetFilename = StreamingFilename;
			if (System.IO.File.Exists (PersistentFilename))
				AssetFilename = PersistentFilename;

			//	if file exists, assume raw path will work.
			//	on android, this WORKS for persistent(downloaded data), but will FAIL for streaming assets
			if ( System.IO.File.Exists (AssetFilename)) {

				//	gr: another hack :/ find a better solution, AGAIN
				if ( Prefix != FilenamePrefix_Raw && Prefix != FilenamePrefix_Experimental && Prefix != FilenamePrefix_Push )
					Prefix = null;
			}
		}
        catch//(System.Exception e)
		{
			//	System.IO funcs throw if AssetFilename has invalid chars (which could come from an obscure window title. So probably not a filename anyway)
		}

		if ( Prefix != null )
			AssetFilename = Prefix + AssetFilename;

		return AssetFilename;
	}



	///	<summary>trigger searching of directories
	///		end directory with ** to recurse
	///		android can use sdcard: and then a path to search specific directories on SD cards(getExternalStorageDirectory)
	///	</summary>
	static public void EnumDirectory(string Directory,bool Recursive=false)
	{
		if ( Recursive && !Directory.EndsWith("**") )
			Directory += "**";

		PopMovie_EnumSourceDirectory( Directory );
	}

	///	<summary>Get the a source filename for the specified index. Use this to iterate over sources. null is returned when we've
	///		(perhaps temporarily whilst paths are searched) run out of source filenames
	///	</summary>
	static public string EnumSource(uint Index)
	{
		//	gr: through trial and error, windows 7, unity 5.20f3 doesn't modify the buffer if the buffersize is over N(not 641 as first thought)
		StringBuilder StringBuffer = new StringBuilder(256);
		var OrigLength = PopMovie_EnumSource(StringBuffer, (uint)StringBuffer.Capacity, Index );
	
        //	out of sources (negative on error)
        if (OrigLength <= 0)
            return null;
	
		string Source = StringBuffer.ToString();

		//	warning if we clipped!
		if (OrigLength > StringBuffer.Capacity)
			Debug.LogWarning("Filename from PopMovie_EnumSource was clipped; " + StringBuffer.Capacity + "/" + OrigLength + "; " + Source);

		return Source;
	}

	///	<summary>Enumerate as many video/file sources as possible in one go to a callback function
	///		see EnumDirectory()
	///	</summary>
	static public void EnumSources(System.Action<string> OnFoundSource,uint MaxSources=100,string SearchDirectory=null)
	{
		//	gr: through trial and error, windows 7, unity 5.20f3 doesn't modify the buffer if the buffersize is over N(not 641 as first thought)
		StringBuilder StringBuffer = new StringBuilder(256);

		EnumDirectory( SearchDirectory );
		EnumDirectory( Application.streamingAssetsPath, true );
		EnumDirectory( Application.persistentDataPath, true );
		EnumDirectory( FilenamePrefix_Sdcard, true );

		for (uint i = 0; i < MaxSources; i++)
		{
			//	if we don't reset the string builder (like a flush after tostring?) then overwriting the c buffer makes no difference and we get the first string again
			StringBuffer.Length = 0;

			var OrigLength = PopMovie_EnumSource(StringBuffer, (uint)StringBuffer.Capacity, i);
			//	out of sources (negative on error)
            if (OrigLength <= 0)
                break;
 
			string Source = StringBuffer.ToString();

			//	warning if we clipped!
			if (OrigLength > StringBuffer.Capacity)
				Debug.LogWarning("Filename from PopMovie_EnumSource was clipped; " + StringBuffer.Capacity + "/" + OrigLength + "; " + Source);

			OnFoundSource(Source);
		}
	}

    static public List<string> EnumSources(uint MaxSources = 100, string SearchDirectory = null)
    {
	    List<string> Sources = new List<String>();

		EnumSources ((string Filename) => {
			Sources.Add (Filename);
		}, MaxSources, SearchDirectory);

		return Sources;
    }


	/// <summary>With the push: protocol, send h264 packet data directly to OS decoder
	///	</summary>
	public bool PushBuffer(byte[] Data,int StreamIndex=0)
	{
		bool Result = PopMovie_PushRawBuffer( mInstance, Data, (uint)Data.Length, StreamIndex );
        FlushDebug();
        return Result;
    }

	
	///	<summary>Internal build number. If you submit any bugs, please include this!
	///	</summary>
	static public string GetVersion()
	{
		return "b79a21f";
	}

	///	<summary>Get raw meta information for the stream in Json format (may contain
	///		more data than encapsulated with PopMovieMeta. See GetMeta()
	///	</summary>
	public string GetMetaJson()
	{
		System.IntPtr StringPtr = PopMovie_GetMetaJson( mInstance );
		//	internal error
		if ( StringPtr == System.IntPtr.Zero )
			return null;

		try
		{
			string Str = Marshal.PtrToStringAnsi(StringPtr);
			PopMovie_ReleaseMetaJsonString( StringPtr );
			return Str;
		}
		catch
		{
			PopMovie_ReleaseMetaJsonString( StringPtr );
			return null;
		}
	}

	
	///	<summary>Templated GetMeta to allow struct'ing of more of the meta
	///	</summary>
	public T GetMeta<T>() where T : class
	{
#if UNITY_5_3_OR_NEWER
		var Json = GetMetaJson();
		try
		{
			var Meta = JsonUtility.FromJson<T>( Json );
			return Meta;
		}
		catch
#endif
		{
			return null;
		}
	}

	///	<summary>Get Meta information from the stream[s] as a struct. GetMetaJson() may contain much more information.
	///		JsonUtility was introduced in 5.3. So for 5.2 this will just return null.
	///	</summary>
	public PopMovieMeta GetMeta()
	{
		return GetMeta<PopMovieMeta>();
	}

	///	<summary>Whenever stream information changes this number changes. Use it to detect new streams (eg. delayed initialisation).
	///			returns -1 on error
	///	</summary>
	public int GetMetaRevision()
	{
		return PopMovie_GetMetaRevision( mInstance );
	}



	///	<summary>Provide a function to be called immediately when a frame is copied. (Use a lambda if you want specific data.
	///			Note: this will be called on the RENDER THREAD, so it is NOT THREADSAFE with unity objects.
	///	</summary>
	public void RegisterOnFrameCopiedCallback(System.Action<uint> OnFrameCopied,int VideoStreamIndex=0)
	{
		//	make a delegate
		var Delegate = new OnTextureCopiedDelegate(OnFrameCopied);
		var DelegatePtr = Marshal.GetFunctionPointerForDelegate(Delegate);
		mOnTextureCopiedDelegates.Add( Delegate);
		PopMovie_RegisterOnTextureCopied( mInstance, (uint)VideoStreamIndex, DelegatePtr );
	}


	///	<summary>Grab the last known performance/frame statistics
	///	</summary>
	public PopMoviePerformance GetPerformanceStats()
	{
		// fill an int buffer and then pull out the data
		int ExpectedVersion = 4;
		int MaxStreams = 10;
		int AssumedStreamDataSize = 15;
		int HeaderSize = 2;	//	version + length
		var DataBuffer = new uint[(MaxStreams * AssumedStreamDataSize) + HeaderSize];
		DataBuffer[0] = 0;
		DataBuffer[1] = 0;
		PopMovie_GetPerformanceStats (mInstance, DataBuffer, (uint)DataBuffer.Length);

		var Version = DataBuffer [0];
		var Length = DataBuffer [1];
		if ( Version != ExpectedVersion )
			throw new System.Exception("Performance data version mismatch; " + Version + ">" + ExpectedVersion + " (expected)");
		if ( Length > DataBuffer.Length )
			throw new System.Exception("Not enough data provided for performance data; " + DataBuffer.Length + ", requires " + Length);

		var Perf = new PopMoviePerformance();
		int StreamSize = 1;
		//	max to avoid infinite loop
		for ( int i=HeaderSize;	i<(int)Length;	i+=Mathf.Max(1,StreamSize) )
		{
			var d = i;
			StreamSize = (int)DataBuffer[d++];

			int StreamIndex = (int)DataBuffer[d++];

			var StreamPerf = new PopMoviePerformance.StreamPerformance();
			StreamPerf.Time = DataBuffer[d++];
			StreamPerf.LastExtractedTime = DataBuffer[d++];
			StreamPerf.LastDecodeSubmittedTime = DataBuffer[d++];
			StreamPerf.LastDecodedTime = DataBuffer[d++];
			StreamPerf.LastBufferedTime = DataBuffer[d++];
			StreamPerf.LastCopiedTime = DataBuffer[d++];
			StreamPerf.FramesDecoded = DataBuffer[d++];
			StreamPerf.FramesPopSkipped = DataBuffer[d++];
			StreamPerf.FramesPushSkipped = DataBuffer[d++];
			StreamPerf.FramesPushFailed = DataBuffer[d++];
			StreamPerf.UpdateTextureTotalDuration = DataBuffer[d++];
			StreamPerf.UpdateTextureLockDuration = DataBuffer[d++];
			StreamPerf.UpdateTextureBlitDuration = DataBuffer[d++];

			//	avoid exception for bad data (shouldn't have multiple streams
			if ( Perf.Streams.ContainsKey(StreamIndex) )
				continue;

			Perf.Streams.Add( StreamIndex, StreamPerf );
		}
		return Perf;
	}
}

//	for mulitple editor runs, clear the enum sources list
#if UNITY_EDITOR
[UnityEditor.InitializeOnLoad]
class PopMovieEditor
{
	static PopMovieEditor ()
	{
		try
		{
			//	false on bootup, true when starting play mode.
			//	don't call this on bootup, to let developers (ME) load a new plugin DLL
			if ( UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode )
			{
				//	about to play
				PopMovie.PopMovie_EnumSourcesClear ();

				//	clear debug strings we may not have flushed
				PopMovie.PopMovie_ReleaseAllExports();
			}
		}
		catch
		{
		}
	}
}
#endif

