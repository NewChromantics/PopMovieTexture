#pragma once

#include <SoyMedia.h>
#include "TStreamDecoder.h"

namespace Mpegts
{
	class TRawExtractor;
	class TRawFileExtractor;
	class TRawSocketExtractor;
	class TProtocol;
}

namespace MpegtsStream
{
	enum Type
	{
		Invalid				= 0xff,
		data				= 0,	//	gr: real?
		mpeg2_video_a		= 0x01,
		mpeg2_video_b		= 0x02,
		mpeg2_video_TSOnly	= 0x80,	//	non-mp2ts
		lpcm_audio			= 0x80,	//	mp2ts/LargePacketHdmv
		h264_video			= 0x1b,
		vc1_video			= 0xea,
		ac3_audio_a			= 0x06,
		ac3_audio_b			= 0x81,
		ac3_audio_c			= 0x83,
		mpeg2_audio_a		= 0x03,
		mpeg2_audio_b		= 0x04,
		dts_audio_a			= 0x82,
		dts_audio_b			= 0x86,
		dts_audio_c			= 0x8a,
	};
	
	inline Type				FromByte(uint8 Value)	{	return static_cast<Type>( Value );	}
	SoyMediaFormat::Type	ToMediaFormat(Type Format,bool Mpeg2TsFormat);
	
	DECLARE_SOYENUM( MpegtsStream );
}

namespace ts
{
	class demuxer;
}

//	this READS out(extracts) packet chunks int packets
class Mpegts::TProtocol : public TMediaPacket_Protocol
{
public:
	TProtocol(std::shared_ptr<ts::demuxer> Header) :
		mHeader	( Header )
	{
	}
	
	virtual TProtocolState::Type	Decode(TStreamBuffer& Buffer) override;
	
public:
	std::shared_ptr<ts::demuxer>	mHeader;
};




//	demuxer for a raw stream
class Mpegts::TRawExtractor : public TStreamExtractor
{
public:
	TRawExtractor(const TMediaExtractorParams& Params) :
		TStreamExtractor	( Params )
	{
	}
	
protected:
	virtual std::shared_ptr<TMediaPacket_Protocol>	AllocMediaPacketProtocol() override;
	virtual void									GetStreams(ArrayBridge<TStreamMeta>&& Streams) override;
	
private:
	std::shared_ptr<ts::demuxer>	mHeader;
};




//	demuxer for a raw stream
class Mpegts::TRawFileExtractor : public TRawExtractor
{
public:
	TRawFileExtractor(const TMediaExtractorParams& Params);
	
protected:
	virtual std::shared_ptr<TStreamReader>	AllocInputStreamReader() override
	{
		return mFileReader;
	}
	
	std::shared_ptr<TStreamReader>		mFileReader;
};



//	demuxer for a raw stream
class Mpegts::TRawSocketExtractor : public TRawExtractor
{
public:
	TRawSocketExtractor(const TMediaExtractorParams& Params);
	
protected:
	virtual std::shared_ptr<TStreamReader>			AllocInputStreamReader() override
	{
		//	this needs a socket connection before I can create the read thread... so... this needs to be able to fail?
		return mConnection->GetReadThread();
	}
	
	std::shared_ptr<THttpConnection>	mConnection;
};



namespace ac3
{
	class counter
	{
	private:
		uint16 st;
		uint32 ctx;
		uint16 skip;
		uint64 frame_num;
	public:
		counter(void):st(0),ctx(0),skip(0),frame_num(0) {}
		
		void parse(const char* p,int l)
		{
			static const uint16 frame_size_32khz[]=
			{
				96,96,120,120,144,144,168,168,192,192,240,240,288,288,336,336,384,384,480,480,576,576,672,672,768,768,960,
				960,1152,1152,1344,1344,1536,1536,1728,1728,1920,1920
			};
			static const uint16 frame_size_44khz[]=
			{
				69,70,87,88,104,105,121,122,139,140,174,175,208,209,243,244,278,279,348,349,417,418,487,488,557,558,696,
				697,835,836,975,976,1114,1115,1253,1254,1393,1394
			};
			static const uint16 frame_size_48khz[]=
			{
				64,64,80,80,96,96,112,112,128,128,160,160,192,192,224,224,256,256,320,320,384,384,448,448,512,512,640,640,
				768,768,896,896,1024,1024,1152,1152,1280,1280
			};
			
			for(int i=0;i<l;)
			{
				if(skip>0)
				{
					int n=l-i;
					if(n>skip)
						n=skip;
					i+=n;
					skip-=n;
					
					if(i>=l)
						break;
				}
				
				
				ctx=(ctx<<8)+((unsigned char*)p)[i];
				
				switch(st)
				{
					case 0:             // wait 0x0b77 marker
						if((ctx&0xffff0000)==0x0b770000)
						{
							st++;
							frame_num++;
						}
						break;
					case 1:
						st++;
						break;
					case 2:
					{
						int frmsizecod=(ctx>>8)&0x3f;
						if(frmsizecod>37)
							frmsizecod=0;
						
						int framesize=0;
						
						switch((ctx>>14)&0x03)
						{
							case 0: framesize=frame_size_48khz[frmsizecod]; break;
							case 1: framesize=frame_size_44khz[frmsizecod]; break;
							case 2: framesize=frame_size_32khz[frmsizecod]; break;
						}
						
						skip=framesize*2-6;
						
						st=0;
						break;
					}
				}
				
				i++;
				
			}
		}
		uint64 get_frame_num(void) const { return frame_num; }
		
		void reset(void)
		{
			st=0;
			ctx=0;
			skip=0;
			frame_num=0;
		}
	};
}

namespace h264
{
	class counter
	{
	private:
		uint32 ctx;
		uint64 frame_num;                            // JVT NAL (h.264) frame counter
	public:
		counter(void):ctx(0),frame_num(0) {}
		
		void parse(const char* p,int l)
		{
			for(int i=0;i<l;i++)
			{
				ctx=(ctx<<8)+((unsigned char*)p)[i];
				if((ctx&0xffffff1f)==0x00000109)    // NAL access unit
					frame_num++;
			}
		}
		
		uint64 get_frame_num(void) const { return frame_num; }
		
		void reset(void)
		{
			ctx=0;
			frame_num=0;
		}
	};
}



namespace ts
{
	class table;
	class demuxer;
	class stream;
	class counter_ac3;
	class counter_h264;
	
	inline uint8 to_byte(const char* p)
	{ return *((unsigned char*)p); }
	
	inline uint16 to_int(const char* p)
	{ uint16 n=((unsigned char*)p)[0]; n<<=8; n+=((unsigned char*)p)[1]; return n; }
	
	inline uint32 to_int32(const char* p)
	{
		uint32 n=((unsigned char*)p)[0];
		n<<=8; n+=((unsigned char*)p)[1];
		n<<=8; n+=((unsigned char*)p)[2];
		n<<=8; n+=((unsigned char*)p)[3];
		return n;
	}
}

class ts::table
{
public:
	enum { max_buf_len=512 };
	
	char buf[max_buf_len];
	
	uint16 len;
	
	uint16 offset;
	
	table(void):offset(0),len(0) {}
	
	void reset(void) { offset=0; len=0; }
};
	

class ts::counter_ac3
{
public:
	counter_ac3(void) {}
	
	void parse(const char* p,int l)
	{
	}
	
	uint64 get_frame_num(void) const { return 0; }
	
	void reset(void)
	{
	}
};
	
class ts::stream
{
public:
	stream(uint16 Pid,uint16 Channel,MpegtsStream::Type Type,bool LargePacketHdmv) :
		mPid				( Pid ),
		mChannel			( Channel ),
		mType				( Type ),
		mLargePacketHdmv	( LargePacketHdmv ),
		stream_id(0),
		dts(0),
		first_dts(0),
		first_pts(0),
		last_pts(0),
		frame_length(0),
		frame_num(0)
	{
	}

	uint64 get_es_frame_num(void) const
	{
		if(frame_num_h264.get_frame_num())
			return frame_num_h264.get_frame_num();
		
		if(frame_num_ac3.get_frame_num())
			return frame_num_ac3.get_frame_num();
		
		return 0;
	}
	
	TStreamMeta		GetMeta();
	
public:
	uint16				mChannel;	// channel number (1,2 ...)
	MpegtsStream::Type	mType;
	uint16				mPid;
	bool				mLargePacketHdmv;
	
	table psi;                              // PAT,PMT cache (only for PSI streams)
	
	uint8 stream_id;                     //	stream id linking video & audio together (ie, not unique)

	uint64 dts;                          // current MPEG stream DTS (presentation time for audio, decode time for video)
	uint64 first_dts;
	uint64 first_pts;
	uint64 last_pts;
	uint32 frame_length;                 // frame length in ticks (90 ticks = 1 ms, 90000/frame_length=fps)
	uint64 frame_num;                    // frame counter
	
	h264::counter frame_num_h264;           // JVT NAL (h.264) frame counter
	ac3::counter  frame_num_ac3;            // A/52B (AC3) frame counter
	

};
	
	
class ts::demuxer
{
public:
	demuxer(std::function<void()> OnStreamsChanged) :
		mOnStreamsChanged	( OnStreamsChanged ),
		base_pts			( 0 )
	{
	}

	
	uint64	decode_pts(const char* ptr);
	double		compute_fps_from_frame_length(uint32 frame_length);
	
	// take 188/192 bytes TS/M2TS packet
	std::shared_ptr<TMediaPacket> demux_ts_packet(ArrayBridge<char>&& Data,bool LargePacketHdmv);

	stream&			AllocStream(uint16 Pid,uint16 Channel,MpegtsStream::Type Type,bool LargePacketHdmv);
	stream&			GetStream(uint16 Pid);	//	throws if we don't know the stream
	void			GetStreams(ArrayBridge<TStreamMeta>& Streams);
	
public:
	std::function<void()>	mOnStreamsChanged;
	std::map<uint16,std::shared_ptr<stream>>	mStreams;	//	streams per pid
	uint64				base_pts;
};


