#pragma once

#include "PopMovieDecoder.h"


class TestVideoDecoder : public TVideoDecoder, public SoyWorkerThread
{
public:
	TestVideoDecoder(const TVideoDecoderParams& Params,std::map<size_t,std::shared_ptr<TPixelBufferManager>>& PixelBufferManagers,std::map<size_t,std::shared_ptr<TAudioBufferManager>>& AudioBufferManagers);
	
	virtual void		StartMovie() override;

	virtual std::chrono::milliseconds	GetSleepDuration() override	
	{
		auto ms = 1000 / mFrameRate;
		return std::chrono::milliseconds( ms );	
	}
	virtual bool		Iteration() override;
	virtual void		GetStreamMeta(ArrayBridge<TStreamMeta>&& Streams) override;
	virtual TVideoMeta	GetMeta() override
	{
		TVideoMeta Meta;
		return Meta;
	}

public:
	size_t					mFrameRate;
	SoyTime					mFirstTimestamp;
	size_t					mCounter;

	//	todo: expand this to test multiple streams
	TStreamMeta				mStreamMeta;
};

