/*

	If you need your audio source to be on a seperate object to your PopMovieSimple component,
	then use this to link them together

*/
using UnityEngine;
using System.Collections;

[AddComponentMenu("PopMovie/PopMovieSimpleAudioSource")]
public class PopMovieSimpleAudioSource : MonoBehaviour {

	public PopMovieSimple	MovieSource;

	[Tooltip("Which audio stream to use")]
	public int				AudioStream = 0;

	[Header("If there is no audio source component on this object, this will create it on startup")]
	public bool				AutoCreateAudioSource = true;


	void Start() {

		if ( AutoCreateAudioSource )
		{
			var AudioSource = this.GetComponent<AudioSource>();
			if ( AudioSource == null )
			{
				gameObject.AddComponent<AudioSource> ();
			}
		}
	}

/*
	void Update () {
	}
*/
	void OnAudioFilterRead(float[] data,int Channels)
	{
		var Movie = (MovieSource == null ) ? null : MovieSource.Movie;
		if (Movie == null)
			return;

		uint StartTime = Movie.GetTimeMs ();
	
		Movie.GetAudioBuffer (data, Channels, StartTime, AudioStream );
	}
}
