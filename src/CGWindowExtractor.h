#pragma once

#include <SoyMedia.h>



namespace Platform
{
	void		EnumWindows(std::function<void(const std::string&)> Append);
}


class CGWindowExtractor : public TMediaExtractor
{
public:
	CGWindowExtractor(const TMediaExtractorParams& Params);

	virtual void									GetStreams(ArrayBridge<TStreamMeta>&& Streams) override;
	virtual std::shared_ptr<Platform::TMediaFormat>	GetStreamFormat(size_t StreamIndex) override		{	return nullptr;	}

protected:
	virtual std::shared_ptr<TMediaPacket>			ReadNextPacket() override;

public:
	std::string		mWindowTitle;
};


