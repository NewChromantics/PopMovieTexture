#include "SoyMpegts.h"

#define PID_PAT	0

typedef uint8 u_int8_t;
typedef uint16 u_int16_t;
typedef uint32 u_int32_t;
typedef uint64 u_int64_t;



std::map<MpegtsStream::Type,std::string> MpegtsStream::EnumMap =
{
	{ MpegtsStream::Invalid,			"invalid" },
	{ MpegtsStream::data,				"data" },
	{ MpegtsStream::mpeg2_video_a,		"mpeg2_video_a" },
	{ MpegtsStream::mpeg2_video_b,		"mpeg2_video_b" },
	{ MpegtsStream::mpeg2_video_TSOnly,	"mpeg2_video_TSOnly" },
	{ MpegtsStream::lpcm_audio,			"lpcm_audio" },
	{ MpegtsStream::h264_video,			"h264_video" },
	{ MpegtsStream::vc1_video,			"vc1_video" },
	{ MpegtsStream::ac3_audio_a,		"ac3_audio_a" },
	{ MpegtsStream::ac3_audio_b,		"ac3_audio_b" },
	{ MpegtsStream::ac3_audio_c,		"ac3_audio_c" },
	{ MpegtsStream::mpeg2_audio_a,		"mpeg2_audio_a" },
	{ MpegtsStream::mpeg2_audio_b,		"mpeg2_audio_b" },
	{ MpegtsStream::dts_audio_a,		"dts_audio_a" },
	{ MpegtsStream::dts_audio_b,		"dts_audio_b" },
	{ MpegtsStream::dts_audio_c,		"dts_audio_c" },
};


SoyMediaFormat::Type MpegtsStream::ToMediaFormat(MpegtsStream::Type Format,bool Mpeg2TsFormat)
{
	switch ( Format )
	{
		case MpegtsStream::Invalid:			return SoyMediaFormat::Invalid;
			
			//	0x80/128 is different depending on packet/container format
		case MpegtsStream::lpcm_audio:
			//case MpegtsStream::mpeg2_video_TSOnly:
			if ( Mpeg2TsFormat )
			{
				return SoyMediaFormat::Mpeg2;
			}
			else
			{
				return SoyMediaFormat::PcmLinear_8;	//	gr: look this up as to size
			}
			break;
			
		case MpegtsStream::data:			return SoyMediaFormat::MetaData;
		case MpegtsStream::mpeg2_video_a:	return SoyMediaFormat::Mpeg2;
		case MpegtsStream::mpeg2_video_b:	return SoyMediaFormat::Mpeg2;
		case MpegtsStream::h264_video:		return SoyMediaFormat::H264_ES;
		case MpegtsStream::vc1_video:		return SoyMediaFormat::VC1;
		case MpegtsStream::ac3_audio_a:		return SoyMediaFormat::Ac3;
		case MpegtsStream::ac3_audio_b:		return SoyMediaFormat::Ac3;
		case MpegtsStream::ac3_audio_c:		return SoyMediaFormat::Ac3;
		case MpegtsStream::mpeg2_audio_a:	return SoyMediaFormat::Mpeg2Audio;
		case MpegtsStream::mpeg2_audio_b:	return SoyMediaFormat::Mpeg2Audio;
		case MpegtsStream::dts_audio_a:		return SoyMediaFormat::Dts;
		case MpegtsStream::dts_audio_b:		return SoyMediaFormat::Dts;
		case MpegtsStream::dts_audio_c:		return SoyMediaFormat::Dts;
			
		default:
			break;
	};
	
	std::stringstream Error;
	Error << "Unknown Mpegts format " << static_cast<int>(Format);
	throw Soy::AssertException( Error.str() );
}



// TODO: join TS

// 1) M2TS timecode (0-1073741823)
/*
 The extra 4-byte header is composed of two fields. The upper 2 bits are the copy_permission_indicator and the lower 30 bits are the arrival_time_stamp. The arrival_time_stamp is equal to the lower 30 bits of the 27 MHz STC at the 0x47 byte of the Transport packet. In a packet that contains a PCR, the PCR will be a few ticks later than the arrival_time_stamp. The exact difference between the arrival_time_stamp and the PCR (and the number of bits between them) indicates the intended fixed bitrate of the variable rate Transport Stream.
 
 The primary function is to allow the variable rate Transport Stream to be converted to a fixed rate stream before decoding (since only fixed rate Transport Streams fit into the T-STD buffering model).
 
 It doesn't really help for random access. 30 bits at 27 MHz only represents 39.77 seconds.
 */

// 2) TS Continuity counter (0-15..0-15..)
// 3) PES PTS/DTS



TStreamMeta ts::stream::GetMeta()
{
	TStreamMeta Meta;
	Meta.mCodec = MpegtsStream::ToMediaFormat( mType, mLargePacketHdmv );
	Meta.mStreamIndex = mPid;
	
	return Meta;
}

u_int64_t ts::demuxer::decode_pts(const char* ptr)
{
	const unsigned char* p=(const unsigned char*)ptr;
	
	u_int64_t pts=((p[0]&0xe)<<29);
	pts|=((p[1]&0xff)<<22);
	pts|=((p[2]&0xfe)<<14);
	pts|=((p[3]&0xff)<<7);
	pts|=((p[4]&0xfe)>>1);
	
	return pts;
}

double ts::demuxer::compute_fps_from_frame_length(u_int32_t frame_length)
{
	return 90000./(double)frame_length;
}

ts::stream& ts::demuxer::AllocStream(uint16 Pid,uint16 Channel,MpegtsStream::Type Type,bool LargePacketHdmv)
{
	auto& pStream = mStreams[Pid];
	
	bool Changed = false;
	
	if ( !pStream )
	{
		pStream.reset( new stream( Pid, Channel, Type, LargePacketHdmv ) );
		Changed = true;
		
	}
	
	//	check things haven't changed
	if ( pStream->mChannel != Channel )
	{
		std::Debug << "Reassigning stream " << Pid << " channel from " << pStream->mChannel << " to " << Channel << std::endl;
		Changed = true;
	}
	
	if ( pStream->mType != Type )
	{
		std::Debug << "Reassigning stream " << Pid << " type from " << pStream->mType << " to " << Type << std::endl;
		Changed = true;
	}
	
	if ( Changed )
	{
		//	don't let this kill the parser
		try
		{
			mOnStreamsChanged();
		}
		catch(std::exception& e)
		{
			std::Debug << "Ignored Mpegts mOnStreamsChanged exception: " << e.what() << std::endl;
		}
	}
	
	return *pStream;
}

ts::stream& ts::demuxer::GetStream(uint16 Pid)
{
	auto StreamIt = mStreams.find( Pid );
	if ( StreamIt == mStreams.end() )
	{
		std::stringstream Error;
		Error << "No stream with pid " << Pid;
		throw Soy::AssertException( Error.str() );
	}
	Soy::Assert( StreamIt->second != nullptr, "Stream with pid exists but null");
	return *StreamIt->second;
}

void ts::demuxer::GetStreams(ArrayBridge<TStreamMeta>& Streams)
{
	for ( auto& pStream : mStreams )
	{
		auto Pid = pStream.first;
		auto& Stream = pStream.second;
		
		if ( !Stream )
			continue;

		if ( Pid == PID_PAT )
			continue;
		
		if ( Stream->mType == MpegtsStream::Invalid )
			continue;
		
		Streams.PushBack( Stream->GetMeta() );
	}
}



namespace Mpegts
{
	class TPacketHeader;
};

class Mpegts::TPacketHeader
{
public:
	TPacketHeader(ArrayBridge<char>& Data,bool LargePacketHdmv);
	
	uint16	mPid;
	size_t	mSize;
	bool	payload_unit_start_indicator;
	size_t	mEndPtrOffset;
};


Mpegts::TPacketHeader::TPacketHeader(ArrayBridge<char>& Data,bool LargePacketHdmv) :
	mSize	( 0 ),
	mPid	( -1 ),
	payload_unit_start_indicator	( false )
{
	mEndPtrOffset = 0;
	
	u_int32_t timecode=0;
	if(LargePacketHdmv)
	{
		timecode = ts::to_int32( Data.GetArray() )&0x3fffffff;
		mSize += 4;
		mEndPtrOffset += 4;
	}
	
	mEndPtrOffset += 188;
	
	//	ts sync byte expected
	if ( Data[mSize]!=0x47 )
	{
		throw Soy::AssertException("TS sync byte missing");
	}
	
	{
		uint16 PidBytes = ts::to_int( &Data[mSize+1] );
		u_int8_t flags = ts::to_byte( &Data[mSize+3] );
		
		bool transport_error=PidBytes&0x8000;
		payload_unit_start_indicator=PidBytes&0x4000;
		bool adaptation_field_exist=flags&0x20;
		bool payload_data_exist=flags&0x10;
		//u_int8_t continuity_counter=flags&0x0f;
		
		mPid = PidBytes & 0x1fff;
		
		//	gr: dunno what this is
		if(transport_error)
		{
			throw Soy::AssertException("Transport error flag");
		}
		
		//	gr: special pid?
		if( mPid==0x1fff )
		{
			throw Soy::AssertException("Special pid 0x1fff ?");
		}
		
		//	gr: dummy packet?
		if ( !payload_data_exist)
		{
			throw Soy::AssertException("packet has no payload");
		}
		
		//	walk
		mSize+=4;
		
		// skip adaptation field
		if(adaptation_field_exist)
		{
			{
				auto* ptr = Data.GetArray() + mSize;
				auto AdaptionFieldSize = ts::to_byte(ptr);
				mSize += 1;
				mSize += AdaptionFieldSize;
			}
			
			Soy::Assert( mSize < Data.GetDataSize(), "Adaption field invalid size" );
		}
	}
}
	


std::shared_ptr<TMediaPacket> ts::demuxer::demux_ts_packet(ArrayBridge<char>&& Data,bool LargePacketHdmv)
{
	//	parse header
	Mpegts::TPacketHeader Header( Data, LargePacketHdmv );
	
	
	//	gr: get rid of this
	const char* end_ptr = &Data[Header.mEndPtrOffset];

	
	
	//	gr: multipletrack.ts
	//	pid 17
	//	pid 0
	
	try
	{
		/*stream& s = */GetStream( Header.mPid );
	}
	catch (std::exception& e)
	{
		std::Debug << "Failed to get stream, allocating. Should remove this! " << e.what() << std::endl;
		AllocStream( Header.mPid, 0xffff, MpegtsStream::Invalid, LargePacketHdmv );
	}
	
	stream& s = GetStream( Header.mPid );
	auto pid = Header.mPid;

	if ( pid==PID_PAT || (s.mChannel!=0xffff && s.mType==MpegtsStream::Invalid))
	{
		auto* ptr = Data.GetArray() + Header.mSize;
		// PSI
		
		if(Header.payload_unit_start_indicator)
		{
			// begin of PSI table
			ptr += 1;
			
			if(ptr>=end_ptr)
				throw Soy::AssertException("PSI table overrun");
			
			//	not... a psi table??
			if(*ptr!=0x00 && *ptr!=0x02)
			{
				return nullptr;
			}
			
			if(end_ptr-ptr<3)
			{
				throw Soy::AssertException("PSI table underrun");
			}
			
			u_int16_t l=to_int(ptr+1);
			
			if ( (l&0x3000) != 0x3000 )
			{
				throw Soy::AssertException("PSI table magic invalid");
			}
			
			l&=0x0fff;
			
			ptr+=3;
			
			auto len=end_ptr-ptr;
			
			if(l>len)
			{
				if(l>ts::table::max_buf_len)
				{
					throw Soy::AssertException("PSI table bigger than we can cope with");
				}
			
				std::Debug << "read new PSI table" << std::endl;
				s.psi.reset();
				
				memcpy(s.psi.buf,ptr,len);
				s.psi.offset+=len;
				s.psi.len=l;
				mOnStreamsChanged();
			
				return nullptr;
			}
			else
			{
				end_ptr=ptr+l;
			}
			
		}
		else
		{
			// next part of PSI
			if(!s.psi.offset)
			{
				throw Soy::AssertException("Missing first part of PSI");
			}
			
			auto* ptr = Data.GetArray() + Header.mSize;
			auto len=end_ptr-ptr;
			
			if(len>ts::table::max_buf_len-s.psi.offset)
			{
				throw Soy::AssertException("2nd part of PSI table too large");
			}
			
			memcpy(s.psi.buf+s.psi.offset,ptr,len);
			s.psi.offset+=len;
			
			if(s.psi.offset<s.psi.len)
			{
				return nullptr;
			}
			else
			{
				ptr=s.psi.buf;
				end_ptr=ptr+s.psi.len;
			}
		}
		
		if( pid == PID_PAT )
		{
			// PAT
			ptr += 5;
			
			if(ptr>=end_ptr)
			{
				throw Soy::AssertException("PAT table overrun");
			}
			
			auto len=end_ptr-ptr-4;
			
			if(len<0 || len%4)
			{
				throw Soy::AssertException("PAT table misaligned");
			}
			
			auto n=len/4;
			
			for(int i=0;i<n;i++,ptr+=4)
			{
				u_int16_t channel = to_int(ptr);
				u_int16_t pid = to_int(ptr+2);
				
				//	e is a marker bit
				if((pid&0xe000)!=0xe000)
				{
					throw Soy::AssertException("PAT table pid magic invalid");
				}
				
				pid&=0x1fff;
				
				AllocStream( pid, channel, MpegtsStream::Invalid, LargePacketHdmv );
			}
		}
		else
		{
			//	program map table
			ptr += 7;
			
			if ( ptr>=end_ptr )
				throw Soy::AssertException("PMT table overrun");
			
			u_int16_t info_len=to_int(ptr)&0x0fff;
			
			ptr+=info_len+2;
			end_ptr-=4;
			
			if ( ptr>=end_ptr )
				throw Soy::AssertException("PMT table overrun2");
			
			while ( ptr<end_ptr )
			{
				if(end_ptr-ptr<5)
					throw Soy::AssertException("PMT table too small<5");
				
				auto type = MpegtsStream::FromByte( to_byte(ptr) );
				u_int16_t pid = to_int(ptr+1);
				
				if((pid&0xe000)!=0xe000)
					throw Soy::AssertException("PMT table magic invalid");
				
				pid &= 0x1fff;
				
				info_len = to_int(ptr+3) & 0x0fff;
				
				ptr += 5+info_len;
				
				//	new pid on this channel
				AllocStream( pid, s.mChannel, type, LargePacketHdmv );
			}
			
			if(ptr!=end_ptr)
				throw Soy::AssertException("PMT EOD misaligned");
		}
		
		//	gr: loaded PAT/PMT
		return nullptr;
	}
	else
	{
		Soy::Assert( s.mType != MpegtsStream::Invalid, "skipping unknown format" );

		auto* ptr = &Data[Header.mSize];
		
		// PES
		
		if(Header.payload_unit_start_indicator)
		{
			s.psi.reset();
			s.psi.len=9;
		}
		
		//	eat chunks of PES into buffer
		while(s.psi.offset<s.psi.len)
		{
			
			auto len=end_ptr-ptr;
			
			if(len<=0)
				return nullptr;
			
			int n=s.psi.len-s.psi.offset;
			
			if(len>n)
				len=n;
			
			memcpy(s.psi.buf+s.psi.offset,ptr,len);
			s.psi.offset+=len;
			
			ptr+=len;
			
			if(s.psi.len==9)
				s.psi.len+=to_byte(s.psi.buf+8);
		}
		
		//	PSI still being read
		if(s.psi.len)
		{
			if(memcmp(s.psi.buf,"\x00\x00\x01",3))
				throw Soy::AssertException("PSI magic invalid");
			
			s.stream_id=to_byte(s.psi.buf+3);
			
			u_int8_t flags=to_byte(s.psi.buf+7);
			
			s.frame_num++;
			
			switch(flags&0xc0)
			{
				case 0x80:          // PTS only
				{
					u_int64_t pts=decode_pts(s.psi.buf+9);
					if(s.dts>0 && pts>s.dts)
					{
						s.frame_length=(u_int32_t)(pts-s.dts);
						/*
						if(is_video_stream_type(s.type,LargePacketHdmv))
						{
							double video_fps = compute_fps_from_frame_length(s.frame_length);
							std::Debug << "Video fps is " << video_fps << std::endl;
						}
						 */
					}
					s.dts=pts;
					
					if(pts>s.last_pts)
						s.last_pts=pts;
					
					if(!s.first_pts)
						s.first_pts=pts;
				}
					break;
					
				case 0xc0:          // PTS,DTS
				{
					u_int64_t pts=decode_pts(s.psi.buf+9);
					u_int64_t dts=decode_pts(s.psi.buf+14);

					if(s.dts>0 && dts>s.dts)
					{
						s.frame_length=(u_int32_t)(dts-s.dts);
						/*
						if(is_video_stream_type(s.type,LargePacketHdmv))
						{
							double video_fps = compute_fps_from_frame_length(s.frame_length);
							std::Debug << "Video fps is " << video_fps << std::endl;
						}
						 */
					}
					
					s.dts=dts;
					
					if(pts>s.last_pts)
						s.last_pts=pts;
					
					if(!s.first_dts)
						s.first_dts=dts;
				}
					break;
			}
			
			
			std::shared_ptr<TMediaPacket> Packet( new TMediaPacket );
			Packet->mMeta.mCodec = SoyMediaFormat::Mpeg2TS_PSI;
			Packet->mMeta.mStreamIndex = pid;
			Packet->mData.Copy( GetRemoteArray( s.psi.buf, s.psi.len, s.psi.len ) );
			s.psi.reset();
			return Packet;
		}
		
		//	is frame
		if(s.frame_num)
		{
			auto len=end_ptr-ptr;
			std::shared_ptr<TMediaPacket> Packet( new TMediaPacket );
			
			Packet->mMeta = s.GetMeta();
			Packet->mDecodeTimecode.mTime = s.dts;
			Packet->mTimecode.mTime = s.last_pts;
			/*
			switch(s.type)
			{
				case 0x1b:
					Packet->mMeta.mCodec = SoyMediaFormat::H264_ES;
					s.frame_num_h264.parse(ptr,len);
					break;
					
				case 0x06:
				case 0x81:
				case 0x83:
					Packet->mMeta.mCodec = SoyMediaFormat::Ac3;
					s.frame_num_ac3.parse(ptr,len);
					break;
			}
			*/
			Packet->mData.Copy( GetRemoteArray( ptr, len ) );
			return Packet;
		}
		else
		{
			//	not a frame?
			return nullptr;
		}
	}
}


void Mpegts::TRawExtractor::GetStreams(ArrayBridge<TStreamMeta>&& Streams)
{
	if ( !mHeader )
		return;
	
	mHeader->GetStreams( Streams );
}

std::shared_ptr<TMediaPacket_Protocol> Mpegts::TRawExtractor::AllocMediaPacketProtocol()
{
	auto OnStreamsChanged = [this]
	{
		Array<TStreamMeta> Streams;
		this->GetStreams( GetArrayBridge( Streams ) );
		this->mOnStreamsChanged.OnTriggered( GetArrayBridge( Streams ) );
	};
	
	if ( !mHeader )
		mHeader.reset( new ts::demuxer( OnStreamsChanged ) );
	
	return std::shared_ptr<TMediaPacket_Protocol>( new Mpegts::TProtocol(mHeader) );
}

TProtocolState::Type Mpegts::TProtocol::Decode(TStreamBuffer& IncomingBuffer)
{
	auto& This = *mHeader;
	
	Array<char> Buffer;
	if ( !IncomingBuffer.Pop( 188, GetArrayBridge(Buffer) ) )
	{
		return TProtocolState::Waiting;
	}
			
	if( Buffer[0]==0x47 && Buffer[4]!=0x47)
	{
		//	188 byte packet
	}
	else if( Buffer[0]!=0x47 && Buffer[4]==0x47)
	{
		//	192 byte packets, pop extra 4 bytes
		if ( !IncomingBuffer.Pop( 4, GetArrayBridge(Buffer) ) )
		{
			IncomingBuffer.UnPop( GetArrayBridge( Buffer ) );
			return TProtocolState::Waiting;
		}
	}
	else
	{
		std::Debug << "unknown stream type in TS" << std::endl;
		return TProtocolState::Ignore;
	}
		
	try
	{
		//	gr: large packet "hdmv" contains an extra timestamp
		bool LargePacketHdmv = Buffer.GetSize() == 192;
		mPacket = This.demux_ts_packet( GetArrayBridge(Buffer), LargePacketHdmv );
		if ( mPacket != nullptr )
		{
			std::Debug << "Mpegts parsed packet " << *mPacket << std::endl;
			return TProtocolState::Finished;
		}
		
		//	nothing to do yet, parse another packet
		return TProtocolState::Waiting;
	}
	catch(std::exception& e)
	{
		//	error with packet, skip
		std::Debug << "Error with TS packet, ignored; " << e.what() << std::endl;
		return TProtocolState::Ignore;
	}
}





Mpegts::TRawFileExtractor::TRawFileExtractor(const TMediaExtractorParams& Params) :
	TRawExtractor	( Params )
{
	auto AllocProtocol = [this]
	{
		return std::shared_ptr<Soy::TReadProtocol>( new TCopyOutProtocol( GetPacketStreamPtr() ) );
	};
	/*
	 #if defined(TARGET_ANDROID)
	 try
	 {
		mFileReader.reset( new Java::TFileStreamReader_ProtocolLambda( Filename, AllocProtocol ) );
		
	 }
	 catch(std::exception& e)
	 {
		std::Debug << "Failed to create android asset fd. Trying normal; " << e.what() << std::endl;
	 }
	 #endif
	 */
	if ( !mFileReader )
		mFileReader.reset( new TFileStreamReader_ProtocolLambda<>( Params.mFilename, AllocProtocol ) );
	
	mFileReader->Start();
	
	this->Start();
}



Mpegts::TRawSocketExtractor::TRawSocketExtractor(const TMediaExtractorParams& Params) :
	TRawExtractor		( Params )
{
	auto Url = Params.mFilename;
	
	auto OnConnected = [](bool&)
	{
		
	};
	auto OnError = [this](const std::string& Error)
	{
		TMediaExtractor::OnError( Error );
	};

	//	todo: content-reader callback so we can stream the data and feed it to the output stream
	auto OnHttpResponse = [this](const Http::TResponseProtocol& Response)
	{
		auto& PacketStream = GetPacketStream();
		PacketStream.Push( GetArrayBridge( Response.mContent ) );
	};
	
	mConnection.reset( new THttpConnection(Url) );
	
	mConnection->mOnConnected.AddListener( OnConnected );
	mConnection->mOnResponse.AddListener( OnHttpResponse );
	mConnection->mOnError.AddListener( OnError );
	mConnection->Start();
	
	//	send/queue initial request
	{
		Http::TRequestProtocol Request;
		std::string Protocol;
		std::string Hostname;
		uint16 Port=0;
		std::string Path;
		Soy::SplitUrl( Url, Protocol, Hostname, Port, Path );
		
		Request.mUrl = Path;
		mConnection->SendRequest(Request);
	}
	
}












