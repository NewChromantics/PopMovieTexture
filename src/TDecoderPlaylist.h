#pragma once

#include "PopMovieDecoder.h"
#include <SoySocket.h>
#include <SoyStream.h>
#include <SoyHttp.h>
#include <SoyProtocol.h>
#include <SoySocketStream.h>
#include <SoyHttpConnection.h>


class TPlaylistItem
{
public:
	const static int	UnknownSequenceIndex = -1;
	const static int	OutOfOrderSequenceIndex = -2;
	
public:
	TPlaylistItem() :
		mSequenceIndex		( UnknownSequenceIndex ),
		mByteRangeStart		( 0 ),
		mByteRangeLength	( 0 ),
		mIsKeyFrame			( false ),
		mBandwidth			( 0 )
	{
	}
	
	std::string			mPath;		//	path of playlist (gr: sometimes full url, fix this so it's always relative)
	std::string			mUrl;		//	could be another playlist! (full url!)
	std::string			mTitle;		//	song title etc. If this is a playlist in HLS, it's the "program-id" so we can identify variants of the same title
	
	size_t				mBandwidth;	//	bits per second of target
	std::string			mCodec;		//	codec of target
	SoyPixelsMeta		mVideoMeta;	//	may just be resolution for target

	int					mSequenceIndex;	//	if not -1, playlist index. if -2, it's specifically out of order, not unset
	
	size_t				mByteRangeStart;	//	byte offset into mUrl
	size_t				mByteRangeLength;	//	if not zero, this item is a chunk of the url

	bool				mIsKeyFrame;		//	if this is a keyframe, the byte offset dictates where I-Frames are in the file. if this item is a playlist, then the playlist has a list of keyframes (for probably the same media)
	
	SoyTime				mDuration;
	Array<std::string>	mAdditionalMeta;
	std::string			mEncryption;	//	if not empty contains encryption info (currently raw, but indicates it IS encrypted)
};
std::ostream& operator<<(std::ostream& out,const TPlaylistItem& in);


class TPlaylist
{
public:
	TPlaylist() :
		mPlaylistFinished		( false )
	{
	}
	
	bool		Merge(const TPlaylist& that);		//	returns if changed
	bool		Merge(const TPlaylistItem& that);
	int			GetLastSequenceIndex() const;		//	walk backwards to get the last sequence index.(walking over discontinuitys) returns -1 if none found (so +1 is valid)
	
public:
	bool					mPlaylistFinished;			//	if we've had "end of playlist" for a live stream
	Array<TPlaylistItem>	mItems;
};

class TM3u : public TPlaylist
{
public:
	TM3u(const std::string& Content,const std::string& PlaylistUrl);
	
	//	next item provided in case the header is specific to the next item rather than the playlist
	void		PushHeader(const std::string& Header,TPlaylistItem& NextItem);

	//	handle specific headers
	bool		ParseHeader(std::string& Key,std::string& Value,TPlaylistItem& NextItem);
	void		ParseStreamInfo(std::string StreamInfo,TPlaylistItem& NextItem);
	void		ParseKeyFrameStreamInfo(const std::string& StreamInfo);
	bool		ParseStreamInfo(const std::string& Key,const std::string& Value,TPlaylistItem& NextItem);
	void		ParseByteRange(const std::string& ByteRangeInfo,TPlaylistItem& NextItem);
	void		ParseAlternativeMedia(const std::string& MediaInfo);
	
public:
	bool					mOnlyHasIFrames;
	std::map<std::string,std::string>	mHeaders;
	Array<std::string>		mAlternativeMedias;		//	handle these later. groups of alternative medias. eg. `TYPE=AUDIO,GROUPID="theaudiostream` to replace STREAM-INF:AUDIO="theaudiostream"
};


class TSubDecoder
{
public:
	SoyListenerId	mOnFrameDecoded_Listener;
	SoyListenerId	mOnFramePopSkipped_Listener;
	SoyListenerId	mOnFramePushSkipped_Listener;
	SoyListenerId	mOnFramePushFailed_Listener;

	TPlaylistItem	mPlaylistItem;		//	which this was generated from
	std::shared_ptr<TVideoDecoder>	mDecoder;
};



//	gr: temporary class with additional meta. merge this into the extractor
class TPlaylistExtractor : public TMediaExtractor
{
public:
	TPlaylistExtractor(const TMediaExtractorParams& Params,TVideoDecoderFactory& ExtractorFactory);

	void			AllocSubDecoder(const TPlaylistItem& PlaylistItem);
	
	bool			Merge(const TPlaylistItem& PlaylistItem);	//	merge this meta. if merged return true (ie. this==PlaylistItem)

	
	virtual void							GetStreams(ArrayBridge<TStreamMeta>&& Streams) override;
	virtual std::shared_ptr<Platform::TMediaFormat>	GetStreamFormat(size_t StreamIndex) override;
	virtual std::shared_ptr<TMediaPacket>	ReadNextPacket() override;

private:
	std::shared_ptr<TPlaylistExtractor>			GetCurrentExtractor();
	
private:
	//	sub extractors. should be in chronological order. discarded when out of date, top should be current.
	//	gr: may need these to be dumb and only create the real extractor when parent requests it
	std::mutex									mChildrenLock;
	Array<std::shared_ptr<TPlaylistExtractor>>	mChildren;
	
	TVideoDecoderFactory&					mExtractorFactory;
};


//	extractor wrapped with extra playlist meta
//	will be redundant when playlist meta is in the media extractor
class TPlaylistExtractorWrapper : public TPlaylistExtractor
{
public:
	TPlaylistExtractorWrapper(const TMediaExtractorParams& Params,TVideoDecoderFactory& ExtractorFactory,std::shared_ptr<TMediaExtractor> Extractor);
	
	virtual void							GetStreams(ArrayBridge<TStreamMeta>&& Streams) override	{	mExtractor->GetStreams( GetArrayBridge(Streams) );	}
	virtual std::shared_ptr<Platform::TMediaFormat>	GetStreamFormat(size_t StreamIndex) override	{	return mExtractor->GetStreamFormat( StreamIndex );	}
	virtual std::shared_ptr<TMediaPacket>	ReadNextPacket() override								{	return mExtractor->ReadNextPacket();	}

public:
	std::shared_ptr<TMediaExtractor>	mExtractor;
};



class TM3u8Extractor : public TPlaylistExtractor
{
public:
	TM3u8Extractor(const TMediaExtractorParams& Params,TVideoDecoderFactory& ExtractorFactory);
	~TM3u8Extractor();
	
	
protected:
	void				DecodeM3U8(const Http::TResponseProtocol& Http,const std::string& PlaylistUrl);
	void				OnHttpError(const std::string& Error);
	void				OnPlaylistChanged(const TPlaylist& Playlist);
	
private:
	std::shared_ptr<THttpConnection>	mFetchThread;
};

