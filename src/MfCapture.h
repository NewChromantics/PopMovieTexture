#pragma once

#include "MfDecoder.h"



namespace MediaFoundation
{
	void		EnumCaptureDevices(std::function<void(const std::string&)> AppendName);

	class TCaptureExtractor;
}


class MediaFoundation::TCaptureExtractor : public MfExtractor
{
public:
	TCaptureExtractor(const TMediaExtractorParams& Params);
	~TCaptureExtractor();

protected:
	virtual void		AllocSourceReader(const std::string& Filename) override;
	virtual bool		CanSeek() override				{	return false;	}
	virtual void		FilterStreams(ArrayBridge<TStreamMeta>& Streams) override;
	virtual void		CorrectIncomingTimecode(TMediaPacket& Timecode) override;

public:
	AutoReleasePtr<IMFMediaSource>		mMediaSource;
};


