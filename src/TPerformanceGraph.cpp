#include "TPerformanceGraph.h"
#include "PopUnity.h"
#include "TBlitter.h"
#include "PopMovieDecoder.h"


namespace Opengl
{
	const char*	BlitFragShader_BarGraph =
		#include "BlitBarGraph.glsl.frag"
	;
}

namespace Directx
{
	const char*	BlitFragShader_BarGraph =
		#include "BlitBarGraph.hlsl.frag"
	;
}



TGraphBar::TGraphBar(float Progress,Soy::TRgb Colour) :
	TGraphBar	( Soy::Rectf(0,0,Progress,1), Colour )
{
}

TGraphBar::TGraphBar(Soy::Rectf Rect,Soy::TRgb Colour) :
	mColour		( Colour ),
	mRect		( Rect )
{
}


void TGraphRendererBase::Render(std::function<vec2f()> BindRenderTarget,std::function<void(std::function<void(TRenderBarFunc)>)> RenderFunc,std::function<void()> UnbindRenderTarget,ArrayBridge<TGraphBar>& Bars,float MidLine)
{
	//	stop deletion mid-render
	std::lock_guard<std::mutex> Lock(mRenderLock );
	
	auto TargetSizePx = BindRenderTarget();
	
	auto RenderLogic = [&](TRenderBarFunc RenderBarFunc)
	{
		float VertBorder = 0.01f;
		float HorzBorder = 0.01f;
		Soy::Rectf BarRect( HorzBorder, VertBorder, 1-HorzBorder, 1 );
		BarRect.h = 1.f/static_cast<float>(Bars.GetSize());
		BarRect.h -= VertBorder*2.f;

		static float MinBarWidthPx = 2.f;
		float MinBarWidth = MinBarWidthPx / TargetSizePx.x;
		
		for ( int i=0;	i<Bars.GetSize();	i++ )
		{
			try
			{
				RenderBarFunc( Bars[i], BarRect, MinBarWidth );
			}
			catch (std::exception& e)
			{
				std::Debug << "RenderBar error: " << e.what() << std::endl;
			}
			BarRect.y += BarRect.h + (VertBorder*2.f);
		}

		//	midline over the top
		float MidLineWidth = MinBarWidth;
		float MidLineHalfWidth = MidLineWidth / 2.f;
		float MidLineLeft = MidLine - MidLineHalfWidth;

		//	snap to edge
		if ( MidLineLeft < 0 )
			MidLineLeft += -MidLineLeft;
		if ( MidLineLeft+MidLineHalfWidth > 1.f )
			MidLineLeft -= 1.f - -MidLineLeft;
		
		Soy::Rectf MidLineRect( MidLineLeft+HorzBorder, 0, MidLineWidth, 1 );
		RenderBarFunc( TGraphBar(MidLineRect, PerformanceGraph::TimeColour), Soy::Rectf(0,0,1,1), MinBarWidth );
	};

	RenderFunc( RenderLogic );

	UnbindRenderTarget();
}

#if defined(ENABLE_OPENGL)
Opengl::TGraphRenderer::TGraphRenderer(std::shared_ptr<Opengl::TContext> Context) :
	mContext	( Context )
{
	Soy::Assert( mContext!=nullptr, "Graph renderer requires opengl context");
}
#endif

#if defined(ENABLE_OPENGL)
Opengl::TGraphRenderer::~TGraphRenderer()
{
	//	wait for render to finish
	std::lock_guard<std::mutex> Lock(mRenderLock );

	//	deffer object shutdown
	PopWorker::DeferDelete( mContext, mGeo );
	PopWorker::DeferDelete( mContext, mShader );
	PopWorker::DeferDelete( mContext, mRenderTarget );
	
	mContext.reset();
}
#endif


#if defined(ENABLE_OPENGL)
void Opengl::TGraphRenderer::Render(TTexture &Target,Soy::TRgb ClearColour,ArrayBridge<TGraphBar> &&Bars,float MidLine)
{
	
	//	render
	auto RenderTarget = GetRenderTarget( Target );
	auto BlitGeo = GetGeo();
	auto BlitShader = GetShader();
	
	Soy::Assert( RenderTarget!=nullptr, "Failed to get render target for blit");
	Soy::Assert( BlitGeo!=nullptr, "Failed to get geo for blit");
	Soy::Assert( BlitShader!=nullptr,"Failed to get shader for blit");

	auto BindRenderTarget = [&]
	{
		//	do bindings
		RenderTarget->Bind();
		static bool DoClearDepth = false;
		static bool DoClearColour = true;
		static bool DoClearStencil = false;
		if ( DoClearDepth )		Opengl::ClearDepth();
		if ( DoClearColour )	Opengl::ClearColour( ClearColour, 1 );
		if ( DoClearStencil )	Opengl::ClearStencil();
		return vec2f( RenderTarget->GetSize().w, RenderTarget->GetSize().h );
	};

	//	gr: this is a really odd paradigm, but as we cannot move the temporary TShaderState between functions, we need it in platform-specific scope, ie this lambda!
	//	gr: this looks like it's totally abstract now, and doesn't need any opengl/dx specific calls...
	auto Render = [&](std::function<void(TRenderBarFunc)> RealRender)
	{
		auto Shader = BlitShader->Bind();
		
		auto RenderBar = [&](const TGraphBar& Bar,Soy::Rectf Rect,float MinWidth)
		{
			//	rescale bar
			Soy::Rectf BarRect = Bar.mRect;
			BarRect.FitToRect( Rect );

			//	min width of 1 pixel if >0
			if ( BarRect.w > 0 )
				BarRect.w = std::max( BarRect.w, MinWidth );
			
			//	set uniforms
			Shader.SetUniform("Colour", Bar.mColour.mRgb );
			Shader.SetUniform("BarRect", BarRect.GetVec4() );
	
			BlitGeo->Draw();
		};

		RealRender( RenderBar );
	};

	auto UnbindRenderTarget = [&]
	{
		auto Shader = BlitShader->Bind();

		RenderTarget->Unbind();
	};


	TGraphRendererBase::Render( BindRenderTarget, Render, UnbindRenderTarget, Bars, MidLine );
}
#endif

#if defined(ENABLE_OPENGL)
std::shared_ptr<Opengl::TRenderTarget> Opengl::TGraphRenderer::GetRenderTarget(TTexture& Target)
{
	if ( mRenderTarget && mRenderTarget->mTexture != Target )
		mRenderTarget.reset();
	
	if ( mRenderTarget )
		return mRenderTarget;

	mRenderTarget.reset( new TRenderTargetFbo( Target ) );
	return mRenderTarget;
}
#endif

#if defined(ENABLE_OPENGL)
std::shared_ptr<Opengl::TGeometry> Opengl::TGraphRenderer::GetGeo()
{
	if ( mGeo )
		return mGeo;
	
	//	for each part of the vertex, add an attribute to describe the overall vertex
	SoyGraphics::TGeometryVertex Vertex;
	Array<uint8> MeshData;
	Array<size_t> Indexes;
	Soy::TBlitter::GetGeo( Vertex, GetArrayBridge(MeshData), GetArrayBridge(Indexes), false );
	
	mGeo.reset( new TGeometry( GetArrayBridge(MeshData), GetArrayBridge(Indexes), Vertex ) );
	return mGeo;
}
#endif

#if defined(ENABLE_OPENGL)
std::shared_ptr<Opengl::TShader> Opengl::TGraphRenderer::GetShader()
{
	if ( mShader )
		return mShader;
	auto Geo = GetGeo();
	if ( !Geo )
		return nullptr;
	
	mShader.reset( new TShader( BlitVertShader, BlitFragShader_BarGraph, Geo->mVertexDescription, "BarGraphShader", *mContext ) );
	return mShader;
}
#endif


#if defined(ENABLE_DIRECTX)
Directx::TGraphRenderer::TGraphRenderer(std::shared_ptr<TContext> Context) :
	mContext	( Context )
{
	Soy::Assert( mContext!=nullptr, "Graph renderer requires directx context");
}
#endif

#if defined(ENABLE_DIRECTX)
Directx::TGraphRenderer::~TGraphRenderer()
{
	//	wait for render to finish
	std::lock_guard<std::mutex> Lock(mRenderLock );

	mGeo.reset();
	mShader.reset();
	mRenderTarget.reset();
	/*
	//	deffer object shutdown
	Opengl::DeferDelete( mContext, mGeo );
    Opengl::DeferDelete( mContext, mShader );
    Opengl::DeferDelete( mContext, mRenderTarget );
	*/
	mContext.reset();
}
#endif

#if defined(ENABLE_DIRECTX)
void Directx::TGraphRenderer::Render(TTexture& Target,Soy::TRgb ClearColour,ArrayBridge<TGraphBar> &&Bars,float MidLine)
{
	//	render
	auto RenderTarget = GetRenderTarget( Target );
	auto BlitGeo = GetGeo();
	auto BlitShader = GetShader();
	
	Soy::Assert( RenderTarget!=nullptr, "Failed to get render target for blit");
	Soy::Assert( BlitGeo!=nullptr, "Failed to get geo for blit");
	Soy::Assert( BlitShader!=nullptr,"Failed to get shader for blit");

	auto BindRenderTarget = [&]
	{
		//	do bindings
		RenderTarget->Bind(*mContext);
		
		static bool DoClearDepth = false;
		static bool DoClearColour = true;
		static bool DoClearStencil = false;
		if ( DoClearDepth )		RenderTarget->ClearDepth(*mContext);
		if ( DoClearColour )	RenderTarget->ClearColour( *mContext, ClearColour, 1 );
		if ( DoClearStencil )	RenderTarget->ClearStencil(*mContext);

		return vec2f( RenderTarget->GetMeta().GetWidth(), RenderTarget->GetMeta().GetHeight() );
	};
	
	//	gr: this is a really odd paradigm, but as we cannot move the temporary TShaderState between functions, we need it in platform-specific scope, ie this lambda!
	auto Render = [&](std::function<void(TRenderBarFunc)> RealRender)
	{
		auto Shader = BlitShader->Bind(*mContext);

		auto RenderBar = [&](const TGraphBar& Bar,Soy::Rectf Rect,float MinWidth)
		{
			//	rescale bar
			Soy::Rectf BarRect = Bar.mRect;
			BarRect.FitToRect( Rect );

			if ( BarRect.w > 0 )
				BarRect.w = std::max( BarRect.w, MinWidth );

			//	set uniforms
			Shader.SetUniform("Colour", Bar.mColour.mRgb );
			Shader.SetUniform("BarRect", BarRect.GetVec4() );
	
			BlitGeo->Draw(*mContext);
		};

		Shader.Bake();
		RealRender( RenderBar );
	};

	auto UnbindRenderTarget = [&]
	{
		RenderTarget->Unbind(*mContext);
	};


	TGraphRendererBase::Render( BindRenderTarget, Render, UnbindRenderTarget, Bars, MidLine );
}
#endif

#if defined(ENABLE_DIRECTX)
std::shared_ptr<Directx::TRenderTarget> Directx::TGraphRenderer::GetRenderTarget(TTexture& Target)
{
	if ( mRenderTarget && *mRenderTarget != Target )
		mRenderTarget.reset();
	
	if ( mRenderTarget )
		return mRenderTarget;

	mRenderTarget.reset( new TRenderTarget( Target, *mContext ) );
	return mRenderTarget;
}
#endif

#if defined(ENABLE_DIRECTX)
std::shared_ptr<Directx::TGeometry> Directx::TGraphRenderer::GetGeo()
{
	if ( mGeo )
		return mGeo;
	
	//	for each part of the vertex, add an attribute to describe the overall vertex
	SoyGraphics::TGeometryVertex Vertex;
	Array<uint8> MeshData;
	Array<size_t> Indexes;
	Soy::TBlitter::GetGeo( Vertex, GetArrayBridge(MeshData), GetArrayBridge(Indexes), false );
	
	mGeo.reset( new TGeometry( GetArrayBridge(MeshData), GetArrayBridge(Indexes), Vertex, *mContext ) );
	return mGeo;
}
#endif

#if defined(ENABLE_DIRECTX)
std::shared_ptr<Directx::TShader> Directx::TGraphRenderer::GetShader()
{
	if ( mShader )
		return mShader;
	auto Geo = GetGeo();
	if ( !Geo )
		return nullptr;
	
	std::map<std::string,std::string> Macros;
	mShader.reset( new TShader( BlitVertShader, BlitFragShader_BarGraph, Geo->mVertexDescription, "BarGraphShader", Macros, *mContext ) );
	return mShader;
}
#endif
