
ConstantBuffer MyCB
{
	float4x4 worldMatrix;
	float4x4 projectionMatrix;
}
void main (float3 pos : POSITION, float4 color : COLOR, out float4 ocolor : COLOR, out float4 opos : S_Position) 
{
	float4x4 worldProj = mul ( projectionMatrix, worldMatrix);
	float3 pos3 = pos;
	pos3 *= 10.f;
	opos = mul (worldProj, float4(pos3,1));
	ocolor = color;
}



/*
ConstantBuffer ShaderConstants
{
	column_major matrix m_WorldViewProj;
};
*/

/*
struct VS_INPUT
{
	float2 UV			: TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 Position		: S_POSITION;
	float2 UV			: TEXCOORD1;
};

VS_OUTPUT main(VS_INPUT Input)
{
	VS_OUTPUT Output;
	//Output.Position = mul(m_WorldViewProj, float4(Input.Position.xyz, 1));
	//Output.Position = float4(Input.Position.xyz, 1);
	Output.Position = float4( Input.UV.x, Input.UV.y, 0, 1);
	Output.UV = Input.UV;

	return Output;
}
*/