#include "CGWindowExtractor.h"



class TWindowHandle
{
public:
	TWindowHandle() :
		mHandle	( 0 )
	{
	}
	
	//	skip windows with no name. I think this is menu's and dock icons maybe?
	bool		EnumerateWindow() const
	{
		if ( mHandle == 0 )
			return false;
		
		if ( mWindowName.empty() )
			return false;
		
		return true;
	}
	
	
	std::string	mName;
	
	CGWindowID	mHandle;
	std::string	mWindowName;
	std::string	mApplicationName;
	sint32		mPid;
	uint32		mWorkspace;
	sint32		mLayer;
	CGRect		mRect;
	float		mAlpha;
	bool		mOnScreen;
	bool		mBackingVideoMemory;
};

bool NSDictionaryHasKey(NSDictionary* Dictionary,CFStringRef Key)
{
	if ( [Dictionary objectForKey:(id)Key] == nil )
		return false;
	return true;
}

std::string NSDictionaryGetString(NSDictionary* Dictionary,CFStringRef Key)
{
	id Element = [Dictionary objectForKey:(id)Key];
	if ( Element == nil )
		return std::string();

	@try
	{
		//NSString* Value = [Element stringValue];
		NSString* Value = Element;
		if ( !Value )
			return std::string();

		return Soy::NSStringToString( Value );
	}
	@catch (NSException* e)
	{
		auto Error = Soy::NSErrorToString(e);
		throw Soy::AssertException( Error );
	}
}

TWindowHandle GetWindowHandle(NSDictionary* WindowDictionary)
{
	auto* entry = WindowDictionary;
	
	// The flags that we pass to CGWindowListCopyWindowInfo will automatically filter out most undesirable windows.
	// However, it is possible that we will get back a window that we cannot read from, so we'll filter those out manually.
	auto sharingState = [entry[(id)kCGWindowSharingState] intValue];
	//	kCGWindowSharingReadWrite
	//	kCGWindowSharingReadOnly
	if ( sharingState == kCGWindowSharingNone )
		throw Soy::AssertException("Window not readable");
	
	TWindowHandle Handle;
	
	
	
	Handle.mApplicationName = NSDictionaryGetString( WindowDictionary, kCGWindowOwnerName );
	Handle.mWindowName = NSDictionaryGetString( WindowDictionary, kCGWindowName );
	
	Handle.mPid = [entry[(id)kCGWindowOwnerPID] intValue];
	Handle.mHandle = [entry[(id)kCGWindowNumber] unsignedIntValue];
	Handle.mWorkspace = [entry[(id)kCGWindowWorkspace] unsignedIntValue];
	Handle.mLayer = [entry[(id)kCGWindowLayer] intValue];
	CGRectMakeWithDictionaryRepresentation( (CFDictionaryRef)entry[(id)kCGWindowBounds], &Handle.mRect );
	Handle.mAlpha = [entry[(id)kCGWindowAlpha] floatValue];
	
	//	optional
	Handle.mOnScreen = [entry[(id)kCGWindowAlpha] boolValue];
	Handle.mBackingVideoMemory = [entry[(id)kCGWindowBackingLocationVideoMemory] boolValue];
	
	//	gr: maybe useful for fast access later
	// The backing store type of the window, one of `kCGBackingStoreRetained',	 `kCGBackingStoreNonretained', or `kCGBackingStoreBuffered'. The value of	 this key is a CFNumber 32-bit signed integer value. */
	//	CG_EXTERN const CFStringRef  kCGWindowStoreType
	
	//	resolve window name
	//	todo: as a group so we get App.Window#3
	if ( Handle.mApplicationName.empty() )
	{
		std::stringstream AppName;
		AppName << "pid" << Handle.mPid;
		Handle.mApplicationName = AppName.str();
	}
	
	if ( Handle.mWindowName.empty() )
	{
		//Handle.mWindowName = "0";
	}

	std::stringstream Name;
	Name << Handle.mApplicationName;
	if ( !Handle.mWindowName.empty() )
		Name << "." << Handle.mWindowName;
	Handle.mName = Name.str();
	
	return Handle;
}

void EnumWindows(ArrayBridge<TWindowHandle>&& Handles,std::function<bool(const std::string&)> Filter)
{
	Soy::TScopeTimerPrint Timer(__func__,50);
	
	CGWindowListOption Flags = kCGWindowListOptionAll;
	/*
	kCGWindowListOptionAll                 = 0,
	kCGWindowListOptionOnScreenOnly        = (1 << 0),
	kCGWindowListOptionOnScreenAboveWindow = (1 << 1),
	kCGWindowListOptionOnScreenBelowWindow = (1 << 2),
	kCGWindowListOptionIncludingWindow     = (1 << 3),
	kCGWindowListExcludeDesktopElements    = (1 << 4)
	 */

	//	grab list and iterate
	CFArrayRef WindowDictionaryList = CGWindowListCopyWindowInfo( Flags, kCGNullWindowID );
	
	try
	{
		for ( int i=0;	i<CFArrayGetCount(WindowDictionaryList);	i++ )
		{
			static bool DebugWindowReading = false;
			NSDictionary* WindowDictionary = (__bridge NSDictionary*)CFArrayGetValueAtIndex( WindowDictionaryList, i );
			try
			{
				auto Handle = GetWindowHandle( WindowDictionary );
			
				if ( !Handle.EnumerateWindow() )
					continue;
				
				if ( !Filter( Handle.mName ) )
					continue;
			
				Handles.PushBack( Handle );
			}
			catch(std::exception& e)
			{
				if ( DebugWindowReading )
					std::Debug << "Error enumerating window; " << e.what() << std::endl;
			}
			catch(...)
			{
				if ( DebugWindowReading )
					std::Debug << "Unknown error enumerating window" << std::endl;
			}
		}
		CFRelease(WindowDictionaryList);
	}
	catch(...)
	{
		CFRelease(WindowDictionaryList);
		throw;
	}
}



void ReadWindowPixels(TWindowHandle Handle,SoyPixelsImpl& Pixels,bool ClientAreaOnly)
{
	static bool ExcludeShadow = true;
	
	CGWindowImageOption Flags = kCGWindowImageDefault;
	
	if ( ExcludeShadow )
		Flags |= kCGWindowImageBoundsIgnoreFraming;
	
	//	http://stackoverflow.com/a/11993127/355753
	//kCGWindowListOptionOnScreenBelowWindow | kCGWindowListOptionIncludingWindow
	auto Rect = Handle.mRect;	//	CGRectInfinite
	CGImageRef WindowImage = CGWindowListCreateImage( Rect, kCGWindowListOptionAll, Handle.mHandle, Flags );

	try
	{
		CGColorSpaceRef ColorSpace = CGColorSpaceCreateDeviceRGB();
		auto Width = CGImageGetWidth(WindowImage);
		auto Height = CGImageGetHeight(WindowImage);
		//	gr: get pixel format from Colourspace
		Pixels.Init( Width, Height, SoyPixelsFormat::RGBA );
		
		/*CGImageAlphaInfo/CGBitmapInfo*/uint32 BitmapFlags = 0;
		static bool SetByteOrder = false;
		if ( SetByteOrder )
			BitmapFlags |= (CGBitmapInfo)kCGBitmapByteOrder32Little;
		/*
		kCGBitmapAlphaInfoMask = 0x1F,
		kCGBitmapFloatComponents = (1 << 8),
		kCGBitmapByteOrderMask = 0x7000,
		kCGBitmapByteOrderDefault = (0 << 12),
		kCGBitmapByteOrder16Little = (1 << 12),
		kCGBitmapByteOrder32Little = (2 << 12),
		kCGBitmapByteOrder16Big = (3 << 12),
		kCGBitmapByteOrder32Big = (4 << 12)
		*/
	
		//	gr: only ones that work
		//	kCGImageAlphaPremultipliedLast,  /* For example, premultiplied RGBA */
		//	kCGImageAlphaPremultipliedFirst, /* For example, premultiplied ARGB */
		//  kCGImageAlphaNoneSkipLast,       /* For example, RBGX. */
		//	kCGImageAlphaNoneSkipFirst,      /* For example, XRGB. */
		
		if ( Pixels.GetFormat() == SoyPixelsFormat::RGB )
			BitmapFlags = kCGImageAlphaNoneSkipLast;
		if ( Pixels.GetFormat() == SoyPixelsFormat::RGBA )
			BitmapFlags = kCGImageAlphaPremultipliedLast;
		if ( Pixels.GetFormat() == SoyPixelsFormat::ARGB )
			BitmapFlags = kCGImageAlphaPremultipliedFirst;
		
		if ( Pixels.GetFormat() == SoyPixelsFormat::Greyscale )
			BitmapFlags = kCGImageAlphaOnly;
		
		//	this writes to pixel buffers, don't need to do lots of conversion!
		auto RowPitchBytes = Pixels.GetRowPitchBytes();
		CGContextRef Context = CGBitmapContextCreate( Pixels.GetPixelsArray().GetArray(),
														Width,
														Height,
														SoyPixelsFormat::GetBitsPerChannel( Pixels.GetFormat() ),
														RowPitchBytes,
														ColorSpace,
													 BitmapFlags);
		if ( !Context )
			throw Soy::AssertException("Failed to create CGBitmapContext");
		
		CGContextDrawImage( Context, CGRectMake(0, 0, Width, Height), WindowImage );
		CGColorSpaceRelease( ColorSpace );
		CGContextRelease( Context);
		
		CGImageRelease( WindowImage );
	}
	catch(std::exception& e)
	{
		CGImageRelease( WindowImage );
		throw;
	}
}


void Platform::EnumWindows(std::function<void(const std::string&)> Append)
{
	//	grab a raw copy of the pixels
	auto WindowFilter = [](const std::string& Name)
	{
		if ( Name.empty() )
			return false;
		return true;
	};
	Array<TWindowHandle> Handles;
	EnumWindows( GetArrayBridge(Handles), WindowFilter );

	for ( int i=0;	i<Handles.GetSize();	i++ )
	{
		auto& Handle = Handles[i];
		Append( Handle.mName );
	}
}




CGWindowExtractor::CGWindowExtractor(const TMediaExtractorParams& Params) :
	TMediaExtractor	( Params ),
	mWindowTitle	( Params.mFilename )
{
	//	if the func is missing, we haven't linked
	auto* Func = CGWindowListCopyWindowInfo;
	if ( !Func )
		throw Soy::AssertException("CoreGraphics library not linked");
	
	Start();
}

void CGWindowExtractor::GetStreams(ArrayBridge<TStreamMeta>&& Streams)
{
	TStreamMeta Meta;
	Meta.mStreamIndex = 0;
	Meta.mCodec = SoyMediaFormat::RGB;
	Streams.PushBack( Meta );
}


std::shared_ptr<TMediaPacket> CGWindowExtractor::ReadNextPacket()
{
	//	grab a raw copy of the pixels
	auto WindowFilter = [this](const std::string& Name)
	{
		if ( !Soy::StringContains( Name, mWindowTitle, false ) )
			return false;
		return true;
	};
	Array<TWindowHandle> Handles;
	EnumWindows( GetArrayBridge(Handles), WindowFilter );

	if ( Handles.IsEmpty() )
	{
		std::stringstream Error;
		Error << "Could not find window named " << mWindowTitle;
		throw Soy::AssertException( Error.str() );
	}

	static bool ClientAreaOnly = false;

	//	grab pixels
	SoyPixels Pixels;
	ReadWindowPixels( Handles[0], Pixels, ClientAreaOnly );

	//	failed to get pixels (without error)
	if ( !Pixels.IsValid() )
		return nullptr;

	//	make a packet
	std::shared_ptr<TMediaPacket> pPacket( new TMediaPacket );
	auto& Packet = *pPacket;
	Packet.mMeta.mCodec = SoyMediaFormat::FromPixelFormat( Pixels.GetFormat() );
	Packet.mMeta.mPixelMeta = Pixels.GetMeta();
	Packet.mData.Copy( Pixels.GetPixelsArray() );

	return pPacket;
}

