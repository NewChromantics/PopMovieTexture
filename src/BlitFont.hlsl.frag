
R"*###*(


#define MAX_CHARS 7

/*
uniform int NumberA;
uniform int NumberB;
uniform int NumberC;
uniform int NumberD;
uniform int NumberE;
uniform int NumberF;
uniform int NumberG;

uniform float2 DrawUVa;
uniform float2 DrawUVb;
*/
static const int NumberA = 1;
static const int NumberB = 9;
static const int NumberC = 8;
static const int NumberD = 4;
static const int NumberE = 2;
static const int NumberF = 3;
static const int NumberG = 4;

static const float2 DrawUVa = float2( 0,0 );
static const float2 DrawUVb = float2( 0.5, 0.5 );
static const float DrawSize = 0.05;
static const float DropShadow = 0.075;


//	gr: bitfield double height glyphs
//	gr: remember, these are COLUMN MAJOR. Code change below
//	gr: rather than replace all matrixes with 0.0 1.0 2.0 and making it unreadable, using letters.
#define OO	0.0
#define AA	1.0
#define BB	2.0
#define CC	3.0

static const float4x4 GLYPH_NONE = {
OO, OO, OO, OO,
OO, OO, OO, OO,
OO, OO, OO, OO,
OO, OO, OO, OO	
};

static const float4x4 GLYPH_0 = {
BB, AA, BB, OO,
CC, OO, CC, OO,
CC, OO, CC, OO,
AA, BB, AA, OO	
};

static const float4x4 GLYPH_1 = {
BB, CC, OO, OO,
OO, CC, OO, OO,
OO, CC, OO, OO,
BB, CC, BB, OO	
};

static const float4x4 GLYPH_2 = {
BB, AA, BB, OO,
OO, OO, CC, OO,
BB, AA, OO, OO,
CC, BB, BB, OO	
};

static const float4x4 GLYPH_3 = {
AA, AA, BB, OO,
OO, BB, AA, OO,
OO, AA, BB, OO,
BB, BB, AA, OO	
};

static const float4x4 GLYPH_4 = {
OO, BB, CC, OO,
CC, OO, CC, OO,
CC, BB, CC, BB,
OO, OO, CC, OO	
};

static const float4x4 GLYPH_5 = {
CC, AA, AA, OO,
CC, OO, OO, OO,
OO, AA, BB, OO,
BB, BB, AA, OO	
};

static const float4x4 GLYPH_6 = {
BB, AA, AA, OO,
CC, BB, BB, OO,
CC, OO, CC, OO,
CC, BB, CC, OO	
};

static const float4x4 GLYPH_7 = {
AA, AA, CC, OO,
OO, OO, CC, OO,
OO, CC, OO, OO,
CC, OO, OO, OO	
};

static const float4x4 GLYPH_8 = {
BB, AA, BB, OO,
AA, BB, AA, OO,
CC, OO, CC, OO,
AA, BB, AA, OO	
};

static const float4x4 GLYPH_9 = {
BB, AA, BB, OO,
CC, OO, CC, OO,
AA, AA, CC, OO,
OO, OO, CC, OO	
};



int abs_int(int x)
{
	return abs(x);
}

float Range(float Min,float Max,float Value)
{
	return (Value-Min) / (Max-Min);
}

float2 Range2(float2 Min,float2 Max,float2 Value)
{
	return (Value-Min) / (Max-Min);
}

float4x4 GetGlyph(int StringChar)
{
	int NumberString[MAX_CHARS];
	NumberString[0] = NumberA;
	NumberString[1] = NumberB;
	NumberString[2] = NumberC;
	NumberString[3] = NumberD;
	NumberString[4] = NumberE;
	NumberString[5] = NumberF;
	NumberString[6] = NumberG;
	
	float4x4 Font[10+1];
	Font[0] = GLYPH_NONE;
	Font[1] = GLYPH_0;
	Font[2] = GLYPH_1;
	Font[3] = GLYPH_2;
	Font[4] = GLYPH_3;
	Font[5] = GLYPH_4;
	Font[6] = GLYPH_5;
	Font[7] = GLYPH_6;
	Font[8] = GLYPH_7;
	Font[9] = GLYPH_8;
	Font[10] = GLYPH_9;
	
	return Font[NumberString[StringChar]+1];
}

float GetGlyphMask(float4x4 Glyph,float2 GlyphUv)
{
	if ( GlyphUv.x < 0.0 || GlyphUv.y < 0.0 || GlyphUv.x > 1.0 || GlyphUv.y > 1.0 )
		return 0.0;

	int row = int( GlyphUv.y * 4.0 );
	int col = int( GlyphUv.x * 4.0 );

	//	gr: opengl is COLUMN MAJOR
	//int Mask = Glyph[row][col];
	int Mask = int( Glyph[row][col] );
	

	bool MaskTop = abs_int(Mask - 2)==1;//	&1
	bool MaskBottom = (Mask >= 2);//	&2
	if ( frac( GlyphUv.y * 4.0) < 0.5 )
		return MaskTop ? 1.0 : 0.0;
	else
		return MaskBottom ? 1.0 : 0.0;
}

float4 GetStringColour(float2 StringUv)
{
	StringUv.x *= float(MAX_CHARS);
	float2 GlyphUv = frac( StringUv );
	int StringIndex = int( StringUv.x );
	float4x4 Glyph = GetGlyph( StringIndex );
	
	//	scale down glyph uv to fit in dropshadow
	GlyphUv *= 1.0 + DropShadow;
	
	float WhiteMask = GetGlyphMask( Glyph, GlyphUv );
	float BlackMask = GetGlyphMask( Glyph, GlyphUv - float2(DropShadow,DropShadow) );
	
	float Mask = max( WhiteMask, BlackMask );
	
	float Colour = WhiteMask;
	
	return float4( Colour, Colour, Colour, Mask );
}


float4 BlitText(float4 Colour,float2 uv,float2 DrawUv)
{
	float2 Min = DrawUv;
	float2 Max = float2( MAX_CHARS, 1.0 ) * DrawSize;
	float2 StringUv = Range2( Min, Min+Max, uv );
	
	if ( StringUv.x > 0.0 && StringUv.y > 0.0 && StringUv.x < 1.0 && StringUv.y < 1.0 )
	{
		//	gr: had to flip this in unity, but not glsl?
		//StringUv.y = 1- StringUv.y;
		float4 White = GetStringColour( StringUv );
		Colour.xyz = lerp( Colour, White, White.a ).xyz;
	}
	
	return Colour;
}



Texture2D Texture0;
SamplerState Texture0Sampler;

static const float3x3 Transform = float3x3(	1,0,0,	0,1,0,	0,0,1	);

struct PixelInputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
};

float4 Frag(PixelInputType input) : SV_TARGET
{
	float4 rgba = Texture0.Sample( Texture0Sampler, input.tex );
	float2 uv = input.tex;

	rgba = BlitText( rgba, uv, DrawUVa );
	rgba = BlitText( rgba, uv, DrawUVb );
		
	rgba.w=1;
	return rgba;
}

)*###*"

