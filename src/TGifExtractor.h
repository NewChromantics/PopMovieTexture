#pragma once

#include "TStreamDecoder.h"


namespace Gif
{
	class TDecoder;
	class THeader;
	class TReader;
	class TExtension;
}


class Gif::TDecoder : public Soy::TReadProtocol
{
public:
	TDecoder(TReader& Reader);
	
	virtual TProtocolState::Type	Decode(TStreamBuffer& Buffer) override;
	
public:
	TReader&	mReader;
};



class Gif::THeader
{
public:
	THeader() :
		mWidth						( 0 ),
		mHeight						( 0 ),
		mTransparentPaletteIndex	( 255 )
	{
	}
	THeader(uint16 Width,uint16 Height,ArrayBridge<Soy::TRgba8>&& Palette,uint8 TransparentPaletteIndex);
	
	Soy::Rectx<uint16>				GetRect() const	{	return Soy::Rectx<uint16>( 0, 0, size_cast<uint16>(mWidth), size_cast<uint16>(mHeight) );	}
	
public:
	BufferArray<Soy::TRgba8,256>	mPalette;	//	global/default palette
	size_t							mWidth;
	size_t							mHeight;
	uint8							mTransparentPaletteIndex;
};


class Gif::TReader
{
public:
	TReader(bool DebugFrameRect,bool DebugFrameTransparency) :
		mHeaderValid			( false ),
		mDebugFrameRect			( DebugFrameRect ),
		mDebugFrameTransparency	( DebugFrameTransparency )
	{
	}

	virtual TProtocolState::Type	Decode(TStreamBuffer& Buffer);
	TProtocolState::Type			DecodeFrame(TStreamBuffer& Buffer);
	TProtocolState::Type			DecodeHeader(TStreamBuffer& Buffer);
	TProtocolState::Type			DecodeExtension(TStreamBuffer& Buffer);
	void							DecodeLzw(ArrayBridge<uint8>& DecompressedData,ArrayBridge<uint8>&& LzwData,uint8 MinCodeSize,Soy::Rectx<uint16> FrameRect);
	bool							DecodeLzwBlock(ArrayBridge<uint8>&& DecompressedData,ArrayBridge<uint8>&& PoppedLzwData,TStreamBuffer& Buffer,Soy::Rectx<uint16> FrameRect);

	void							PopPackets(ArrayBridge<std::shared_ptr<TMediaPacket>>& Packets);
	
protected:
	void				BuildFrame(SoyPixelsImpl& Pixels,const ArrayBridge<uint8>&& PixelData,Soy::Rectx<uint16> Rect,BufferArray<Soy::TRgba8,256>& FramePalette);
	void				OnDecodedFrame(SoyPixelsImpl& Pixels,const float3x3& Transform);
	void				OnEof();
	
public:
	THeader				mHeader;
	bool				mHeaderValid;
	Array<TExtension>	mExtensionStack;	//	palette/frame changes etc as the file goes on
	
	std::mutex			mFramesLock;
	SoyTime				mNextFrameTime;
	Array<std::shared_ptr<TMediaPacket>>	mFrames;
	
	//	listen to these events to pipe back to perf graph etc
	SoyEvent<SoyTime>	mOnFrameExtracted;
	SoyEvent<SoyTime>	mOnFrameDecodeStarted;
	
	bool				mDebugFrameRect;
	bool				mDebugFrameTransparency;
};


class Gif::TExtension
{
public:
	TExtension()	{}
	TExtension(uint8 Type,ArrayBridge<uint8>&& Data);
};




class TGifMediaPacket_Protocol : public TMediaPacket_Protocol
{
public:
	TGifMediaPacket_Protocol();
	TGifMediaPacket_Protocol(Gif::TReader& Reader);
	
	virtual TProtocolState::Type	Decode(TStreamBuffer& Buffer) override;
	virtual void					GetPackets(ArrayBridge<std::shared_ptr<TMediaPacket>>&& Packets) override;

public:
	Gif::TReader&		mGif;
};



class TAnimatedGifFileExtractor : public TRawFileExtractor<TGifMediaPacket_Protocol>
{
public:
	TAnimatedGifFileExtractor(const TMediaExtractorParams& Params) :
		TRawFileExtractor	( Params ),
		mGif				( Params.mDebugIntraFrameRect, Params.mDebugIntraFrameTransparency )
	{
	}
	
	virtual std::shared_ptr<TMediaPacket_Protocol>	AllocMediaPacketProtocol() override	{	return std::shared_ptr<TMediaPacket_Protocol>( new TGifMediaPacket_Protocol(mGif) );	}

public:
	Gif::TReader		mGif;
};
