#pragma once

#include "PopMovieTexture.h"
#include <SoyUnity.h>

#if defined(ENABLE_DIRECTX)
#include <SoyDirectx.h>
#endif
/*

	How to Debug PS4:
		Build a pkg and install via network neighbourhood
		Put PRX & source somewhere on host
			PRX contains symbols. May need to enable in c++ options. (Can't see linker option)
		Set symbol dir to PRX's dir
		New Function Breakpoint to C name (some c++ names are detected, but harder to do)
		Enable "Source different" (always seems to be considered different...)
		Debug installed PS4 package and should hit breakpoint.
			Check Module's debug window to make sure module gets (dynamically) loaded and symbols appear as loaded
*/

//	some global params
namespace Unity
{
	static size_t		mMinTimerMs = 5;
	static bool			mDebugPluginEvent = true;
	static Unity::sint	mEventId = 0x00845678;
	static bool			mVerboseUpdateFunctions = false;	//	debug errors if UpdateTexture, UpdatePerfGraph, UpdateAudio etc fails
	
	extern int			GetPluginEventId();
	extern bool			IsDebugPluginEventEnabled();
};


//	control
__export Unity::uint	PopMovie_Alloc(const char* Filename,Unity::uint ParamBits,Unity::uint ParamBits2,Unity::uint PreSeekMs,Unity::uint AudioSampleRate,Unity::uint AudioChannelCount,char* ErrorBuffer,Unity::uint ErrorBufferSize);
__export bool			PopMovie_Free(Unity::uint Instance);
__export bool			PopMovie_SetTime(Unity::uint Instance,Unity::uint TimeMs);

//	graphics
__export bool			PopMovie_UpdateRenderTexture(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::RenderTexturePixelFormat::Type PixelFormat,Unity::sint StreamIndex);
__export bool			PopMovie_UpdateTexture2D(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::Texture2DPixelFormat::Type PixelFormat,Unity::sint StreamIndex);

//	audio
__export bool			PopMovie_GetAudioBuffer(Unity::uint Instance,float* AudioBuffer,Unity::uint AudioBufferSize,Unity::uint Channels,Unity::uint SampleRate,Unity::uint StartTimeMs,Unity::sint StreamIndex);
__export bool			PopMovie_UpdateAudioRenderTexture(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::RenderTexturePixelFormat::Type PixelFormat,Unity::sint AudioStreamIndex);
__export bool			PopMovie_UpdateAudioTexture2D(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::Texture2DPixelFormat::Type PixelFormat,Unity::sint AudioStreamIndex);

//	text - returns original length so we can tell unity side if the string was clipped
__export Unity::sint	PopMovie_UpdateString(Unity::uint Instance,char* Buffer,Unity::uint BufferSize,Unity::boolean SkipOldData,Unity::uint TextStreamIndex);
__export const char*	PopMovie_GetFrameText(Unity::uint Instance,Unity::uint TimeMs,Unity::uint TextStreamIndex);
__export void			PopMovie_ReleaseFrameText(const char* String);

//	meta extraction
__export Unity::uint	PopMovie_GetDurationMs(Unity::uint Instance);
__export Unity::uint	PopMovie_GetLastFrameCopiedMs(Unity::uint Instance,Unity::sint VideoStreamIndex);
__export const char*	PopMovie_GetMetaJson(Unity::uint Instance);
__export void			PopMovie_ReleaseMetaJsonString(const char* String);
__export Unity::sint	PopMovie_GetMetaRevision(Unity::uint Instance);
__export void			PopMovie_RegisterOnTextureCopied(Unity::uint Instance,Unity::uint VideoStreamIndex,void* Callback);
__export void			PopMovie_UnregisterOnTextureCopied(Unity::uint Instance,Unity::uint VideoStreamIndex,void* Callback);

__export bool			PopMovie_PushRawBuffer(Unity::uint Instance,uint8_t* Buffer,Unity::uint BufferSize,Unity::sint StreamIndex);


__export bool			PopMovie_UpdatePerformanceGraphRenderTexture(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::RenderTexturePixelFormat::Type PixelFormat,Unity::sint StreamIndex,Unity::boolean LocalisedGraph);
__export bool			PopMovie_UpdatePerformanceGraphTexture2D(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::Texture2DPixelFormat::Type PixelFormat,Unity::sint StreamIndex,Unity::boolean LocalisedGraph);
__export bool			PopMovie_UpdateMemoryGraphRenderTexture(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::RenderTexturePixelFormat::Type PixelFormat);
__export bool			PopMovie_UpdateMemoryGraphTexture2D(Unity::uint Instance,Unity::NativeTexturePtr TextureId,Unity::sint Width,Unity::sint Height,Unity::Texture2DPixelFormat::Type PixelFormat);

__export void			PopMovie_GetPerformanceStats(Unity::uint Instance,Unity::uint* DataBuffer,Unity::uint DataBufferSize);



__export Unity::sint	PopMovie_EnumSource(char* Buffer,Unity::uint BufferSize,Unity::uint Index);
__export void			PopMovie_EnumSourceDirectory(char* Directory);
__export void			PopMovie_EnumSourcesClear();

__export Unity::sint	PopMovie_GetPluginEventId();

__export const char*	PopMovie_PopDebugString();
__export void			PopMovie_ReleaseDebugString(const char* String);
__export void			PopMovie_ReleaseAllExports();

__export void			PopMovie_DebugPrint();
__export void			PopMovie_DebugThrowCatch();
__export void			PopMovie_DebugTestAlloc();
__export void			PopMovie_DebugThread();

