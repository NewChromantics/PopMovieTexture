using UnityEngine;
using System.Collections;


/// <summary>
///		PopMovieSimple is a very simple example of how to use a PopMovie instance to
///		play a movie to a specified texture using games timedelta to control the playback.
/// </summary>
[AddComponentMenu("PopMovie/PopMovieSimple")]
public class PopMovieSimple : MonoBehaviour {

	public Texture			TargetTexture;
	public string			Filename = "streamingassets:Test.mp4";

	[Header("Current time in the movie in seconds")]
	[Range(0,10)]
	public float			MovieTime = 0;

	[Header("Scalar to make the movie playback faster or slower")]
	[Range(0,3)]
	public float			MovieTimeScalar = 1;

	public PopMovieParams	Parameters;
	public PopMovie			Movie;

	public bool				PlayOnAwake = true;
	public bool				Playing = false;

	public UnityEngine.Events.UnityEvent	OnFinished;
	public UnityEngine.Events.UnityEvent	OnStreamsChanged;

	[Tooltip("Which audio stream to use")]
	public int				AudioStream = 0;

	[Tooltip("Which Video stream to use")]
	public int				VideoStream = 0;

	[Tooltip("Enable debug logging when movie starts. Same as the global option, but automatically turns it on")]
	public bool				EnableDebugLog = false;

	private int				LastMetaRevision = 0;

	void Awake() {

		if (PlayOnAwake ) {
			Play ();
		}
	}

	public void Play()
	{
		if (Movie == null) {
			Movie = new PopMovie (Filename, Parameters, MovieTime);
			if ( EnableDebugLog )
				PopMovie.EnableDebugLog = true;
			LastMetaRevision = 0;
		}

		Playing = true;
	}

	public void Pause()
	{
		Playing = false;
	}

	public void Stop()
	{
		Playing = false;
		if ( Movie != null )
		{
			Movie.Free ();
			Movie = null;
		}
		MovieTime = 0;
	}

	public void UpdateTextures()
	{
		if (Movie != null && TargetTexture != null)
			Movie.UpdateTexture (TargetTexture, VideoStream);
	}

	public void Update () {

		if (Movie != null) {
			var MetaRevision = Movie.GetMetaRevision();
			if (MetaRevision != -1 && LastMetaRevision != MetaRevision)
			{
				OnStreamsChanged.Invoke();
			}
		}

		if (Playing) {
			MovieTime += Time.deltaTime * MovieTimeScalar;

			if (Movie != null)
				Movie.SetTime (MovieTime);

			UpdateTextures();

			//	detect finish
			if ( Movie != null )
			{
				var Duration = Movie.GetDuration();
				//	if duration is zero, we don't know it yet (could be streaming, video camera, or may not have loaded meta information yet)
				if ( Duration > 0 && MovieTime >= Duration )
				{
					OnFinished.Invoke();
					Playing = false;
				}
			}
		}

	}


	//	this gets automatically called if this object has an AudioSource component
	void OnAudioFilterRead(float[] data,int Channels)
	{
		if (Movie == null)
			return;

		uint StartTime = Movie.GetTimeMs ();

		Movie.GetAudioBuffer (data, Channels, StartTime, AudioStream );
	}
}
