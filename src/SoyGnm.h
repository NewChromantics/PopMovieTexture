#pragma once

#include <SoyGnm.h>
#include <gnm.h>
#include <gnmx.h>
#include <gnmx/lwgfxcontext.h>
#include <gnmx/shader_parser.h>
#include <SoyThread.h>
#include <SoyGraphics.h>

class SoyPixelsImpl;
class SoyPixelsMeta;
class SoyPixelsRemote;

namespace Gnm
{
	class TTexture;
	class TShader;
	class TGpuHeap;

	sce::Gnm::DataFormat	GetFormat(SoyGraphics::TElementType::Type Format);
}


class Gnm::TGpuHeap : public prmem::Heap
{
public:
	TGpuHeap(std::function<void*(size_t)> GpuAlloc,std::function<bool(void*)> GpuFree);

protected:
	virtual void*	RealAllocImpl(size_t Size) override;
	virtual bool	CanFreeImpl(void* Object,size_t Size) override;
	virtual bool	RealFreeImpl(void* Object,size_t Size) override;

protected:
	std::function<void*(size_t)>	mAlloc;
	std::function<bool(void*)>		mFree;
};


class Gnm::TContext : public PopWorker::TJobQueue, public PopWorker::TContext
{
public:
	TContext(sce::Gnmx::LightweightGfxContext* Context,std::function<void*(size_t)> GpuAlloc,std::function<bool(void*)> GpuFree);

	bool			HasMultithreadAccess() const		{	return false;	}

	virtual bool	Lock() override						{	return true;	}
	virtual void	Unlock() override					{}
	virtual bool	IsLocked(std::thread::id Thread) override	{	return false;	}
	void			Iteration();

	sce::Gnmx::LightweightGfxContext&	GetContext();	//	get the PS4 context which we don't have until first iteration/renderthread call

public:
	sce::Gnmx::LightweightGfxContext*	mContext;
	Gnm::TGpuHeap						mHeap;		//	interface to gpu heap
};


class Gnm::TTexture
{
public:
	TTexture(void* TexturePtr);
	TTexture() :
		mTexture	( nullptr )
	{
	}
	TTexture(const TTexture& That) :
		mTexture	( That.mTexture )
	{
	}
	
	void			Write(const SoyPixelsImpl& Pixels);
	SoyPixelsMeta	GetMeta() const;
	SoyPixelsRemote	GetPixels();
	bool			IsValid() const		{	return mTexture!=nullptr;	}
	
	bool			operator==(const TTexture& Texture) const	{	return mTexture == Texture.mTexture;	}
	bool			operator!=(const TTexture& Texture) const	{	return mTexture != Texture.mTexture;	}

public:
	sce::Gnm::Texture*	mTexture;
};

/*/
class Gnm::TShader
{
public:
	TShader(void* ShaderBinary);
	

};
*/
