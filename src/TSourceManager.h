#pragma once


#include <SoyThread.h>

class TSourceManager : public SoyWorkerThread
{
public:
	TSourceManager();
	~TSourceManager();

	virtual bool		Iteration() override;

	void				Clear();					//	mostly for the editor to get a clean list, but maybe we can handle this properly to stop & restart the iteration
	
	void				PushFilename(const std::string& Filename);
	void				EnumDirectory(const std::string& Directory);
	void				EnumDirectory(const char* Directory);
	bool				GetFilename(size_t Index,std::string& Filename);
	size_t				GetFilenameCount() const	{	return mFilenames.GetSize();	}
	
	virtual std::chrono::milliseconds	GetSleepDuration() override;
	std::chrono::milliseconds	GetPerItemSleepDuration();

private:

	//	gr: may want this as a map to make lookup (dupe-check) faster, but then we lose ordering
	//	gr: may also want to store by prefix & filename
	std::mutex			mFilenamesLock;
	Array<std::string>	mFilenames;

	std::mutex			mDirectoriesLock;
	Array<std::string>	mDirectories;
};
