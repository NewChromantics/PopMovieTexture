#include "TDecoderPlaylist.h"



std::ostream& operator<<(std::ostream& out,const TPlaylistItem& in)
{
	if ( in.mIsKeyFrame )
		out << "K";
	
	if ( in.mSequenceIndex >= 0 )
		out << "#" << in.mSequenceIndex << " ";

	if ( !in.mTitle.empty() )
		out << in.mTitle << " ";
	
	out << in.mUrl;
	
	if ( in.mByteRangeLength != 0 )
		out << "[" << in.mByteRangeStart << "..." << (in.mByteRangeStart+in.mByteRangeLength) << "] ";
	
	out << " " << in.mDuration.GetTime() << "ms";
	
	if ( !in.mEncryption.empty() )
		out << " (encrypted)";
	
	return out;
}

bool TPlaylist::Merge(const TPlaylist& that)
{
	bool Changed = false;
	for ( int i=0;	i<that.mItems.GetSize();	i++ )
	{
		Changed |= Merge( that.mItems[i] );
	}
	return Changed;
}


bool TPlaylist::Merge(const TPlaylistItem& that)
{
	for ( int i=0;	i<mItems.GetSize();	i++ )
	{
		//	check for dupe
		//auto& Item = mItems[i];
		
		//	see if any bits are different.
		//	identify by... url?
	}
	
	//	new item
	mItems.PushBack( that );
	return true;
}

int TPlaylist::GetLastSequenceIndex() const
{
	for ( int i=size_cast<int>(mItems.GetSize())-1;	i>=0;	i-- )
	{
		int Index = mItems[i].mSequenceIndex;
		
		//	not sure what to do here... ignored
		Soy::Assert( Index != TPlaylistItem::UnknownSequenceIndex, "Not sure what to do with unknown sequence index, when calculating last index");
		
		//	ignore out of order items
		if ( Index == TPlaylistItem::OutOfOrderSequenceIndex )
			continue;
		
		return Index;
	}
	
	//	no sequenced items, so last index is unknown (-1 so we can blindly do +1 to get next)
	return -1;
}

TM3u::TM3u(const std::string& Content,const std::string& PlaylistUrl) :
	mOnlyHasIFrames	( false )
{
	Array<std::string> Lines;
	Soy::SplitStringLines( GetArrayBridge(Lines), Content );

	Soy::Assert( !Lines.IsEmpty(), "M3u content has no lines" );
	
	const std::string& M3uHeader = "#EXTM3U";
	auto Header = Lines.PopAt(0);
	if ( Header != M3uHeader )
	{
		std::stringstream Error;
		Error << "M3u heade is not " << M3uHeader << " is " << Header.substr(0,10);
		throw Soy::AssertException( Error.str() );
	}

	TPlaylistItem NextItem;
	
	
	while ( !Lines.IsEmpty() )
	{
		auto Line = Lines.PopAt(0);
		if ( Line.empty() )
			continue;
		
		//	playlist header
		if ( Soy::StringTrimLeft( Line, "#EXT-X-", true ) )
		{
			PushHeader( Line, NextItem );
			continue;
		}
		
		//	item header
		if ( Soy::StringTrimLeft( Line, "#EXTINF:", true ) )
		{
			//	#EXTINF:durationf,title,?
			Array<std::string> Metas;
			Soy::StringSplitByMatches( GetArrayBridge(Metas), Line, "," );

			if ( Metas.GetSize() >= 1 )
			{
				//	floating point in v3. https://developer.apple.com/library/ios/technotes/tn2288/_index.html
				float Duration = 0;
				if ( !Soy::StringToType( Duration, Metas[0] ) )
				{
					std::Debug << "Failed to get duration from EXTINF meta " << Metas[0] << std::endl;
				}
				auto DurationMs = static_cast<uint64>( Duration*1000.f );
				NextItem.mDuration = SoyTime( std::chrono::milliseconds( DurationMs ) );
			}
			if ( Metas.GetSize() >= 2 )
			{
				NextItem.mTitle = Metas[1];
			}
			//	save everything else
			for ( int m=2;	m<Metas.GetSize();	m++ )
				NextItem.mAdditionalMeta.PushBack( Metas[m] );
			continue;
		}
		
		//	filename!
		NextItem.mUrl = Soy::ResolveUrl( PlaylistUrl, Line );
		NextItem.mPath = Soy::GetUrlPath( NextItem.mUrl );
		
		
		if ( NextItem.mSequenceIndex == TPlaylistItem::UnknownSequenceIndex )
			NextItem.mSequenceIndex = GetLastSequenceIndex() + 1;
		NextItem.mIsKeyFrame = mOnlyHasIFrames;
		
		//	save & go to next one
		mItems.PushBack( NextItem );
		NextItem = TPlaylistItem();
	}
}


void TM3u::PushHeader(const std::string &Header,TPlaylistItem& NextItem)
{
	BufferArray<std::string,2> HeaderParts;
	Soy::StringSplitByMatches( GetArrayBridge(HeaderParts), Header, ":" );
	if ( HeaderParts.IsEmpty() )
	{
		std::Debug << "M3u failed to split header: " << Header << " ignoring. " << std::endl;
		return;
	}
	
	if ( HeaderParts.GetSize() == 1 )
		HeaderParts.PushBack( std::string() );
	
	auto& Key = HeaderParts[0];
	auto& Value = HeaderParts[1];
	
	//	handle specific headers
	if ( ParseHeader( Key, Value, NextItem ) )
		return;
	
	if ( mHeaders.count(Key) )
	{
		std::Debug << "M3u header key " << Key << " already exists; old=" << mHeaders[Key] << ", new=" << Value << ". Ignored." << std::endl;
		return;
	}
	
	mHeaders[Key] = Value;
}

bool TM3u::ParseHeader(std::string& Key,std::string& Value,TPlaylistItem& NextItem)
{
	//	HLS specifics for m3u
	//	https://developer.apple.com/library/ios/technotes/tn2288/_index.html
	if ( Key == "PLAYLIST-TYPE" )
	{
		//	tag provides mutability information about the playlist file.
		//	It applies to the entire playlist file. This tag may contain a value of
		//	either EVENT or VOD. If the tag is present and has a value of EVENT,
		//	the server must not change or delete any part of the playlist file
		//	(although it may append lines to it). If the tag is present and has a value of
		//	VOD, the playlist file must not change.
	}
	
	//	"first item's index", we can assume NextItem is the first item here
	if ( Key == "MEDIA-SEQUENCE" )
	{
		Soy::StringToType( NextItem.mSequenceIndex, Value );
		return true;
	}
	
	//	"out of order" video. Eg. advert injected into playlist. need to make sure this doesn't affect the sequence ordering
	if ( Key == "DISCONTINUITY" )
	{
		NextItem.mSequenceIndex = TPlaylistItem::OutOfOrderSequenceIndex;
		return true;
	}
	
	//	playlist-supplimentry playlist which contains a list of keyframes. this could be loaded along side the current playlist to provide the extra meta data
	//	this generates a new out-of-order playlist item referencing this keyframe playlist
	if ( Key == "I-FRAME-STREAM-INF" )
	{
		ParseKeyFrameStreamInfo( Value );
		return true;
	}
	
	//	meta for stream listed in playlist
	if ( Key == "STREAM-INF" )
	{
		ParseStreamInfo( Value, NextItem );
		return true;
	}
	
	//	media item is a chunk of an url - should be version 4+
	if ( Key == "BYTERANGE" )
	{
		ParseByteRange( Value, NextItem );
		return true;
	}
	
	//	item's encryption setting.
	//	gr: this may be a "current encryption" setting and continue for next media's until changed
	if ( Key == "KEY" )
	{
		NextItem.mEncryption = Value;
		return true;
	}
	
	//	todo: parse these alternative media streams
	if ( Key == "MEDIA" )
	{
		ParseAlternativeMedia( Value );
		return true;
	}
	
	//	all entries are keyframe offsets
	if ( Key == "I-FRAMES-ONLY" )
	{
		this->mOnlyHasIFrames = true;
		return true;
	}
	
	if ( Key == "ENDLIST" )
	{
		//	found end of list... suggests live stream has finished after last item
		mPlaylistFinished = true;
		return true;
	}
	

	return false;
}

void TM3u::ParseAlternativeMedia(const std::string& MediaInfo)
{
	mAlternativeMedias.PushBack( MediaInfo );
}

void TM3u::ParseByteRange(const std::string& ByteRangeInfo,TPlaylistItem& NextItem)
{
	//	length[@offset]
	int Length=0;
	int Offset=0;
	auto Parse = [&Length,&Offset](const std::string& Part,const char& Delin)
	{
		//	the optional second half starts with an @, otherwise this should be a terminator
		if ( Delin == '@' )
			Soy::StringToType( Offset, Part );
		else
			Soy::StringToType( Length, Part );
		return true;
	};
	Soy::StringSplitByMatches( Parse, ByteRangeInfo, "@" );
	NextItem.mByteRangeLength = size_cast<size_t>( Length );
	NextItem.mByteRangeStart = size_cast<size_t>( Offset );
}

void TM3u::ParseKeyFrameStreamInfo(const std::string& StreamInfo)
{
	//	generates a playlist item referencing a meta-playlist of keyframes
	TPlaylistItem KeyframePlaylist;
	KeyframePlaylist.mIsKeyFrame = true;
	KeyframePlaylist.mSequenceIndex = TPlaylistItem::OutOfOrderSequenceIndex;
	
	ParseStreamInfo( StreamInfo, KeyframePlaylist );
	
	mItems.PushBack( KeyframePlaylist );
}


void TM3u::ParseStreamInfo(std::string StreamInfo,TPlaylistItem& NextItem)
{
	//	https://developer.apple.com/library/ios/technotes/tn2288/_index.html
	//	#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=150000,RESOLUTION=416x234,CODECS="avc1.42e00a,mp4a.40.2"
	//	gr: re-use cl protocol!
	while ( !StreamInfo.empty() )
	{
		std::string Key = Soy::StringPopUntil( StreamInfo, '=' );
		
		//	if next is quoted, pop the whole string
		std::string Value;
		if ( StreamInfo[0] == '"' )
		{
			StreamInfo.erase( StreamInfo.begin() );
			Value = Soy::StringPopUntil( StreamInfo, '"' );
		}
		else
		{
			//	change this to match eol AND ,
			Value = Soy::StringPopUntil( StreamInfo, ',' );
		}
		
		//	assume reset-of-string is value
		if ( Value.empty() )
			Value = StreamInfo;
		
		ParseStreamInfo( Key, Value, NextItem );
	}
}

bool TM3u::ParseStreamInfo(const std::string& Key,const std::string& Value,TPlaylistItem& NextItem)
{
	if ( Key == "PROGRAM-ID" )
	{
		NextItem.mTitle = Value;
		return true;
	}
	
	if ( Key == "BANDWIDTH" )
	{
		int Bandwidth = 0;
		Soy::StringToType( Bandwidth, Value );
		NextItem.mBandwidth = size_cast<size_t>( Bandwidth );
		return true;
	}
	
	if ( Key == "RESOLUTION" )
	{
		//	wwwxhhh
		int w=0,h=0;
		auto ParseWh = [&w,&h](const std::string& Part,const char& Delim)
		{
			return Soy::StringToType( w==0 ? w : h, Part );
		};
		Soy::StringSplitByMatches( ParseWh, Value, "x" );
		NextItem.mVideoMeta = SoyPixelsMeta( w, h, SoyPixelsFormat::Invalid );
		return true;
	}
	
	if ( Key == "CODECS" )
	{
		NextItem.mCodec = Value;
		return true;
	}
	
	//	only for I-FRAME-STREAM-INF
	if ( Key == "URI" )
	{
		NextItem.mUrl = Value;
		return true;
	}
	
	return false;
}


TPlaylistExtractor::TPlaylistExtractor(const TMediaExtractorParams& Params,TVideoDecoderFactory& ExtractorFactory) :
	TMediaExtractor		( Params ),
	mExtractorFactory	( ExtractorFactory )
{
	
}

void TPlaylistExtractor::AllocSubDecoder(const TPlaylistItem& PlaylistItem)
{
	std::lock_guard<std::mutex> Lock( mChildrenLock );
	
	//	see if this already exists
	for ( int c=0;	c<mChildren.GetSize();	c++ )
	{
		auto& Child = *mChildren[c];
		if ( Child.Merge( PlaylistItem ) )
			return;
	}
	
	//	alloc a new one
	static SoyEvent<const SoyTime> mOnFrameExtractedEvent;
	SoyTime mReadAheadMs;

	TMediaExtractorParams SubExtractorParams( PlaylistItem.mUrl, PlaylistItem.mPath, mOnPacketExtracted, nullptr );
	SubExtractorParams.mReadAheadMs = mReadAheadMs;
	auto SubExtractor = mExtractorFactory.AllocExtractor( SubExtractorParams );
	if ( !SubExtractor )
	{
		std::stringstream Error;
		Error << "Failed to create media extractor for playlist item " << PlaylistItem;
		throw Soy::AssertException( Error.str() );
	}
	
	std::shared_ptr<TPlaylistExtractor> SubExtractorWrapper( new TPlaylistExtractorWrapper( SubExtractorParams, mExtractorFactory, SubExtractor ) );
	mChildren.PushBack( SubExtractorWrapper );
}


bool TPlaylistExtractor::Merge(const TPlaylistItem& PlaylistItem)
{
	Soy_AssertTodo();
	return false;
}

std::shared_ptr<TPlaylistExtractor> TPlaylistExtractor::GetCurrentExtractor()
{
	std::lock_guard<std::mutex> Lock( mChildrenLock );
	if ( mChildren.IsEmpty() )
		return nullptr;
	
	return mChildren[0];
}


void TPlaylistExtractor::GetStreams(ArrayBridge<TStreamMeta>&& Streams)
{
	auto Extractor = GetCurrentExtractor();
	if ( !Extractor )
		return;
	
	Extractor->GetStreams( GetArrayBridge(Streams) );
}

std::shared_ptr<Platform::TMediaFormat> TPlaylistExtractor::GetStreamFormat(size_t StreamIndex)
{
	auto Extractor = GetCurrentExtractor();
	if ( !Extractor )
		return nullptr;
	
	return Extractor->GetStreamFormat( StreamIndex );
}

std::shared_ptr<TMediaPacket> TPlaylistExtractor::ReadNextPacket()
{
	auto Extractor = GetCurrentExtractor();
	if ( !Extractor )
		return nullptr;
	
	return Extractor->ReadNextPacket();
}



TPlaylistExtractorWrapper::TPlaylistExtractorWrapper(const TMediaExtractorParams& Params,TVideoDecoderFactory& ExtractorFactory,std::shared_ptr<TMediaExtractor> Extractor) :
	TPlaylistExtractor	( Params, ExtractorFactory ),
	mExtractor			( Extractor )
{
	Soy::Assert( mExtractor != nullptr, "Expected extractor" );
}
	


TM3u8Extractor::TM3u8Extractor(const TMediaExtractorParams& Params,TVideoDecoderFactory& ExtractorFactory) :
	TPlaylistExtractor	( Params, ExtractorFactory )
{
	auto PlaylistUrl = Params.mFilename;
	
	auto OnConnected = [](bool&)
	{
		std::Debug << "TM3u8Extractor::OnConnected" << std::endl;
	};
	auto OnHttpResponse = [this,PlaylistUrl](const Http::TResponseProtocol& Response)
	{
		DecodeM3U8( Response, PlaylistUrl );
	};
	auto OnError = [this](const std::string& Error)
	{
		OnHttpError( Error );
	};
	
	//	create http connections to device
	mFetchThread.reset( new THttpConnection( PlaylistUrl ) );
	mFetchThread->mOnConnected.AddListener( OnConnected );
	mFetchThread->mOnResponse.AddListener( OnHttpResponse );
	mFetchThread->mOnError.AddListener( OnError );
	mFetchThread->Start();
	
	
	//	send/queue initial request
	{
		Http::TRequestProtocol Request;
		std::string Protocol;
		std::string Hostname;
		uint16 Port=0;
		std::string Path;
		Soy::SplitUrl( PlaylistUrl, Protocol, Hostname, Port, Path );

		Request.mUrl = Path;
		mFetchThread->SendRequest(Request);
	}
	
	Start();
}


TM3u8Extractor::~TM3u8Extractor()
{
	if ( mFetchThread )
	{
		mFetchThread->WaitToFinish();
		mFetchThread.reset();
	}
}

void TM3u8Extractor::DecodeM3U8(const Http::TResponseProtocol& Http,const std::string& PlaylistUrl)
{
	//	parse m3u8 - if it fails, assume it's a Html error response or something
	try
	{
		auto HttpContent = Soy::ArrayToString( GetArrayBridge(Http.mContent) );
		TM3u Playlist( HttpContent, PlaylistUrl );
		
		OnPlaylistChanged( Playlist );
	}
	catch(std::exception& e)
	{
		std::stringstream Error;
		Error << "Failed to parse m3u file; " << e.what();
		OnHttpError( Error.str() );
	}
}

void TM3u8Extractor::OnHttpError(const std::string &Error)
{
	std::Debug << "Http error: " << Error << std::endl;
	OnError(Error);
}

void TM3u8Extractor::OnPlaylistChanged(const TPlaylist& Playlist)
{
	OnClearError();
	
	std::Debug << "Playlist " << Playlist.mItems.GetSize() << " items" << std::endl;
	for ( int i=0;	i<Playlist.mItems.GetSize();	i++ )
	{
		auto& Item = Playlist.mItems[i];
		std::Debug << Item << std::endl;
	}
	
	//	alloc sub decoder for each item (this should ignore/merge existing items so we can re-fresh the playlist)
	for ( int i=0;	i<Playlist.mItems.GetSize();	i++ )
	{
		auto& Item = Playlist.mItems[i];
		AllocSubDecoder( Item );
	}
}

