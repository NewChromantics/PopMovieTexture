#include "SoyBink.h"

#if !defined(ENABLE_BINK)
#error Cannot compile Bink without ENABLE_BINK
#endif

#if defined(TARGET_WINDOWS)
#pragma message("Compiling WITH bink support")
#else
#warning Compiling WITH bink support
#endif

#define WAIT_INFINITE	-1

#if defined(TARGET_PS4)
#pragma message("linking libBink2Orbis.a")
#pragma comment(lib, "libBink2Orbis.a")
#elif TARGET_WINDOWS == 64
#pragma message("linking bink2w64.lib")
#pragma comment(lib, "bink2w64.lib")
#elif TARGET_WINDOWS == 32
#pragma message("linking bink2w32.lib")
#pragma comment(lib, "bink2w32.lib")
#endif

#if defined(DELAY_LOAD_BINK) && !defined(TARGET_WINDOWS)
#undef DELAY_LOAD_BINK
#endif

#if defined(DELAY_LOAD_BINK)
#include <SoyRuntimeLibrary.h>
	#include "delayimp.h"
	#pragma comment(lib, "delayimp.lib")

	#if TARGET_WINDOWS == 64
		auto BinkDllName = "bink2w64.dll";
	#else
		auto BinkDllName = "bink2w32.dll";
	#endif
#endif

namespace Bink
{
	std::string		GetError();
	void			IsOkay(S32 Result,const std::string& Context);

	SoyPixelsFormat::Type		GetPixelFormat()	{	return SoyPixelsFormat::BGRA;	}
	U32							GetPixelFormat(SoyPixelsFormat::Type Format);

	size_t						GetVideoStreamIndex(size_t VideoIndex)	{	return (VideoIndex==0) ? 0 : (1+VideoIndex);	}
	size_t						GetAudioStreamIndex()	{	return 1;	}

	float						GetFramesPerSec(struct BINK& Handle);
	SoyTime						GetTime(struct BINK& Handle,size_t FrameNumber);
	SoyTime						GetTime(struct BINK& Handle);
}

std::string Bink::GetError()
{
	auto Error = BinkGetError();
	if ( !Error )
		return std::string("<null error>");

	return std::string(Error);
}

void Bink::IsOkay(S32 Result,const std::string& Context)
{
	if ( Result == 1 )
		return;

	std::stringstream Error;
	Error << "Bink Error in " << Context << ": " << Result << " (" << GetError() << ")";
	throw Soy::AssertException( Error.str() );
}

float Bink::GetFramesPerSec(struct BINK& Handle)
{
	if ( Handle.FrameRateDiv == 0 )
		return 0.f;

	float FramesPerSecond = Handle.FrameRate / static_cast<float>( Handle.FrameRateDiv );
	return FramesPerSecond;
}


SoyTime Bink::GetTime(struct BINK& Handle,size_t FrameNumber)
{
	auto fps = GetFramesPerSec( Handle );
	auto Time = FrameNumber / fps;
	int TimeMs = Time * 1000.0f;
	return SoyTime( std::chrono::milliseconds(TimeMs) );
}

SoyTime Bink::GetTime(struct BINK& Handle)
{
	return GetTime( Handle, Handle.FrameNum );
}

//	gr: move this to part of Soy::TRuntimeLibrary
#if defined(TARGET_WINDOWS)
HRESULT LoadDll(const char* DllName)
{
	HRESULT Result = !ERROR_SUCCESS;
	__try
	{			
		Result = __HrLoadAllImportsForDll(DllName);
		return Result;
	}
	__except( EXCEPTION_EXECUTE_HANDLER )
	{
		return Result;
	}
	return Result;
}
#endif


Bink::TExtractor::TExtractor(const TMediaExtractorParams& Params) :
	TMediaExtractor			( Params ),
	mHandle					( nullptr )
{
#if defined(DELAY_LOAD_BINK)
	//	load dll. This will throw if it fails, so let it go!
	mBinkDll.reset( new Soy::TRuntimeLibrary(BinkDllName) );

	//	gr: don't seem to need this,(just loadlibrary) but leaving it here anyway...
	static bool LoadImports = false;
	if ( LoadImports )
	{
		std::stringstream Error;
		Error << "Failed to load Bink DLL " << BinkDllName;
		auto Result = LoadDll( BinkDllName );
		Platform::IsOkay( Result, Error.str() );
	}
#endif
	
	//	setup threads
	//On Windows, thread_index can be a value from 0 to 7 and is one of 8 threads. These threads run at normal priority and is pegged to CPUS in a way to minimize hyperthread CPU-aliasing. The async_parameter value is ignored on Windows. 
	//On Xbox 360, thread_index can be a value from 0 to 5 and is one of 6 threads - these threads run at normal priority and are pegged to each hardware Xbox CPU (so, thread index 0 runs on CPU 0/SMT thread 0, thread index 1 runs on CPU 0/SMT thread 1, thread index 2 runs on CPU 1/SMT thread 0, etc). The async_parameter value is ignored on Xbox 360. 
	//	gr: PS4 example used 4 threads
	//		U32 threads[] = { 1, 2, 3, 4 };
	int UseThreads = mParams.mEnableDecoderThreading ? 4 : 0;

	if ( UseThreads > 0 )
	{
		for ( int i=0;	i<UseThreads;	i++ )
		{
			try
			{
				S32 ThreadNum = 1 + i;
				void* Param = nullptr;
				auto Result = BinkStartAsyncThread( ThreadNum, Param );
				std::stringstream Error;
				Error << "BinkStartAsyncThread(" << ThreadNum << ")";
				Bink::IsOkay( Result, Error.str() );
				mThreads.PushBack( ThreadNum );
			}
			catch(std::exception& e)
			{
				std::Debug << "Failed to create bink thread: " << e.what() << std::endl;
			}
		}
	}

	std::Debug << "Bink using threads=" << mThreads.GetSize() << std::endl;

	//	by default we want to extract planes and merge in our own shader
	bool AllocFrameBuffers = true;
	if ( mParams.mForceNonPlanarOutput )
		AllocFrameBuffers = false;
	if ( mParams.mSplitVideoPlanesIntoStreams )
		AllocFrameBuffers = true;


	U32 Flags = 0;
	if ( Params.mExtractAudioStreams )
		Flags |= BINKSNDTRACK;

	if ( AllocFrameBuffers )
		Flags |= BINKNOFRAMEBUFFERS;

	if ( mParams.mExtractAlpha )
	{
		Flags |= BINKALPHA;
	}

	{
		ofScopeTimerWarning Timer("BinkOpen", 0 );
		mHandle = BinkOpen( Params.mFilename.c_str(), Flags );
	}
	if ( !mHandle )
	{
		std::stringstream Error;
		Error << "BinkOpen (" << Params.mFilename << ") Failed " << GetError();
		throw Soy::AssertException( Error.str() );
	}

	if ( AllocFrameBuffers )
	{
		struct BINKFRAMEBUFFERS BufferInfo;
		BinkGetFrameBuffersInfo( mHandle, &BufferInfo );
		mBufferPlanes.reset( new TTextureBufferPlanes( BufferInfo, GetVideoMeta(), mHandle ) );
	}
	else
	{
		mBufferRgba.reset( new TTextureBufferRgba(GetVideoMeta()) );
	}
	
	std::Debug << "Bink opened " << Params.mFilename << std::endl;
	OnStreamsChanged();
}


Bink::TExtractor::~TExtractor()
{
	//	wait for any threads to stop
	std::lock_guard<std::mutex> HandleLock( mHandleLock );
	if ( mHandle )
	{
		//	wait for async stuff to end
		if ( !mThreads.IsEmpty() )
		{
			auto Result = BinkDoFrameAsyncWait( mHandle, WAIT_INFINITE );
		}

		BinkClose( mHandle );
		mHandle = nullptr;

		if ( !mThreads.IsEmpty() )
		{
			auto Result = BinkRequestStopAsyncThreadsMulti( mThreads.GetArray(), mThreads.GetSize() );
			auto Result2 = BinkWaitStopAsyncThreadsMulti( mThreads.GetArray(), mThreads.GetSize() );
		}
	}
}

SoyPixelsMeta Bink::TExtractor::GetVideoMeta()
{
	return SoyPixelsMeta( mHandle->Width, mHandle->Height, GetPixelFormat() );
}

void Bink::TExtractor::GetPlaneStreamMetas(ArrayBridge<SoyPixelsMeta>&& Metas,bool SplitPlanesIntoStreams)
{
	if ( mBufferRgba )
		mBufferRgba->GetPlaneStreamMetas( Metas );

	if ( mBufferPlanes )
		mBufferPlanes->GetPlaneStreamMetas( Metas, SplitPlanesIntoStreams );
}

void Bink::TExtractor::GetStreams(ArrayBridge<TStreamMeta>&& Streams)
{
	if ( mHandle == nullptr )
		return;

	auto& Handle = *mHandle;

	if ( Handle.videoon )
	{		
		TStreamMeta Meta;
		Meta.mDuration = GetTime( Handle, Handle.Frames );
		Meta.mFramesPerSecond = GetFramesPerSec( Handle );
		Meta.mAverageBitRate = Handle.fileframerate * Meta.mFramesPerSecond;

		BufferArray<SoyPixelsMeta,4> PlaneMetas;
		GetPlaneStreamMetas( GetArrayBridge( PlaneMetas ), mParams.mSplitVideoPlanesIntoStreams );

		for ( int p=0;	p<PlaneMetas.GetSize();	p++ )
		{
			Meta.mStreamIndex = GetVideoStreamIndex(p);
			Meta.SetPixelMeta( PlaneMetas[p] );
			Streams.PushBack( Meta );
		}
	}

	if ( Handle.soundon )
	{
		TStreamMeta Meta;
		Meta.mStreamIndex = GetAudioStreamIndex();
		Streams.PushBack( Meta );
	}
}


U32 Bink::GetPixelFormat(SoyPixelsFormat::Type Format)
{
	switch ( Format )
	{
		case SoyPixelsFormat::ARGB:	return BINKSURFACE32ARGB;
		case SoyPixelsFormat::RGBA:	return BINKSURFACE32RGBA;
		case SoyPixelsFormat::BGRA:	return BINKSURFACE32BGRA;
		//case SoyPixelsFormat::RGBA:	return BINKSURFACE32RGBx;
		//case SoyPixelsFormat::BGRA:	return BINKSURFACE32BGRx;
		default:break;
	}
//#define BINKSURFACE5551        8
//#define BINKSURFACE555         9
//#define BINKSURFACE565        10
//#define BINKSURFACEMASK       15
	std::stringstream Error;
	Error << "Failed to get bink format for " << Format;
	throw Soy::AssertException( Error.str() );
}


bool Bink::TExtractor::IsPixelBufferStreamLocked(size_t BufferIndex)
{
	//	if all unlocked, move to next frame
	auto& LockMap = mPixelBufferStreamLocks[BufferIndex];
	for ( auto it=LockMap.begin();	it!=LockMap.end();	it++ )
	{
		if ( it->second )
		{
			return true;
		}
	}
	return false;
}

bool Bink::TExtractor::CanSleep()
{
	//	don't sleep if queued packets
	if ( !mPacketQueue.IsEmpty() )
		return false;

	//	sleep if where we're going to decode to next is locked
	auto TargetFrameBuffer = GetDecodeTargetFrameBufferIndex();
	if ( IsPixelBufferStreamLocked(TargetFrameBuffer) )
		return true;

	//	try for next packet
	return false;
}

std::shared_ptr<TMediaPacket> Bink::TExtractor::DecodeNextPacket()
{
	if ( !mPacketQueue.IsEmpty() )
		return mPacketQueue.PopAt(0);

	static bool UseBinkTiming = false;

	//	timer-wait for next frame
	//	gr: can we skip this and do timing ourselves?
	if ( UseBinkTiming )
	{
		std::lock_guard<std::mutex> HandleLock( mHandleLock );
		if ( BinkWait( mHandle ) )
		{
			//	artificial sleep
			static auto SleepMs = 10;
			if ( mParams.mVerboseDebug )
				std::Debug << "BinkWait sleep for " << SleepMs << " ms" << std::endl;
			std::this_thread::sleep_for( std::chrono::milliseconds(SleepMs) );
			return nullptr;
		}
	}

	std::function<void()> LoadNextFrame;
	std::function<bool()> DecompressFrame;

	if ( !mThreads.IsEmpty() )
	{
		DecompressFrame = [&]
		{			
			bool Started = BinkDoFrameAsyncMulti( mHandle, mThreads.GetArray(), mThreads.GetSize() )!=0;
			if ( !Started )
				return false;
			
			while ( true )
			{
				int WaitMicroSecs = 1000;	//	1ms
				bool BackgroundComplete = BinkDoFrameAsyncWait(mHandle, WaitMicroSecs) == 1;
				if ( BackgroundComplete )
					break;
			}
			return true;
		};

		LoadNextFrame = [&]
		{
			if ( mParams.mVerboseDebug )
				std::Debug << "Bink::TFrameBuffer::DecodeNextPacket LoadNextFrame" << std::endl;
			//	if we're at the end and call this, it'll loop, so don't do that
			if ( mHandle->FrameNum < mHandle->Frames )
				BinkNextFrame( mHandle );
		};
	}
	else
	{
		DecompressFrame = [&]
		{
			bool Skip = BinkDoFrame( mHandle )!=0;
			return !Skip;
		};

		LoadNextFrame = [&]
		{
			if ( mParams.mVerboseDebug )
				std::Debug << "Bink::TFrameBuffer::DecodeNextPacket LoadNextFrame" << std::endl;
			//	if we're at the end and call this, it'll loop, so don't do that
			if ( mHandle->FrameNum < mHandle->Frames )
				BinkNextFrame( mHandle );
		};
	}

	//	check we've not been stopped before acquiring lock
	if ( !IsWorking() )
		return nullptr;
	std::lock_guard<std::mutex> HandleLock( mHandleLock );

	//	gr: alter stream here for each plane
	BufferArray<SoyPixelsMeta,4> PlaneMetas;
	GetPlaneStreamMetas( GetArrayBridge( PlaneMetas ), mParams.mSplitVideoPlanesIntoStreams );

	//	warning for users!
	if ( PlaneMetas.GetSize() > 0 && mParams.mPeekBeforeDefferedCopy )
	{
		std::Debug << "Warning, with multiple plane extraction turn OFF mPeekBeforeDefferedCopy otherwise frames may be out of sync in renderthread copy" << std::endl;
	}


	//	earlier rejection check so we dont decompress everything
	if ( mParams.mExtractorPreDecodeSkip )
	{
		static bool SkipMultiple = true;
		int Skipped = 0;
		//	
		while ( true )
		{
			bool Eof = ( mHandle->FrameNum >= mHandle->Frames );
			auto FrameTime = GetTime( *mHandle, mHandle->FrameNum );
			if ( Eof )
				break;

			//	gr: currently just checking first stream, then we'll manually update stats for the others
			auto FirstVideoStreamIndex = 0;
			auto FirstStreamIndex = Bink::GetVideoStreamIndex(FirstVideoStreamIndex);
			bool Decode = CanPushPacket( FrameTime, FirstStreamIndex, false );

			if ( Decode )
				break;

			std::Debug << "Skipping " << FrameTime << " (now=" << GetSeekTime() << std::endl;

			//	skip this packet
			Skipped++;
			LoadNextFrame();
			DecompressFrame();
			if ( !SkipMultiple )
				return nullptr;
		}
		if ( Skipped > 0 )
			std::Debug << "SkipPacketBeforeDecompress skipped=" << Skipped << std::endl;
	}

	//	dont decompress until unlocked
	auto TargetFrameBuffer = GetDecodeTargetFrameBufferIndex();
	if ( IsPixelBufferStreamLocked(TargetFrameBuffer) )
	{
		//	gr: this should only come up on spurious wakes
		static auto Sleep = 1;
		//if ( mParams.mVerboseDebug )
		std::Debug << "Bink::TFrameBuffer::DecodeNextPacket IsPixelBufferStreamLocked. Thread should be suspended... sleeping for " << Sleep << "ms" << std::endl;
		std::this_thread::sleep_for( std::chrono::milliseconds(Sleep) );
		return nullptr;
	}


	bool UseFrame = DecompressFrame();
	while ( UseBinkTiming && BinkShouldSkip( mHandle ) )
	{
		LoadNextFrame();
		UseFrame = DecompressFrame();
	}
	
	SoyTime PixelBufferTime;
	if ( !UseFrame )
	{
		bool Eof = ( mHandle->FrameNum >= mHandle->Frames );
		std::Debug << "DecompressFrame returned skipped (eof=" << Eof << ")" << std::endl;
		//	nothing decompressed yet
		//LoadNextFrame();
		return nullptr;
	}

	//	bink frames aren't buffered so once it's used, we go to next one
	bool Eof = ( mHandle->FrameNum >= mHandle->Frames );
	PixelBufferTime = GetTime( *mHandle, mHandle->FrameNum );

	auto UnlockPixelBuffer = [=](size_t VideoStreamIndex,size_t BufferIndex)
	{
		mPixelBufferStreamLocks[BufferIndex][VideoStreamIndex] = false;

		//	gr: helps thread not to wake unless we've unlocked all (maybe call CanSleep() :)
		if ( IsPixelBufferStreamLocked(BufferIndex) )
		{
			if ( mParams.mVerboseDebug )
				std::Debug << "Bink::TFrameBuffer::DecodeNextPacket UnlockPixelBuffer, still locked" << std::endl;
			return;
		}
		if ( mParams.mVerboseDebug )
			std::Debug << "Bink::TFrameBuffer::DecodeNextPacket UnlockPixelBuffer, unlocked" << std::endl;
		
		Wake();
	};



	auto FrameBufferIndex = GetCurrentFrameBufferIndex();

	for ( int VideoStreamIndex=0;	VideoStreamIndex<PlaneMetas.GetSize();	VideoStreamIndex++ )
	{
		auto StreamIndex = Bink::GetVideoStreamIndex(VideoStreamIndex);
	
		if ( !mParams.mExtractorPreDecodeSkip )
		{
			//	skip this packet
			if ( !CanPushPacket( PixelBufferTime, StreamIndex, false ) )
			{
				if ( mParams.mVerboseDebug )
					std::Debug << "Bink::TFrameBuffer::DecodeNextPacket skipped video stream index " << VideoStreamIndex << std::endl;
				continue;
			}
		}

		BufferArray<SoyPixelsImpl*,10> Textures;
		float3x3 Transform;
		GetBufferPixels( GetArrayBridge(Textures), Transform, VideoStreamIndex, FrameBufferIndex );
		std::shared_ptr<TPixelBuffer> PixelBuffer;
		
		if ( mParams.mCopyBuffersInExtraction && Textures.GetSize() == 1 )
		{
			Soy::TScopeTimerPrint PixelBufferCopy("PixelBufferCopy", 2);
			//	copy pixels straight out
			PixelBuffer.reset( new TDumbPixelBuffer( *Textures[0], Transform ) );
		}
		else
		{
			//	alloc a pixel buffer and lock this stream. when its done, it'll unlock. when they're all done, we move to next frame
			auto OnUnlock = [=]
			{
				UnlockPixelBuffer(VideoStreamIndex,FrameBufferIndex);
			};
			if ( mParams.mVerboseDebug )
				std::Debug << "Bink::TFrameBuffer::DecodeNextPacket creating frame buffer for video stream index " << VideoStreamIndex << std::endl;
			PixelBuffer.reset( new Bink::TFrameBuffer( *this, VideoStreamIndex, FrameBufferIndex, OnUnlock ) );
			mPixelBufferStreamLocks[FrameBufferIndex][VideoStreamIndex] = true;
		}
		
		std::shared_ptr<TMediaPacket> Packet( new TMediaPacket() );
		Packet->mPixelBuffer = PixelBuffer;
		Packet->mTimecode = PixelBufferTime;
		if ( Eof )
			Packet->mEof = Eof;
		Packet->mMeta.mStreamIndex = StreamIndex;

		if ( mParams.mOnFrameExtracted )
			mParams.mOnFrameExtracted( PixelBufferTime, Packet->mMeta.mStreamIndex );
	
		if ( mParams.mOnPrePushFrame )
			mParams.mOnPrePushFrame( *Packet->mPixelBuffer, mParams );

		mPacketQueue.PushBack( Packet );
	}

	//	move to next frame now we've pulled out all the meta
	LoadNextFrame();

	//	all were skipped
	if ( mPacketQueue.IsEmpty() )
	{
		if ( mParams.mVerboseDebug )
			std::Debug << "Bink::TFrameBuffer::DecodeNextPacket all frames were skipped" << std::endl;
	}

	//	pop from queue
	if ( !mPacketQueue.IsEmpty() )
		return mPacketQueue.PopAt(0);
	return nullptr;
}


std::shared_ptr<TMediaPacket> Bink::TExtractor::ReadNextPacket()
{
	Soy::Assert( mHandle != nullptr, "Handle expected");

	if ( mHandle->FrameNum >= mHandle->Frames )
	{
		std::shared_ptr<TMediaPacket> Packet( new TMediaPacket() );
		Packet->mEof = true;
		return Packet;
	}

	try
	{
		auto Packet = DecodeNextPacket();
		if ( !Packet )
			return nullptr;
		
		OnPacketExtracted( Packet->mTimecode, Packet->mMeta.mStreamIndex );
		return Packet;
	}
	catch(std::exception& e)
	{
		std::Debug << "Error extracting next bink frame: " << e.what() << std::endl;
	}
	catch(...)
	{
		std::Debug << "Error extracting next bink frame: Unknown exception" << std::endl;
	}
	
	return nullptr;
}


void Bink::TExtractor::GetMeta(TJsonWriter& Json)
{
	TMediaExtractor::GetMeta( Json );
}

size_t Bink::TExtractor::GetCurrentFrameBufferIndex()
{
	if ( mBufferRgba )
		return mBufferRgba->GetCurrentFrameBufferIndex();
	if ( mBufferPlanes )
		return mBufferPlanes->GetCurrentFrameBufferIndex();
	return 0;
}

size_t Bink::TExtractor::GetDecodeTargetFrameBufferIndex()
{
	if ( mBufferRgba )
		return mBufferRgba->GetDecodeTargetFrameBufferIndex();
	if ( mBufferPlanes )
		return mBufferPlanes->GetDecodeTargetFrameBufferIndex();
	return 0;
}

void Bink::TExtractor::GetBufferPixels(ArrayBridge<SoyPixelsImpl*>& Textures,float3x3& Transform,size_t VideoStreamIndex,size_t BufferIndex)
{
	//std::Debug << "Bink::TFrameBuffer::Lock pixels" << std::endl;

	if ( mBufferRgba && VideoStreamIndex == 0 )
	{
		//std::Debug << "mExtractor.mBufferRgba->GetPixels" << std::endl;

		auto& BufferPixels = mBufferRgba->GetPixels( *mHandle, Transform );
		Textures.PushBack( &BufferPixels );
		return;
	}

	if ( mBufferPlanes )
	{
		if ( mParams.mSplitVideoPlanesIntoStreams )
		{
			//std::Debug << "mExtractor.mBufferPlanes->GetPixels" << std::endl;
			auto& BufferPixels = mBufferPlanes->GetPixels( VideoStreamIndex, BufferIndex, Transform );
			Textures.PushBack( &BufferPixels );
		}
		else
		{
			//std::Debug << "mExtractor.mBufferPlanes->GetPixels array" << std::endl;
			mBufferPlanes->GetPixels( Textures, BufferIndex, Transform );
		}
		return;
	}

}


Bink::TFrameBuffer::TFrameBuffer(TExtractor& Extractor,size_t VideoStreamIndex,size_t BufferIndex,std::function<void()> OnUnlock) :
	mExtractor			( Extractor ),
	mOnUnlock			( OnUnlock ),
	mVideoStreamIndex	( VideoStreamIndex ),
	mBufferIndex		( BufferIndex )
{
}

Bink::TFrameBuffer::~TFrameBuffer()
{
	Unlock();
}

void Bink::TFrameBuffer::Lock(ArrayBridge<SoyPixelsImpl*>&& Textures,float3x3& Transform)
{
	mExtractor.GetBufferPixels( Textures, Transform, mVideoStreamIndex, mBufferIndex );

}

void Bink::TFrameBuffer::Lock(ArrayBridge<Gnm::TTexture>&& Textures,Gnm::TContext& Context,float3x3& Transform)
{
}

void Bink::TFrameBuffer::Unlock()
{
	if ( mOnUnlock )
	{
		mOnUnlock();
		mOnUnlock = nullptr;
	}
}

/*
#if defined(ENABLE_GNM)
Bink::TTextureBufferGnm::TTextureBufferGnm(struct BINKFRAMEBUFFERS& BufferInfo) :
	mLuma		( nullptr ),
	mChromaR	( nullptr ),
	mChromaB	( nullptr ),
	mAlpha		( nullptr )
{
	BINKFRAMEBUFFERS * fb = &textures->bink_buffers;

	for ( int i = 0 ; i < fb->TotalFrames ; i++ )
	{
		BINKFRAMEPLANESET * ps = &fb->Frames[i];

		if (ps->YPlane.Allocate)
			CreateTexture(&textures->Ydata[i], textures->Ytexture[i], &ps->YPlane.Buffer, &ps->YPlane.BufferPitch, fb->YABufferWidth, fb->YABufferHeight, textures->video_width, textures->video_height);

		if (ps->cRPlane.Allocate)
			CreateTexture(&textures->cRdata[i], textures->cRtexture[i], &ps->cRPlane.Buffer, &ps->cRPlane.BufferPitch, fb->cRcBBufferWidth, fb->cRcBBufferHeight, textures->video_width/2, textures->video_height/2);

		if (ps->cBPlane.Allocate)
			CreateTexture(&textures->cBdata[i], textures->cBtexture[i], &ps->cBPlane.Buffer, &ps->cBPlane.BufferPitch, fb->cRcBBufferWidth, fb->cRcBBufferHeight, textures->video_width/2, textures->video_height/2);

		if (ps->APlane.Allocate)
			CreateTexture(&textures->Adata[i], textures->Atexture[i], &ps->APlane.Buffer, &ps->APlane.BufferPitch, fb->YABufferWidth, fb->YABufferHeight, textures->video_width, textures->video_height);
	}

	// Register our locked texture pointers with Bink
	BinkRegisterFrameBuffers( bink, fb );
}
#endif
*/



Bink::TTextureBufferPlanes::TTextureBufferPlanes(struct BINKFRAMEBUFFERS& _BufferInfo,SoyPixelsMeta VideoMeta,HBINK Handle)
{
	mBufferInfo = _BufferInfo;
	auto& BufferInfo = mBufferInfo;

	std::Debug << "Bink Allocating " << BufferInfo.TotalFrames << " frame buffers..." << std::endl;

	for ( int i=0;	i<BufferInfo.TotalFrames;	i++ )
	{
		BINKFRAMEPLANESET& PlaneSet = BufferInfo.Frames[i];
		
		if ( PlaneSet.YPlane.Allocate )
		{
			SoyPixelsMeta BufferMeta( BufferInfo.YABufferWidth, BufferInfo.YABufferHeight, SoyPixelsFormat::Luma_Full );
			SoyPixelsMeta ImageMeta( VideoMeta.GetWidth(), VideoMeta.GetHeight(), SoyPixelsFormat::Luma_Full );
			auto& Pixels = AllocLuma( BufferMeta, ImageMeta );
			PlaneSet.YPlane.Buffer = Pixels.GetPixelsArray().GetArray();
			PlaneSet.YPlane.BufferPitch = Pixels.GetMeta().GetRowDataSize();
		}
		
		if ( PlaneSet.cRPlane.Allocate )
		{
			SoyPixelsMeta BufferMeta( BufferInfo.cRcBBufferWidth, BufferInfo.cRcBBufferHeight, SoyPixelsFormat::ChromaU_8 );
			SoyPixelsMeta ImageMeta( VideoMeta.GetWidth()/2, VideoMeta.GetHeight()/2, SoyPixelsFormat::ChromaU_8 );
			auto& Pixels = AllocChromaR( BufferMeta, ImageMeta );
			PlaneSet.cRPlane.Buffer = Pixels.GetPixelsArray().GetArray();
			PlaneSet.cRPlane.BufferPitch = Pixels.GetMeta().GetRowDataSize();
		}

		if ( PlaneSet.cBPlane.Allocate )
		{
			SoyPixelsMeta BufferMeta( BufferInfo.cRcBBufferWidth, BufferInfo.cRcBBufferHeight, SoyPixelsFormat::ChromaV_8 );
			SoyPixelsMeta ImageMeta( VideoMeta.GetWidth()/2, VideoMeta.GetHeight()/2, SoyPixelsFormat::ChromaV_8 );
			auto& Pixels = AllocChromaB( BufferMeta, ImageMeta );
			PlaneSet.cBPlane.Buffer = Pixels.GetPixelsArray().GetArray();
			PlaneSet.cBPlane.BufferPitch = Pixels.GetMeta().GetRowDataSize();
		}

		if ( PlaneSet.APlane.Allocate )
		{
			SoyPixelsMeta BufferMeta( BufferInfo.YABufferWidth, BufferInfo.YABufferWidth, SoyPixelsFormat::Greyscale );
			SoyPixelsMeta ImageMeta( VideoMeta.GetWidth(), VideoMeta.GetHeight(), SoyPixelsFormat::Greyscale );
			auto& Pixels = AllocAlpha( BufferMeta, ImageMeta );
			PlaneSet.APlane.Buffer = Pixels.GetPixelsArray().GetArray();
			PlaneSet.APlane.BufferPitch = Pixels.GetMeta().GetRowDataSize();
		}
		
	}
	
	BinkRegisterFrameBuffers( Handle, &mBufferInfo );
}


size_t Bink::TTextureBufferPlanes::GetCurrentFrameBufferIndex()	
{	
	auto Max = std::max<size_t>( mLuma.GetSize(), 1 );
	auto Index = mBufferInfo.FrameNum;
	Index %= Max;
	return Index;
}

size_t Bink::TTextureBufferPlanes::GetDecodeTargetFrameBufferIndex()	
{	
	auto Max = std::max<size_t>( mLuma.GetSize(), 1 );
	//	gr: assuming its 1 more
	auto Index = mBufferInfo.FrameNum + 1;
	Index %= Max;
	return Index;	
}


void Bink::TTextureBufferPlanes::GetPlaneStreamMetas(ArrayBridge<SoyPixelsMeta>& Meta,bool SplitPlanesIntoStreams)
{
	if ( !this->mLuma.IsEmpty() )
		Meta.PushBack( mLuma[0]->GetMeta() );

	if ( SplitPlanesIntoStreams )
	{
		if ( !this->mChromaB.IsEmpty() )
			Meta.PushBack( mChromaB[0]->GetMeta() );

		if ( !this->mChromaR.IsEmpty() )
			Meta.PushBack( mChromaR[0]->GetMeta() );

		if ( !this->mAlpha.IsEmpty() )
			Meta.PushBack( mAlpha[0]->GetMeta() );
	}
}

SoyPixelsImpl& Bink::TTextureBufferPlanes::GetPixels(size_t VideoStreamIndex,size_t BufferIndex,float3x3& Transform)
{
	Array<std::shared_ptr<SoyPixelsImpl>>* pStreamArray = nullptr;

	switch ( VideoStreamIndex )
	{
		case 0:	pStreamArray = &mLuma;		break;
		case 1:	pStreamArray = &mChromaR;	break;
		case 2:	pStreamArray = &mChromaB;	break;
		case 3:	pStreamArray = &mAlpha;		break;
	}

	if ( !pStreamArray )
	{
		std::stringstream Error;
		Error << "Plane Stream #" << VideoStreamIndex << " doesn't exist in TTextureBufferPlanes";
		throw Soy::AssertException( Error.str() );
	}

	auto& StreamArray = *pStreamArray;
	if ( StreamArray.IsEmpty() )
	{
		std::stringstream Error;
		Error << "Plane Stream #" << VideoStreamIndex << " not allocated (not present?) in TTextureBufferPlanes";
		throw Soy::AssertException( Error.str() );
	}

	auto FrameBufferIndex = GetCurrentFrameBufferIndex();
	if ( BufferIndex != FrameBufferIndex )
	{
		std::Debug << "Warning: stream " << VideoStreamIndex << " copying frame buffer " << BufferIndex << " but bink current is " << FrameBufferIndex << std::endl;
	}
	return *StreamArray[BufferIndex];
}

void Bink::TTextureBufferPlanes::GetPixels(ArrayBridge<SoyPixelsImpl*>& Textures,size_t BufferIndex,float3x3& Transform)
{
	auto FrameBufferIndex = GetCurrentFrameBufferIndex();
	if ( BufferIndex != FrameBufferIndex )
	{
		std::Debug << "Warning: all-streams copying frame buffer " << BufferIndex << " but bink current is " << FrameBufferIndex << std::endl;
	}

	if ( !mLuma.IsEmpty() )
		Textures.PushBack( mLuma[BufferIndex].get() );

	if ( !mChromaR.IsEmpty() )
		Textures.PushBack( mChromaR[BufferIndex].get() );

	if ( !mChromaB.IsEmpty() )
		Textures.PushBack( mChromaB[BufferIndex].get() );
	
	if ( !mAlpha.IsEmpty() )
		Textures.PushBack( mAlpha[BufferIndex].get() );
}



SoyPixelsImpl& Bink::TTextureBufferPlanes::AllocLuma(SoyPixelsMeta BufferMeta,SoyPixelsMeta ImageMeta)
{
	auto Overflow = BufferMeta.GetWidth() % 256;
	if ( Overflow > 0 )
		BufferMeta.DumbSetWidth( BufferMeta.GetWidth() + 256 - Overflow );

	std::shared_ptr<SoyPixelsImpl> Pixels( new SoyPixels(BufferMeta) );
	std::Debug << __func__ << " (" << BufferMeta << ", " << ImageMeta << ")" << std::endl;
	mLuma.PushBack( Pixels );
	return *Pixels;
}

SoyPixelsImpl& Bink::TTextureBufferPlanes::AllocChromaR(SoyPixelsMeta BufferMeta,SoyPixelsMeta ImageMeta)
{
	auto Overflow = BufferMeta.GetWidth() % 256;
	if ( Overflow > 0 )
		BufferMeta.DumbSetWidth( BufferMeta.GetWidth() + 256 - Overflow );

	std::shared_ptr<SoyPixelsImpl> Pixels( new SoyPixels(BufferMeta) );
	std::Debug << __func__ << " (" << BufferMeta << ", " << ImageMeta << ")" << std::endl;
	mChromaR.PushBack( Pixels );
	return *Pixels;
}

SoyPixelsImpl& Bink::TTextureBufferPlanes::AllocChromaB(SoyPixelsMeta BufferMeta,SoyPixelsMeta ImageMeta)
{
	auto Overflow = BufferMeta.GetWidth() % 256;
	if ( Overflow > 0 )
		BufferMeta.DumbSetWidth( BufferMeta.GetWidth() + 256 - Overflow );

	std::shared_ptr<SoyPixelsImpl> Pixels( new SoyPixels(BufferMeta) );
	std::Debug << __func__ << " (" << BufferMeta << ", " << ImageMeta << ")" << std::endl;
	mChromaB.PushBack( Pixels );
	return *Pixels;
}

SoyPixelsImpl& Bink::TTextureBufferPlanes::AllocAlpha(SoyPixelsMeta BufferMeta,SoyPixelsMeta ImageMeta)
{
	auto Overflow = BufferMeta.GetWidth() % 256;
	if ( Overflow > 0 )
		BufferMeta.DumbSetWidth( BufferMeta.GetWidth() + 256 - Overflow );

	std::shared_ptr<SoyPixelsImpl> Pixels( new SoyPixels(BufferMeta) );
	std::Debug << __func__ << " (" << BufferMeta << ", " << ImageMeta << ")" << std::endl;
	mAlpha.PushBack( Pixels );
	return *Pixels;
}


void Bink::TTextureBufferRgba::GetPlaneStreamMetas(ArrayBridge<SoyPixelsMeta>& Meta)
{
	Meta.PushBack( mRgba.GetMeta() );
}

SoyPixelsImpl& Bink::TTextureBufferRgba::GetPixels(struct BINK& Handle,float3x3& Transform)
{
	Soy::TScopeTimerPrint Timer("Bink::TTextureBufferRgba::GetPixels", 1 );
	auto& Pixels = mRgba;

	auto Width = Handle.Width;
	int Components = 4;
	auto Pitch = (Width * Components);
	auto PitchPad = 16 - (Pitch % 16);
	auto WidthPadded = Width + (PitchPad / Components);
	auto Height = Handle.Height;
	SoyPixelsMeta HandleMeta( Width, Height, SoyPixelsFormat::BGRA );

	Pixels.Init( HandleMeta );

	auto Format = GetPixelFormat( HandleMeta.GetFormat() );
	//	note: pointer needs to align to 16 too!
	auto* PixelsData = Pixels.GetPixelsArray().GetArray();

	auto Result = BinkCopyToBuffer( &Handle, PixelsData, Pixels.GetMeta().GetRowDataSize(), Pixels.GetHeight(), 0, 0, Format );

	//	gr: don;t have a way to handle this!
	bool Skipped = (Result==1);
	if ( Skipped )
	{
		throw Soy::AssertException("Bink copy skipped... handle this!");
	}

	return mRgba;
}



