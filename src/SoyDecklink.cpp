#include "SoyDecklink.h"

/*
The DeckLink API can be accessed from a sandboxed applications if the following requirements are met:
� Application is built against Mac OS X 10.7 or later 
� Ensure �Enable App sandboxing� is ticked in your application�s Xcode project, 
� Ensure you have selected a valid code signing identity, 
� Insert the following property into your application�s entitlements file: Refer to the Sandboxed Signal Generator target in the SignalGenerator sample application in the SDK.

key
com.apple.security.temporaryexception.mach-lookup.globalname
value
com.blackmagic-design.desktopvideo. DeckLinkHardwareXPCService

Further information can be found in the App Sandbox Design Guide available on Apple�s Mac Developer Library website
*/

namespace Decklink
{
	TContext&					GetContext();
	std::string					GetSerial(IDeckLink& Device,size_t Index);

	std::shared_ptr<TContext>	gContext;
}

bool MatchSerial(const std::string& Serial,const std::string& Match)
{
	if ( Match == "*" )
		return true;

	if ( Soy::StringContains( Serial, Match, false ) )
		return true;

	return false;
}


void Decklink::EnumDevices(std::function<void(const std::string&)> AppendName)
{
	auto Filter = [&](AutoReleasePtr<IDeckLinkInput>& Input,const std::string& Serial)
	{
		//if ( !MatchFilter( Serial, ))
		AppendName( Serial );
	};

	//	gr: this throws if no decklink driver, so just ignore
	try
	{
		auto& Context = GetContext();
		Context.EnumDevices( Filter );
	}
	catch(std::exception& e)
	{
	}
}


Decklink::TContext& Decklink::GetContext()
{
	if ( !gContext )
		gContext.reset( new TContext );

	return *gContext;
}


Decklink::TContext::TContext()
{
#if defined(TARGET_WINDOWS)
	//	gr: judging from the samples, when this is missing, it's because the drivers are not installed. (verified by me)
	auto Result = CoCreateInstance( CLSID_CDeckLinkIterator, nullptr, CLSCTX_ALL, IID_IDeckLinkIterator, (void**)&mIterator.mObject );
	Platform::IsOkay( Result, "No decklink driver installed.");
#else
	mIterator = CreateDeckLinkIteratorInstance();
#endif
	if ( !mIterator )
		throw Soy::AssertException("Decklink iterator is null");
}

Decklink::TContext::~TContext()
{
	std::lock_guard<std::mutex> Lock( mIteratorLock );
	mIterator.Release();
}

std::string Decklink::GetSerial(IDeckLink& Device,size_t Index)
{
	//	gr: argh overflow worries
	std::string DisplayName;
	{
		BSTR RealDisplayName = nullptr;
		auto Result = Device.GetDisplayName( &RealDisplayName );
		if ( Result == S_OK && RealDisplayName )
			DisplayName = Soy::WStringToString( RealDisplayName );
	}

	std::string ModelName;
	{
		BSTR RealModelName = nullptr;
		auto Result = Device.GetModelName( &RealModelName );
		if ( Result == S_OK && RealModelName )
			ModelName = Soy::WStringToString( RealModelName );
	}
	
	std::stringstream Serial;
	if ( !DisplayName.empty() && !ModelName.empty() )
	{
		Serial << DisplayName << "/" << ModelName;
	}
	else if ( !DisplayName.empty() )
	{
		Serial << DisplayName;
	}
	else if ( !ModelName.empty() )
	{
		Serial << ModelName;
	}
	else
	{
		Serial << Index;
	}

	return Serial.str();
}

void Decklink::TContext::EnumDevices(std::function<void(AutoReleasePtr<IDeckLinkInput>&,const std::string&)> EnumDevice)
{
	std::lock_guard<std::mutex> Lock( mIteratorLock );
	Soy::Assert( mIterator!=nullptr, "Iterator expected" );

	IDeckLink* CurrentDevice = nullptr;
	size_t CurrentDeviceIndex = 0;
	while ( true )
	{
		try
		{
			auto Result = mIterator->Next( &CurrentDevice );
			if ( CurrentDevice == nullptr )
				break;
			Platform::IsOkay( Result, "Enumerating devices");
			AutoReleasePtr<IDeckLink> Device( CurrentDevice, false );
			
			//	check it's an input
			AutoReleasePtr<IDeckLinkInput> DeviceInput;
			Result = Device->QueryInterface( IID_IDeckLinkInput, (void**)&DeviceInput.mObject );
			if ( Result != S_OK )
				continue;

			auto Serial = GetSerial( *Device, CurrentDeviceIndex );

			EnumDevice( DeviceInput, Serial );
		}
		catch(std::exception& e)
		{
			std::Debug << "Error enumerating device; " << e.what() << std::endl;
			break;
		}
	}

}

AutoReleasePtr<IDeckLinkInput> Decklink::TContext::GetDevice(const std::string& MatchSerial)
{
	Array<AutoReleasePtr<IDeckLinkInput>> MatchingDevices;
	
	auto Filter = [&](AutoReleasePtr<IDeckLinkInput>& Input,const std::string& Serial)
	{
		if ( !::MatchSerial( Serial, MatchSerial ) )
			return;

		MatchingDevices.PushBack( Input );
	};

	EnumDevices( Filter );

	if ( MatchingDevices.IsEmpty() )
	{
		std::stringstream Error;
		Error << "No devices matching " << MatchSerial;
		throw Soy::AssertException( Error.str() );
	}

	return MatchingDevices[0];
}





Decklink::TExtractor::TExtractor(const TMediaExtractorParams& Params) :
	TMediaExtractor			( Params )
{
	mInput = GetContext().GetDevice( Params.mFilename );
	Start();
}

void Decklink::TExtractor::GetStreams(ArrayBridge<TStreamMeta>&& Streams)
{
	/*
	TStreamMeta Meta;
	Meta.mStreamIndex = 0;
	Meta.mCodec = SoyMediaFormat::RGB;
	Streams.PushBack( Meta );
	*/
}


std::shared_ptr<TMediaPacket> Decklink::TExtractor::ReadNextPacket()
{
//	OnPacketExtracted( Packet.mTimecode, Packet.mMeta.mStreamIndex );
	return nullptr;
}


void Decklink::TExtractor::GetMeta(TJsonWriter& Json)
{
	TMediaExtractor::GetMeta( Json );
}

