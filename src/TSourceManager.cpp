#include "TSourceManager.h"

#include "PopMovieTexture.h"

TSourceManager::TSourceManager() :
	SoyWorkerThread	( "TSourceManager", SoyWorkerWaitMode::Sleep )
{
	Start();
}


TSourceManager::~TSourceManager()
{
	WaitToFinish();

	//	make sure nobody is using the data any more...
	std::lock_guard<std::mutex> Lock( mFilenamesLock );
}

bool TSourceManager::Iteration()
{
	auto OnFileFound = [this](const std::string& Filename,const std::string& Prefix)
	{
		std::stringstream FinalFilename;
		FinalFilename << Prefix << Filename;
		PushFilename( FinalFilename.str() );
	};

	//	do another iteration
	Array<std::string> Directories;
	{
		std::lock_guard<std::mutex> Lock( mDirectoriesLock );
		Directories.Copy( mDirectories );
	}

	//	abort when thread is stopped
	auto Block = [this]
	{
		return IsWorking();
	};

	auto Throttle = [this]
	{
		std::this_thread::sleep_for( GetPerItemSleepDuration() );
	};
	
	try
	{
		PopMovie::EnumSources( OnFileFound, GetArrayBridge(Directories), Block, Throttle );
	}
	catch(std::exception& e)
	{
		std::Debug << "Exception enumerating sources; " << e.what() << std::endl;
	}

	return true;
}

std::chrono::milliseconds TSourceManager::GetSleepDuration()
{
	//	thread loops
	return std::chrono::milliseconds( 500 );
}

std::chrono::milliseconds TSourceManager::GetPerItemSleepDuration()
{
	//	effectively throttles how many we can discover instantly, but the system is supposed to be asynchronous anyway
	static int ItemSleepMs = 200;
	return std::chrono::milliseconds( ItemSleepMs );
}



void TSourceManager::PushFilename(const std::string& Filename)
{
	std::lock_guard<std::mutex> Lock( mFilenamesLock );

	//	check for dupes
	if ( mFilenames.Find(Filename) )
		return;

	mFilenames.PushBack( Filename );
}

void TSourceManager::EnumDirectory(const char* Directory)
{
	if ( !Directory )
		return;

	EnumDirectory( std::string(Directory) );
}

void TSourceManager::EnumDirectory(const std::string& Directory)
{
	if ( Directory.empty() )
		return;

	std::lock_guard<std::mutex> Lock( mDirectoriesLock );

	//	check for dupes
	if ( mDirectories.Find(Directory) )
		return;

	mDirectories.PushBack( Directory );

	Wake();
}


bool TSourceManager::GetFilename(size_t Index,std::string& Filename)
{
	if ( Index >= mFilenames.GetSize() )
		return false;
	
	std::lock_guard<std::mutex> Lock( mFilenamesLock );
	Filename = mFilenames[Index];
	return true;
}

void TSourceManager::Clear()
{
	//	todo: restart thread?
	{
		std::lock_guard<std::mutex> Lock( mFilenamesLock );
		mFilenames.Clear();
	}
	
	{
		std::lock_guard<std::mutex> Lock( mDirectoriesLock );
		mDirectories.Clear();
	}
}


