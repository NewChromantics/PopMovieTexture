#pragma once

#include <SoyTypes.h>
#include <SoyThread.h>
#include <SoyMedia.h>
#include <SoyGraphics.h>

#if defined(ENABLE_OPENGL)
#include <SoyOpenglContext.h>
#endif

#if defined(TARGET_IOS)
#import <CoreMedia/CMTime.h>
#endif

class TVideoDecoder;
class TVideoDecoderParams;
class TPixelBufferManager;
class TAudioBufferManager;

#define MAX_STREAMS	10

namespace Opengl
{
	class TContext;
}

namespace Directx
{
	class TContext;
	class TTexture;
}

namespace Platform
{
	class TMediaFormat;		//	implemented per platform as allocated object
}



//	for the framework, external allocator
namespace PopMovieDecoder
{
	std::shared_ptr<TVideoDecoder>	AllocDecoder(TVideoDecoderParams Params);
};


/*
#if defined(TARGET_OSX)
FOUNDATION_EXPORT double PopMovieTextureOsxFrameworkVersionNumber;
FOUNDATION_EXPORT const unsigned char PopMovieTextureOsxFrameworkVersionString[];
#endif
*/

class TVideoDecoderParams
{
public:
	static const std::string			gProtocol_TestDecoder;
	static const std::string			gProtocol_AndroidSdCard;
	static const std::string			gProtocol_AndroidAssets;
	static const std::string			gProtocol_AndroidJar;		//	jar/obb/zip
	static const std::string			gProtocol_CaptureDevice;
	static const std::string			gProtocol_Kinect;
	static const std::string			gProtocol_RawH264;
	static const std::string			gProtocol_Push;
	static const std::string			gProtocol_Legacy;
	static const std::string			gProtocol_Experimental;
	static const std::string			gProtocol_Window;
	static const std::string			gProtocol_Decklink;
	static const std::string			gSuffix_HLS;
	
public:
	TVideoDecoderParams() :
		mAllowSlowCopy			( true ),
		mAllowFastCopy			( true ),
		mAvfCopyBuffer			( false ),
		mPeekBeforeDefferedCopy	( true ),
		mDebugNoNewPixelBuffer	( false ),
		mDebugBlit				( false ),
		mApplyVideoTransform	( true ),
		mDecoderUseHardwareBuffer	( true ),
		mAssumedTargetTextureMeta	( 2048, 2048, SoyPixelsFormat::RGBA ),
		mExtractAheadMs			( std::chrono::milliseconds(500) ),
		mForceNonPlanarOutput	( false ),
		mForceYuvColourFormat	( SoyPixelsFormat::Invalid ),
		mBlitFatalErrors		( true ),
		mBlitMergeYuv			( true ),
		mClearBeforeBlit		( true ),
		mExtractAudioStreams	( true ),
		mExtractVideoStreams	( true ),
		mExtractDepthStreams	( false ),
		mExtractSkeletonStreams	( false ),
		mExtractAlpha			( true ),
		mCopyFrameAtUpdate		( false ),
		mOnlyExtractKeyframes	( false ),
		mResetInternalTimestamp	( true ),
		mApplyHeightPadding		( true ),
		mApplyWidthPadding		( true ),
		mWindowIncludeBorders	( true ),
		mVerboseDebug			( false ),
		mWin7Emulation			( false ),
		mSplitAudioChannelsIntoStreams	( false ),
		mLiveUseClockTime		( false ),
		mSplitVideoPlanesIntoStreams	( false ),
		mEnableDecoderThreading		( true ),
		mCopyBuffersInExtraction	( false ),
		mExtractorPreDecodeSkip		( false ),
		mAndroidSingleBufferMode	( false ),
		mDrawTimestamp				( false )
	{
	}
	
	SoyGraphics::TTextureUploadParams	mTextureUploadParams;
	TPixelBufferParams				mPixelBufferParams;
	std::string	mFilename;
	bool		mAllowSlowCopy;
	bool		mAllowFastCopy;
	bool		mAvfCopyBuffer;				//	at OS level, copy buffer, else we need to lock it to copy
	bool		mPeekBeforeDefferedCopy;
	bool		mDebugNoNewPixelBuffer;
	SoyTime		mTimecodeOffset;
	bool		mDebugBlit;					//	use test shader for blitting
	bool		mApplyVideoTransform;
	bool		mDecoderUseHardwareBuffer;		//	on android, this means decoding to surface texture
	SoyPixelsMeta	mAssumedTargetTextureMeta;	//	for pre-allocation or hardware buffers which need allocating before we see images, use these dimensions/format. ideally... this should be gone, but better than magic numbers in platform code
	SoyTime		mExtractAheadMs;			//	when extracting, read up to this far ahead of the current time to make sure we stay ahead. Will affect memory usage as more packets are stored
	bool		mForceNonPlanarOutput;		//	don't allow mulitple-plane pixel output (for frameworks that want a simple interface or no blitter access)
	SoyPixelsFormat::Type	mForceYuvColourFormat;	//	if valid, decode any YUV planes as a particular colour space
	bool		mBlitFatalErrors;			//	when on, we show the "error" shader
	bool		mBlitMergeYuv;				//	do expected YUV conversion (turn off for debugging)
	bool		mClearBeforeBlit;			//	clear render targets before blit (debug & speed savings)
	bool		mExtractAudioStreams;
	bool		mExtractVideoStreams;
	bool		mExtractDepthStreams;
	bool		mExtractSkeletonStreams;
	bool		mExtractAlpha;
	bool		mSplitAudioChannelsIntoStreams;
	bool		mCopyFrameAtUpdate;			//	deffered job has explicit timecode. needs a better name
	bool		mOnlyExtractKeyframes;		//	for debug
	bool		mResetInternalTimestamp;
	bool		mApplyHeightPadding;		//	windows YUV videos sometimes are misaligned and need to be padded to %16 height. Turn it off with this
	bool		mApplyWidthPadding;
	bool		mWindowIncludeBorders;		//	full or client area in window: capture
	bool		mVerboseDebug;				//	enable LOTS of debug output.
	bool		mWin7Emulation;				//	force down some paths that always happen in win 7
	bool		mLiveUseClockTime;
	bool		mSplitVideoPlanesIntoStreams;
	bool		mEnableDecoderThreading;
	bool		mCopyBuffersInExtraction;
	bool		mExtractorPreDecodeSkip;
	bool		mAndroidSingleBufferMode;	//	alloc surface textures in single or multi(I assume) buffer mode
	bool		mDrawTimestamp;				//	draw timestamp on texture
	vec2f		mDrawTimestampPosA;			//	uv coords of first timestamp
	vec2f		mDrawTimestampPosB;			//	uv coords of second timestamp (for stereo)
};
std::ostream& operator<<(std::ostream &out,const TVideoDecoderParams& in);

//	gr: remove this!
class TVideoMeta
{
public:
	TVideoMeta() :
		mCodec	( SoyMediaFormat::Invalid )
	{
	}
	
public:
	SoyTime					mDuration;			//	known end. Invalid if infinite
	SoyTime					mKnownDuration;		//	how many frames we know of so far
	SoyMediaFormat::Type	mCodec;
	std::string				mDebug;				//	other debug info
};


class TVideoDecoder
{
public:
	TVideoDecoder(const TVideoDecoderParams& Params,std::map<size_t,std::shared_ptr<TPixelBufferManager>>& PixelBufferManagers,std::map<size_t,std::shared_ptr<TAudioBufferManager>>& AudioBufferManagers);
	virtual ~TVideoDecoder();
	
	virtual void		StartMovie();
	
	virtual TVideoMeta	GetMeta()=0;
	virtual void		GetMeta(TJsonWriter& Json);
	size_t				GetMetaRevision()					{	return mMetaRevision;	}

	//	for now, deffered errors are found this way, and then we explicitly render an error shader
	virtual bool		HasFatalError(std::string& Error)	{	return false;	}
	
	virtual void		SetPlayerTime(SoyTime Timestamp);	//	to aid pre-emptive frame skip
	SoyTime				GetPlayerTime() const		{	return mPlayerTime;	}
	
	virtual void			GetStreamMeta(ArrayBridge<TStreamMeta>&& StreamMetas)=0;
	TStreamMeta				GetStreamMeta(size_t StreamIndex);				//	throws if stream doesn't exist

	size_t					GetVideoStreamIndex(size_t VideoStreamIndex);	//	convert "video index" to real index. throws if doesn't exist
	size_t					GetAudioStreamIndex(size_t AudioStreamIndex);	//	convert "video index" to real index. throws if doesn't exist
	size_t					GetTextStreamIndex(size_t TextStreamIndex);		//	convert "Text index" to real index. throws if doesn't exist
	TPixelBufferManager&	GetPixelBufferManager(size_t StreamIndex);
	TAudioBufferManager&	GetAudioBufferManager(size_t StreamIndex);
	TTextBufferManager&		GetTextBufferManager(size_t StreamIndex);
	TMediaBufferManager&	GetBufferManager(size_t StreamIndex);
	void					GetBufferManagers(ArrayBridge<std::shared_ptr<TMediaBufferManager>>&& Managers);
	void					ForEachBufferManager(std::function<void(size_t,TMediaBufferManager&)> FuncPerStreamIndex);

	//	wrappers
	/*
	std::shared_ptr<TPixelBuffer>	PopPixelBuffer(SoyTime& Timestamp,)		{	return GetPixelBufferManager().PopPixelBuffer( Timestamp );	}
	bool							PushPixelBuffer(TPixelBufferFrame& PixelBuffer,std::function<bool()> Block)	{	return GetPixelBufferManager().PushPixelBuffer( PixelBuffer, Block );	}
	bool							PrePushPixelBuffer(SoyTime Timestamp)	{	return GetPixelBufferManager().PrePushPixelBuffer( Timestamp );	}
	bool				PeekPixelBuffer(SoyTime Timestamp);
	bool				IsPixelBufferFull() const							{	return GetPixelBufferManager().IsPixelBufferFull();	}
	void				CorrectDecodedFrameTimestamp(SoyTime& Timestamp)	{	return GetPixelBufferManager().CorrectDecodedFrameTimestamp( Timestamp );	}
	 */
	void				ReleaseFrames();
	virtual void		OnDeviceShutdown()	{}

protected:
	std::shared_ptr<TPixelBufferManager>	AllocPixelBufferManager(const TStreamMeta& Stream);
	std::shared_ptr<TAudioBufferManager>	AllocAudioBufferManager(size_t StreamIndex);
	std::shared_ptr<TTextBufferManager>		AllocTextBufferManager(size_t StreamIndex);

	void				OnFrameDecoded();		//	used to measure decoding speed
	void				OnBufferManagersChanged(size_t StreamIndex);
	void				UpdateMetaRevision()	{	mMetaRevision++;	}

private:
	std::shared_ptr<TPixelBuffer>	PopPixelBuffer_Unsafe(SoyTime Timestamp,int& FramesSkipped);
	void							CacheStreamIndexTables();		

public:
	TVideoDecoderParams				mParams;
	SoyEvent<size_t>				mOnBufferManagersChanged;

private:
	std::map<size_t,std::shared_ptr<TPixelBufferManager>>	mPixelBufferManagers;
	std::map<size_t,std::shared_ptr<TAudioBufferManager>>	mAudioBufferManagers;
	std::map<size_t,std::shared_ptr<TTextBufferManager>>	mTextBufferManagers;

	//	cached lookups
	BufferArray<size_t,MAX_STREAMS>	mVideoStreamIndexToStreamIndex;
	BufferArray<size_t,MAX_STREAMS>	mAudioStreamIndexToStreamIndex;
	BufferArray<size_t,MAX_STREAMS>	mTextStreamIndexToStreamIndex;

	SoyTime							mPlayerTime;
	size_t							mMetaRevision;
};



//	this will be removed when everything is a "raw decoder"
//	gr: refactor to alloc extractors from filenames, encoders from Codecs and decoders from codecs
class TVideoDecoderFactory
{
public:
	virtual std::shared_ptr<TMediaExtractor>	AllocExtractor(const TMediaExtractorParams& Params)=0;
	virtual std::shared_ptr<TVideoDecoder>	Alloc(TVideoDecoderParams Params)=0;
	virtual std::shared_ptr<TVideoDecoder>	Alloc(TVideoDecoderParams Params,std::map<size_t,std::shared_ptr<TPixelBufferManager>>& PixelBufferManagers,std::map<size_t,std::shared_ptr<TAudioBufferManager>>& AudioBufferManagers)=0;
};



class TRawDecoder : public TVideoDecoder
{
public:
	TRawDecoder(const TVideoDecoderParams& Params,std::map<size_t,std::shared_ptr<TPixelBufferManager>>& PixelBufferManagers,std::map<size_t,std::shared_ptr<TAudioBufferManager>>& AudioBufferManagers,std::shared_ptr<Opengl::TContext> OpenglContext);
	~TRawDecoder();
	
	virtual void		SetPlayerTime(SoyTime Timestamp) override;
	virtual void		GetStreamMeta(ArrayBridge<TStreamMeta>&& Streams) override;
	virtual void		GetMeta(TJsonWriter& Json) override;
	virtual TVideoMeta	GetMeta() override;
	virtual bool		HasFatalError(std::string& Error) override;
	
	template<typename TYPE>
#if defined(ENABLE_RTTI)
	TYPE&				GetExtractor()			{	return dynamic_cast<TYPE&>( *mExtractor );	}
#else
	//	gr: maybe need a solution for android.
	TYPE&				GetExtractor()			{	return reinterpret_cast<TYPE&>( *mExtractor );	}
#endif

protected:
	//	gr: these WILL be moved to a factory
	virtual std::shared_ptr<TMediaDecoder>	AllocVideoDecoder(const std::string& Name,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> Input,std::shared_ptr<TPixelBufferManager> Output,const TVideoDecoderParams& Params,std::shared_ptr<Platform::TMediaFormat> Format,std::shared_ptr<Opengl::TContext> OpenglContext);
	virtual std::shared_ptr<TMediaDecoder>	AllocAudioDecoder(const std::string& Name,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> Input,std::shared_ptr<TAudioBufferManager> Output,const TVideoDecoderParams& Params,std::shared_ptr<Platform::TMediaFormat> Format);
	virtual std::shared_ptr<TMediaDecoder>	AllocTextDecoder(const std::string& Name,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> Input,std::shared_ptr<TTextBufferManager> Output,const TVideoDecoderParams& Params,std::shared_ptr<Platform::TMediaFormat> Format);
	std::shared_ptr<TMediaDecoder>			AllocAudioSplitDecoder(const std::string& Name,std::shared_ptr<TMediaPacketBuffer> Input,std::shared_ptr<TAudioBufferManager> Output,std::shared_ptr<TAudioBufferManager>& RealOutput,std::shared_ptr<TMediaDecoder>& RealDecoder,size_t RealChannel);

	void				AllocEncoders(const ArrayBridge<TStreamMeta>& Streams);
	
protected:
	std::map<size_t,std::shared_ptr<TMediaDecoder>>	mDecoders;
	
	std::shared_ptr<TMediaExtractor>	mExtractor;
	std::string							mFatalError;	//	gr: never used!
	std::shared_ptr<Opengl::TContext>	mContext;		//	bit dangerous?
};




