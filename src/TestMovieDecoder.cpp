#include "TestMovieDecoder.h"
#include <future>

TestVideoDecoder::TestVideoDecoder(const TVideoDecoderParams& Params,std::map<size_t,std::shared_ptr<TPixelBufferManager>>& PixelBufferManagers,std::map<size_t,std::shared_ptr<TAudioBufferManager>>& AudioBufferManagers) :
	TVideoDecoder	( Params, PixelBufferManagers, AudioBufferManagers ),
	SoyWorkerThread	( "TestVideoDecoder", SoyWorkerWaitMode::Sleep ),
	mCounter		( 0 ),
	mFrameRate		( 60 )
{
	//	just for testing multi stream indexing
	mStreamMeta.mStreamIndex = 1;
	
	auto Meta = SoyPixelsMeta( 256, 256, SoyPixelsFormat::RGBA );

	//	pull frame rate off if it's there
	std::string FrameRateString = Params.mFilename;
	auto Filename = Soy::StringPopUntil( FrameRateString, '@', false, false );
	if ( !FrameRateString.empty() )
	{
		Soy::StringToType( mFrameRate, FrameRateString );
	}

	Soy::StringToType( Meta, Filename );

	//	gr: for metal (which doesn't support RGB) we do rgba
	mStreamMeta.mPixelMeta = Meta;

	std::Debug << "Initialised test decoder with " << Meta << " at " << mFrameRate << " fps" << std::endl;

	AllocPixelBufferManager( mStreamMeta );
}

void TestVideoDecoder::StartMovie()
{
	Start(false);
}

bool TestVideoDecoder::Iteration()
{
	auto& Meta = mStreamMeta.mPixelMeta;
	auto ColourCount = Meta.GetChannels();
	
	auto Width = Meta.GetWidth();
	auto Height = Meta.GetHeight();
	
	//	create buffer
	std::shared_ptr<TDumbPixelBuffer> pBuffer;
	try
	{
		pBuffer.reset(new TDumbPixelBuffer(Meta));
	}
	catch (std::exception& e)
	{
		std::Debug << "Error creating pixel buffer: " << e.what() << std::endl;
		return false;
	}
	
	auto& Pixels = pBuffer->mPixels.GetPixelsArray();
	
	//	fill buffer with cycling colour
	mCounter++;
	auto CurrentColour = mCounter % ColourCount;
	
	auto ChannelCount = Meta.GetChannels();
	static bool RandomAlpha = true;
		
	int ScanlineY = mCounter % Height;

	//	instead of waiting on futures, just wait for atomic to get to zero
	std::atomic<int> PendingLines( Height );
	auto* PixelsPtr = Pixels.GetArray();


	//	generate a line to copy
	Array<uint8> Line;

	auto GenerateLine = [&]
	{
		BufferArray<uint8,100> Colour;
		Colour.SetSize(ColourCount);
		for ( int c=0;	c<ColourCount;	c++ )
			Colour[c] = (CurrentColour==c) ? 0xFF : 0x10;
		if ( ChannelCount == 4 && RandomAlpha )
			Colour[ChannelCount-1] = std::rand() % 255;
		Line.SetSize( Width*ChannelCount );
		for( int x=0;	x<Width*ChannelCount;	x++ )
		{
			Line[x] = Colour[x%ColourCount];
		}
	};

	auto WriteScanline = [&](int y)
	{
		auto index = (y * Width) * ChannelCount;
		memset( &PixelsPtr[index], 0xff, ChannelCount*Width );
	};

	auto WriteLines = [&](int FirstY,int LineCount)
	{
		int LastY = std::min<int>( Height, FirstY+LineCount );
		for ( int y=FirstY;	y<LastY;	y++ )
		{
			auto index = (y * Width) * ChannelCount;
			memcpy( &PixelsPtr[index], Line.GetArray(), Line.GetDataSize() );
			PendingLines--;
		}
	};

	{
		ofScopeTimerWarning InitTimer("Generating lines...", GetSleepDuration().count() );
		GenerateLine();

		int Threads = 5;
		//	+1 to overflow last
		int BlockSize = (Height/Threads)+1;
		for( int t=0;	t<Threads;	t++ )
		{
			auto f = std::async( std::launch::async, WriteLines, t*BlockSize, BlockSize );
			//Futures.push_back( std::move(f) );
		}

		ofScopeTimerWarning WaitTimer("Waiting for pending lines...", GetSleepDuration().count() );
		/*
		for ( int f=0;	f<Futures.size();	f++ )
		Futures[f].wait();
		*/
		while( PendingLines != 0 )
		{
		}
		WriteScanline(ScanlineY);
	}

	SoyTime FrameTime( true );
	if ( !mFirstTimestamp.IsValid() )
		mFirstTimestamp = FrameTime;
	FrameTime -= mFirstTimestamp;

	auto StreamIndex = mStreamMeta.mStreamIndex;
	TPixelBufferFrame Frame;
	Frame.mPixels = pBuffer;
	Frame.mTimestamp = FrameTime;
	GetPixelBufferManager(StreamIndex).CorrectDecodedFrameTimestamp( Frame.mTimestamp );
	GetPixelBufferManager(StreamIndex).mOnFrameDecoded.OnTriggered( Frame.mTimestamp );
	
	//	block while thread is alive
	auto Block = [this]
	{
		return false;
	};	
	GetPixelBufferManager(StreamIndex).PushPixelBuffer( Frame, Block );

	return true;
}

void TestVideoDecoder::GetStreamMeta(ArrayBridge<TStreamMeta>&& Streams)
{
	Streams.PushBack( mStreamMeta );
}

