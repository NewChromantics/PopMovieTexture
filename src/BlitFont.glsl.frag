
R"*###*(

#define frac(x) fract(x)
#define lerp(a,b,t) mix(a,b,t)

#define MAX_CHARS 7

uniform int NumberA;
uniform int NumberB;
uniform int NumberC;
uniform int NumberD;
uniform int NumberE;
uniform int NumberF;
uniform int NumberG;

uniform float2 DrawUVa;
uniform float2 DrawUVb;
const float DrawSize = 0.05;
const float DropShadow = 0.075;


//	gr: bitfield double height glyphs
//	gr: remember, these are COLUMN MAJOR. Code change below
//	gr: rather than replace all matrixes with 0.0 1.0 2.0 and making it unreadable, using letters.
#define OO	0.0
#define AA	1.0
#define BB	2.0
#define CC	3.0

mat4 GLYPH_NONE = mat4(
vec4(OO, OO, OO, OO),
vec4(OO, OO, OO, OO),
vec4(OO, OO, OO, OO),
 vec4(OO, OO, OO, OO) );

mat4 GLYPH_0 = mat4(
vec4(BB, AA, BB, OO),
vec4(CC, OO, CC, OO),
vec4(CC, OO, CC, OO),
vec4(AA, BB, AA, OO) );

mat4 GLYPH_1 = mat4(
vec4(BB, CC, OO, OO),
vec4(OO, CC, OO, OO),
vec4(OO, CC, OO, OO),
vec4(BB, CC, BB, OO) );

mat4 GLYPH_2 = mat4(
vec4(BB, AA, BB, OO),
vec4(OO, OO, CC, OO),
vec4(BB, AA, OO, OO),
vec4(CC, BB, BB, OO) );

mat4 GLYPH_3 = mat4(
vec4(AA, AA, BB, OO),
vec4(OO, BB, AA, OO),
vec4(OO, AA, BB, OO),
vec4(BB, BB, AA, OO) );

mat4 GLYPH_4 = mat4(
vec4(OO, BB, CC, OO),
vec4(CC, OO, CC, OO),
vec4(CC, BB, CC, BB),
vec4(OO, OO, CC, OO) );

mat4 GLYPH_5 = mat4(
vec4(CC, AA, AA, OO),
vec4(CC, OO, OO, OO),
vec4(OO, AA, BB, OO),
vec4(BB, BB, AA, OO) );

mat4 GLYPH_6 = mat4(
vec4(BB, AA, AA, OO),
vec4(CC, BB, BB, OO),
vec4(CC, OO, CC, OO),
vec4(CC, BB, CC, OO) );

mat4 GLYPH_7 = mat4(
vec4(AA, AA, CC, OO),
vec4(OO, OO, CC, OO),
vec4(OO, CC, OO, OO),
vec4(CC, OO, OO, OO) );

mat4 GLYPH_8 = mat4(
vec4(BB, AA, BB, OO),
vec4(AA, BB, AA, OO),
vec4(CC, OO, CC, OO),
vec4(AA, BB, AA, OO) );

mat4 GLYPH_9 = mat4(
vec4(BB, AA, BB, OO),
vec4(CC, OO, CC, OO),
vec4(AA, AA, CC, OO),
vec4(OO, OO, CC, OO) );



//	gr: abs on GLSL<130 is float-only
//#define abs_int(x)	int( abs( (float)(x) ) )
int abs_int(int x)
{
	float xf = float(x);
	return int( abs(xf) );
}

float Range(float Min,float Max,float Value)
{
	return (Value-Min) / (Max-Min);
}

float2 Range2(float2 Min,float2 Max,float2 Value)
{
	return (Value-Min) / (Max-Min);
}

mat4 GetGlyph(int StringChar)
{
	int NumberString[MAX_CHARS];
	NumberString[0] = NumberA;
	NumberString[1] = NumberB;
	NumberString[2] = NumberC;
	NumberString[3] = NumberD;
	NumberString[4] = NumberE;
	NumberString[5] = NumberF;
	NumberString[6] = NumberG;
	
	mat4 Font[10+1];
	Font[0] = GLYPH_NONE;
	Font[1] = GLYPH_0;
	Font[2] = GLYPH_1;
	Font[3] = GLYPH_2;
	Font[4] = GLYPH_3;
	Font[5] = GLYPH_4;
	Font[6] = GLYPH_5;
	Font[7] = GLYPH_6;
	Font[8] = GLYPH_7;
	Font[9] = GLYPH_8;
	Font[10] = GLYPH_9;
	
	return Font[NumberString[StringChar]+1];
}

float GetGlyphMask(mat4 Glyph,float2 GlyphUv)
{
	if ( GlyphUv.x < 0.0 || GlyphUv.y < 0.0 || GlyphUv.x > 1.0 || GlyphUv.y > 1.0 )
		return 0.0;

	int row = int( GlyphUv.y * 4.0 );
	int col = int( GlyphUv.x * 4.0 );

	//	gr: opengl is COLUMN MAJOR
	//int Mask = Glyph[row][col];
	int Mask = int( Glyph[row][col] );
	

	bool MaskTop = abs_int(Mask - 2)==1;//	&1
	bool MaskBottom = (Mask >= 2);//	&2
	if ( frac( GlyphUv.y * 4.0) < 0.5 )
		return MaskTop ? 1.0 : 0.0;
	else
		return MaskBottom ? 1.0 : 0.0;
}

float4 GetStringColour(float2 StringUv)
{
	StringUv.x *= float(MAX_CHARS);
	float2 GlyphUv = frac( StringUv );
	int StringIndex = int( StringUv.x );
	mat4 Glyph = GetGlyph( StringIndex );
	
	//	scale down glyph uv to fit in dropshadow
	GlyphUv *= 1.0 + DropShadow;
	
	float WhiteMask = GetGlyphMask( Glyph, GlyphUv );
	float BlackMask = GetGlyphMask( Glyph, GlyphUv - float2(DropShadow,DropShadow) );
	
	float Mask = max( WhiteMask, BlackMask );
	
	float Colour = WhiteMask;
	
	return float4( Colour, Colour, Colour, Mask );
}


float4 BlitText(float4 Colour,float2 uv,float2 DrawUv)
{
	float2 Min = DrawUv;
	float2 Max = float2( MAX_CHARS, 1.0 ) * DrawSize;
	float2 StringUv = Range2( Min, Min+Max, uv );
	
	if ( StringUv.x > 0.0 && StringUv.y > 0.0 && StringUv.x < 1.0 && StringUv.y < 1.0 )
	{
		//	gr: had to flip this in unity, but not glsl?
		//StringUv.y = 1- StringUv.y;
		float4 White = GetStringColour( StringUv );
		Colour.xyz = lerp( Colour, White, White.a ).xyz;
	}
	
	return Colour;
}


varying highp vec2 oTexCoord;
uniform sampler2D Texture0;
const highp mat3 Transform = mat3(	1,0,0,	0,1,0,	0,0,1	);

void main()
{
	highp vec2 uv = (vec3(oTexCoord.x,oTexCoord.y,1)*Transform).xy;
	highp vec4 rgba = texture2D(Texture0,uv);

	rgba = BlitText( rgba, uv, DrawUVa );
	rgba = BlitText( rgba, uv, DrawUVb );
	
	gl_FragColor = rgba;
}

)*###*"

