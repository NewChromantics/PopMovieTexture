/*

	Generic inspector extension for PopMovie instances

*/
using UnityEngine;
using System.Collections;					// required for Coroutines
using System.Runtime.InteropServices;		// required for DllImport
using System;								// requred for IntPtr
using System.Text;
using System.Collections.Generic;
using UnityEditor;


//	derive from this inspector to get all the generic inspector stuff
//[CustomEditor(typeof(PopMovieDERVIEDTYPE))]
public class PopMovieInspector : Editor
{
	protected static bool ShowGlobalOptions = false;

	protected static bool ShowPerformanceGraphs = false;
	protected static bool ShowLocalisedPerformanceGraphs = true;

	protected List<RenderTexture> PerformanceGraphs;
	protected int PerformanceGraphWidth = 200;
	protected int PerformanceGraphHeight = 100;

	protected static bool ShowAudioGraphs = false;
	protected List<RenderTexture> AudioGraphs;

	protected static bool ShowMeta = false;

	protected static bool ShowDevices = false;


	protected void ShowInspectorDebug(PopMovie Instance,Editor ThisEditor)
	{
		//	global/static stuff
		ShowGlobalOptions = EditorGUILayout.Foldout(ShowGlobalOptions, "Global Options");
		if ( ShowGlobalOptions )
		{
			int CurrentListeners = (PopMovie.DebugCallback!=null) ? PopMovie.DebugCallback.GetInvocationList ().Length : 0;
			PopMovie.EnableDebugLog = GUILayout.Toggle (PopMovie.EnableDebugLog, "Enable Debug Log (Currently " + CurrentListeners + " listeners)" );

			if (GUILayout.Button("Flush debug"))
				PopMovie.FlushDebug( Debug.Log );
		}

		ShowPerformanceGraphs = EditorGUILayout.Foldout (ShowPerformanceGraphs, "Show Performance Graphs");
		if (ShowPerformanceGraphs )
		{
			ShowLocalisedPerformanceGraphs = GUILayout.Toggle (ShowLocalisedPerformanceGraphs, "Localised performance graphs");
			if (PerformanceGraphs == null)
				PerformanceGraphs = new List<RenderTexture> ();

			bool Drawn = false;

			//	make a graph for each stream
			for ( int i=0;	Instance!=null && i<10;	i++ )
			{
				while (i >= PerformanceGraphs.Count)
					PerformanceGraphs.Add (new RenderTexture (PerformanceGraphWidth, PerformanceGraphHeight, 0));

				//	update it and draw if that succeeds (tells us if that stream exists)
				if (!Instance.UpdatePerformanceGraphTexture(PerformanceGraphs[i], i, ShowLocalisedPerformanceGraphs))
				{
					//	gr: windows can typically start at stream 1. (audio before video) this isn't the Nth stream, its the internal stream number... SO... lets just keep going past the max
					continue;
				}

				GUILayout.Label ("Stream " + i );
				GUILayout.Box( PerformanceGraphs[i] );
				Drawn = true;
			}

			if (!Drawn) {
				EditorGUILayout.HelpBox ("No streams " + (Instance==null?"(null instance)":""), MessageType.Warning);
			}
		}


		ShowAudioGraphs = EditorGUILayout.Foldout (ShowAudioGraphs, "Show Audio Graphs");
		if ( ShowAudioGraphs )
		{
			if (AudioGraphs == null)
				AudioGraphs = new List<RenderTexture> ();

			bool Drawn = false;

			//	make a graph for each stream
			for ( int i=0;	Instance != null && i<10;	i++ )
			{
				while (i >= AudioGraphs.Count)
					AudioGraphs.Add (new RenderTexture (PerformanceGraphWidth, PerformanceGraphHeight, 0));

				//	update it and draw if that succeeds (tells us if that stream exists)
				if (!Instance.UpdateAudioTexture(AudioGraphs[i], i))
				{
					//	gr: windows can typically start at stream 1. (audio before video) this isn't the Nth stream, its the internal stream number... SO... lets just keep going past the max
					break;
				}

				GUILayout.Label ("Audio Stream " + i );
				GUILayout.Box( AudioGraphs[i] );
				Drawn = true;
			}

			if (!Drawn) {
				EditorGUILayout.HelpBox ("No streams " + (Instance==null?"(null instance)":""), MessageType.Warning);
			}							
		}

		ShowMeta = EditorGUILayout.Foldout (ShowMeta, "Show Meta");
		if ( ShowMeta )
		{
			if ( Instance != null )
			{
				var Json = Instance.GetMetaJson();
				EditorGUILayout.HelpBox( Json, MessageType.Info );
#if UNITY_5_2 || UNITY_5_3_OR_NEWER
				if (GUILayout.Button("Copy to clipboard"))
					GUIUtility.systemCopyBuffer = Json;
#endif
			}
			else
			{
				EditorGUILayout.HelpBox ( "null instance", MessageType.Warning);
			}
		}

		ShowDevices = EditorGUILayout.Foldout(ShowDevices, "Show Devices");
		if (ShowDevices)
		{
			uint MaxDevices = 40;
			string Devices = null;
			for ( uint i=0;	i< MaxDevices;	i++ )
			{
				var SourceName = PopMovie.EnumSource(i);
				if (SourceName == null)
					continue;
				if (!SourceName.StartsWith(PopMovie.FilenamePrefix_Device) &&
					!SourceName.StartsWith(PopMovie.FilenamePrefix_Decklink)
					)
				{
					continue;
				}

				if (Devices == null)
					Devices = SourceName;
				else
					Devices += "\n" + SourceName;
			}

			if (Devices == null)
				Devices = "No sources found.";
			EditorGUILayout.HelpBox(Devices, MessageType.Info);
		}

		EditorGUILayout.HelpBox("PopMovie Version " + PopMovie.GetVersion(), MessageType.Info);

		//	repaint for live meta
		if ( Instance != null )
			ThisEditor.Repaint();
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}
}



/*
	Example of how to use the inspector on a custom type
*/
[CustomEditor(typeof(PopMovieSimple))]
public class PopMovieSimpleInspector : PopMovieInspector
{
	public override void OnInspectorGUI()
	{
		var Object = target as PopMovieSimple;
		ShowInspectorDebug( Object.Movie, this );
		DrawDefaultInspector();
	}
}

