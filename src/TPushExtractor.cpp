#include "TPushExtractor.h"



TPushExtractor::TPushExtractor(const TMediaExtractorParams& Params) :
	TMediaExtractor		( Params )
{
}

TPushExtractor::~TPushExtractor()
{
}

std::shared_ptr<Platform::TMediaFormat> TPushExtractor::GetStreamFormat(size_t StreamIndex)
{
	return nullptr;
}

std::shared_ptr<TMediaPacket> TPushExtractor::ReadNextPacket()
{
	std::lock_guard<std::mutex> Lock( mInputPacketsLock );

	if ( mInputPackets.IsEmpty() )
		return nullptr;

	return mInputPackets.PopAt(0);
}

void TPushExtractor::GetStreams(ArrayBridge<TStreamMeta>&& Streams)
{
	for ( auto& Meta : mStreams )
	{
		Streams.PushBack( Meta.second );
	}
}


//	 same as TStreamExtractor::OnStreamPacketExtracted
void TPushExtractor::OnStreamPacketExtracted(std::shared_ptr<TMediaPacket>& Packet)
{
	Soy::Assert( Packet !=nullptr, "TStreamExtractor::OnPacketDecoded; Expected packet" );

	//	first bit of stream info
	auto StreamIndex = Packet->mMeta.mStreamIndex;
	if ( mStreams.find(StreamIndex) == mStreams.end() )
	{
		mStreams[StreamIndex] = Packet->mMeta;
		mStreams[StreamIndex].mDuration = SoyTime();
		OnStreamsChanged();
	}

	//	update duration if we know when it finishes
	if ( Packet->mEof )
	{
		auto& Duration = mStreams[StreamIndex].mDuration.mTime;
		if ( Duration == 0 )
		{
			//	note: EOF needs a time really :/ maybe we can automatically catch this by recording last frame time for each stream... which may need to come from the buffer?
			//	1=corrected
			if ( Packet->mTimecode.mTime <= 1 )
				std::Debug << "Warning, EOF packet has no timecode." << std::endl;

			Duration = Packet->mTimecode.mTime + Packet->mDuration.mTime;
			//	gr: for images, timecode of 1 and frameduration of 1, we want a total duration of 1 really. MAYBE that's the right calculation for movies too?
			if ( Duration > 0 )
				Duration--;

			//	just in case we have no duration, or odd time etc.. make sure the duration is not zero (unknown)
			if ( Duration == 0 )
				Duration = 1;
		}
	}

	mInputPacketsLock.lock();
	mInputPackets.PushBack( Packet );
	mInputPacketsLock.unlock();

	//	wake up the TMediaExtractor to grab new packets
	OnPacketExtracted( Packet );
}


void TPushExtractor::PushBuffer(ArrayBridge<uint8_t>& Buffer,size_t StreamIndex)
{
	auto Packet = std::make_shared<TMediaPacket>();

	Packet->mMeta.mCodec = SoyMediaFormat::H264_ES;
	Packet->mData.PushBackArray( Buffer );

	OnStreamPacketExtracted( Packet );
}

