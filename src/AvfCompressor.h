#pragma once


#include <SoyEnum.h>
#include <SoyTypes.h>
#include <heaparray.hpp>
#include <SoyTime.h>
#include "PopMovieDecoder.h"
#include "AvfMovieDecoder.h"
#include "SoyAvf.h"
#include <SoyH264.h>


namespace AvfCompressor
{
	class TInstance;
	class TSession;		//	objc interface
	class TParams;
};


namespace Avf
{
	std::shared_ptr<TMediaDecoder>	AllocVideoDecoder(const std::string& Name,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> Input,std::shared_ptr<TPixelBufferManager> Output,const TVideoDecoderParams& Params,std::shared_ptr<Platform::TMediaFormat> Format);
	std::shared_ptr<TMediaDecoder>	AllocAudioDecoder(const std::string& Name,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> Input,std::shared_ptr<TAudioBufferManager> Output);
};





class AvfCompressor::TParams
{
public:
	TParams() :
		mAverageBitrate		( 1000 ),
		mProfile			( H264Profile::Baseline ),
		mRealtimeEncoding	( true ),
		mRequireHardwareAcceleration	( false ),
		mMaxKeyframeInterval	( std::chrono::milliseconds(2000) )
	{
	}

public:
	bool					mRequireHardwareAcceleration;
	size_t					mAverageBitrate;
	H264Profile::Type		mProfile;
	bool					mRealtimeEncoding;
	SoyTime					mMaxKeyframeInterval;	//	Videotoolbox actually restricts this per-second. 0 is no-limit
};

class TFrameMeta
{
public:
	TFrameMeta() :
		mKeyframe	( false )
	{
	}
	
public:
	SoyTime		mTime;
	bool		mKeyframe;
};

class TH264Frame
{
public:
	TFrameMeta		mMeta;
	Array<uint8>	mFrame;	//	mpeg data, not NAL packet
};

/*

//	http://asciiwwdc.com/2014/sessions/513
class AvfCompressor::TInstance : public TCaster
{
public:
	TInstance(const TCasterParams& Params);
	
	virtual void		Write(const Opengl::TTexture& Image,SoyTime Timecode,Opengl::TContext& Context) override;
	virtual void		Write(const std::shared_ptr<SoyPixelsImpl> Image,SoyTime Timecode) override;

	void				PushCompressedFrame(std::shared_ptr<TH264Frame> Frame);
	void				OnError(const std::string& Error);

public:
	std::shared_ptr<TSession>	mSession;
	SoyPixelsMeta				mOutputMeta;
	Array<std::shared_ptr<TH264Frame>>	mFrames;
};

*/






class AvfMediaExtractor : public TMediaExtractor
{
public:
	AvfMediaExtractor(const TMediaExtractorParams& Params);
	~AvfMediaExtractor();
	
	virtual void		Stop() override;
	
	virtual void		GetStreams(ArrayBridge<TStreamMeta>&& Streams) override;
	
	virtual std::shared_ptr<Platform::TMediaFormat>	GetStreamFormat(size_t StreamIndex) override;
	
	bool							CanSeek();
	virtual bool					OnSeek() override;	//	reposition extractors whereever possible. return true to invoke a data flush (ie. if you moved the extractor)
	virtual bool					CanSeekBackwards() override	{	return true;	}	//	by default, don't allow this, until it's implemented for that extractor
	
protected:
	virtual std::shared_ptr<TMediaPacket>	ReadNextPacket() override;
	std::shared_ptr<TMediaPacket>			ReadNextPacket(size_t StreamIndex);

#if defined(__OBJC__)
	NSURL*							GetUrl(const std::string& Filename);
#endif
	
private:
	void							CreateAssetReader();
	void							DeleteAssetReader();
	
protected:
	size_t							mAudioSampleRate;
	
private:
	//	we need to manage the "next buffer" ourselves, so pull the latest packet from each stream
	//	and return the earliest each time
	std::map<size_t,std::shared_ptr<TMediaPacket>>	mNextPacketPerStream;
	SoyTime							mLastReadTime;		//	for seeking, real timecode
	
#if defined(__OBJC__)
	std::shared_ptr<AvfAsset>		mAsset;
	Array<std::shared_ptr<ObjcPtr<AVAssetTrack>>>				mTracks;

	std::mutex						mAssetReaderLock;		//	lock for all below
	ObjcPtr<AVAssetReader>			mAssetReader;
	Array<std::shared_ptr<ObjcPtr<AVAssetReaderTrackOutput>>>	mTrackOutputs;
#endif
};


class AvfMediaDecoder : public TMediaDecoder
{
public:
	AvfMediaDecoder(const std::string& ThreadName,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> InputBuffer,std::shared_ptr<TPixelBufferManager> OutputBuffer,std::shared_ptr<Platform::TMediaFormat> FormatDesc,const TVideoDecoderParams& Params);
	~AvfMediaDecoder();
	
	virtual bool		ProcessPacket(const TMediaPacket& Packet) override;
	
	void				OnDecodeError(const std::string& Error,SoyTime Timecode);
	void				OnDecodedFrame(void* CVImageBufferRef,SoyTime Timecode);
	
	virtual bool		IsDecodingFramesInOrder() const override			{	return false;	}

private:
	SoyEvent<bool>								mOnStreamChanged;
	std::shared_ptr<TMediaPacket>				mSpsPacket;
	std::shared_ptr<TMediaPacket>				mPpsPacket;
	Array<TMediaPacket>							mQueuedPackets;
	
public:
	std::shared_ptr<AvfDecoderRenderer>			mDecoderRenderer;
	std::shared_ptr<AvfCompressor::TSession>	mSession;	//	objc

	//	these go together...
	std::shared_ptr<Platform::TMediaFormat>		mFormatDesc;
	TStreamMeta									mStream;
};

class AvfAudioDecoder : public TMediaDecoder
{
public:
	AvfAudioDecoder(const std::string& ThreadName,const TStreamMeta& Stream,std::shared_ptr<TMediaPacketBuffer> InputBuffer,std::shared_ptr<TAudioBufferManager> OutputBuffer);
	
	virtual bool		ProcessPacket(const TMediaPacket& Packet) override;
	
	void				ConvertPcmLinear16ToPcmFloat(const ArrayBridge<sint16>&& Input,ArrayBridge<float>&& Output);
};

